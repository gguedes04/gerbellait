package br.gerbellait.rest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.gerbellait.arquitetura.exceptions.RestLayerException;
import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.arquitetura.rest.AbstractRest;
import br.gerbellait.sistema.entity.Usuario;
import br.gerbellait.sistema.entity.UsuarioToken;
import br.gerbellait.sistema.service.AutenticaService;

@Path("/gerbellait")
public class ControllerRest extends AbstractRest {

	@Inject
	private AutenticaService autenticaService;

	@Context
	private HttpServletRequest request;

	@POST
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
	public Response autenticar(Usuario usuario) throws RestLayerException {
		try {
			UsuarioToken usuarioToken = autenticaService.autenticar(usuario);
			request.getSession().setAttribute(usuarioToken.getToken(), usuarioToken);
			return Response.ok(usuarioToken).build();
		} catch (ServiceLayerException e) {
			throw new RestLayerException(e.getMessage());
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@Override
	protected Object getCurrentUser(HttpServletRequest request) {
		String token = request.getHeader("Authorization");
		return token != null ? request.getSession().getAttribute(token) : null;
	}
}