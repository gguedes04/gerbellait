package br.gerbellait.listener;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import br.gerbellait.arquitetura.resources.LoadResources;

@WebListener
public class LoadResourcesListener implements ServletContextListener {

    @Inject
    private LoadResources configuration;

    @Override
    public void contextInitialized(ServletContextEvent event) {
        ServletContext context = event.getServletContext();
        configuration.processEvent(context);
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        // not used
    }

}