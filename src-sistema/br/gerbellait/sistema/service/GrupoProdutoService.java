package br.gerbellait.sistema.service;

import java.util.List;

import br.gerbellait.sistema.entity.GrupoProduto;

public interface GrupoProdutoService {

	List<GrupoProduto> getAll();

	GrupoProduto getById(GrupoProduto grupo);
}