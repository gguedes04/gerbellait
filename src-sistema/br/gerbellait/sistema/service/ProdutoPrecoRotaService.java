package br.gerbellait.sistema.service;

import java.util.List;

import br.gerbellait.sistema.entity.Produto;
import br.gerbellait.sistema.entity.ProdutoPrecoRota;
import br.gerbellait.sistema.entity.Rota;

public interface ProdutoPrecoRotaService {

	public List<ProdutoPrecoRota> getBy(Produto produto);

	boolean deletar(ProdutoPrecoRota produtoPrecoRota);

	void doSave(Produto produto);

	List<Produto> getPrecoVendaRotaResource(Produto produto, List<Rota> rotas);

	void saveList(List<ProdutoPrecoRota> rotas);
}