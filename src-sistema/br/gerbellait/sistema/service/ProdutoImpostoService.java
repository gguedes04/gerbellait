package br.gerbellait.sistema.service;

import java.util.List;

import br.gerbellait.sistema.entity.Produto;
import br.gerbellait.sistema.entity.ProdutoImposto;

public interface ProdutoImpostoService {

	public List<ProdutoImposto> getBy(Produto produto);

	boolean deletar(ProdutoImposto produtoImposto);

	void doSave(Produto produto);
}