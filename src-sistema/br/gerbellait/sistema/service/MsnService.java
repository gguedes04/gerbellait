package br.gerbellait.sistema.service;

import java.util.List;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.sistema.entity.Msn;

public interface MsnService {

	Msn save(Msn msn) throws ServiceLayerException;

	Msn getById(Long id);

	boolean deletar(Msn msn);

	List<Msn> getAll();

	List<Msn> getByDto(Msn msn);

}