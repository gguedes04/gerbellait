package br.gerbellait.sistema.service;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.sistema.entity.Pessoa;

public interface PessoaService {

	List<Pessoa> getAll();

	List<Pessoa> getByDto(Pessoa pessoa);

	List<Pessoa> getClientesByDto(Pessoa pessoa);

	List<Pessoa> getClientesPedByDto(Pessoa pessoa);

	InputStream toExport(Pessoa pessoa) throws UnsupportedEncodingException;

	Pessoa getById(Long id);

	Pessoa save(Pessoa pessoa) throws ServiceLayerException;

	boolean deletar(Pessoa pessoa);

	List<Pessoa> getFornecedoresByDto(Pessoa pessoa);

	Boolean isFornecedor(Long idUsuario);

	Boolean isCliente(Long idUsuario);
}