package br.gerbellait.sistema.service;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.List;

import br.gerbellait.sistema.bean.MapaCargaFilterBean;
import br.gerbellait.sistema.bean.MapaCargaPedidoBean;
import br.gerbellait.sistema.bean.MapaCargaProdutoBean;
import br.gerbellait.sistema.entity.MapaCarga;
import br.gerbellait.sistema.entity.Pedido;

public interface MapaCargaService {
    
    List<MapaCarga> findLoadMapReport();
    
    List<MapaCargaPedidoBean> getPedidos(MapaCargaFilterBean filter);
    
    List<MapaCargaProdutoBean> getProdutos(MapaCargaFilterBean filter);
    
    Long save(List<MapaCargaPedidoBean> list);
    
    InputStream relatorioCargaExport(MapaCargaFilterBean filter) throws UnsupportedEncodingException, SQLException;
    
    InputStream relatorioCobrancaExport(MapaCargaFilterBean filter) throws UnsupportedEncodingException, SQLException;
    
    List<MapaCarga> getLoadMapByFilter(MapaCargaFilterBean filter);
    
    void deleteLoadMap(Long id);
    
    List<MapaCargaPedidoBean> getPedidosNaoVinculados(MapaCargaFilterBean filter);
    
    List<MapaCargaPedidoBean> getPedidosVinculados(Long mapaCargaId);
    
    boolean hasLinkedPedido(Long pedido);
}
