package br.gerbellait.sistema.service;

import java.util.List;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.sistema.entity.AjusteEstoque;
import br.gerbellait.sistema.entity.Produto;

public interface AjusteEstoqueService {

	public List<AjusteEstoque> getAll();

	public List<AjusteEstoque> getByProduto(Produto produto);

	List<AjusteEstoque> getBy(AjusteEstoque filtro, boolean isPaginate);

	AjusteEstoque getById(Long id);

	AjusteEstoque save(AjusteEstoque ajusteEstoque) throws ServiceLayerException;

	boolean deletar(AjusteEstoque ajusteEstoque);
}