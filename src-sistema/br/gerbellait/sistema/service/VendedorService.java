package br.gerbellait.sistema.service;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.sistema.bean.VendedorFilterBean;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.entity.Vendedor;

public interface VendedorService {

	List<Vendedor> allBySituacao(ExcluidoEnum flExcluido);

	List<Vendedor> getAll();
	
	List<VendedorFilterBean> allFilterSellers();

	Vendedor save(Vendedor vendedor) throws ServiceLayerException;

	boolean deletar(Vendedor vendedor);

	Vendedor getById(Long id);

	InputStream toExport(Vendedor vendedor) throws UnsupportedEncodingException;

	List<Vendedor> getByDto(Vendedor vendedor);
}
