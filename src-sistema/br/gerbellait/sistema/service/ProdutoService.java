package br.gerbellait.sistema.service;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.List;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.sistema.bean.HistoricoEstoqueBean;
import br.gerbellait.sistema.bean.TableState;
import br.gerbellait.sistema.entity.AjusteEstoque;
import br.gerbellait.sistema.entity.Pedido;
import br.gerbellait.sistema.entity.Produto;

public interface ProdutoService {
    
    List<Produto> getAll();
    
    List<Produto> getByDto(Produto produto);
    
    InputStream toExport(Produto Produto) throws UnsupportedEncodingException;
    
    Produto getById(Long id);
    
    Produto save(Produto produto) throws ServiceLayerException;
    
    boolean deletar(Produto produto);
    
    void ajusteEstoque(AjusteEstoque ajusteEstoque);
    
    List<Produto> getByDesc(Long clienteId, String text);
    
    List<Produto> getByPedido(Pedido pedido);
    
    boolean updateQtEstoque(Produto produto);
    
    List<HistoricoEstoqueBean> getHistoricoEstoque(TableState tableState);
    
    InputStream relatorioEstoqueExport(TableState tableState) throws UnsupportedEncodingException, SQLException;
}
