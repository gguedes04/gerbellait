package br.gerbellait.sistema.service;

import java.util.List;

import br.gerbellait.sistema.entity.Endereco;
import br.gerbellait.sistema.entity.Pessoa;

public interface EnderecoService {

	public List<Endereco> getByPessoa(Pessoa pessoa);

	void doSave(Pessoa pessoa);
}