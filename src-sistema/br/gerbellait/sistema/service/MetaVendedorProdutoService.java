package br.gerbellait.sistema.service;

import java.util.List;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.sistema.entity.MetaVendedorProduto;
import br.gerbellait.sistema.entity.PeriodoMeta;
import br.gerbellait.sistema.entity.Produto;
import br.gerbellait.sistema.entity.Vendedor;

public interface MetaVendedorProdutoService {

	List<MetaVendedorProduto> all();

	MetaVendedorProduto save(MetaVendedorProduto metaVendedorProduto) throws ServiceLayerException;

	MetaVendedorProduto doSave(MetaVendedorProduto metaVendedorProduto) throws ServiceLayerException;

	MetaVendedorProduto getMetaVendedorProduto(List<PeriodoMeta> periodos, Vendedor vendedor, Produto produto);

	void doInsertByVendedor(Vendedor vendedor) throws ServiceLayerException;

	void doInsertByProduto(Produto produto) throws ServiceLayerException;

	void saveList(List<MetaVendedorProduto> limites) throws ServiceLayerException;

	List<Produto> getLimiteVendasResource(MetaVendedorProduto filtro, List<Vendedor> vendedores);

	List<Produto> getQuantidadeVendaResource(MetaVendedorProduto filtro, List<Vendedor> vendedores);

	List<Produto> getPrecoMedioVendaResource(MetaVendedorProduto filtro, List<Vendedor> vendedores);

	List<Produto> getPercDescontoQtdeMinimaResource(MetaVendedorProduto filtro, List<Vendedor> vendedores);
}