package br.gerbellait.sistema.service;

import java.util.List;

import br.gerbellait.sistema.entity.Perfil;

public interface PerfilService {

	List<Perfil> getAll();

}