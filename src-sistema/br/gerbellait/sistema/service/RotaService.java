package br.gerbellait.sistema.service;

import java.util.List;

import br.gerbellait.sistema.entity.Rota;

public interface RotaService {

	List<Rota> getAll();

	Rota doGetById(Rota rota);

	List<Rota> getByVendedor(Long idVendedor);

}