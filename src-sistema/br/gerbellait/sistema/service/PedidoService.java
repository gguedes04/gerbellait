package br.gerbellait.sistema.service;

import java.io.InputStream;
import java.util.List;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.sistema.bean.TableState;
import br.gerbellait.sistema.bean.UltimasVendasBean;
import br.gerbellait.sistema.entity.Pedido;

public interface PedidoService {
    
    Pedido save(Pedido pedido) throws ServiceLayerException;

    boolean deletar(Pedido pedido);
    
    List<Pedido> getByDto(TableState tableState);
    
    Pedido getById(Long idPedido);
    
    InputStream relatorio(Pedido pedido) throws Exception;
    
    List<UltimasVendasBean> getUltimasVendas(Pedido pedido);
}
