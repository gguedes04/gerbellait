package br.gerbellait.sistema.service;

import java.util.List;

import br.gerbellait.sistema.entity.Cidade;
import br.gerbellait.sistema.entity.Uf;

public interface CidadeService {

	List<Cidade> getByUf(Uf uf);

	Cidade getById(Cidade cidade);
}