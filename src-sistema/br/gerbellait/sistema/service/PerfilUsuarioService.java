package br.gerbellait.sistema.service;

import java.util.List;

import br.gerbellait.sistema.entity.PerfilUsuario;
import br.gerbellait.sistema.entity.Usuario;

public interface PerfilUsuarioService {

	List<PerfilUsuario> allByUser(Usuario usuarioFiltro);

	List<PerfilUsuario> doAllByUser(Usuario usuarioFiltro);

	boolean deleteByUser(Usuario usuario);

	void doSave(Usuario usuario);

}