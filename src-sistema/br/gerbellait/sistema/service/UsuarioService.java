package br.gerbellait.sistema.service;

import java.io.IOException;
import java.util.List;

import br.gerbellait.arquitetura.ApplicationContext;
import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.sistema.entity.Usuario;

public interface UsuarioService {

	List<Usuario> all();

	Usuario getBy(String login, String senha);

	Usuario save(Usuario usuario) throws ServiceLayerException, IOException;

	void saveJson(String json) throws ServiceLayerException, IOException;

	boolean deletar(Usuario usuario);

	Usuario getById(Long id) throws IOException;

	List<Usuario> getByDto(Usuario usuarioFiltro);

	boolean emailEsqueceuSenha(ApplicationContext context, Usuario usuario) throws IOException;

	boolean salvaNovaSenha(ApplicationContext context, Usuario usuario) throws IOException;

	List<Usuario> getVendedores();

	Boolean isVendedor(Long id) throws IOException;
}