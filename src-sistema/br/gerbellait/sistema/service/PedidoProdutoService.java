package br.gerbellait.sistema.service;

import br.gerbellait.sistema.entity.Pedido;
import br.gerbellait.sistema.entity.Produto;

public interface PedidoProdutoService {
    
    void save(Pedido pedido, Produto produto, boolean notExists);
    
    boolean contains(Pedido pedido, Produto produto);
    
}
