package br.gerbellait.sistema.service;

import java.util.List;

import br.gerbellait.sistema.entity.ItemMenuPerfil;
import br.gerbellait.sistema.entity.Perfil;

public interface ItemMenuPerfilService {

	List<ItemMenuPerfil> allByPerfil(Perfil perfil);

	List<ItemMenuPerfil> doAllByPerfil(Perfil perfil);

	List<String> getDescByPerfil(Perfil perfil);
}