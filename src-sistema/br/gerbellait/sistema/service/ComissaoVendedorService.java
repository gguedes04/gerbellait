package br.gerbellait.sistema.service;

import java.util.List;

import br.gerbellait.sistema.entity.ComissaoVendedor;
import br.gerbellait.sistema.entity.Vendedor;

public interface ComissaoVendedorService {

	public List<ComissaoVendedor> getBy(Vendedor vendedor);

	boolean deletar(ComissaoVendedor rotaVendedor);

	void doSave(Vendedor vendedor);
}