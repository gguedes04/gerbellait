package br.gerbellait.sistema.service;

import java.util.List;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.sistema.entity.Transportador;

public interface TransportadorService {

	List<Transportador> getAll();

	Transportador getById(Transportador transportador);

	Transportador save(Transportador transportador) throws ServiceLayerException;

	boolean deletar(Transportador transportador);

	List<Transportador> getByDto(Transportador transportador);

}