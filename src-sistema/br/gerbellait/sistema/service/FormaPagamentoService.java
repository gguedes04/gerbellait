package br.gerbellait.sistema.service;

import java.util.List;

import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.entity.FormaPagamento;

public interface FormaPagamentoService {

	List<FormaPagamento> getAll();

	List<FormaPagamento> allBySituacao(ExcluidoEnum flExcluido);
    
    FormaPagamento getById(FormaPagamento formaPagamento);
}
