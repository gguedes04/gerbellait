package br.gerbellait.sistema.service.impl;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Transactional;
import br.gerbellait.arquitetura.service.AbstractService;
import br.gerbellait.arquitetura.util.Utils;
import br.gerbellait.sistema.bean.MapaCargaFilterBean;
import br.gerbellait.sistema.bean.MapaCargaPedidoBean;
import br.gerbellait.sistema.bean.MapaCargaProdutoBean;
import br.gerbellait.sistema.dao.MapaCargaDao;
import br.gerbellait.sistema.entity.FormaPagamento;
import br.gerbellait.sistema.entity.MapaCarga;
import br.gerbellait.sistema.entity.Pedido;
import br.gerbellait.sistema.service.MapaCargaService;
import br.gerbellait.sistema.service.PedidoService;

@Stateless
public class MapaCargaServiceImpl extends AbstractService<MapaCarga> implements MapaCargaService {
    private static final Logger logger = Logger.getLogger(MapaCargaServiceImpl.class);
    
    @Inject
    private MapaCargaDao mapaCargaDao;
    @Inject
    private PedidoService pedidoService;
    
    @Override
    protected void validate(MapaCarga mapaCarga) throws ServiceLayerException {
        if (mapaCarga == null) {
            throw new ServiceLayerException("Objeto MapaCarga null.");
        }
    }
    
    
    @Override
    public List<MapaCarga> findLoadMapReport() {
        try {
            return mapaCargaDao.findLoadMapReport();
        } catch (SQLException e) {
            logger.debug("Error on findLoadMapReport", e);
        }
        return Collections.emptyList();
    }
    
    @Override
    @Transactional
    public List<MapaCargaPedidoBean> getPedidos(MapaCargaFilterBean filter) {
        try {
            return mapaCargaDao.getPedidos(filter);
        } catch (SQLException e) {
            logger.debug("Error on getPedidos", e);
        }
        return Collections.emptyList();
    }
    
    @Override
    @Transactional
    public List<MapaCargaProdutoBean> getProdutos(MapaCargaFilterBean filter) {
        try {
            return mapaCargaDao.getProdutos(filter);
        } catch (SQLException e) {
            logger.debug("Error on getProdutos", e);
        }
        return Collections.emptyList();
    }
    
    @Override
    @Transactional
    public Long save(List<MapaCargaPedidoBean> list) {
        try {
            return mapaCargaDao.save(list);
        } catch (SQLException e) {
            logger.debug("Error to save", e);
        }
        return 0L;
    }
    
    @Override
    @Transactional
    public InputStream relatorioCargaExport(MapaCargaFilterBean filter) throws UnsupportedEncodingException, SQLException {
        String separator = "\t";
        List<MapaCargaProdutoBean> result = getProdutos(filter);
        StringBuilder report = new StringBuilder();
        if (!Utils.isNullOrEmpty(result)) {
            
            report.append("Relatório de Carga");
            report.append(separator);
            report.append(separator);
            report.append(separator);
            report.append(separator);
            report.append(separator);
            report.append(separator);
            report.append(separator);
            report.append("Data:").append(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyy")));
            report.append("\n");
            
            
            report.append("Rota:");
            report.append(separator);
            report.append(filter.getSelectedRoutes().stream().map(String::valueOf).collect(Collectors.joining(", ")));
            report.append(separator);
            report.append(separator);
            report.append(separator);
            report.append(separator);
            report.append("Período:");
            report.append(separator);
            report.append(filter.getDataInicial() == null ? "" : filter.getDataInicial());
            report.append(separator);
            report.append("à ").append(filter.getDataFinal() == null ? "" : filter.getDataFinal());
            report.append("\n");
            
            report.append("Veículo:");
            report.append(separator);
            report.append(separator);
            report.append(separator);
            report.append("Motorista:");
            report.append(separator);
            report.append(separator);
            report.append(separator);
            report.append(separator);
            report.append("\n");
            
            report.append("Cód");
            report.append(separator);
            report.append("Descrição");
            report.append(separator);
            report.append("Unid");
            report.append(separator);
            report.append("Qtd Venda");
            report.append(separator);
            report.append("Qtd Troca");
            report.append(separator);
            report.append("Total Carga");
            report.append(separator);
            report.append("Caixa");
            report.append(separator);
            report.append("Unidade");
            report.append("\n");
            
            for (MapaCargaProdutoBean dto : result) {
                report.append(dto.getProductId());
                report.append(separator);
                report.append(dto.getProductDesc());
                report.append(separator);
                report.append(dto.getUnit());
                report.append(separator);
                report.append(dto.getTotalSelles());
                report.append(separator);
                report.append(dto.getTotalChanges());
                report.append(separator);
                report.append(dto.getTotalLoad());
                report.append(separator);
                report.append(currencyFormatter(dto.getTotal()));
                report.append(separator);
                report.append(dto.getQtd());
                report.append("\n");
            }
        }
        return new ByteArrayInputStream(report.toString().getBytes("UTF-8"));
    }
    
    @Override
    @Transactional
    public InputStream relatorioCobrancaExport(MapaCargaFilterBean filter) throws UnsupportedEncodingException, SQLException {
        String separator = "\t";
        List<MapaCargaPedidoBean> result = getPedidos(filter);
        StringBuilder report = new StringBuilder();
        if (!Utils.isNullOrEmpty(result)) {
            
            report.append("Cobrança - Rota: ").append(filter.getSelectedRoutes().stream().map(String::valueOf).collect(Collectors.joining(", ")));
            report.append(separator);
            report.append(separator);
            report.append(separator);
            report.append(separator);
            report.append(separator);
            report.append(separator);
            report.append(separator);
            report.append("Data:").append(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyy")));
            report.append("\n");
            
            
            report.append("Motorista:");
            report.append(separator);
            report.append(separator);
            report.append(separator);
            report.append(separator);
            report.append(separator);
            report.append("Perído:");
            report.append(separator);
            report.append(filter.getDataInicial() == null ? "" : filter.getDataInicial());
            report.append(separator);
            report.append("à ").append(filter.getDataFinal() == null ? "" : filter.getDataFinal());
            report.append("\n");
            
            report.append("Veículo:");
            report.append(separator);
            report.append(separator);
            report.append(separator);
            report.append("KM Saída:");
            report.append(separator);
            report.append(separator);
            report.append(separator);
            report.append("KM Chegada");
            report.append("\n");
            
            report.append("Pedido");
            report.append(separator);
            report.append("Cód");
            report.append(separator);
            report.append("Cliente");
            report.append(separator);
            report.append("Vlr Venda");
            report.append(separator);
            report.append("ST");
            report.append(separator);
            report.append("Form Pgto");
            report.append(separator);
            report.append("Obs pedido");
            report.append(separator);
            report.append("14");
            report.append(separator);
            report.append("Tr");
            report.append(separator);
            report.append("Obs");
            report.append("\n");
            
            Map<String, Double> valoresFormaPagamento = new HashMap<>();
            Map<String, Double> totais = new HashMap();
            
            totais.put("TOTAL_LIQUIDO", 0D);
            totais.put("TOTAL_BRUTO", 0D);
            
            for (MapaCargaPedidoBean dto : result) {
                if (!filter.getSelectedOrders().stream().anyMatch(mapaCargaPedidoBean -> mapaCargaPedidoBean.getPedido().equals(dto.getPedido())))
                    continue;
                Pedido pedido = pedidoService.getById(dto.getPedido());
                report.append(dto.getPedido());
                report.append(separator);
                report.append(dto.getCliente());
                report.append(separator);
                report.append(dto.getNome());
                report.append(separator);
                report.append(currencyFormatter(dto.getValor()));
                report.append(separator);
                report.append(separator);
                report.append(pedido.getFormaPagamento().getDescricao());
                report.append(separator);
                report.append(pedido.getObservacao() == null ? "" : pedido.getObservacao());
                report.append(separator);
                report.append("s".equals(pedido.getFlGeraNf()) ? "X" : "");
                report.append(separator);
                report.append(pedido.getProdutoList().size() > 0 ? "X" : "");
                report.append(separator);
                report.append(!"".equals(pedido.getCliente().getObservacao()) ? "X" : "");
                report.append("\n");
                
                FormaPagamento formaPagamento = pedido.getFormaPagamento();
                if (formaPagamento.getPrazo() == 0) {
                    totais.put("TOTAL_LIQUIDO", totais.get("TOTAL_LIQUIDO") + dto.getValor());
                } else {
                    totais.put("TOTAL_BRUTO", totais.get("TOTAL_BRUTO") + dto.getValor());
                }
                String key = formaPagamento.getDescricao();
                if (valoresFormaPagamento.containsKey(key)) {
                    Double valor = Double.valueOf(valoresFormaPagamento.get(key).toString()) + dto.getValor();
                    valoresFormaPagamento.put(key, valor);
                } else {
                    valoresFormaPagamento.put(key, dto.getValor());
                }
            }
            report.append("\n");
            for (Map.Entry<String, Double> formasPagamento : valoresFormaPagamento.entrySet()) {
                report.append(formasPagamento.getKey());
                report.append(separator);
                report.append(currencyFormatter(formasPagamento.getValue()));
                report.append("\n");
            }
            report.append("\n");
            report.append("TOTAL LIQUIDO");
            report.append(separator);
            report.append(currencyFormatter(totais.get("TOTAL_LIQUIDO")));
            report.append("\n");
            report.append("TOTAL BRUTO");
            report.append(separator);
            report.append(currencyFormatter(totais.get("TOTAL_BRUTO") + totais.get("TOTAL_LIQUIDO")));
            report.append("\n");
            
        }
        
        return new ByteArrayInputStream(report.toString().getBytes("UTF-8"));
    }
    
    
    private String currencyFormatter(Double value) {
        Locale locale = new Locale("pt", "BR");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(locale);
        return formatter.format(value);
    }
    
    
    @Override
    @Transactional
    public List<MapaCarga> getLoadMapByFilter(MapaCargaFilterBean filter) {
        try {
            return mapaCargaDao.getLoadMapByFilter(filter);
        } catch (SQLException e) {
            logger.debug("Erro ao listar o mapa de carga", e);
        }
        return Collections.emptyList();
    }
    
    @Override
    @Transactional
    public void deleteLoadMap(Long id) {
        try {
            mapaCargaDao.deleteLoadMap(id);
        } catch (SQLException e) {
            logger.debug("Erro ao deletar o mapa de carga", e);
        }
    }
    
    @Override
    @Transactional
    public List<MapaCargaPedidoBean> getPedidosVinculados(Long mapaCargaId) {
        try {
            return mapaCargaDao.getPedidosVinculados(mapaCargaId);
        } catch (SQLException e) {
            logger.debug("Erro ao listar pedidos não vinculados", e);
        }
        return Collections.emptyList();
    }
    
    @Override
    @Transactional
    public List<MapaCargaPedidoBean> getPedidosNaoVinculados(MapaCargaFilterBean filter) {
        try {
            return mapaCargaDao.getPedidosNaoVinculados(filter);
        } catch (SQLException e) {
            logger.debug("Erro ao listar pedidos vinculados", e);
        }
        return Collections.emptyList();
    }
    
    @Override
    @Transactional
    public boolean hasLinkedPedido(Long pedido) {
        try {
            return mapaCargaDao.hasLinkedPedido(pedido);
        } catch (Exception e) {
            logger.debug("Erro ao verificar pedido vinculado", e);
        }
        return false;
    }
}
