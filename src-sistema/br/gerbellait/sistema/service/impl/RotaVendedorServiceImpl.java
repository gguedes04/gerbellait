package br.gerbellait.sistema.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Transactional;
import br.gerbellait.arquitetura.service.AbstractService;
import br.gerbellait.arquitetura.util.Utils;
import br.gerbellait.sistema.dao.RotaVendedorDao;
import br.gerbellait.sistema.entity.RotaVendedor;
import br.gerbellait.sistema.entity.Vendedor;
import br.gerbellait.sistema.service.RotaVendedorService;
import br.gerbellait.sistema.util.Constants;

@Stateless
public class RotaVendedorServiceImpl extends AbstractService<RotaVendedor> implements RotaVendedorService {

	@Inject
	private RotaVendedorDao dao;

	@Override
	protected void validate(RotaVendedor rotaVendedor) throws ServiceLayerException {
		if (rotaVendedor == null) {
			throw new ServiceLayerException("Objeto RotaVendedor null.");
		}
	}

	@Override
	public void doSave(Vendedor vendedor) {
		if (!Utils.isNullOrEmpty(vendedor.getRotas())) {
			for (RotaVendedor rotaVendedor : vendedor.getRotas()) {
				rotaVendedor.setVendedor(new Vendedor(vendedor.getId()));

				if (rotaVendedor.getId() == null) {
					this.dao.insert(rotaVendedor);

				} else if (!Utils.isNullOrEmpty(rotaVendedor.getAcao())
						&& rotaVendedor.getAcao().equalsIgnoreCase(Constants.ALTERAR)) {
					this.dao.update(rotaVendedor);

				} else if (!Utils.isNullOrEmpty(rotaVendedor.getAcao())
						&& rotaVendedor.getAcao().equalsIgnoreCase(Constants.REMOVER)) {
					this.dao.delete(rotaVendedor);
				}
			}
		}
	}

	@Override
	@Transactional
	public List<RotaVendedor> getBy(Vendedor vendedor) {
		return this.dao.getBy(vendedor);
	}

	@Override
	@Transactional(readOnly = false)
	public boolean deletar(RotaVendedor rotaVendedor) {
		return this.dao.deletar(rotaVendedor);
	}
}