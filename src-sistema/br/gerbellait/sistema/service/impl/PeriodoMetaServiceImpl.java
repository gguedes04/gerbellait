package br.gerbellait.sistema.service.impl;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Transactional;
import br.gerbellait.arquitetura.service.AbstractService;
import br.gerbellait.sistema.dao.PeriodoMetaDao;
import br.gerbellait.sistema.entity.PeriodoMeta;
import br.gerbellait.sistema.service.PeriodoMetaService;

@Stateless
public class PeriodoMetaServiceImpl extends AbstractService<PeriodoMeta> implements PeriodoMetaService {

	@Inject
	private PeriodoMetaDao dao;

	@Override
	protected void validate(PeriodoMeta periodoMeta) throws ServiceLayerException {
		if (periodoMeta == null) {
			throw new ServiceLayerException("Objeto PeriodoMeta	 null.");
		}
	}

	@Override
	@Transactional
	public PeriodoMeta save(PeriodoMeta periodoMeta) throws ServiceLayerException {
		this.validate(periodoMeta);
		periodoMeta.setDataAlteracao(new Date());
		if (periodoMeta.getId() != null) {
			this.dao.update(periodoMeta);
		} else {
			this.dao.insert(periodoMeta);
		}
		return periodoMeta;
	}

	@Override
	@Transactional
	public List<PeriodoMeta> all() {

		return this.dao.all();
	}

	@Override
	@Transactional
	public PeriodoMeta getPeriodoAtual() {

		return this.dao.getPeriodoAtual();
	}
}