package br.gerbellait.sistema.service.impl;

import java.sql.SQLException;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import br.gerbellait.sistema.dao.PedidoProdutoDao;
import br.gerbellait.sistema.entity.Pedido;
import br.gerbellait.sistema.entity.Produto;
import br.gerbellait.sistema.service.PedidoProdutoService;
import br.gerbellait.sistema.service.ProdutoService;

@Stateless
public class PedidoProdutoServiceImpl implements PedidoProdutoService {
    private static final Logger LOGGER = Logger.getLogger(PedidoProdutoServiceImpl.class);
    @Inject
    private PedidoProdutoDao pedidoProdutoDao;
    @Inject
    private ProdutoService produtoService;
    
    @Override
    public void save(Pedido pedido, Produto produto, boolean notExists) {
        pedidoProdutoDao.save(pedido, produto, notExists);
        // produtoService.updateQtEstoque(produto);
    }
    
    @Override
    public boolean contains(Pedido pedido, Produto produto) {
        try {
            return pedidoProdutoDao.contains(pedido, produto);
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return false;
    }
}
