package br.gerbellait.sistema.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Transactional;
import br.gerbellait.arquitetura.service.AbstractService;
import br.gerbellait.arquitetura.util.Utils;
import br.gerbellait.sistema.dao.ProdutoDao;
import br.gerbellait.sistema.dao.ProdutoPrecoRotaDao;
import br.gerbellait.sistema.entity.Produto;
import br.gerbellait.sistema.entity.ProdutoPrecoRota;
import br.gerbellait.sistema.entity.Rota;
import br.gerbellait.sistema.service.ProdutoPrecoRotaService;
import br.gerbellait.sistema.service.RotaService;
import br.gerbellait.sistema.util.Constants;

@Stateless
public class ProdutoPrecoRotaServiceImpl extends AbstractService<ProdutoPrecoRota> implements ProdutoPrecoRotaService {

	@Inject
	private ProdutoPrecoRotaDao dao;

	@Inject
	private RotaService rotaService;

	@Inject
	private ProdutoDao produtoDao;

	@Override
	protected void validate(ProdutoPrecoRota produtoPrecoRota) throws ServiceLayerException {
		if (produtoPrecoRota == null) {
			throw new ServiceLayerException("Objeto ProdutoPrecoRota null.");
		}
	}

	@Override
	@Transactional(readOnly = false)
	public void saveList(List<ProdutoPrecoRota> rotas) {
		if (!Utils.isNullOrEmpty(rotas)) {
			for (ProdutoPrecoRota produtoPrecoRota : rotas) {
				this.doSave(produtoPrecoRota);
			}
		}
	}

	public void doSave(ProdutoPrecoRota rota) {
		if (rota.getId() == null) {
			dao.insert(rota);
		} else {
			dao.update(rota);
		}
	}

	@Override
	public void doSave(Produto produto) {
		if (!Utils.isNullOrEmpty(produto.getRotas())) {
			for (ProdutoPrecoRota produtoPrecoRota : produto.getRotas()) {
				produtoPrecoRota.setProduto(new Produto(produto.getId()));
				produtoPrecoRota.setDataAlteracao(new Date());

				if (produtoPrecoRota.getId() == null) {
					produtoPrecoRota.setValorPrecoVendaProduto(BigDecimal.ZERO);
					this.dao.insert(produtoPrecoRota);

				} else if (!Utils.isNullOrEmpty(produtoPrecoRota.getAcao())
						&& produtoPrecoRota.getAcao().equalsIgnoreCase(Constants.ALTERAR)) {
					this.dao.update(produtoPrecoRota);

				} else if (!Utils.isNullOrEmpty(produtoPrecoRota.getAcao())
						&& produtoPrecoRota.getAcao().equalsIgnoreCase(Constants.REMOVER)) {
					this.dao.delete(produtoPrecoRota);
				}
			}
		}
	}

	@Override
	@Transactional
	public List<ProdutoPrecoRota> getBy(Produto produto) {
		List<ProdutoPrecoRota> retorno = this.dao.getBy(produto);
		if (!Utils.isNullOrEmpty(retorno)) {
			for (ProdutoPrecoRota produtoPrecoRota : retorno) {
				produtoPrecoRota.setRota(rotaService.doGetById(produtoPrecoRota.getRota()));
			}
		}
		return retorno;
	}

	@Override
	@Transactional(readOnly = false)
	public boolean deletar(ProdutoPrecoRota produtoPrecoRota) {
		return this.dao.deletar(produtoPrecoRota);
	}

	@Override
	@Transactional
	public List<Produto> getPrecoVendaRotaResource(Produto produto, List<Rota> rotas) {
		List<Produto> retorno = produtoDao.getByDto(produto);
		for (Produto prod : retorno) {
			for (Rota rota : rotas) {
				ProdutoPrecoRota produtoPrecoRota = new ProdutoPrecoRota();
				produtoPrecoRota.setProduto(new Produto(prod.getId(), prod.getNome()));
				produtoPrecoRota.getProduto().setIdCodProduto(prod.getIdCodProduto());
				produtoPrecoRota.setRota(new Rota(rota.getId(), rota.getDescricao()));
				ProdutoPrecoRota produtoPrecoRotaBanco = dao.getBy(produtoPrecoRota);
				if (produtoPrecoRotaBanco != null && produtoPrecoRotaBanco.getId() != null) {
					produtoPrecoRota.setProduto(new Produto(prod.getId(), prod.getNome()));
					produtoPrecoRota.getProduto().setIdCodProduto(prod.getIdCodProduto());
					produtoPrecoRota.setRota(new Rota(rota.getId(), rota.getDescricao()));
					prod.getRotas().add(produtoPrecoRotaBanco);
				} else {
					prod.getRotas().add(produtoPrecoRota);
				}
			}
		}
		return retorno;
	}
}