package br.gerbellait.sistema.service.impl;

import java.io.IOException;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.gerbellait.arquitetura.ApplicationContext;
import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Transactional;
import br.gerbellait.arquitetura.rest.adapter.BadDoubleJsonAdapter;
import br.gerbellait.arquitetura.service.AbstractService;
import br.gerbellait.arquitetura.util.EmailUtil;
import br.gerbellait.arquitetura.util.Utils;
import br.gerbellait.sistema.dao.UsuarioDao;
import br.gerbellait.sistema.entity.Usuario;
import br.gerbellait.sistema.service.PerfilUsuarioService;
import br.gerbellait.sistema.service.UsuarioService;
import br.gerbellait.sistema.util.Constants;
import br.gerbellait.sistema.util.CryptoUtil;

@Stateless
public class UsuarioServiceImpl extends AbstractService<Usuario> implements UsuarioService {
	private static final Logger LOGGER = Logger.getLogger(UsuarioServiceImpl.class);

	@Inject
	private PerfilUsuarioService perfilUsuarioService;

	@Inject
	private UsuarioDao dao;

	@Override
	protected void validate(Usuario usuario) throws ServiceLayerException {
		if (usuario == null) {
			throw new ServiceLayerException("Objeto usuário null.");
		}
		if (Utils.isNullOrEmpty(usuario.getLogin())) {
			throw new ServiceLayerException("Login é obrigatório.");
		}
	}

	private void preparaSalvar(Usuario usuario) throws IOException {
		if (usuario.getId() != null) {
			Usuario usuarioBanco = this.doGetById(usuario.getId());
			usuario.setSenha(CryptoUtil.toEncoding(usuarioBanco.getSenha()));
		} else {
			usuario.setSenha(CryptoUtil.toEncoding(usuario.getSenha()));
		}
	}

	@Override
	@Transactional
	public Usuario save(Usuario usuario) throws ServiceLayerException, IOException {
		this.validate(usuario);
		this.preparaSalvar(usuario);
		if (usuario.getId() != null) {
			this.dao.update(usuario);
		} else {
			usuario.setSituacao(Constants.ATIVO);
			this.dao.insert(usuario);
		}
		perfilUsuarioService.doSave(usuario);
		return usuario;
	}

	@Override
	@Transactional(readOnly = false)
	public void saveJson(String json) throws ServiceLayerException, IOException {
		Gson gson = new GsonBuilder().registerTypeAdapter(Usuario.class, new BadDoubleJsonAdapter()).create();
		Usuario usuario = gson.fromJson(json, Usuario.class);
		this.validate(usuario);
		this.preparaSalvar(usuario);
		salvar(usuario, dao);
		perfilUsuarioService.doSave(usuario);
	}

	@Override
	@Transactional
	public List<Usuario> all() {
		List<Usuario> retorno = dao.all();
		if (retorno != null) {
			for (Usuario usuario : retorno) {
				usuario.setPerfis(perfilUsuarioService.doAllByUser(usuario));
			}
		}
		return retorno;
	}

	@Override
	@Transactional
	public List<Usuario> getByDto(Usuario usuarioFiltro) {
		List<Usuario> retorno = dao.getByDto(usuarioFiltro);
		if (retorno != null) {
			for (Usuario usuario : retorno) {
				usuario.setPerfis(perfilUsuarioService.doAllByUser(usuario));
			}
		}
		return retorno;
	}

	@Override
	@Transactional
	public List<Usuario> getVendedores() {
		List<Usuario> retorno = dao.getVendedores();
		if (retorno != null) {
			for (Usuario usuario : retorno) {
				usuario.setPerfis(perfilUsuarioService.doAllByUser(usuario));
			}
		}
		return retorno;
	}

	@Override
	@Transactional
	public Usuario getBy(String login, String senha) {
		Usuario retorno = dao.getBy(login, CryptoUtil.toEncoding(senha));
		if (retorno != null) {
			retorno.setPerfis(perfilUsuarioService.doAllByUser(retorno));
		}
		return retorno;
	}

	@Override
	@Transactional(readOnly = false)
	public boolean deletar(Usuario usuario) {
		try {
			Usuario usuarioExcluir = this.doGetById(usuario.getId());
			perfilUsuarioService.deleteByUser(usuarioExcluir);
			delete(usuarioExcluir, dao);
			return true;
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return false;
	}

	public Usuario doGetById(Long id) throws IOException {
		Usuario retorno = dao.getById(new Usuario(id));
		if (retorno != null) {
			retorno.setSenha(CryptoUtil.toDecoding(retorno.getSenha()));
			retorno.setPerfis(perfilUsuarioService.doAllByUser(retorno));
		}
		return retorno;
	}

	@Override
	@Transactional
	public Usuario getById(Long id) throws IOException {
		return this.doGetById(id);
	}

	@Override
	@Transactional
	public boolean emailEsqueceuSenha(ApplicationContext context, Usuario usuario) throws IOException {
		try {
			List<Usuario> usuarios = dao.getByDto(usuario);
			if (!Utils.isNullOrEmpty(usuarios)) {
				Usuario usuarioBanco = usuarios.get(0);
				EmailUtil emailUtil = new EmailUtil();
				String subject = "Bella Itália - Esqueceu a Senha";
				StringBuilder message = new StringBuilder();
				message.append("Credenciais de acesso<br/>");
				message.append("login: " + usuarioBanco.getLogin() + "<br/>");
				String senha = CryptoUtil.toEncoding("bellaitalia2018");
				message.append("senha: " + senha + "<br/>");
				if (usuarioBanco.getId() != null) {
					usuarioBanco.setSenha(CryptoUtil.toEncoding("bellaitalia2018"));
					this.dao.update(usuarioBanco);
				}
				return emailUtil.sendMail(context, subject, message, usuario.getEmail());
			}
		} catch (ServiceLayerException e) {
			LOGGER.error(e);
		}
		return false;
	}

	@Transactional
	public boolean salvaNovaSenha(ApplicationContext context, Usuario usuario) throws IOException {
		try {
			List<Usuario> usuarios = dao.getByDto(usuario);
			if (!Utils.isNullOrEmpty(usuarios)) {
				Usuario usuarioBanco = usuarios.get(0);
				if (usuarioBanco.getId() != null) {
					usuarioBanco.setIcPrimeiroAcesso(false);
					usuarioBanco.setSenha(CryptoUtil.toEncoding(usuario.getSenha()));
					this.dao.update(usuarioBanco);
				}
				return true;
			}
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return false;
	}

	@Override
	@Transactional
	public Boolean isVendedor(Long id) throws IOException {
		Usuario usuario = this.doGetById(id);
		return usuario.getVendedor() != null && usuario.getVendedor().getId() != null;
	}
}