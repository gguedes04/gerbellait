package br.gerbellait.sistema.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Transactional;
import br.gerbellait.arquitetura.service.AbstractService;
import br.gerbellait.sistema.dao.PerfilUsuarioDao;
import br.gerbellait.sistema.entity.PerfilUsuario;
import br.gerbellait.sistema.entity.Usuario;
import br.gerbellait.sistema.service.PerfilUsuarioService;

@Stateless
public class PerfilUsuarioServiceImpl extends AbstractService<PerfilUsuario> implements PerfilUsuarioService {
	private static final Logger LOGGER = Logger.getLogger(PerfilUsuarioServiceImpl.class);

	@Inject
	private PerfilUsuarioDao dao;

	@Override
	protected void validate(PerfilUsuario perfilUsuario) throws ServiceLayerException {
		if (perfilUsuario == null) {
			throw new ServiceLayerException("Objeto perfilUsuario null.");
		}
		if (perfilUsuario.getUsuario() == null) {
			throw new ServiceLayerException("Usuário e obrigatório.");
		}
		if (perfilUsuario.getPerfil() == null) {
			throw new ServiceLayerException("Perfil é obrigatória.");
		}
	}

	@Override
	public void doSave(Usuario usuario) {
		this.deleteByUser(usuario);
		PerfilUsuario perfilUsuario = new PerfilUsuario();
		perfilUsuario.setUsuario(usuario);
		perfilUsuario.setPerfil(usuario.getPerfil());
		this.dao.insert(perfilUsuario);
	}

	@Override
	public List<PerfilUsuario> doAllByUser(Usuario usuarioFiltro) {
		return dao.allByUser(usuarioFiltro);
	}

	@Override
	@Transactional
	public List<PerfilUsuario> allByUser(Usuario usuarioFiltro) {
		return this.allByUser(usuarioFiltro);
	}

	@Override
	public boolean deleteByUser(Usuario usuario) {
		try {
			return this.dao.deleteByUser(usuario);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return false;
	}
}