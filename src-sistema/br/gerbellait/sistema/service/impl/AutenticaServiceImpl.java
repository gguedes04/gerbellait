package br.gerbellait.sistema.service.impl;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.novell.ldap.util.Base64;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Transactional;
import br.gerbellait.arquitetura.service.AbstractService;
import br.gerbellait.sistema.entity.Usuario;
import br.gerbellait.sistema.entity.UsuarioToken;
import br.gerbellait.sistema.service.AutenticaService;
import br.gerbellait.sistema.service.ItemMenuPerfilService;
import br.gerbellait.sistema.service.UsuarioService;

@Stateless
public class AutenticaServiceImpl extends AbstractService<Usuario> implements AutenticaService {

	@Inject
	private ItemMenuPerfilService itemMenuPerfilService;

	@Inject
	private UsuarioService usuarioService;

	@Override
	@Transactional
	public UsuarioToken autenticar(Usuario usuario) throws ServiceLayerException {
		Usuario usuarioBase = usuarioService.getBy(usuario.getLogin(), usuario.getSenha());
		UsuarioToken usuarioToken = new UsuarioToken();
		if (usuarioBase != null) {
			usuarioToken.setId(usuarioBase.getId());
			usuarioToken.setLogin(usuarioBase.getLogin());
			usuarioToken.setToken(Base64.encode(usuarioBase.getLogin()));
			// cada usuário possui somente um perfil
			usuarioToken.setFuncoes(itemMenuPerfilService.getDescByPerfil(usuarioBase.getPerfil()));
			usuarioToken.setGrupo(usuarioBase.getPerfil().getNome());
			usuarioToken.setSituacao(usuarioBase.getSituacao());
			usuarioToken.setIcPrimeiroAcesso(usuarioBase.getIcPrimeiroAcesso());
			// caso o usuário seja um vendedor
			if (usuarioBase.getVendedor() != null && usuarioBase.getVendedor().getId() != null) {
				usuarioToken.setIdVendedor(usuarioBase.getVendedor().getId());
			}
		}
		return usuarioToken;
	}

	@Override
	protected void validate(Usuario usuario) throws ServiceLayerException {
		if (usuario == null) {
			throw new ServiceLayerException(this.getMessage("MN001"));
		}
	}

}