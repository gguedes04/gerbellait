package br.gerbellait.sistema.service.impl;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Transactional;
import br.gerbellait.arquitetura.service.AbstractService;
import br.gerbellait.arquitetura.util.StringUtils;
import br.gerbellait.arquitetura.util.Utils;
import br.gerbellait.sistema.bean.PedidoImpBean;
import br.gerbellait.sistema.bean.TableState;
import br.gerbellait.sistema.bean.UltimasVendasBean;
import br.gerbellait.sistema.dao.PedidoDao;
import br.gerbellait.sistema.entity.Pedido;
import br.gerbellait.sistema.entity.Produto;
import br.gerbellait.sistema.service.*;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellUtil;
import org.jboss.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Stateless
public class PedidoServiceImpl extends AbstractService<Pedido> implements PedidoService {
    
    private static final Logger LOGGER = Logger.getLogger(PedidoServiceImpl.class);
    
    @Inject
    private PedidoDao dao;
    @Inject
    private FormaPagamentoService formaPagamentoService;
    @Inject
    private PessoaService pessoaService;
    @Inject
    private VendedorService vendedorService;
    @Inject
    private ProdutoService produtoService;
    @Inject
    private PedidoProdutoService pedidoProdutoService;
    @Inject
    private MapaCargaService mapaCargaService;
    
    @Override
    protected void validate(Pedido pedido) throws ServiceLayerException {
        if (pedido == null) {
            throw new ServiceLayerException("Objeto null.");
        }
    }
    
    @Override
    @Transactional
    public Pedido save(Pedido pedido) throws ServiceLayerException {
        this.validate(pedido);
        pedido.setDataAlteracao(new Date());
        if (pedido.getVendedor() == null) {
            pedido.setVendedor(vendedorService.getById(pedido.getCliente().getIdVendedorRepresentante()));
        }
        boolean notExists = pedido.getId() == null;
        if (!notExists) {
            pedido = this.dao.update(pedido);
        } else {
            pedido.setDataPedido(new Date());
            pedido = this.dao.insert(pedido);
        }
        for (Produto produto : pedido.getProdutoList()) {
            pedidoProdutoService.save(pedido, produto, notExists);
        }
        return pedido;
    }

    @Override
    @Transactional
    public boolean deletar(Pedido pedido) {
        try {
            this.dao.deletar(pedido);
            return true;
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return false;
    }
    
    @Override
    @Transactional
    public List<Pedido> getByDto(TableState tableState) {
        try {
            return dao.getByDto(tableState);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return Collections.emptyList();
    }
    
    @Override
    @Transactional
    public List<UltimasVendasBean> getUltimasVendas(Pedido pedido) {
        try {
            return dao.getUltimasVendas(pedido);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return Collections.emptyList();
    }

    @Override
    @Transactional
    public Pedido getById(Long idPedido) {
        try {
            Pedido pedido = dao.getById(new Pedido(idPedido));
            pedido.setFormaPagamento(formaPagamentoService.getById(pedido.getFormaPagamento()));
            pedido.setCliente(pessoaService.getById(pedido.getCliente().getId()));
            pedido.setVendedor(vendedorService.getById(pedido.getVendedor().getId()));
            pedido.setProdutoList(produtoService.getByPedido(pedido));
            pedido.setHasLoadMap(mapaCargaService.hasLinkedPedido(idPedido));
            pedido.setUltimasVendas(dao.getUltimasVendas(pedido));
            return pedido;
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return null;
    }
    
    @Override
    @Transactional
    public InputStream relatorio(Pedido pedido) throws Exception {
        List<PedidoImpBean> result = dao.relatorio(pedido);
        
        if (Utils.isNullOrEmpty(result)) throw new Exception("Não ha dados para este pedido");
        
        PedidoImpBean first = result.get(0);
        
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet pedidoSheet = workbook.createSheet("Pedido");
        
        int rownum = 0;
        Row row = pedidoSheet.createRow(rownum++);
        
        row.createCell(0).setCellValue(pedido.getId());
        pedidoSheet.setColumnWidth(0, getColumnWidth(10));
        CellUtil.setAlignment(row.getCell(0), HorizontalAlignment.CENTER);
        
        row.createCell(1).setCellValue(first.getNmPessoa());
        pedidoSheet.setColumnWidth(1, getColumnWidth(first.getNmPessoa().length()));
        
        row.createCell(5).setCellValue(StringUtils.isNullOrEmpty(first.getTelefone()) ? "(00) 0000-0000" : first.getTelefone());
        pedidoSheet.setColumnWidth(5, getColumnWidth(first.getTelefone().length()));
        CellUtil.setAlignment(row.getCell(5), HorizontalAlignment.CENTER);
        
        
        String value = pedido.getIdPedidoCliente() + " - " + LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        row.createCell(7).setCellValue(value);
        pedidoSheet.setColumnWidth(7, getColumnWidth(value.length()));
        CellUtil.setAlignment(row.getCell(7), HorizontalAlignment.RIGHT);
        
        Row secondRow = pedidoSheet.createRow(rownum++);
        
        secondRow.createCell(0).setCellValue(first.getLogradouro() + ", " + first.getNumero() + " - " + first.getBairro() + " - " + first.getCidade());
        pedidoSheet.addMergedRegion(new CellRangeAddress(1, 1, 0, 5));
        
        secondRow.createCell(6).setCellValue(first.getFormaPagamento() + " - " + first.getPrazo());
        CellUtil.setAlignment(secondRow.getCell(6), HorizontalAlignment.RIGHT);
        pedidoSheet.addMergedRegion(new CellRangeAddress(1, 1, 6, 7));
        
        rownum += 3;
        Row headerRow = pedidoSheet.createRow(rownum);
        
        int cellnum = 0;
        headerRow.createCell(cellnum).setCellValue("Cód");
        CellUtil.setAlignment(headerRow.getCell(cellnum), HorizontalAlignment.CENTER);
        headerRow.createCell(++cellnum).setCellValue("Produto");
        headerRow.createCell(++cellnum).setCellValue("Und");
        CellUtil.setAlignment(headerRow.getCell(cellnum), HorizontalAlignment.CENTER);
        headerRow.createCell(++cellnum).setCellValue("Qdade");
        CellUtil.setAlignment(headerRow.getCell(cellnum), HorizontalAlignment.CENTER);
        headerRow.createCell(++cellnum).setCellValue("Troca");
        CellUtil.setAlignment(headerRow.getCell(cellnum), HorizontalAlignment.CENTER);
        headerRow.createCell(++cellnum).setCellValue("Preço");
        CellUtil.setAlignment(headerRow.getCell(cellnum), HorizontalAlignment.CENTER);
        headerRow.createCell(++cellnum).setCellValue("ST");
        CellUtil.setAlignment(headerRow.getCell(cellnum), HorizontalAlignment.CENTER);
        headerRow.createCell(++cellnum).setCellValue("Valor");
        CellUtil.setAlignment(headerRow.getCell(cellnum), HorizontalAlignment.CENTER);
        
        for (PedidoImpBean bean : result) {
            Row dinamicRow = pedidoSheet.createRow(++rownum);
            cellnum = 0;
            dinamicRow.createCell(cellnum).setCellValue(bean.getCodProduto());
            CellUtil.setAlignment(dinamicRow.getCell(cellnum), HorizontalAlignment.CENTER);
            dinamicRow.createCell(++cellnum).setCellValue(bean.getNmProduto());
            dinamicRow.createCell(++cellnum).setCellValue(bean.getUnidade());
            CellUtil.setAlignment(dinamicRow.getCell(cellnum), HorizontalAlignment.CENTER);
            dinamicRow.createCell(++cellnum).setCellValue(bean.getQtProduto());
            CellUtil.setAlignment(dinamicRow.getCell(cellnum), HorizontalAlignment.CENTER);
            dinamicRow.createCell(++cellnum).setCellValue(bean.getQtTrocaProduto());
            CellUtil.setAlignment(dinamicRow.getCell(cellnum), HorizontalAlignment.CENTER);
            dinamicRow.createCell(++cellnum).setCellValue(currencyFormatter(bean.getVlUnitario()));
            CellUtil.setAlignment(dinamicRow.getCell(cellnum), HorizontalAlignment.CENTER);
            dinamicRow.createCell(++cellnum).setCellValue(currencyFormatter(calculateSt(pedido, bean)));
            CellUtil.setAlignment(dinamicRow.getCell(cellnum), HorizontalAlignment.CENTER);
            dinamicRow.createCell(++cellnum).setCellValue(currencyFormatter(bean.getVlTotalProduto()));
            CellUtil.setAlignment(dinamicRow.getCell(cellnum), HorizontalAlignment.CENTER);
        }
        
        rownum = ++rownum + 2;
        Row totalRow = pedidoSheet.createRow(rownum);
        
        totalRow.createCell(0).setCellValue("Valor Pedido");
        totalRow.createCell(1).setCellValue(currencyFormatter(first.getVlPedido()));
        
        totalRow.createCell(3).setCellValue("Desc");
        totalRow.createCell(4).setCellValue(currencyFormatter(first.getDesconto()));
        
        totalRow.createCell(6).setCellValue("Total");
        totalRow.createCell(7).setCellValue(currencyFormatter(first.getVlPedido() - first.getDesconto()));
        
        rownum = ++rownum + 2;
        Row obsRow = pedidoSheet.createRow(rownum);
        
        obsRow.createCell(0).setCellValue("Obs");
        obsRow.createCell(1).setCellValue(first.getObservacao());
        CellUtil.setVerticalAlignment(obsRow.getCell(1), VerticalAlignment.TOP);
        pedidoSheet.addMergedRegion(new CellRangeAddress(rownum, rownum + 3, 1, 7));
        
        FileOutputStream outFile = new FileOutputStream(new File("pedido.excel"));
        workbook.write(outFile);
        outFile.close();
        System.out.println("Arquivo Excel editado com sucesso!");
        return new FileInputStream("pedido.excel");
    }
    
    private int getColumnWidth(int length) {
        return ((int) (length * 1.14388)) * 256;
    }
    
    private Double calculateSt(Pedido pedido, PedidoImpBean bean) {
        double vst = 0D;
        double vmva;
        
        if ("S".equals(pedido.getFlGeraNf())) {
            if ("S".equals(pedido.getCliente().getFlgOptanteSimples()) && bean.getPercSimples() > 0) {
//        'mva com reducao
                vmva = (bean.getVlUnitario() * bean.getQtProduto()) +
                        (bean.getVlUnitario() * bean.getQtProduto()) * (bean.getMva() * pedido.getCliente().getPercDescPadrao().doubleValue());
            } else {
//        'mva sem reducao
                vmva = (bean.getVlUnitario() * bean.getQtProduto()) +
                        (bean.getVlUnitario() * bean.getQtProduto()) * (bean.getMva());
            }
            
            if ("1".equals(pedido.getCliente().getIdTipoEmpresa())) {
                
                if ("S".equals(bean.getPossuiSt())) {
                    
                    double vicmsentrada = (bean.getVlUnitario() * bean.getQtProduto()) * (bean.getIcmsEntrada());
                    double vicmssaida = vmva * bean.getIcmsSaida();
                    
                    if ((vicmsentrada - vicmssaida) < 0) {
                        vst = (vicmsentrada - vicmssaida) * -1;
                    } else {
                        vst = (vicmsentrada - vicmssaida);
                    }
                }
            }
        }
        return vst;
    }
    
    
    private String currencyFormatter(Double value) {
        Locale locale = new Locale("pt", "BR");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(locale);
        return formatter.format(value);
    }
    
}
