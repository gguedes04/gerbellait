package br.gerbellait.sistema.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Transactional;
import br.gerbellait.arquitetura.service.AbstractService;
import br.gerbellait.arquitetura.util.Utils;
import br.gerbellait.sistema.dao.ComissaoVendedorDao;
import br.gerbellait.sistema.entity.ComissaoVendedor;
import br.gerbellait.sistema.entity.Vendedor;
import br.gerbellait.sistema.service.ComissaoVendedorService;
import br.gerbellait.sistema.util.Constants;

@Stateless
public class ComissaoVendedorServiceImpl extends AbstractService<ComissaoVendedor> implements ComissaoVendedorService {

	@Inject
	private ComissaoVendedorDao dao;

	@Override
	protected void validate(ComissaoVendedor comissaoVendedor) throws ServiceLayerException {
		if (comissaoVendedor == null) {
			throw new ServiceLayerException("Objeto ComissaoVendedor null.");
		}
	}

	@Override
	public void doSave(Vendedor vendedor) {
		if (!Utils.isNullOrEmpty(vendedor.getComissoes())) {
			for (ComissaoVendedor comissaoVendedor : vendedor.getComissoes()) {
				comissaoVendedor.setVendedor(new Vendedor(vendedor.getId()));

				if (comissaoVendedor.getId() == null) {
					this.dao.insert(comissaoVendedor);

				} else if (!Utils.isNullOrEmpty(comissaoVendedor.getAcao())
						&& comissaoVendedor.getAcao().equalsIgnoreCase(Constants.ALTERAR)) {
					this.dao.update(comissaoVendedor);

				} else if (!Utils.isNullOrEmpty(comissaoVendedor.getAcao())
						&& comissaoVendedor.getAcao().equalsIgnoreCase(Constants.REMOVER)) {
					this.dao.delete(comissaoVendedor);
				}
			}
		}
	}

	@Override
	@Transactional
	public List<ComissaoVendedor> getBy(Vendedor vendedor) {
		return this.dao.getBy(vendedor);
	}

	@Override
	@Transactional(readOnly = false)
	public boolean deletar(ComissaoVendedor comissaoVendedor) {
		return this.dao.deletar(comissaoVendedor);
	}
}