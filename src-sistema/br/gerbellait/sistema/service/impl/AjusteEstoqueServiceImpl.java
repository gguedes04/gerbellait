package br.gerbellait.sistema.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Transactional;
import br.gerbellait.arquitetura.service.AbstractService;
import br.gerbellait.arquitetura.util.Utils;
import br.gerbellait.sistema.dao.AjusteEstoqueDao;
import br.gerbellait.sistema.entity.AjusteEstoque;
import br.gerbellait.sistema.entity.Produto;
import br.gerbellait.sistema.service.AjusteEstoqueService;
import br.gerbellait.sistema.service.ProdutoService;

@Stateless
public class AjusteEstoqueServiceImpl extends AbstractService<AjusteEstoque> implements AjusteEstoqueService {

	private static final Logger LOGGER = Logger.getLogger(AjusteEstoqueServiceImpl.class);

	@Inject
	private AjusteEstoqueDao dao;

	@Inject
	private ProdutoService produtoService;

	@Override
	protected void validate(AjusteEstoque t) throws ServiceLayerException {
		if (t.getProduto() == null || t.getProduto().getId() == null) {
			throw new ServiceLayerException("Objeto ajusteEstoque null.");
		}
	}

	@Override
	@Transactional
	public AjusteEstoque save(AjusteEstoque ajusteEstoque) throws ServiceLayerException {
		this.validate(ajusteEstoque);
		this.produtoService.ajusteEstoque(ajusteEstoque);
		if (ajusteEstoque.getId() != null) {
			return this.dao.update(ajusteEstoque);
		} else {
			return this.dao.insert(ajusteEstoque);
		}
	}

	@Override
	@Transactional(readOnly = false)
	public boolean deletar(AjusteEstoque ajusteEstoque) {
		try {
			this.dao.delete(ajusteEstoque);
			return true;
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return false;
	}

	@Override
	@Transactional
	public AjusteEstoque getById(Long id) {
		AjusteEstoque retorno = this.dao.getById(new AjusteEstoque(id));
		if (retorno != null) {
			retorno.setProduto(produtoService.getById(retorno.getProduto().getId()));
		}
		return retorno;
	}

	@Override
	@Transactional
	public List<AjusteEstoque> getAll() {
		List<AjusteEstoque> retorno = this.dao.getAll();
		if (!Utils.isNullOrEmpty(retorno)) {
			for (AjusteEstoque ajusteEstoque : retorno) {
				ajusteEstoque.setProduto(produtoService.getById(ajusteEstoque.getProduto().getId()));
			}
		}
		return retorno;
	}

	@Override
	@Transactional
	public List<AjusteEstoque> getBy(AjusteEstoque filtro, boolean isPaginate) {
		List<AjusteEstoque> retorno = this.dao.getBy(filtro, isPaginate);
		if (!Utils.isNullOrEmpty(retorno)) {
			for (AjusteEstoque ajusteEstoque : retorno) {
				ajusteEstoque.setProduto(produtoService.getById(ajusteEstoque.getProduto().getId()));
			}
		}
		return retorno;
	}

	@Override
	@Transactional
	public List<AjusteEstoque> getByProduto(Produto produto) {
		return this.dao.getByProduto(produto);
	}

}