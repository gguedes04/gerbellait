package br.gerbellait.sistema.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Transactional;
import br.gerbellait.arquitetura.service.AbstractService;
import br.gerbellait.sistema.dao.RotaDao;
import br.gerbellait.sistema.entity.Rota;
import br.gerbellait.sistema.service.RotaService;

@Stateless
public class RotaServiceImpl extends AbstractService<Rota> implements RotaService {

	@Inject
	private RotaDao dao;

	@Override
	protected void validate(Rota rota) throws ServiceLayerException {
		if (rota == null) {
			throw new ServiceLayerException("Objeto UF null.");
		}
	}

	@Override
	@Transactional
	public List<Rota> getByVendedor(Long idVendedor) {
		return this.dao.getByVendedor(idVendedor);
	}

	@Override
	@Transactional
	public List<Rota> getAll() {
		return (List<Rota>) this.dao.getAll(Rota.class);
	}

	@Override
	public Rota doGetById(Rota rota) {
		return this.dao.getById(rota);
	}
}