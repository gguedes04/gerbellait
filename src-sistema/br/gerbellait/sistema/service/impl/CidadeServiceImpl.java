package br.gerbellait.sistema.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Transactional;
import br.gerbellait.arquitetura.service.AbstractService;
import br.gerbellait.arquitetura.util.Utils;
import br.gerbellait.sistema.dao.CidadeDao;
import br.gerbellait.sistema.entity.Cidade;
import br.gerbellait.sistema.entity.Uf;
import br.gerbellait.sistema.service.CidadeService;
import br.gerbellait.sistema.service.UfService;

@Stateless
public class CidadeServiceImpl extends AbstractService<Cidade> implements CidadeService {

	@Inject
	private CidadeDao dao;

	@Inject
	private UfService ufService;

	@Override
	protected void validate(Cidade cidade) throws ServiceLayerException {
		if (cidade == null) {
			throw new ServiceLayerException("Objeto UF null.");
		}
		if (Utils.isNullOrEmpty(cidade.getNome())) {
			throw new ServiceLayerException("Nome é obrigatório.");
		}
	}

	@Override
	@Transactional
	public List<Cidade> getByUf(Uf uf) {
		return this.dao.getByUf(uf);
	}

	@Override
	@Transactional
	public Cidade getById(Cidade cidade) {
		Cidade retorno = this.dao.getById(cidade);
		if (retorno != null) {
			retorno.setUf(ufService.getById(retorno.getUf()));
		}
		return retorno;
	}
}