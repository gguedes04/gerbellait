package br.gerbellait.sistema.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Transactional;
import br.gerbellait.arquitetura.service.AbstractService;
import br.gerbellait.arquitetura.util.Utils;
import br.gerbellait.sistema.dao.EnderecoDao;
import br.gerbellait.sistema.entity.Endereco;
import br.gerbellait.sistema.entity.Pessoa;
import br.gerbellait.sistema.service.CidadeService;
import br.gerbellait.sistema.service.EnderecoService;
import br.gerbellait.sistema.service.TipoEnderecoService;
import br.gerbellait.sistema.util.Constants;

@Stateless
public class EnderecoServiceImpl extends AbstractService<Endereco> implements EnderecoService {

	@Inject
	private EnderecoDao dao;

	@Inject
	private CidadeService cidadeService;

	@Inject
	private TipoEnderecoService tipoEnderecoService;

	@Override
	protected void validate(Endereco endereco) throws ServiceLayerException {
		if (endereco == null) {
			throw new ServiceLayerException("Objeto UF null.");
		}
	}

	@Override
	public void doSave(Pessoa pessoa) {
		if (!Utils.isNullOrEmpty(pessoa.getEnderecos())) {
			for (Endereco endereco : pessoa.getEnderecos()) {
				endereco.setPessoa(new Pessoa(pessoa.getId()));
				if (endereco.getId() == null) {
					this.dao.insert(endereco);

				} else if (!Utils.isNullOrEmpty(endereco.getAcao())
						&& endereco.getAcao().equalsIgnoreCase(Constants.ALTERAR)) {
					this.dao.update(endereco);

				} else if (!Utils.isNullOrEmpty(endereco.getAcao())
						&& endereco.getAcao().equalsIgnoreCase(Constants.REMOVER)) {
					this.dao.delete(endereco);
				}
			}
		}
	}

	@Override
	@Transactional
	public List<Endereco> getByPessoa(Pessoa pessoa) {
		List<Endereco> retorno = this.dao.getByPessoa(pessoa);
		this.carregaDados(retorno);
		return retorno;
	}

	private void carregaDados(List<Endereco> retorno) {
		if (!Utils.isNullOrEmpty(retorno)) {
			for (Endereco endereco : retorno) {
				endereco.setCidade(cidadeService.getById(endereco.getCidade()));
				endereco.setTipoEndereco(tipoEnderecoService.getById(endereco.getTipoEndereco()));
			}
		}
	}
}