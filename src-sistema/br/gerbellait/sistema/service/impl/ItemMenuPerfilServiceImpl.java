package br.gerbellait.sistema.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Transactional;
import br.gerbellait.arquitetura.service.AbstractService;
import br.gerbellait.sistema.dao.ItemMenuPerfilDao;
import br.gerbellait.sistema.entity.ItemMenuPerfil;
import br.gerbellait.sistema.entity.Perfil;
import br.gerbellait.sistema.service.ItemMenuPerfilService;

@Stateless
public class ItemMenuPerfilServiceImpl extends AbstractService<ItemMenuPerfil> implements ItemMenuPerfilService {

	@Inject
	private ItemMenuPerfilDao dao;

	@Override
	protected void validate(ItemMenuPerfil itemMenuPerfil) throws ServiceLayerException {
		if (itemMenuPerfil == null) {
			throw new ServiceLayerException("Objeto itemMenuPerfil null.");
		}
		if (itemMenuPerfil.getItemMenu() == null) {
			throw new ServiceLayerException("Item de menu é obrigatário.");
		}
		if (itemMenuPerfil.getPerfil() == null) {
			throw new ServiceLayerException("Perfil é obrigatório.");
		}
	}

	@Override
	public List<ItemMenuPerfil> doAllByPerfil(Perfil perfil) {
		return dao.allByPerfil(perfil);
	}

	@Override
	@Transactional
	public List<ItemMenuPerfil> allByPerfil(Perfil perfil) {
		return this.doAllByPerfil(perfil);
	}

	@Override
	public List<String> getDescByPerfil(Perfil perfil) {
		List<String> retorno = new ArrayList<>();
		List<ItemMenuPerfil> itens = this.doAllByPerfil(perfil);
		if (itens != null && !itens.isEmpty()) {
			for (ItemMenuPerfil itemMenuPerfil : itens) {
				retorno.add(itemMenuPerfil.getItemMenu().getNome().trim().toUpperCase());
			}
		}
		return retorno;
	}
}