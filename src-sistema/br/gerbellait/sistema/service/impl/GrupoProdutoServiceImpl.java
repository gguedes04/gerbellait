package br.gerbellait.sistema.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Transactional;
import br.gerbellait.arquitetura.service.AbstractService;
import br.gerbellait.sistema.dao.GrupoProdutoDao;
import br.gerbellait.sistema.entity.GrupoProduto;
import br.gerbellait.sistema.service.GrupoProdutoService;

@Stateless
public class GrupoProdutoServiceImpl extends AbstractService<GrupoProduto> implements GrupoProdutoService {

	@Inject
	private GrupoProdutoDao dao;

	@Override
	protected void validate(GrupoProduto grupo) throws ServiceLayerException {
		if (grupo == null) {
			throw new ServiceLayerException("Objeto UF null.");
		}
	}

	@Override
	@Transactional
	public List<GrupoProduto> getAll() {
		return this.dao.all();
	}

	@Override
	@Transactional
	public GrupoProduto getById(GrupoProduto grupo) {
		return this.dao.getById(grupo);
	}
}