package br.gerbellait.sistema.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Transactional;
import br.gerbellait.arquitetura.service.AbstractService;
import br.gerbellait.arquitetura.util.Utils;
import br.gerbellait.sistema.dao.ProdutoImpostoDao;
import br.gerbellait.sistema.entity.Produto;
import br.gerbellait.sistema.entity.ProdutoImposto;
import br.gerbellait.sistema.service.ProdutoImpostoService;
import br.gerbellait.sistema.util.Constants;

@Stateless
public class ProdutoImpostoServiceImpl extends AbstractService<ProdutoImposto> implements ProdutoImpostoService {

	@Inject
	private ProdutoImpostoDao dao;

	@Override
	protected void validate(ProdutoImposto produtoImposto) throws ServiceLayerException {
		if (produtoImposto == null) {
			throw new ServiceLayerException("Objeto ProdutoImposto null.");
		}
	}

	@Override
	public void doSave(Produto produto) {
		if (!Utils.isNullOrEmpty(produto.getImpostos())) {
			for (ProdutoImposto produtoImposto : produto.getImpostos()) {
				produtoImposto.setProduto(new Produto(produto.getId()));

				if (produtoImposto.getId() == null) {
					this.dao.insert(produtoImposto);

				} else if (!Utils.isNullOrEmpty(produtoImposto.getAcao())
						&& produtoImposto.getAcao().equalsIgnoreCase(Constants.ALTERAR)) {
					this.dao.update(produtoImposto);

				} else if (!Utils.isNullOrEmpty(produtoImposto.getAcao())
						&& produtoImposto.getAcao().equalsIgnoreCase(Constants.REMOVER)) {
					this.dao.delete(produtoImposto);
				}
			}
		}
	}

	@Override
	@Transactional
	public List<ProdutoImposto> getBy(Produto produto) {
		return this.dao.getBy(produto);
	}

	@Override
	@Transactional(readOnly = false)
	public boolean deletar(ProdutoImposto produtoImposto) {
		return this.dao.deletar(produtoImposto);
	}
}