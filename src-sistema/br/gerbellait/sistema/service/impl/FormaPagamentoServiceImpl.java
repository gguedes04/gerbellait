package br.gerbellait.sistema.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Transactional;
import br.gerbellait.arquitetura.service.AbstractService;
import br.gerbellait.sistema.dao.FormaPagamentoDao;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.entity.FormaPagamento;
import br.gerbellait.sistema.service.FormaPagamentoService;

@Stateless
public class FormaPagamentoServiceImpl extends AbstractService<FormaPagamento> implements FormaPagamentoService {
    
    @Inject
    private FormaPagamentoDao dao;
    
    @Override
    protected void validate(FormaPagamento formaPagamento) throws ServiceLayerException {
        if (formaPagamento == null) {
            throw new ServiceLayerException("Objeto UF null.");
        }
    }
    
    @Override
    @Transactional
    public List<FormaPagamento> getAll() {
        return this.dao.all();
    }
    
    @Override
    @Transactional
    public FormaPagamento getById(FormaPagamento formaPagamento) {
        return this.dao.getById(formaPagamento);
    }
    
    
    @Override
    @Transactional
    public List<FormaPagamento> allBySituacao(ExcluidoEnum flExcluido) {
        return this.dao.allBySituacao(flExcluido);
    }
}
