package br.gerbellait.sistema.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Transactional;
import br.gerbellait.arquitetura.service.AbstractService;
import br.gerbellait.arquitetura.util.Utils;
import br.gerbellait.sistema.dao.UfDao;
import br.gerbellait.sistema.entity.Uf;
import br.gerbellait.sistema.service.UfService;

@Stateless
public class UfServiceImpl extends AbstractService<Uf> implements UfService {

	@Inject
	private UfDao dao;

	@Override
	protected void validate(Uf perfil) throws ServiceLayerException {
		if (perfil == null) {
			throw new ServiceLayerException("Objeto UF null.");
		}
		if (Utils.isNullOrEmpty(perfil.getNome())) {
			throw new ServiceLayerException("Nome é obrigatório.");
		}
		if (Utils.isNullOrEmpty(perfil.getSigla())) {
			throw new ServiceLayerException("Sigla é obrigatória.");
		}
	}

	@Override
	@Transactional
	public List<Uf> getAll() {
		return this.dao.all();
	}

	@Override
	@Transactional
	public Uf getById(Uf uf) {
		return this.dao.getById(uf);
	}
}