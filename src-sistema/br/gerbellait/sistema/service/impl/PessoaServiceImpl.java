package br.gerbellait.sistema.service.impl;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Transactional;
import br.gerbellait.arquitetura.service.AbstractService;
import br.gerbellait.arquitetura.util.Utils;
import br.gerbellait.sistema.dao.PessoaDao;
import br.gerbellait.sistema.dao.enumerador.RegimeTributarioEnum;
import br.gerbellait.sistema.entity.Pessoa;
import br.gerbellait.sistema.entity.TipoEndereco;
import br.gerbellait.sistema.entity.TipoPessoa;
import br.gerbellait.sistema.service.EnderecoService;
import br.gerbellait.sistema.service.PessoaService;
import br.gerbellait.sistema.util.Constants;

@Stateless
public class PessoaServiceImpl extends AbstractService<Pessoa> implements PessoaService {

	private static final Logger LOGGER = Logger.getLogger(PessoaServiceImpl.class);

	@Inject
	private PessoaDao dao;

	@Inject
	private EnderecoService enderecoService;

	@Override
	protected void validate(Pessoa pessoa) throws ServiceLayerException {
		if (pessoa == null) {
			throw new ServiceLayerException("Objeto UF null.");
		}
		if (Utils.isNullOrEmpty(pessoa.getEnderecos())) {
			throw new ServiceLayerException("Ao menos um endereço deve ser incluído.");
		}
	}

	@Override
	@Transactional
	public Pessoa save(Pessoa pessoa) throws ServiceLayerException {
		this.validate(pessoa);
		pessoa.setDataAlteracao(new Date());
		pessoa.setNome(Utils.normalize(pessoa.getNome()).toUpperCase());
		if (pessoa.getIdRegimeTributario() != null
				&& pessoa.getIdRegimeTributario().equals(RegimeTributarioEnum.ME.getId())) {
			pessoa.setInscricaoEstadual(null);
		}
		if (pessoa.getId() != null) {
			this.dao.update(pessoa);
		} else {
			pessoa.setIdCodPessoa(this.dao.nextId());
			this.dao.insert(pessoa);
		}
		this.enderecoService.doSave(pessoa);
		return pessoa;
	}

	@Override
	@Transactional
	public boolean deletar(Pessoa pessoa) {
		try {
			this.dao.deletar(pessoa);
			return true;
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return false;
	}

	public Pessoa doGetById(Long id) {
		Pessoa retorno = dao.getByCodigo(new Pessoa(id));
		if (retorno != null && retorno.getId() != null) {
			retorno.setEnderecos(enderecoService.getByPessoa(retorno));
		}
		return retorno;
	}

	@Override
	@Transactional
	public Pessoa getById(Long id) {
		return this.doGetById(id);
	}

	@Override
	@Transactional
	public List<Pessoa> getAll() {
		return this.dao.all();
	}

	@Override
	@Transactional
	public List<Pessoa> getClientesByDto(Pessoa pessoa) {
		pessoa.setTipoPessoa(new TipoPessoa(Constants.TIPO_PESSOA_CLIENTE));
		pessoa.getEnderecoComercial().setTipoEndereco(new TipoEndereco(Constants.TIPO_ENDERECO_COMERCIAL));
		return dao.getByDto(pessoa);
	}

	@Override
	@Transactional
	public List<Pessoa> getClientesPedByDto(Pessoa pessoa) {
		pessoa.setTipoPessoa(new TipoPessoa(Constants.TIPO_PESSOA_CLIENTE));
		pessoa.getEnderecoComercial().setTipoEndereco(new TipoEndereco(Constants.TIPO_ENDERECO_COMERCIAL));
		return dao.getByDtoPedido(pessoa);
	}

	@Override
	@Transactional
	public List<Pessoa> getFornecedoresByDto(Pessoa pessoa) {
		pessoa.setTipoPessoa(new TipoPessoa(Constants.TIPO_PESSOA_FORNECEDOR));
		return dao.getByDto(pessoa);
	}

	@Override
	@Transactional
	public Boolean isFornecedor(Long idUsuario) {
		Pessoa usuario = this.getById(idUsuario);
		return usuario != null && usuario.getTipoPessoa() != null
				&& usuario.getTipoPessoa().getId().equals(Constants.TIPO_PESSOA_FORNECEDOR);
	}

	@Override
	@Transactional
	public Boolean isCliente(Long idUsuario) {
		Pessoa usuario = this.getById(idUsuario);
		return usuario != null && usuario.getTipoPessoa() != null
				&& usuario.getTipoPessoa().getId().equals(Constants.TIPO_PESSOA_CLIENTE);
	}

	@Override
	@Transactional
	public List<Pessoa> getByDto(Pessoa pessoa) {
		return dao.getByDto(pessoa);
	}

	@Override
	@Transactional
	public InputStream toExport(Pessoa pessoa) throws UnsupportedEncodingException {
		String separadorColuna = "\t";
		List<Pessoa> lista = dao.getExport(pessoa);
		StringBuilder retorno = new StringBuilder();
		if (!Utils.isNullOrEmpty(lista)) {

			retorno.append("Código");
			retorno.append(separadorColuna);
			retorno.append("Nome");
			retorno.append(separadorColuna);
			retorno.append("Nome Fantasia");
			retorno.append(separadorColuna);
			retorno.append("Bairro");
			retorno.append(separadorColuna);
			retorno.append("Cidade");
			retorno.append(separadorColuna);
			retorno.append("CNPJ");
			retorno.append(separadorColuna);
			retorno.append("Prazo");
			retorno.append(separadorColuna);
			retorno.append("Forma Pgto");
			retorno.append(separadorColuna);
			retorno.append("Vendedor");
			retorno.append(separadorColuna);
			retorno.append("Situação");
			retorno.append("\n");

			for (Pessoa dto : lista) {
				retorno.append(dto.getId());
				retorno.append(separadorColuna);
				retorno.append(dto.getNome());
				retorno.append(separadorColuna);
				retorno.append(dto.getNomeFantasia());
				retorno.append(separadorColuna);
				retorno.append(dto.getEnderecoComercial().getBairro());
				retorno.append(separadorColuna);
				retorno.append(dto.getEnderecoComercial().getCidade().getNome());
				retorno.append(separadorColuna);
				retorno.append(dto.getCnpj());
				retorno.append(separadorColuna);
				retorno.append(dto.getPrazo());
				retorno.append(separadorColuna);
				retorno.append(dto.getFormaPagamento().getDescricao());
				retorno.append(separadorColuna);
				retorno.append(dto.getNomeRepresentante());
				retorno.append(separadorColuna);
				retorno.append(dto.getFlExcluido().toString());
				retorno.append("\n");
			}
		}
		return new ByteArrayInputStream(retorno.toString().getBytes("UTF-8"));
	}
}