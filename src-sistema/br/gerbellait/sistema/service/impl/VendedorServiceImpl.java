package br.gerbellait.sistema.service.impl;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Transactional;
import br.gerbellait.arquitetura.service.AbstractService;
import br.gerbellait.arquitetura.util.Utils;
import br.gerbellait.sistema.bean.VendedorFilterBean;
import br.gerbellait.sistema.dao.VendedorDao;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.entity.Vendedor;
import br.gerbellait.sistema.service.ComissaoVendedorService;
import br.gerbellait.sistema.service.MetaVendedorProdutoService;
import br.gerbellait.sistema.service.RotaVendedorService;
import br.gerbellait.sistema.service.VendedorService;

@Stateless
public class VendedorServiceImpl extends AbstractService<Vendedor> implements VendedorService {

	private static final Logger LOGGER = Logger.getLogger(VendedorServiceImpl.class);

	@Inject
	private VendedorDao dao;

	@Inject
	private RotaVendedorService rotaVendedorService;

	@Inject
	private ComissaoVendedorService comissaoVendedorService;

	@Inject
	private MetaVendedorProdutoService metaVendedorProdutoService;

	@Override
	protected void validate(Vendedor vendedor) throws ServiceLayerException {
		if (vendedor == null) {
			throw new ServiceLayerException("Objeto UF null.");
		}
	}

	@Override
	@Transactional
	public Vendedor getById(Long id) {
		Vendedor retorno = this.dao.getById(new Vendedor(id));
		if (retorno != null) {
			retorno.setRotas(rotaVendedorService.getBy(retorno));
			retorno.setComissoes(comissaoVendedorService.getBy(retorno));
		}
		return retorno;
	}

	private void doInsert(Vendedor vendedor) throws ServiceLayerException {
		this.dao.insert(vendedor);
		this.metaVendedorProdutoService.doInsertByVendedor(vendedor);

	}

	@Override
	@Transactional
	public Vendedor save(Vendedor vendedor) throws ServiceLayerException {
		this.validate(vendedor);
		vendedor.setDataAlteracao(new Date());
		if (vendedor.getId() != null) {
			this.dao.update(vendedor);
		} else {
			this.doInsert(vendedor);
		}
		rotaVendedorService.doSave(vendedor);
		comissaoVendedorService.doSave(vendedor);
		return vendedor;
	}

	@Override
	@Transactional(readOnly = false)
	public boolean deletar(Vendedor vendedor) {
		try {
			this.dao.deletar(vendedor);
			return true;
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return false;
	}

	@Override
	@Transactional
	public List<Vendedor> getAll() {
		return this.dao.all();
	}

	@Override
	@Transactional
	public List<Vendedor> allBySituacao(ExcluidoEnum flExcluido) {
		return this.dao.allBySituacao(flExcluido);
	}

	@Override
	@Transactional
	public List<Vendedor> getByDto(Vendedor vendedor) {
		return dao.getByDto(vendedor);
	}

	@Override
	@Transactional
	public InputStream toExport(Vendedor vendedor) throws UnsupportedEncodingException {
		String separadorColuna = "\t";
		List<Vendedor> lista = dao.getExport(vendedor);
		StringBuilder retorno = new StringBuilder();
		if (!Utils.isNullOrEmpty(lista)) {

			retorno.append("Código");
			retorno.append(separadorColuna);
			retorno.append("Nome");
			retorno.append(separadorColuna);
			retorno.append("Tipo de Venda");
			retorno.append(separadorColuna);
			retorno.append("Valor Venda Geral");
			retorno.append(separadorColuna);
			retorno.append("Percentual Troca");
			retorno.append(separadorColuna);
			retorno.append("Situação");
			retorno.append("\n");

			for (Vendedor dto : lista) {
				retorno.append(dto.getId());
				retorno.append(separadorColuna);
				retorno.append(dto.getNome());
				retorno.append(separadorColuna);
				retorno.append(dto.getTipo());
				retorno.append(separadorColuna);
				retorno.append(dto.getValorVendaGeral());
				retorno.append(separadorColuna);
				retorno.append(dto.getPercentualTroca());
				retorno.append(separadorColuna);
				retorno.append(dto.getFlExcluido().toString());
				retorno.append("\n");
			}
		}
		return new ByteArrayInputStream(retorno.toString().getBytes("UTF-8"));
	}
    
    @Override
    public List<VendedorFilterBean> allFilterSellers() {
        return this.dao.allFilterSellers();
    }
}
