package br.gerbellait.sistema.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Transactional;
import br.gerbellait.arquitetura.service.AbstractService;
import br.gerbellait.arquitetura.util.Utils;
import br.gerbellait.sistema.dao.PerfilDao;
import br.gerbellait.sistema.entity.Perfil;
import br.gerbellait.sistema.service.PerfilService;

@Stateless
public class PerfilServiceImpl extends AbstractService<Perfil> implements PerfilService {

	@Inject
	private PerfilDao dao;

	@Override
	protected void validate(Perfil perfil) throws ServiceLayerException {
		if (perfil == null) {
			throw new ServiceLayerException("Objeto perfil null.");
		}
		if (Utils.isNullOrEmpty(perfil.getNome())) {
			throw new ServiceLayerException("Nome é obrigatório.");
		}
	}

	@Override
	@Transactional
	public List<Perfil> getAll() {
		return this.dao.getAll();
	}
}