package br.gerbellait.sistema.service.impl;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Transactional;
import br.gerbellait.arquitetura.service.AbstractService;
import br.gerbellait.arquitetura.util.Utils;
import br.gerbellait.sistema.bean.HistoricoEstoqueBean;
import br.gerbellait.sistema.bean.TableState;
import br.gerbellait.sistema.dao.ProdutoDao;
import br.gerbellait.sistema.entity.AjusteEstoque;
import br.gerbellait.sistema.entity.Pedido;
import br.gerbellait.sistema.entity.Pessoa;
import br.gerbellait.sistema.entity.Produto;
import br.gerbellait.sistema.service.GrupoProdutoService;
import br.gerbellait.sistema.service.MetaVendedorProdutoService;
import br.gerbellait.sistema.service.PessoaService;
import br.gerbellait.sistema.service.ProdutoImpostoService;
import br.gerbellait.sistema.service.ProdutoPrecoRotaService;
import br.gerbellait.sistema.service.ProdutoService;
import br.gerbellait.sistema.util.Constants;

@Stateless
public class ProdutoServiceImpl extends AbstractService<Produto> implements ProdutoService {
    
    private static final Logger LOGGER = Logger.getLogger(ProdutoServiceImpl.class);
    
    @Inject
    private ProdutoDao dao;
    
    @Inject
    private GrupoProdutoService grupoProdutoService;
    
    @Inject
    private ProdutoImpostoService produtoImpostoService;
    
    @Inject
    private ProdutoPrecoRotaService produtoPrecoRotaService;
    
    @Inject
    private MetaVendedorProdutoService metaVendedorProdutoService;
    
    @Inject
    private PessoaService pessoaService;
    
    @Override
    protected void validate(Produto produto) throws ServiceLayerException {
        if (produto == null) {
            throw new ServiceLayerException("Objeto UF null.");
        }
        if (Utils.isNullOrEmpty(produto.getImpostos())) {
            throw new ServiceLayerException("Produto: ao menos um imposto deve ser incluído.");
        }
        if (Utils.isNullOrEmpty(produto.getRotas())) {
            throw new ServiceLayerException("Produto: ao menos uma rota deve ser incluída.");
        }
    }
    
    private void doInsert(Produto produto) throws ServiceLayerException {
        produto.setIdCodProduto(this.dao.nextId().toString());
        this.dao.insert(produto);
        this.metaVendedorProdutoService.doInsertByProduto(produto);
    }
    
    @Override
    public void ajusteEstoque(AjusteEstoque ajusteEstoque) {
        Produto produto = this.dao.getById(ajusteEstoque.getProduto());
        if (produto != null) {
            BigDecimal quantidadeEstoque = BigDecimal.ZERO;
            if (ajusteEstoque.getTipoAjusteEstoque().equalsIgnoreCase(Constants.TIPO_ESTOQUE_ENTRADA)) {
                quantidadeEstoque = produto.getQuantidadeEstoqueAtual().add(ajusteEstoque.getQuantidadeAjusteEstoque());
                
            } else if (ajusteEstoque.getTipoAjusteEstoque().equalsIgnoreCase(Constants.TIPO_ESTOQUE_SAIDA)) {
                quantidadeEstoque = produto.getQuantidadeEstoqueAtual()
                        .subtract(ajusteEstoque.getQuantidadeAjusteEstoque());
                
            } else if (ajusteEstoque.getTipoAjusteEstoque().equalsIgnoreCase(Constants.TIPO_ESTOQUE_TRANSFERENCIA)) {
                quantidadeEstoque = produto.getQuantidadeEstoqueAtual().add(ajusteEstoque.getQuantidadeAjusteEstoque());
            }
            produto.setQuantidadeEstoqueAtual(quantidadeEstoque);
        }
        this.dao.update(produto);
    }
    
    @Override
    @Transactional
    public Produto save(Produto produto) throws ServiceLayerException {
        this.validate(produto);
        produto.setNome(Utils.normalize(produto.getNome()).toUpperCase());
        produto.setDataAlteracao(new Date());
        if (produto.getId() != null) {
            this.dao.update(produto);
        } else {
            produto.setIdCodProduto(this.dao.nextId().toString());
            this.doInsert(produto);
        }
        produtoImpostoService.doSave(produto);
        produtoPrecoRotaService.doSave(produto);
        return produto;
    }
    
    @Override
    @Transactional(readOnly = false)
    public boolean deletar(Produto produto) {
        try {
            this.dao.deletar(produto);
            return true;
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return false;
    }
    
    private Produto doGetById(Long id) {
        Produto retorno = dao.getByCodigo(new Produto(id));
        if (retorno != null && retorno.getId() != null) {
            retorno.setGrupoProduto(grupoProdutoService.getById(retorno.getGrupoProduto()));
            if (retorno.getFornecedor() != null && retorno.getFornecedor().getId() != null) {
                retorno.setFornecedor(pessoaService.getById(retorno.getFornecedor().getId()));
            }
        }
        return retorno;
    }
    
    @Override
    @Transactional
    public Produto getById(Long id) {
        Produto retorno = this.doGetById(id);
        if (retorno != null) {
            retorno.setImpostos(produtoImpostoService.getBy(retorno));
            retorno.setRotas(produtoPrecoRotaService.getBy(retorno));
        }
        return retorno;
    }
    
    @Override
    @Transactional
    public List<Produto> getByDesc(Long clienteId, String text) {
        try {
            return dao.getByDesc(new Pessoa(clienteId), text);
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return Collections.emptyList();
    }
    
    @Override
    public List<Produto> getByPedido(Pedido pedido) {
        try {
            return dao.getByPedido(pedido);
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return Collections.emptyList();
    }
    
    @Override
    @Transactional
    public List<Produto> getAll() {
        return this.dao.all();
    }
    
    @Override
    @Transactional
    public List<Produto> getByDto(Produto produto) {
        return dao.getByDto(produto);
    }
    
    @Override
    @Transactional
    public InputStream relatorioEstoqueExport(TableState tableState) throws UnsupportedEncodingException, SQLException {
        String separator = "\t";
        List<HistoricoEstoqueBean> result = dao.getHistoricoEstoque(tableState);
        StringBuilder retorno = new StringBuilder();
        if (!Utils.isNullOrEmpty(result)) {
            
            retorno.append("Cód. Produto");
            retorno.append(separator);
            retorno.append("Descrição");
            retorno.append(separator);
            retorno.append("Unidade");
            retorno.append(separator);
            retorno.append("Estoque Atual");
            retorno.append(separator);
            retorno.append("1 dia");
            retorno.append(separator);
            retorno.append("2 dia");
            retorno.append(separator);
            retorno.append("3 dia");
            retorno.append(separator);
            retorno.append("4 dia");
            retorno.append(separator);
            retorno.append("5 dia");
            retorno.append(separator);
            retorno.append("Total");
            retorno.append("\n");
            
            for (HistoricoEstoqueBean dto : result) {
                retorno.append(dto.getProductId());
                retorno.append(separator);
                retorno.append(dto.getProductDesc());
                retorno.append(separator);
                retorno.append(dto.getUnit());
                retorno.append(separator);
                retorno.append(dto.getCurrentStock());
                retorno.append(separator);
                retorno.append(dto.getDay1());
                retorno.append(separator);
                retorno.append(dto.getDay2());
                retorno.append(separator);
                retorno.append(dto.getDay3());
                retorno.append(separator);
                retorno.append(dto.getDay4());
                retorno.append(separator);
                retorno.append(dto.getDay5());
                retorno.append(separator);
                retorno.append(dto.getTotal());
                retorno.append("\n");
            }
        }
        return new ByteArrayInputStream(retorno.toString().getBytes("UTF-8"));
    }
    
    @Override
    @Transactional
    public InputStream toExport(Produto produto) throws UnsupportedEncodingException {
        String separadorColuna = "\t";
        List<Produto> lista = dao.getExport(produto);
        StringBuilder retorno = new StringBuilder();
        if (!Utils.isNullOrEmpty(lista)) {
            
            retorno.append("Código");
            retorno.append(separadorColuna);
            retorno.append("Descrição");
            retorno.append(separadorColuna);
            retorno.append("Unidade");
            retorno.append(separadorColuna);
            retorno.append("Grupo");
            retorno.append(separadorColuna);
            retorno.append("CF");
            retorno.append(separadorColuna);
            retorno.append("ST");
            retorno.append(separadorColuna);
            retorno.append("Situação");
            retorno.append("\n");
            
            for (Produto dto : lista) {
                retorno.append(dto.getId());
                retorno.append(separadorColuna);
                retorno.append(dto.getNome());
                retorno.append(separadorColuna);
                retorno.append(dto.getTipoUnidade());
                retorno.append(separadorColuna);
                retorno.append(dto.getGrupoProduto().getDescricao());
                retorno.append(separadorColuna);
                retorno.append(dto.getCf());
                retorno.append(separadorColuna);
                retorno.append(dto.getSt());
                retorno.append(separadorColuna);
                retorno.append(dto.getFlExcluido().toString());
                retorno.append("\n");
            }
        }
        return new ByteArrayInputStream(retorno.toString().getBytes("UTF-8"));
    }
    
    @Override
    public boolean updateQtEstoque(Produto produto) {
        try {
            return dao.updateQtEstoque(produto);
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return false;
    }
    
    @Override
    @Transactional
    public List<HistoricoEstoqueBean> getHistoricoEstoque(TableState tableState) {
        try {
            return dao.getHistoricoEstoque(tableState);
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return Collections.emptyList();
    }
}
