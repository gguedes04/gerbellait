package br.gerbellait.sistema.service.impl;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Transactional;
import br.gerbellait.arquitetura.service.AbstractService;
import br.gerbellait.sistema.dao.MsnDao;
import br.gerbellait.sistema.entity.Msn;
import br.gerbellait.sistema.service.MsnService;

@Stateless
public class MsnServiceImpl extends AbstractService<Msn> implements MsnService {

	private static final Logger LOGGER = Logger.getLogger(MsnServiceImpl.class);

	@Inject
	private MsnDao dao;

	@Override
	protected void validate(Msn msn) throws ServiceLayerException {
		if (msn == null) {
			throw new ServiceLayerException("Objeto perfil null.");
		}
	}

	@Override
	@Transactional
	public Msn save(Msn msn) throws ServiceLayerException {
		this.validate(msn);
		msn.setDataAlteracao(new Date());
		if (msn.getId() != null) {
			this.dao.update(msn);
		} else {
			this.dao.insert(msn);
		}
		return msn;
	}

	@Override
	@Transactional
	public Msn getById(Long id) {
		return this.dao.getById(new Msn(id));
	}

	@Override
	@Transactional(readOnly = false)
	public boolean deletar(Msn msn) {
		try {
			this.dao.deletar(msn);
			return true;
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return false;
	}

	@Override
	@Transactional
	public List<Msn> getAll() {
		return this.dao.all();
	}

	@Override
	@Transactional
	public List<Msn> getByDto(Msn msn) {
		return dao.getByDto(msn);
	}
}