package br.gerbellait.sistema.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Transactional;
import br.gerbellait.arquitetura.service.AbstractService;
import br.gerbellait.arquitetura.util.Utils;
import br.gerbellait.sistema.dao.MetaVendedorProdutoDao;
import br.gerbellait.sistema.dao.ProdutoDao;
import br.gerbellait.sistema.dao.VendedorDao;
import br.gerbellait.sistema.entity.MetaVendedorProduto;
import br.gerbellait.sistema.entity.PeriodoMeta;
import br.gerbellait.sistema.entity.Produto;
import br.gerbellait.sistema.entity.Vendedor;
import br.gerbellait.sistema.service.MetaVendedorProdutoService;
import br.gerbellait.sistema.service.PeriodoMetaService;
import br.gerbellait.sistema.util.Constants;
import br.gerbellait.sistema.util.DateUtil;

@Stateless
public class MetaVendedorProdutoServiceImpl extends AbstractService<MetaVendedorProduto>
		implements MetaVendedorProdutoService {

	@Inject
	private MetaVendedorProdutoDao dao;

	@Inject
	private ProdutoDao produtoDao;

	@Inject
	private VendedorDao vendedorDao;

	@Inject
	private PeriodoMetaService periodoMetaService;

	@Override
	protected void validate(MetaVendedorProduto metaVendedorProduto) throws ServiceLayerException {
		if (metaVendedorProduto == null) {
			throw new ServiceLayerException("Objeto MetaVendedorProduto null.");
		}
	}

	@Override
	public MetaVendedorProduto getMetaVendedorProduto(List<PeriodoMeta> periodos, Vendedor vendedor, Produto produto) {
		MetaVendedorProduto retorno = new MetaVendedorProduto();
		retorno.setVendedor(new Vendedor(vendedor.getId()));
		retorno.setProduto(new Produto(produto.getId()));
		retorno.setQuantidadeVenda(BigDecimal.ZERO);
		retorno.setValorPrecoMedioVenda(BigDecimal.ZERO);
		retorno.setQuantidadeLimiteVenda(BigDecimal.ZERO);
		retorno.setPercentualDesconto(BigDecimal.ZERO);
		retorno.setQuantidadeMinima(BigDecimal.ZERO);
		retorno.setQuantidadeLimiteVendaInicial(BigDecimal.ZERO);
		retorno.setDataInicial(periodos.get(0).getDataInicialMeta());
		retorno.setDataFinal(periodos.get(0).getDataFinalMeta());
		return retorno;
	}

	@Override
	public void doInsertByProduto(Produto produto) throws ServiceLayerException {
		List<Vendedor> vendedores = vendedorDao.all();
		if (!Utils.isNullOrEmpty(vendedores)) {
			List<PeriodoMeta> periodos = periodoMetaService.all();
			for (Vendedor vendedor : vendedores) {
				this.doSave(this.getMetaVendedorProduto(periodos, vendedor, produto));
			}
		}
	}

	@Override
	public void doInsertByVendedor(Vendedor vendedor) throws ServiceLayerException {
		List<Produto> produtos = produtoDao.all();
		if (!Utils.isNullOrEmpty(produtos)) {
			List<PeriodoMeta> periodos = periodoMetaService.all();
			for (Produto produto : produtos) {
				this.doSave(this.getMetaVendedorProduto(periodos, vendedor, produto));
			}
		}
	}

	@Override
	@Transactional
	public void saveList(List<MetaVendedorProduto> limites) throws ServiceLayerException {
		if (!Utils.isNullOrEmpty(limites)) {
			for (MetaVendedorProduto limite : limites) {
				this.doSave(limite);
			}
		}
	}

	@Override
	public MetaVendedorProduto doSave(MetaVendedorProduto metaVendedorProduto) throws ServiceLayerException {
		this.validate(metaVendedorProduto);
		metaVendedorProduto.setDataAlteracao(new Date());
		if (metaVendedorProduto.getId() != null) {
			this.dao.update(metaVendedorProduto);
		} else {
			this.dao.insert(metaVendedorProduto);
		}
		return metaVendedorProduto;
	}

	@Override
	@Transactional
	public MetaVendedorProduto save(MetaVendedorProduto metaVendedorProduto) throws ServiceLayerException {
		return this.doSave(metaVendedorProduto);
	}

	@Override
	@Transactional
	public List<MetaVendedorProduto> all() {
		return this.dao.all();
	}

	private List<Produto> getProdutos(MetaVendedorProduto filtro, List<Vendedor> vendedores) {
		Produto produto = new Produto();
		produto.setNome(filtro.getNome());
		List<Produto> retorno = produtoDao.getByDto(produto);
		for (Produto prod : retorno) {
			prod.setPeriodoDescricao(DateUtil.dateToFormatDefault(filtro.getDataInicial()) + " à "
					+ DateUtil.dateToFormatDefault(filtro.getDataFinal()));
			for (Vendedor vendedor : vendedores) {
				MetaVendedorProduto metaVendedorProduto = new MetaVendedorProduto();
				metaVendedorProduto.setProduto(new Produto(prod.getId(), prod.getNome()));
				metaVendedorProduto.getProduto().setIdCodProduto(prod.getIdCodProduto());
				metaVendedorProduto.setVendedor(new Vendedor(vendedor.getId(), vendedor.getNome()));
				metaVendedorProduto.setDataInicial(filtro.getDataInicial());
				metaVendedorProduto.setDataFinal(filtro.getDataFinal());
				MetaVendedorProduto metaVendedorProdutoBanco = dao.getBy(metaVendedorProduto);
				if (metaVendedorProdutoBanco != null && metaVendedorProdutoBanco.getId() != null) {
					metaVendedorProduto.setProduto(new Produto(prod.getId(), prod.getNome()));
					metaVendedorProduto.getProduto().setIdCodProduto(prod.getIdCodProduto());
					metaVendedorProduto.setVendedor(new Vendedor(vendedor.getId(), vendedor.getNome()));
					prod.getMetasVendedores().add(metaVendedorProdutoBanco);
				} else {
					prod.getMetasVendedores().add(metaVendedorProduto);
				}
			}
		}
		return retorno;
	}

	@Override
	@Transactional
	public List<Produto> getLimiteVendasResource(MetaVendedorProduto filtro, List<Vendedor> vendedores) {
		filtro.getProduto().getIdGrupoProdutoList().add(Constants.GRUPO_PRODUTO_MATERIA_PRIMA);
		filtro.getProduto().getIdGrupoProdutoList().add(Constants.GRUPO_PRODUTO_TERCEIROS);
		return this.getProdutos(filtro, vendedores);
	}

	@Override
	@Transactional
	public List<Produto> getQuantidadeVendaResource(MetaVendedorProduto filtro, List<Vendedor> vendedores) {
		filtro.getProduto().getIdGrupoProdutoList().add(Constants.GRUPO_PRODUTO_MATERIA_PRIMA);
		filtro.getProduto().getIdGrupoProdutoList().add(Constants.GRUPO_PRODUTO_TERCEIROS);
		return this.getProdutos(filtro, vendedores);
	}

	@Override
	@Transactional
	public List<Produto> getPrecoMedioVendaResource(MetaVendedorProduto filtro, List<Vendedor> vendedores) {
		// filtro.getProduto().getIdGrupoProdutoList().add(Constants.GRUPO_PRODUTO_MATERIA_PRIMA);
		// filtro.getProduto().getIdGrupoProdutoList().add(Constants.GRUPO_PRODUTO_TERCEIROS);
		return this.getProdutos(filtro, vendedores);
	}

	@Override
	@Transactional
	public List<Produto> getPercDescontoQtdeMinimaResource(MetaVendedorProduto filtro, List<Vendedor> vendedores) {
		filtro.getProduto().getIdGrupoProdutoList().add(Constants.GRUPO_PRODUTO_MATERIA_PRIMA);
		filtro.getProduto().getIdGrupoProdutoList().add(Constants.GRUPO_PRODUTO_TERCEIROS);
		return this.getProdutos(filtro, vendedores);
	}
}