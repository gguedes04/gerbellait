package br.gerbellait.sistema.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Transactional;
import br.gerbellait.arquitetura.service.AbstractService;
import br.gerbellait.arquitetura.util.Utils;
import br.gerbellait.sistema.dao.TransportadorDao;
import br.gerbellait.sistema.entity.Transportador;
import br.gerbellait.sistema.service.CidadeService;
import br.gerbellait.sistema.service.TransportadorService;
import br.gerbellait.sistema.service.UfService;

@Stateless
public class TransportadorServiceImpl extends AbstractService<Transportador> implements TransportadorService {

	private static final Logger LOGGER = Logger.getLogger(TransportadorServiceImpl.class);

	@Inject
	private CidadeService cidadeService;

	@Inject
	private UfService ufService;

	@Inject
	private TransportadorDao dao;

	@Override
	protected void validate(Transportador transportador) throws ServiceLayerException {
		if (transportador == null) {
			throw new ServiceLayerException("Objeto transportador null.");

		} else if (transportador.getCidade() == null) {
			throw new ServiceLayerException("Transportador.cidade null.");
		}
	}

	@Override
	@Transactional(readOnly = false)
	public Transportador save(Transportador transportador) throws ServiceLayerException {
		this.validate(transportador);
		transportador.setCnpjcpf(Utils.onlyNumbers(transportador.getCnpjcpf()));
		transportador.setCep(Utils.onlyNumbers(transportador.getCep()));
		if (transportador.getId() != null) {
			this.dao.update(transportador);
		} else {
			this.dao.insert(transportador);
		}
		return transportador;
	}

	@Override
	@Transactional(readOnly = false)
	public boolean deletar(Transportador transportador) {
		try {
			this.dao.delete(transportador);
			return true;
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return false;
	}

	@Override
	@Transactional
	public List<Transportador> getAll() {
		return this.dao.getAll();
	}

	@Override
	@Transactional
	public Transportador getById(Transportador transportador) {
		Transportador retorno = this.dao.getById(transportador);
		if (retorno != null) {
			retorno.setCidade(cidadeService.getById(retorno.getCidade()));
			retorno.setUf(ufService.getById(retorno.getUf()));
		}
		return retorno;
	}

	@Override
	@Transactional
	public List<Transportador> getByDto(Transportador transportador) {
		transportador.setCnpjcpf(Utils.onlyNumbers(transportador.getCnpjcpf()));
		transportador.setCep(Utils.onlyNumbers(transportador.getCep()));
		return this.dao.getByDto(transportador);
	}

}