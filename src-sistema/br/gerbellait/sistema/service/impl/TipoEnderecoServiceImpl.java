package br.gerbellait.sistema.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Transactional;
import br.gerbellait.arquitetura.service.AbstractService;
import br.gerbellait.arquitetura.util.Utils;
import br.gerbellait.sistema.dao.TipoEnderecoDao;
import br.gerbellait.sistema.entity.TipoEndereco;
import br.gerbellait.sistema.service.TipoEnderecoService;

@Stateless
public class TipoEnderecoServiceImpl extends AbstractService<TipoEndereco> implements TipoEnderecoService {
    
    @Inject
    private TipoEnderecoDao dao;
    
    @Override
    protected void validate(TipoEndereco tipoEndereco) throws ServiceLayerException {
        if (tipoEndereco == null) {
            throw new ServiceLayerException("Objeto Tipo Endereco null.");
        }
        if (Utils.isNullOrEmpty(tipoEndereco.getNome())) {
            throw new ServiceLayerException("Nome é obrigatório.");
        }
    }
    
    @Override
    @Transactional
    public TipoEndereco getById(TipoEndereco tipoEndereco) {
        return this.dao.getById(tipoEndereco);
    }
    
    @Override
    @Transactional
    public List<TipoEndereco> getAll() {
        return this.dao.all();
    }
}
