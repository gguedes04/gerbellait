package br.gerbellait.sistema.service;

import java.util.List;

import br.gerbellait.sistema.entity.RotaVendedor;
import br.gerbellait.sistema.entity.Vendedor;

public interface RotaVendedorService {

	public List<RotaVendedor> getBy(Vendedor vendedor);

	boolean deletar(RotaVendedor rotaVendedor);

	void doSave(Vendedor vendedor);
}