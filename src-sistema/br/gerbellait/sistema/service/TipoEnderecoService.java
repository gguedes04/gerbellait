package br.gerbellait.sistema.service;

import java.util.List;

import br.gerbellait.sistema.entity.TipoEndereco;

public interface TipoEnderecoService {

	List<TipoEndereco> getAll();

	TipoEndereco getById(TipoEndereco tipoEndereco);
}