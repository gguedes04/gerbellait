package br.gerbellait.sistema.service;

import java.util.List;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.sistema.entity.PeriodoMeta;

public interface PeriodoMetaService {

	List<PeriodoMeta> all();

	PeriodoMeta save(PeriodoMeta periodoMeta) throws ServiceLayerException;

	PeriodoMeta getPeriodoAtual();
}