package br.gerbellait.sistema.service;

import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.sistema.entity.Usuario;
import br.gerbellait.sistema.entity.UsuarioToken;

public interface AutenticaService {

	UsuarioToken autenticar(Usuario usuario) throws ServiceLayerException;

}