package br.gerbellait.sistema.service;

import java.util.List;

import br.gerbellait.sistema.entity.Uf;

public interface UfService {

	List<Uf> getAll();

	Uf getById(Uf uf);
}