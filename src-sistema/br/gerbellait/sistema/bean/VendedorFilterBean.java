package br.gerbellait.sistema.bean;

import java.util.Objects;

public class VendedorFilterBean {
    private Long id;
    private String nome;
    
    public VendedorFilterBean(Long id, String nome) {
        this.id = id;
        this.nome = nome;
    }
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getNome() {
        return nome;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    @java.lang.Override
    public java.lang.String toString() {
        return "VendedorFilterBean{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                '}';
    }
    
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        if (!super.equals(object)) return false;
        VendedorFilterBean that = (VendedorFilterBean) object;
        return java.util.Objects.equals(id, that.id) &&
                java.util.Objects.equals(nome, that.nome);
    }
    
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, nome);
    }
}
