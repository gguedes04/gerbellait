package br.gerbellait.sistema.bean;

import java.io.Serializable;
import java.util.Objects;

import br.gerbellait.sistema.entity.Pessoa;
import br.gerbellait.sistema.entity.Produto;
import br.gerbellait.sistema.entity.Rota;
import br.gerbellait.sistema.entity.Vendedor;
import br.gerbellait.sistema.entity.Pedido;

public class PedidosFilterBean implements Serializable {
    
    private static final long serialVersionUID = -5508632970918704684L;
    private Produto produto;
    private Pessoa cliente;
    private Pessoa fornecedor;
    private Vendedor vendedor;
    private Pedido pedido;
    private String vendaInicial;
    private String vendaFinal;
    private Rota rota;
    private String entregaInicial;
    private String entregaFinal;
    private Long usuarioLogado;
    
    public Produto getProduto() {
        return produto;
    }
    
    public void setProduto(Produto produto) {
        this.produto = produto;
    }
    
    public Pessoa getCliente() {
        return cliente;
    }
    
    public void setCliente(Pessoa cliente) {
        this.cliente = cliente;
    }
    
    public Pessoa getFornecedor() {
        return fornecedor;
    }
    
    public void setFornecedor(Pessoa fornecedor) {
        this.fornecedor = fornecedor;
    }
    
    public Vendedor getVendedor() {
        return vendedor;
    }
    
    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public String getVendaInicial() {
        return vendaInicial;
    }
    
    public void setVendaInicial(String vendaInicial) {
        this.vendaInicial = vendaInicial;
    }
    
    public String getVendaFinal() {
        return vendaFinal;
    }
    
    public void setVendaFinal(String vendaFinal) {
        this.vendaFinal = vendaFinal;
    }
    
    public Rota getRota() {
        return rota;
    }
    
    public void setRota(Rota rota) {
        this.rota = rota;
    }
    
    public String getEntregaInicial() {
        return entregaInicial;
    }
    
    public void setEntregaInicial(String entregaInicial) {
        this.entregaInicial = entregaInicial;
    }
    
    public String getEntregaFinal() {
        return entregaFinal;
    }
    
    public void setEntregaFinal(String entregaFinal) {
        this.entregaFinal = entregaFinal;
    }
    
    public Long getUsuarioLogado() {
        return usuarioLogado;
    }
    
    public void setUsuarioLogado(Long usuarioLogado) {
        this.usuarioLogado = usuarioLogado;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PedidosFilterBean that = (PedidosFilterBean) o;
        return Objects.equals(produto, that.produto) &&
                Objects.equals(cliente, that.cliente) &&
                Objects.equals(fornecedor, that.fornecedor) &&
                Objects.equals(vendedor, that.vendedor) &&
                Objects.equals(pedido, that.pedido) &&
                Objects.equals(vendaInicial, that.vendaInicial) &&
                Objects.equals(vendaFinal, that.vendaFinal) &&
                Objects.equals(rota, that.rota) &&
                Objects.equals(entregaInicial, that.entregaInicial) &&
                Objects.equals(entregaFinal, that.entregaFinal) &&
                Objects.equals(usuarioLogado, that.usuarioLogado);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(produto, cliente, fornecedor, vendedor, pedido, vendaInicial, vendaFinal, rota, entregaInicial, entregaFinal, usuarioLogado);
    }
    
    @Override
    public String
    
    toString() {
        return "PedidosFilterBean{" +
                "produto=" + produto +
                ", cliente=" + cliente +
                ", fornecedor=" + fornecedor +
                ", vendedor=" + vendedor +
                ", pedido=" + pedido +
                ", vendaInicial='" + vendaInicial + '\'' +
                ", vendaFinal='" + vendaFinal + '\'' +
                ", rota=" + rota +
                ", entregaInicial='" + entregaInicial + '\'' +
                ", entregaFinal='" + entregaFinal + '\'' +
                ", usuarioLogado=" + usuarioLogado +
                '}';
    }
    
}
