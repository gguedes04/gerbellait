package br.gerbellait.sistema.bean;

import java.io.Serializable;
import java.util.Objects;

public class HistoricoEstoqueBean implements Serializable {
    
    private static final long serialVersionUID = -4661950721749120448L;
    private Long productId;
    private String productCodeCli;
    private String productDesc;
    private String unit;
    private int currentStock;
    private Double day1;
    private Double day2;
    private Double day3;
    private Double day4;
    private Double day5;
    private Double total;
    
    public Long getProductId() {
        return productId;
    }
    
    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductCli() {
        return productCodeCli;
    }

    public void setProductCli(String productCodeCli) {
        this.productCodeCli = productCodeCli;
    }

    public String getProductDesc() {
        return productDesc;
    }
    
    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }
    
    public String getUnit() {
        return unit;
    }
    
    public void setUnit(String unit) {
        this.unit = unit;
    }
    
    public int getCurrentStock() {
        return currentStock;
    }
    
    public void setCurrentStock(int currentStock) {
        this.currentStock = currentStock;
    }
    
    public Double getDay1() {
        return day1;
    }
    
    public void setDay1(Double day1) {
        this.day1 = day1;
    }
    
    public Double getDay2() {
        return day2;
    }
    
    public void setDay2(Double day2) {
        this.day2 = day2;
    }
    
    public Double getDay3() {
        return day3;
    }
    
    public void setDay3(Double day3) {
        this.day3 = day3;
    }
    
    public Double getDay4() {
        return day4;
    }
    
    public void setDay4(Double day4) {
        this.day4 = day4;
    }
    
    public Double getDay5() {
        return day5;
    }
    
    public void setDay5(Double day5) {
        this.day5 = day5;
    }
    
    public Double getTotal() {
        return total;
    }
    
    public void setTotal(Double total) {
        this.total = total;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HistoricoEstoqueBean that = (HistoricoEstoqueBean) o;
        return currentStock == that.currentStock &&
                Objects.equals(productId, that.productId) &&
                Objects.equals(productCodeCli, that.productCodeCli) &&
                Objects.equals(productDesc, that.productDesc) &&
                Objects.equals(unit, that.unit) &&
                Objects.equals(day1, that.day1) &&
                Objects.equals(day2, that.day2) &&
                Objects.equals(day3, that.day3) &&
                Objects.equals(day4, that.day4) &&
                Objects.equals(day5, that.day5) &&
                Objects.equals(total, that.total);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(productId, productCodeCli, productDesc, unit, currentStock, day1, day2, day3, day4, day5, total);
    }
    
    @Override
    public String toString() {
        return "HistoricoEstoqueBean{" +
                "productId=" + productId +
                ", productCodeCli='" + productCodeCli +
                ", productDesc='" + productDesc + '\'' +
                ", unit='" + unit + '\'' +
                ", currentStock=" + currentStock +
                ", day1=" + day1 +
                ", day2=" + day2 +
                ", day3=" + day3 +
                ", day4=" + day4 +
                ", day5=" + day5 +
                ", total=" + total +
                '}';
    }
}
