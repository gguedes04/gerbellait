package br.gerbellait.sistema.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class UltimasVendasBean implements Serializable {
    
    private static final long serialVersionUID = 7980986922059998325L;
    private long idPedido;
    private long idVendedor;
    private long idClien;
    private long idPedidc;
    private Date dtPedid;
    private Date dtEntre;
    private Double vlDesco;
    private long idForma;
    private String flgGera;
    private String flExclu;
    private String observa;
    private String flgSta;
    private Date dtAlte;
    private Double vlPedi;
    private Date dtVenc;
    private long idVend;
    private long idProduto;
    private long idCodProduto;
    private String nmProduto;
    private String tpUnidade;
    private Double qtdeProduto;
    private Double vlUnitario;
    private Double vlTotal;
    private Double qtdeTroca;
    private Double vlTotalTroca;
    private Double vlDesconto;
    private Date dtAlteracao;
    private boolean checked;
    
    public long getIdPedido() {
        return idPedido;
    }
    
    public void setIdPedido(long idPedido) {
        this.idPedido = idPedido;
    }
    
    public long getIdVendedor() {
        return idVendedor;
    }
    
    public void setIdVendedor(long idVendedor) {
        this.idVendedor = idVendedor;
    }
    
    public long getIdClien() {
        return idClien;
    }
    
    public void setIdClien(long idClien) {
        this.idClien = idClien;
    }
    
    public long getIdPedidc() {
        return idPedidc;
    }
    
    public void setIdPedidc(long idPedidc) {
        this.idPedidc = idPedidc;
    }
    
    public Date getDtPedid() {
        return dtPedid;
    }
    
    public void setDtPedid(Date dtPedid) {
        this.dtPedid = dtPedid;
    }
    
    public Date getDtEntre() {
        return dtEntre;
    }
    
    public void setDtEntre(Date dtEntre) {
        this.dtEntre = dtEntre;
    }
    
    public Double getVlDesco() {
        return vlDesco;
    }
    
    public void setVlDesco(Double vlDesco) {
        this.vlDesco = vlDesco;
    }
    
    public long getIdForma() {
        return idForma;
    }
    
    public void setIdForma(long idForma) {
        this.idForma = idForma;
    }
    
    public String getFlgGera() {
        return flgGera;
    }
    
    public void setFlgGera(String flgGera) {
        this.flgGera = flgGera;
    }
    
    public String getFlExclu() {
        return flExclu;
    }
    
    public void setFlExclu(String flExclu) {
        this.flExclu = flExclu;
    }
    
    public String getObserva() {
        return observa;
    }
    
    public void setObserva(String observa) {
        this.observa = observa;
    }
    
    public String getFlgSta() {
        return flgSta;
    }
    
    public void setFlgSta(String flgSta) {
        this.flgSta = flgSta;
    }
    
    public Date getDtAlte() {
        return dtAlte;
    }
    
    public void setDtAlte(Date dtAlte) {
        this.dtAlte = dtAlte;
    }
    
    public Double getVlPedi() {
        return vlPedi;
    }
    
    public void setVlPedi(Double vlPedi) {
        this.vlPedi = vlPedi;
    }
    
    public Date getDtVenc() {
        return dtVenc;
    }
    
    public void setDtVenc(Date dtVenc) {
        this.dtVenc = dtVenc;
    }
    
    public long getIdVend() {
        return idVend;
    }
    
    public void setIdVend(long idVend) {
        this.idVend = idVend;
    }
    
    public long getIdProduto() {
        return idProduto;
    }
    
    public void setIdProduto(long idProduto) {
        this.idProduto = idProduto;
    }
    
    public long getIdCodProduto() {
        return idCodProduto;
    }
    
    public void setIdCodProduto(long idCodProduto) {
        this.idCodProduto = idCodProduto;
    }
    
    public String getNmProduto() {
        return nmProduto;
    }
    
    public void setNmProduto(String nmProduto) {
        this.nmProduto = nmProduto;
    }
    
    public String getTpUnidade() {
        return tpUnidade;
    }
    
    public void setTpUnidade(String tpUnidade) {
        this.tpUnidade = tpUnidade;
    }
    
    public Double getQtdeProduto() {
        return qtdeProduto;
    }
    
    public void setQtdeProduto(Double qtdeProduto) {
        this.qtdeProduto = qtdeProduto;
    }
    
    public Double getVlUnitario() {
        return vlUnitario;
    }
    
    public void setVlUnitario(Double vlUnitario) {
        this.vlUnitario = vlUnitario;
    }
    
    public Double getVlTotal() {
        return vlTotal;
    }
    
    public void setVlTotal(Double vlTotal) {
        this.vlTotal = vlTotal;
    }
    
    public Double getQtdeTroca() {
        return qtdeTroca;
    }
    
    public void setQtdeTroca(Double qtdeTroca) {
        this.qtdeTroca = qtdeTroca;
    }
    
    public Double getVlTotalTroca() {
        return vlTotalTroca;
    }
    
    public void setVlTotalTroca(Double vlTotalTroca) {
        this.vlTotalTroca = vlTotalTroca;
    }
    
    public Double getVlDesconto() {
        return vlDesconto;
    }
    
    public void setVlDesconto(Double vlDesconto) {
        this.vlDesconto = vlDesconto;
    }
    
    public Date getDtAlteracao() {
        return dtAlteracao;
    }
    
    public void setDtAlteracao(Date dtAlteracao) {
        this.dtAlteracao = dtAlteracao;
    }
    
    public boolean isChecked() {
        return checked;
    }
    
    public void setChecked(boolean checked) {
        this.checked = checked;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UltimasVendasBean that = (UltimasVendasBean) o;
        return idPedido == that.idPedido &&
                idVendedor == that.idVendedor &&
                idClien == that.idClien &&
                idPedidc == that.idPedidc &&
                idForma == that.idForma &&
                idVend == that.idVend &&
                idProduto == that.idProduto &&
                idCodProduto == that.idCodProduto &&
                Objects.equals(dtPedid, that.dtPedid) &&
                Objects.equals(dtEntre, that.dtEntre) &&
                Objects.equals(vlDesco, that.vlDesco) &&
                Objects.equals(flgGera, that.flgGera) &&
                Objects.equals(flExclu, that.flExclu) &&
                Objects.equals(observa, that.observa) &&
                Objects.equals(flgSta, that.flgSta) &&
                Objects.equals(dtAlte, that.dtAlte) &&
                Objects.equals(vlPedi, that.vlPedi) &&
                Objects.equals(dtVenc, that.dtVenc) &&
                Objects.equals(nmProduto, that.nmProduto) &&
                Objects.equals(tpUnidade, that.tpUnidade) &&
                Objects.equals(qtdeProduto, that.qtdeProduto) &&
                Objects.equals(vlUnitario, that.vlUnitario) &&
                Objects.equals(vlTotal, that.vlTotal) &&
                Objects.equals(qtdeTroca, that.qtdeTroca) &&
                Objects.equals(vlTotalTroca, that.vlTotalTroca) &&
                Objects.equals(vlDesconto, that.vlDesconto) &&
                Objects.equals(dtAlteracao, that.dtAlteracao);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(idPedido, idVendedor, idClien, idPedidc, dtPedid, dtEntre, vlDesco, idForma, flgGera, flExclu, observa, flgSta, dtAlte, vlPedi, dtVenc, idVend, idProduto, idCodProduto, nmProduto, tpUnidade, qtdeProduto, vlUnitario, vlTotal, qtdeTroca, vlTotalTroca, vlDesconto, dtAlteracao);
    }
}
