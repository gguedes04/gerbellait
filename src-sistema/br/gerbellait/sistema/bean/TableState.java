package br.gerbellait.sistema.bean;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;

public class TableState implements Serializable {
    
    private static final long serialVersionUID = -9161235014216663851L;
    private Pagination pagination;
    private Sort sort;
    private Search search;
    
    public Pagination getPagination() {
        return pagination;
    }
    
    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }
    
    public Sort getSort() {
        return sort;
    }
    
    public void setSort(Sort sort) {
        this.sort = sort;
    }
    
    public Search getSearch() {
        return search;
    }
    
    public void setSearch(Search search) {
        this.search = search;
    }
    
    public class Pagination implements Serializable {
        
        private static final long serialVersionUID = 4087398335406894027L;
        private int start = 0;
        private int number;
        private int numberOfPages;
        
        public int getStart() {
            return start;
        }
        
        public void setStart(int start) {
            this.start = start;
        }
        
        public int getNumber() {
            return number;
        }
        
        public void setNumber(int number) {
            this.number = number;
        }
        
        public int getNumberOfPages() {
            return numberOfPages;
        }
        
        public void setNumberOfPages(int totalRecords) {
            try {
                this.numberOfPages = (int) Math.ceil(totalRecords / (float) this.getNumber());
            } catch (Exception e) {
                this.numberOfPages = 0;
            }
        }
        
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Pagination that = (Pagination) o;
            return start == that.start &&
                    number == that.number &&
                    numberOfPages == that.numberOfPages;
        }
        
        @Override
        public int hashCode() {
            return Objects.hash(start, number, numberOfPages);
        }
        
        @Override
        public String toString() {
            return "Pagination{" +
                    "start=" + start +
                    ", number=" + number +
                    ", numberOfPages=" + numberOfPages +
                    '}';
        }
    }
    
    public class Search implements Serializable {
        
        private static final long serialVersionUID = 4163492946003855670L;
        private Map<String, Object> predicateObject;
        
        public Map<String, Object> getPredicateObject() {
            return predicateObject;
        }
        
        public void setPredicateObject(Map<String, Object> predicateObject) {
            this.predicateObject = predicateObject;
        }
        
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Search search2 = (Search) o;
            return Objects.equals(predicateObject, search2.predicateObject);
        }
        
        @Override
        public int hashCode() {
            return Objects.hash(predicateObject);
        }
        
        @Override
        public String toString() {
            return "Search{" +
                    "predicateObject=" + predicateObject +
                    '}';
        }
    }
    
    public class Sort implements Serializable {
        
        private static final long serialVersionUID = -2584911409546139585L;
        private String predicate;
        private boolean reverse;
        
        public String getPredicate() {
            return predicate;
        }
        
        public void setPredicate(String predicate) {
            this.predicate = predicate;
        }
        
        public boolean isReverse() {
            return reverse;
        }
        
        public void setReverse(boolean reverse) {
            this.reverse = reverse;
        }
        
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Sort sort = (Sort) o;
            return reverse == sort.reverse &&
                    Objects.equals(predicate, sort.predicate);
        }
        
        @Override
        public int hashCode() {
            return Objects.hash(predicate, reverse);
        }
        
        @Override
        public String toString() {
            return "Sort{" +
                    "predicate=" + predicate +
                    ", reverse=" + reverse +
                    '}';
        }
    }
    
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TableState that = (TableState) o;
        return Objects.equals(pagination, that.pagination) &&
                Objects.equals(sort, that.sort) &&
                Objects.equals(search, that.search);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(pagination, sort, search);
    }
    
    @Override
    public String toString() {
        return "TableState{" +
                "pagination=" + pagination +
                ", sort=" + sort +
                ", search=" + search +
                '}';
    }
}
