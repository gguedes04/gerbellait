package br.gerbellait.sistema.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class MapaCargaPedidoBean implements Serializable {
    
    private static final long serialVersionUID = 6075923191925739777L;
    private Long pedido;
    private Long pedidocodecli;
    private Long cliente;
    private String nome;
    private Date data;
    private Double valor;
    private Long rota;
    private boolean checked;
    
    public Long getPedido() {
        return pedido;
    }
    
    public void setPedido(Long pedido) {
        this.pedido = pedido;
    }

    public Long getPedidoCodeCli() {
        return pedidocodecli;
    }

    public void setPedidoCodeCli(Long pedidocodecli) {
        this.pedidocodecli = pedidocodecli;
    }

    public Long getCliente() {
        return cliente;
    }
    
    public void setCliente(Long cliente) {
        this.cliente = cliente;
    }
    
    public String getNome() {
        return nome;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public Date getData() {
        return data;
    }
    
    public void setData(Date data) {
        this.data = data;
    }
    
    public Double getValor() {
        return valor;
    }
    
    public void setValor(Double valor) {
        this.valor = valor;
    }
    
    public Long getRota() {
        return rota;
    }
    
    public void setRota(Long rota) {
        this.rota = rota;
    }
    
    public boolean isChecked() {
        return checked;
    }
    
    public void setChecked(boolean checked) {
        this.checked = checked;
    }
    
    @Override
    public String toString() {
        return "MapaCargaPedidoBean{" +
                "pedido=" + pedido +
                ", pedidocodecli=" + pedidocodecli +
                ", cliente=" + cliente +
                ", nome='" + nome + '\'' +
                ", data=" + data +
                ", valor=" + valor +
                ", rota=" + rota +
                '}';
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MapaCargaPedidoBean that = (MapaCargaPedidoBean) o;
        return Objects.equals(pedido, that.pedido) &&
                Objects.equals(pedidocodecli, that.pedidocodecli) &&
                Objects.equals(cliente, that.cliente) &&
                Objects.equals(nome, that.nome) &&
                Objects.equals(data, that.data) &&
                Objects.equals(valor, that.valor) &&
                Objects.equals(rota, that.rota);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(pedido, pedidocodecli, cliente, nome, data, valor, rota);
    }
}
