package br.gerbellait.sistema.bean;

import java.io.Serializable;
import java.util.Objects;

public class MapaCargaProdutoBean implements Serializable {
    
    private static final long serialVersionUID = -7306781286410640823L;
    private Long customerOrder;
    private Long productId;
    private String productIdCli;
    private String productDesc;
    private String unit;
    private Double totalSelles;
    private Double totalChanges;
    private Double totalLoad;
    private Double averagePrice;
    private Double minPrice;
    private Double maxPrice;
    private Double total;
    private int qtd;
    
    public Long getCustomerOrder() {
        return customerOrder;
    }
    
    public void setCustomerOrder(Long customerOrder) {
        this.customerOrder = customerOrder;
    }
    
    public Long getProductId() {
        return productId;
    }
    
    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductCli() {
        return productIdCli;
    }

    public void setProductIdCli(String productIdCli) {
        this.productIdCli = productIdCli;
    }

    public String getProductDesc() {
        return productDesc;
    }
    
    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }
    
    public String getUnit() {
        return unit;
    }
    
    public void setUnit(String unit) {
        this.unit = unit;
    }
    
    public Double getTotalSelles() {
        return totalSelles;
    }
    
    public void setTotalSelles(Double totalSelles) {
        this.totalSelles = totalSelles;
    }
    
    public Double getTotalChanges() {
        return totalChanges;
    }
    
    public void setTotalChanges(Double totalChanges) {
        this.totalChanges = totalChanges;
    }
    
    public Double getTotalLoad() {
        return totalLoad;
    }
    
    public void setTotalLoad(Double totalLoad) {
        this.totalLoad = totalLoad;
    }
    
    public Double getAveragePrice() {
        return averagePrice;
    }
    
    public void setAveragePrice(Double averagePrice) {
        this.averagePrice = averagePrice;
    }
    
    public Double getMinPrice() {
        return minPrice;
    }
    
    public void setMinPrice(Double minPrice) {
        this.minPrice = minPrice;
    }
    
    public Double getMaxPrice() {
        return maxPrice;
    }
    
    public void setMaxPrice(Double maxPrice) {
        this.maxPrice = maxPrice;
    }
    
    public Double getTotal() {
        return total;
    }
    
    public void setTotal(Double total) {
        this.total = total;
    }
    
    public int getQtd() {
        return qtd;
    }
    
    public void setQtd(int qtd) {
        this.qtd = qtd;
    }
    
    @Override
    public String toString() {
        return "MapaCargaProdutoBean{" +
                "customerOrder=" + customerOrder +
                ", productId=" + productId +
                ", productIdCli=" + productIdCli +
                ", productDesc='" + productDesc + '\'' +
                ", unit='" + unit + '\'' +
                ", totalSelles=" + totalSelles +
                ", totalChanges=" + totalChanges +
                ", totalLoad=" + totalLoad +
                ", averagePrice=" + averagePrice +
                ", minPrice=" + minPrice +
                ", maxPrice=" + maxPrice +
                ", total=" + total +
                ", qtd=" + qtd +
                '}';
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MapaCargaProdutoBean that = (MapaCargaProdutoBean) o;
        return qtd == that.qtd &&
                Objects.equals(customerOrder, that.customerOrder) &&
                Objects.equals(productId, that.productId) &&
                Objects.equals(productIdCli, that.productIdCli) &&
                Objects.equals(productDesc, that.productDesc) &&
                Objects.equals(unit, that.unit) &&
                Objects.equals(totalSelles, that.totalSelles) &&
                Objects.equals(totalChanges, that.totalChanges) &&
                Objects.equals(totalLoad, that.totalLoad) &&
                Objects.equals(averagePrice, that.averagePrice) &&
                Objects.equals(minPrice, that.minPrice) &&
                Objects.equals(maxPrice, that.maxPrice) &&
                Objects.equals(total, that.total);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(customerOrder, productId, productIdCli, productDesc, unit, totalSelles, totalChanges, totalLoad, averagePrice, minPrice, maxPrice, total, qtd);
    }
}
