package br.gerbellait.sistema.bean;

import java.io.Serializable;
import java.util.Objects;

public class PedidoProdutoBean implements Serializable {
    
    private static final long serialVersionUID = -1974361964684012305L;
    private Long productId;
    private String productCode;
    private String productDesc;
    private String unit;
    private Double weight;
    private boolean sellRelease;
    private String barCode;
    private Double sellPrice;
    private Long route;
    private int sellLimitCount;
    private Double promotionalPrice;
    private int sellMinCount;
    private String option;
    
    public Long getProductId() {
        return productId;
    }
    
    public void setProductId(Long productId) {
        this.productId = productId;
    }
    
    public String getProductCode() {
        return productCode;
    }
    
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
    
    public String getProductDesc() {
        return productDesc;
    }
    
    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }
    
    public String getUnit() {
        return unit;
    }
    
    public void setUnit(String unit) {
        this.unit = unit;
    }
    
    public Double getWeight() {
        return weight;
    }
    
    public void setWeight(Double weight) {
        this.weight = weight;
    }
    
    public boolean isSellRelease() {
        return sellRelease;
    }
    
    public void setSellRelease(boolean sellRelease) {
        this.sellRelease = sellRelease;
    }
    
    public String getBarCode() {
        return barCode;
    }
    
    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }
    
    public Double getSellPrice() {
        return sellPrice;
    }
    
    public void setSellPrice(Double sellPrice) {
        this.sellPrice = sellPrice;
    }
    
    public Long getRoute() {
        return route;
    }
    
    public void setRoute(Long route) {
        this.route = route;
    }
    
    public int getSellLimitCount() {
        return sellLimitCount;
    }
    
    public void setSellLimitCount(int sellLimitCount) {
        this.sellLimitCount = sellLimitCount;
    }
    
    public Double getPromotionalPrice() {
        return promotionalPrice;
    }
    
    public void setPromotionalPrice(Double promotionalPrice) {
        this.promotionalPrice = promotionalPrice;
    }
    
    public int getSellMinCount() {
        return sellMinCount;
    }
    
    public void setSellMinCount(int sellMinCount) {
        this.sellMinCount = sellMinCount;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PedidoProdutoBean that = (PedidoProdutoBean) o;

        if (sellRelease != that.sellRelease) return false;
        if (sellLimitCount != that.sellLimitCount) return false;
        if (sellMinCount != that.sellMinCount) return false;
        if (productId != null ? !productId.equals(that.productId) : that.productId != null) return false;
        if (productCode != null ? !productCode.equals(that.productCode) : that.productCode != null) return false;
        if (productDesc != null ? !productDesc.equals(that.productDesc) : that.productDesc != null) return false;
        if (unit != null ? !unit.equals(that.unit) : that.unit != null) return false;
        if (weight != null ? !weight.equals(that.weight) : that.weight != null) return false;
        if (barCode != null ? !barCode.equals(that.barCode) : that.barCode != null) return false;
        if (sellPrice != null ? !sellPrice.equals(that.sellPrice) : that.sellPrice != null) return false;
        if (route != null ? !route.equals(that.route) : that.route != null) return false;
        if (promotionalPrice != null ? !promotionalPrice.equals(that.promotionalPrice) : that.promotionalPrice != null)
            return false;
        return !(option != null ? !option.equals(that.option) : that.option != null);

    }

    @Override
    public int hashCode() {
        int result = productId != null ? productId.hashCode() : 0;
        result = 31 * result + (productCode != null ? productCode.hashCode() : 0);
        result = 31 * result + (productDesc != null ? productDesc.hashCode() : 0);
        result = 31 * result + (unit != null ? unit.hashCode() : 0);
        result = 31 * result + (weight != null ? weight.hashCode() : 0);
        result = 31 * result + (sellRelease ? 1 : 0);
        result = 31 * result + (barCode != null ? barCode.hashCode() : 0);
        result = 31 * result + (sellPrice != null ? sellPrice.hashCode() : 0);
        result = 31 * result + (route != null ? route.hashCode() : 0);
        result = 31 * result + sellLimitCount;
        result = 31 * result + (promotionalPrice != null ? promotionalPrice.hashCode() : 0);
        result = 31 * result + sellMinCount;
        result = 31 * result + (option != null ? option.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PedidoProdutoBean{" +
                "productId=" + productId +
                ", productCode='" + productCode + '\'' +
                ", productDesc='" + productDesc + '\'' +
                ", unit='" + unit + '\'' +
                ", weight=" + weight +
                ", sellRelease=" + sellRelease +
                ", barCode='" + barCode + '\'' +
                ", sellPrice=" + sellPrice +
                ", route=" + route +
                ", sellLimitCount=" + sellLimitCount +
                ", promotionalPrice=" + promotionalPrice +
                ", sellMinCount=" + sellMinCount +
                ", option='" + option + '\'' +
                '}';
    }
}
