package br.gerbellait.sistema.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class PedidoImpBean implements Serializable {
    
    private static final long serialVersionUID = -36378548866085026L;
    private Long pedidoCliente;
    private Long codPessoa;
    private String nmPessoa;
    private String cnpj;
    private String optanteSimples;
    private String logradouro;
    private String bairro;
    private String cidade;
    private String uf;
    private String inscrEstadual;
    private String telefone;
    private String cep;
    private Double vlTotal;
    private String formaPagamento;
    private Date dtPedido;
    private String nmFantasia;
    private int prazo;
    private String unidade;
    private Double desconto;
    private String observacao;
    private String geraNf;
    private String numero;
    private Double mva;
    private int percSimples;
    private Double icmsEntrada;
    private Double icmsSaida;
    private String possuiSt;
    private Long idTipoPessoa;
    private Double vlPedido;
    private Long codProduto;
    private String nmProduto;
    private Double qtProduto;
    private Double qtTrocaProduto;
    private Double vlUnitario;
    private Double vlTotalProduto;
    
    public Long getPedidoCliente() {
        return pedidoCliente;
    }
    
    public void setPedidoCliente(Long pedidoCliente) {
        this.pedidoCliente = pedidoCliente;
    }
    
    public Long getCodPessoa() {
        return codPessoa;
    }
    
    public void setCodPessoa(Long codPessoa) {
        this.codPessoa = codPessoa;
    }
    
    public String getNmPessoa() {
        return nmPessoa;
    }
    
    public void setNmPessoa(String nmPessoa) {
        this.nmPessoa = nmPessoa;
    }
    
    public String getCnpj() {
        return cnpj;
    }
    
    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }
    
    public String getOptanteSimples() {
        return optanteSimples;
    }
    
    public void setOptanteSimples(String optanteSimples) {
        this.optanteSimples = optanteSimples;
    }
    
    public String getLogradouro() {
        return logradouro;
    }
    
    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }
    
    public String getBairro() {
        return bairro;
    }
    
    public void setBairro(String bairro) {
        this.bairro = bairro;
    }
    
    public String getCidade() {
        return cidade;
    }
    
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }
    
    public String getUf() {
        return uf;
    }
    
    public void setUf(String uf) {
        this.uf = uf;
    }
    
    public String getInscrEstadual() {
        return inscrEstadual;
    }
    
    public void setInscrEstadual(String inscrEstadual) {
        this.inscrEstadual = inscrEstadual;
    }
    
    public String getTelefone() {
        return telefone;
    }
    
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
    
    public String getCep() {
        return cep;
    }
    
    public void setCep(String cep) {
        this.cep = cep;
    }
    
    public Double getVlTotal() {
        return vlTotal;
    }
    
    public void setVlTotal(Double vlTotal) {
        this.vlTotal = vlTotal;
    }
    
    public String getFormaPagamento() {
        return formaPagamento;
    }
    
    public void setFormaPagamento(String formaPagamento) {
        this.formaPagamento = formaPagamento;
    }
    
    public Date getDtPedido() {
        return dtPedido;
    }
    
    public void setDtPedido(Date dtPedido) {
        this.dtPedido = dtPedido;
    }
    
    public String getNmFantasia() {
        return nmFantasia;
    }
    
    public void setNmFantasia(String nmFantasia) {
        this.nmFantasia = nmFantasia;
    }
    
    public int getPrazo() {
        return prazo;
    }
    
    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }
    
    public String getUnidade() {
        return unidade;
    }
    
    public void setUnidade(String unidade) {
        this.unidade = unidade;
    }
    
    public Double getDesconto() {
        return desconto;
    }
    
    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }
    
    public String getObservacao() {
        return observacao;
    }
    
    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
    
    public String getGeraNf() {
        return geraNf;
    }
    
    public void setGeraNf(String geraNf) {
        this.geraNf = geraNf;
    }
    
    public String getNumero() {
        return numero;
    }
    
    public void setNumero(String numero) {
        this.numero = numero;
    }
    
    public Double getMva() {
        return mva;
    }
    
    public void setMva(Double mva) {
        this.mva = mva;
    }
    
    public int getPercSimples() {
        return percSimples;
    }
    
    public void setPercSimples(int percSimples) {
        this.percSimples = percSimples;
    }
    
    public Double getIcmsEntrada() {
        return icmsEntrada;
    }
    
    public void setIcmsEntrada(Double icmsEntrada) {
        this.icmsEntrada = icmsEntrada;
    }
    
    public Double getIcmsSaida() {
        return icmsSaida;
    }
    
    public void setIcmsSaida(Double icmsSaida) {
        this.icmsSaida = icmsSaida;
    }
    
    public String getPossuiSt() {
        return possuiSt;
    }
    
    public void setPossuiSt(String possuiSt) {
        this.possuiSt = possuiSt;
    }
    
    public Long getIdTipoPessoa() {
        return idTipoPessoa;
    }
    
    public void setIdTipoPessoa(Long idTipoPessoa) {
        this.idTipoPessoa = idTipoPessoa;
    }
    
    public Double getVlPedido() {
        return vlPedido;
    }
    
    public void setVlPedido(Double vlPedido) {
        this.vlPedido = vlPedido;
    }
    
    public Long getCodProduto() {
        return codProduto;
    }
    
    public void setCodProduto(Long codProduto) {
        this.codProduto = codProduto;
    }
    
    public String getNmProduto() {
        return nmProduto;
    }
    
    public void setNmProduto(String nmProduto) {
        this.nmProduto = nmProduto;
    }
    
    public Double getQtProduto() {
        return qtProduto;
    }
    
    public void setQtProduto(Double qtProduto) {
        this.qtProduto = qtProduto;
    }
    
    public Double getQtTrocaProduto() {
        return qtTrocaProduto;
    }
    
    public void setQtTrocaProduto(Double qtTrocaProduto) {
        this.qtTrocaProduto = qtTrocaProduto;
    }
    
    public Double getVlUnitario() {
        return vlUnitario;
    }
    
    public void setVlUnitario(Double vlUnitario) {
        this.vlUnitario = vlUnitario;
    }
    
    public Double getVlTotalProduto() {
        return vlTotalProduto;
    }
    
    public void setVlTotalProduto(Double vlTotalProduto) {
        this.vlTotalProduto = vlTotalProduto;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PedidoImpBean that = (PedidoImpBean) o;
        return prazo == that.prazo &&
                Objects.equals(pedidoCliente, that.pedidoCliente) &&
                Objects.equals(codPessoa, that.codPessoa) &&
                Objects.equals(nmPessoa, that.nmPessoa) &&
                Objects.equals(cnpj, that.cnpj) &&
                Objects.equals(optanteSimples, that.optanteSimples) &&
                Objects.equals(logradouro, that.logradouro) &&
                Objects.equals(bairro, that.bairro) &&
                Objects.equals(cidade, that.cidade) &&
                Objects.equals(uf, that.uf) &&
                Objects.equals(inscrEstadual, that.inscrEstadual) &&
                Objects.equals(telefone, that.telefone) &&
                Objects.equals(cep, that.cep) &&
                Objects.equals(vlTotal, that.vlTotal) &&
                Objects.equals(formaPagamento, that.formaPagamento) &&
                Objects.equals(dtPedido, that.dtPedido) &&
                Objects.equals(nmFantasia, that.nmFantasia) &&
                Objects.equals(unidade, that.unidade) &&
                Objects.equals(desconto, that.desconto) &&
                Objects.equals(observacao, that.observacao) &&
                Objects.equals(geraNf, that.geraNf) &&
                Objects.equals(numero, that.numero) &&
                Objects.equals(mva, that.mva) &&
                Objects.equals(percSimples, that.percSimples) &&
                Objects.equals(icmsEntrada, that.icmsEntrada) &&
                Objects.equals(icmsSaida, that.icmsSaida) &&
                Objects.equals(possuiSt, that.possuiSt) &&
                Objects.equals(idTipoPessoa, that.idTipoPessoa) &&
                Objects.equals(vlPedido, that.vlPedido) &&
                Objects.equals(codProduto, that.codProduto) &&
                Objects.equals(nmProduto, that.nmProduto) &&
                Objects.equals(qtProduto, that.qtProduto) &&
                Objects.equals(qtTrocaProduto, that.qtTrocaProduto) &&
                Objects.equals(vlUnitario, that.vlUnitario) &&
                Objects.equals(vlTotalProduto, that.vlTotalProduto);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(pedidoCliente, codPessoa, nmPessoa, cnpj, optanteSimples, logradouro, bairro, cidade, uf, inscrEstadual, telefone, cep, vlTotal, formaPagamento, dtPedido, nmFantasia, prazo, unidade, desconto, observacao, geraNf, numero, mva, percSimples, icmsEntrada, icmsSaida, possuiSt, idTipoPessoa, vlPedido, codProduto, nmProduto, qtProduto, qtTrocaProduto, vlUnitario, vlTotalProduto);
    }
    
    @Override
    public String toString() {
        return "PedidoImpBean{" +
                "pedidoCliente=" + pedidoCliente +
                ", codPessoa=" + codPessoa +
                ", nmPessoa='" + nmPessoa + '\'' +
                ", cnpj='" + cnpj + '\'' +
                ", optanteSimples='" + optanteSimples + '\'' +
                ", logradouro='" + logradouro + '\'' +
                ", bairro='" + bairro + '\'' +
                ", cidade='" + cidade + '\'' +
                ", uf='" + uf + '\'' +
                ", inscrEstadual='" + inscrEstadual + '\'' +
                ", telefone='" + telefone + '\'' +
                ", cep='" + cep + '\'' +
                ", vlTotal=" + vlTotal +
                ", formaPagamento='" + formaPagamento + '\'' +
                ", dtPedido=" + dtPedido +
                ", nmFantasia='" + nmFantasia + '\'' +
                ", prazo=" + prazo +
                ", unidade='" + unidade + '\'' +
                ", desconto=" + desconto +
                ", observacao='" + observacao + '\'' +
                ", geraNf='" + geraNf + '\'' +
                ", numero='" + numero + '\'' +
                ", mva='" + mva + '\'' +
                ", percSimples='" + percSimples + '\'' +
                ", icmsEntrada='" + icmsEntrada + '\'' +
                ", icmsSaida='" + icmsSaida + '\'' +
                ", possuiSt='" + possuiSt + '\'' +
                ", idTipoPessoa=" + idTipoPessoa +
                ", vlPedido=" + vlPedido +
                ", codProduto=" + codProduto +
                ", nmProduto='" + nmProduto + '\'' +
                ", qtProduto=" + qtProduto +
                ", qtTrocaProduto=" + qtTrocaProduto +
                ", vlUnitario=" + vlUnitario +
                ", vlTotalProduto=" + vlTotalProduto +
                '}';
    }
}
