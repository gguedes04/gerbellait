package br.gerbellait.sistema.bean;

import java.io.Serializable;
import java.util.List;

import br.gerbellait.sistema.entity.MapaCarga;

public class MapaCargaFilterBean implements Serializable {
    
    private static final long serialVersionUID = 1032274272999487579L;
    private List<Long> selectedSellers;
    private List<Long> selectedRoutes;
    private String dataInicial;
    private String dataFinal;
    private MapaCarga loadMapNumber;
    private List<MapaCargaPedidoBean> selectedOrders;
    
    
    public List<Long> getSelectedSellers() {
        return selectedSellers;
    }
    
    public void setSelectedSellers(List<Long> selectedSellers) {
        this.selectedSellers = selectedSellers;
    }
    
    public List<Long> getSelectedRoutes() {
        return selectedRoutes;
    }
    
    public void setSelectedRoutes(List<Long> selectedRoutes) {
        this.selectedRoutes = selectedRoutes;
    }
    
    public String getDataInicial() {
        return dataInicial;
    }
    
    public void setDataInicial(String dataInicial) {
        this.dataInicial = dataInicial;
    }
    
    public String getDataFinal() {
        return dataFinal;
    }
    
    public void setDataFinal(String dataFinal) {
        this.dataFinal = dataFinal;
    }
    
    public MapaCarga getLoadMapNumber() {
        return loadMapNumber;
    }
    
    public void setLoadMapNumber(MapaCarga loadMapNumber) {
        this.loadMapNumber = loadMapNumber;
    }
    
    public List<MapaCargaPedidoBean> getSelectedOrders() {
        return selectedOrders;
    }
    
    public void setSelectedOrders(List<MapaCargaPedidoBean> selectedOrders) {
        this.selectedOrders = selectedOrders;
    }
    
    @Override
    public String toString() {
        return "MapaCargaFilterBean{" +
                "selectedSellers=" + selectedSellers +
                ", selectedRoutes=" + selectedRoutes +
                ", dataInicial='" + dataInicial + '\'' +
                ", dataFinal='" + dataFinal + '\'' +
                ", loadMapNumber=" + loadMapNumber +
                ", selectedOrders=" + selectedOrders +
                '}';
    }
}
