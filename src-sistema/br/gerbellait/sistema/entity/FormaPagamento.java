package br.gerbellait.sistema.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.gerbellait.arquitetura.entities.AbstractEntity;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;

public class FormaPagamento extends AbstractEntity {
    private static final long serialVersionUID = 1L;
    private String descricao;
    private Integer prazo;
    private BigDecimal valorMinimoPedido;
    private Date dataAlteracao;
    private ExcluidoEnum flExcluido;
    private String permiteDesconto;
    
    public FormaPagamento() {
        // empty
    }
    
    public FormaPagamento(Long id) {
        this.setId(id);
    }
    
    public FormaPagamento(String descricao) {
        this.setDescricao(descricao);
    }
    
    public String getDescricao() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    public Integer getPrazo() {
        return prazo;
    }
    
    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
    
    public BigDecimal getValorMinimoPedido() {
        return valorMinimoPedido;
    }
    
    public void setValorMinimoPedido(BigDecimal valorMinimoPedido) {
        this.valorMinimoPedido = valorMinimoPedido;
    }
    
    public Date getDataAlteracao() {
        return dataAlteracao;
    }
    
    public void setDataAlteracao(Date dataAlteracao) {
        this.dataAlteracao = dataAlteracao;
    }
    
    @JsonFormat(pattern = "dd/MM/yyyy")
    public Date getVencimento() {
        LocalDate localDate = LocalDate.now();
        localDate = localDate.plusDays(this.prazo == null ? 0 : this.prazo);
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }
    
    private void setVencimento(Date vencimento) {
        // empty
    }
    
    public ExcluidoEnum getFlExcluido() {
        if (flExcluido == null) {
            flExcluido = ExcluidoEnum.ATIVO;
        }
        return flExcluido;
    }
    
    public void setFlExcluido(ExcluidoEnum flExcluido) {
        this.flExcluido = flExcluido;
    }
    
    public String getPermiteDesconto() {
        return permiteDesconto;
    }
    
    public void setPermiteDesconto(String permiteDesconto) {
        this.permiteDesconto = permiteDesconto;
    }
    
    @Override
    public String toString() {
        return "FormaPagamento{" +
                "descricao='" + descricao + '\'' +
                ", prazo=" + prazo +
                ", valorMinimoPedido=" + valorMinimoPedido +
                ", dataAlteracao=" + dataAlteracao +
                ", flExcluido=" + flExcluido +
                ", permiteDesconto='" + permiteDesconto + '\'' +
                '}';
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        FormaPagamento that = (FormaPagamento) o;
        return Objects.equals(descricao, that.descricao) &&
                Objects.equals(prazo, that.prazo) &&
                Objects.equals(valorMinimoPedido, that.valorMinimoPedido) &&
                Objects.equals(dataAlteracao, that.dataAlteracao) &&
                flExcluido == that.flExcluido &&
                Objects.equals(permiteDesconto, that.permiteDesconto);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), descricao, prazo, valorMinimoPedido, dataAlteracao, flExcluido, permiteDesconto);
    }
    
}
