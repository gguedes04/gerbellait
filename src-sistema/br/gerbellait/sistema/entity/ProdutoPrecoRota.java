package br.gerbellait.sistema.entity;

import java.math.BigDecimal;
import java.util.Date;

import br.gerbellait.arquitetura.entities.AbstractEntity;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;

public class ProdutoPrecoRota extends AbstractEntity {
	private static final long serialVersionUID = 1L;
	private Produto produto;
	private Rota rota;
	private BigDecimal valorPrecoVendaProduto;
	private Date dataAlteracao;
	private Integer flExcluido;

	// excluir, alterar, remover, novo
	private transient String acao;

	private transient ExcluidoEnum situacao;

	public ProdutoPrecoRota() {
		// empty
	}

	public ProdutoPrecoRota(Long id) {
		this.setId(id);
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public String getAcao() {
		return acao;
	}

	public void setAcao(String acao) {
		this.acao = acao;
	}

	public Rota getRota() {
		return rota;
	}

	public void setRota(Rota rota) {
		this.rota = rota;
	}

	public BigDecimal getValorPrecoVendaProduto() {
		return valorPrecoVendaProduto;
	}

	public void setValorPrecoVendaProduto(BigDecimal valorPrecoVendaProduto) {
		this.valorPrecoVendaProduto = valorPrecoVendaProduto;
	}

	public Date getDataAlteracao() {
		return dataAlteracao;
	}

	public void setDataAlteracao(Date dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

	public Integer getFlExcluido() {
		if (flExcluido == null) {
			flExcluido = ExcluidoEnum.ATIVO.getId();
		}
		return flExcluido;
	}

	public void setFlExcluido(Integer flExcluido) {
		this.flExcluido = flExcluido;
	}

	public ExcluidoEnum getSituacao() {
		if (situacao == null) {
			situacao = ExcluidoEnum.ATIVO;
		}
		if (getFlExcluido() != null) {
			situacao = ExcluidoEnum.getById(getFlExcluido());
		}
		return situacao;
	}

	public void setSituacao(ExcluidoEnum situacao) {
		this.situacao = situacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((dataAlteracao == null) ? 0 : dataAlteracao.hashCode());
		result = prime * result + ((flExcluido == null) ? 0 : flExcluido.hashCode());
		result = prime * result + ((produto == null) ? 0 : produto.hashCode());
		result = prime * result + ((rota == null) ? 0 : rota.hashCode());
		result = prime * result + ((valorPrecoVendaProduto == null) ? 0 : valorPrecoVendaProduto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProdutoPrecoRota other = (ProdutoPrecoRota) obj;
		if (dataAlteracao == null) {
			if (other.dataAlteracao != null)
				return false;
		} else if (!dataAlteracao.equals(other.dataAlteracao))
			return false;
		if (flExcluido == null) {
			if (other.flExcluido != null)
				return false;
		} else if (!flExcluido.equals(other.flExcluido))
			return false;
		if (produto == null) {
			if (other.produto != null)
				return false;
		} else if (!produto.equals(other.produto))
			return false;
		if (rota == null) {
			if (other.rota != null)
				return false;
		} else if (!rota.equals(other.rota))
			return false;
		if (valorPrecoVendaProduto == null) {
			if (other.valorPrecoVendaProduto != null)
				return false;
		} else if (!valorPrecoVendaProduto.equals(other.valorPrecoVendaProduto))
			return false;
		return true;
	}

}