package br.gerbellait.sistema.entity;

import java.math.BigDecimal;

import br.gerbellait.arquitetura.entities.AbstractEntity;

public class ComissaoVendedor extends AbstractEntity {
	private static final long serialVersionUID = 1L;
	private Vendedor vendedor;
	private GrupoProduto grupoProduto;
	private BigDecimal percentualComissao;

	// excluir, alterar, remover, novo
	private transient String acao;

	public ComissaoVendedor() {
		// empty
	}

	public ComissaoVendedor(Long id) {
		this.setId(id);
	}

	public String getAcao() {
		return acao;
	}

	public void setAcao(String acao) {
		this.acao = acao;
	}

	public Vendedor getVendedor() {
		return vendedor;
	}

	public void setVendedor(Vendedor vendedor) {
		this.vendedor = vendedor;
	}

	public GrupoProduto getGrupoProduto() {
		if (grupoProduto == null) {
			grupoProduto = new GrupoProduto();
		}
		return grupoProduto;
	}

	public void setGrupoProduto(GrupoProduto grupoProduto) {
		this.grupoProduto = grupoProduto;
	}

	public BigDecimal getPercentualComissao() {
		return percentualComissao;
	}

	public void setPercentualComissao(BigDecimal percentualComissao) {
		this.percentualComissao = percentualComissao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((grupoProduto == null) ? 0 : grupoProduto.hashCode());
		result = prime * result + ((percentualComissao == null) ? 0 : percentualComissao.hashCode());
		result = prime * result + ((vendedor == null) ? 0 : vendedor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComissaoVendedor other = (ComissaoVendedor) obj;
		if (grupoProduto == null) {
			if (other.grupoProduto != null)
				return false;
		} else if (!grupoProduto.equals(other.grupoProduto))
			return false;
		if (percentualComissao == null) {
			if (other.percentualComissao != null)
				return false;
		} else if (!percentualComissao.equals(other.percentualComissao))
			return false;
		if (vendedor == null) {
			if (other.vendedor != null)
				return false;
		} else if (!vendedor.equals(other.vendedor))
			return false;
		return true;
	}
}