package br.gerbellait.sistema.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import br.gerbellait.arquitetura.entities.AbstractEntity;
import br.gerbellait.arquitetura.util.Utils;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.dao.enumerador.TipoTrocaEnum;
import br.gerbellait.sistema.dao.enumerador.TipoUnidadeEnum;
import br.gerbellait.sistema.util.DateUtil;
import br.gerbellait.sistema.util.Paginate;

public class Produto extends AbstractEntity {
    private static final long serialVersionUID = 1L;
    
    private String idCodProduto;
    private String nome;
    private String tipoUnidade;
    private GrupoProduto grupoProduto;
    private String cf;
    private Pessoa fornecedor;
    private BigDecimal valorComissao;
    private BigDecimal peso;
    private BigDecimal percDesconto;
    private BigDecimal quantidadeMinVenda;
    private BigDecimal quantidadeEstoqueAtual;
    private String flgLiberadoVenda;
    private String codigoBarras;
    private Date dataAlteracao;
    private Integer situacao;
    private String tipoTroca;
    private String st;
    private Long qtdeLimitVenda;
    private Double precoVenda;
    private Long qtdeProduto;
    private Long qtdeTroca;
    private Long desconto;
    private Double vlUnitario;
    
    private List<ProdutoImposto> impostos;
    private List<ProdutoPrecoRota> rotas;
    private List<MetaVendedorProduto> metasVendedores;
    
    private transient String periodoDescricao;
    private transient List<Integer> idGrupoProdutoList;
    private transient String dataAlteracaoFormatada;
    private transient ExcluidoEnum flExcluido;
    private transient TipoUnidadeEnum tipoUnidadeEnum;
    private transient TipoTrocaEnum tipoTrocaEnum;
    private transient Paginate paginate;
    
    public Produto() {
        // empty
    }
    
    public Produto(Long id) {
        this.setId(id);
    }
    
    public Produto(Long id, String nome) {
        this.setId(id);
        this.setNome(nome);
    }
    
    public String getPeriodoDescricao() {
        return periodoDescricao;
    }
    
    public void setPeriodoDescricao(String periodoDescricao) {
        this.periodoDescricao = periodoDescricao;
    }
    
    public List<Integer> getIdGrupoProdutoList() {
        if (idGrupoProdutoList == null) {
            idGrupoProdutoList = new ArrayList<>();
        }
        return idGrupoProdutoList;
    }
    
    public void setIdGrupoProdutoList(List<Integer> idGrupoProdutoList) {
        this.idGrupoProdutoList = idGrupoProdutoList;
    }
    
    public String getIdCodProduto() {
        return idCodProduto;
    }
    
    public void setIdCodProduto(String idCodProduto) {
        this.idCodProduto = idCodProduto;
    }
    
    public String getNome() {
        return nome;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public String getTipoUnidade() {
        return tipoUnidade;
    }
    
    public String getSt() {
        return st;
    }
    
    public void setSt(String st) {
        this.st = st;
    }
    
    public void setTipoUnidade(String tipoUnidade) {
        this.tipoUnidade = tipoUnidade;
    }
    
    public GrupoProduto getGrupoProduto() {
        return grupoProduto;
    }
    
    public void setGrupoProduto(GrupoProduto grupoProduto) {
        this.grupoProduto = grupoProduto;
    }
    
    public String getCf() {
        return cf;
    }
    
    public void setCf(String cf) {
        this.cf = cf;
    }
    
    public Pessoa getFornecedor() {
        return fornecedor;
    }
    
    public void setFornecedor(Pessoa fornecedor) {
        this.fornecedor = fornecedor;
    }
    
    public BigDecimal getValorComissao() {
        return valorComissao;
    }
    
    public void setValorComissao(BigDecimal valorComissao) {
        this.valorComissao = valorComissao;
    }
    
    public BigDecimal getPeso() {
        return peso;
    }
    
    public void setPeso(BigDecimal peso) {
        this.peso = peso;
    }
    
    public BigDecimal getPercDesconto() {
        return percDesconto;
    }
    
    public void setPercDesconto(BigDecimal percDesconto) {
        this.percDesconto = percDesconto;
    }
    
    public BigDecimal getQuantidadeMinVenda() {
        return quantidadeMinVenda;
    }
    
    public void setQuantidadeMinVenda(BigDecimal quantidadeMinVenda) {
        this.quantidadeMinVenda = quantidadeMinVenda;
    }
    
    public BigDecimal getQuantidadeEstoqueAtual() {
        return quantidadeEstoqueAtual;
    }
    
    public void setQuantidadeEstoqueAtual(BigDecimal quantidadeEstoqueAtual) {
        this.quantidadeEstoqueAtual = quantidadeEstoqueAtual;
    }
    
    public String getFlgLiberadoVenda() {
        return flgLiberadoVenda;
    }
    
    public void setFlgLiberadoVenda(String flgLiberadoVenda) {
        this.flgLiberadoVenda = flgLiberadoVenda;
    }
    
    public String getCodigoBarras() {
        return codigoBarras;
    }
    
    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }
    
    public Date getDataAlteracao() {
        return dataAlteracao;
    }
    
    public void setDataAlteracao(Date dataAlteracao) {
        this.dataAlteracao = dataAlteracao;
    }
    
    public String getDataAlteracaoFormatada() {
        this.dataAlteracaoFormatada = DateUtil.dateToFormatDefault(this.getDataAlteracao());
        return this.dataAlteracaoFormatada;
    }
    
    public void setDataAlteracaoFormatada(String dataAlteracaoFormatada) {
        this.dataAlteracaoFormatada = dataAlteracaoFormatada;
    }
    
    public Integer getSituacao() {
        return situacao;
    }
    
    public void setSituacao(Integer situacao) {
        this.situacao = situacao;
    }
    
    public String getTipoTroca() {
        return tipoTroca;
    }
    
    public void setTipoTroca(String tipoTroca) {
        this.tipoTroca = tipoTroca;
    }
    
    public ExcluidoEnum getFlExcluido() {
        if (flExcluido == null) {
            flExcluido = ExcluidoEnum.ATIVO;
        }
        if (getSituacao() != null) {
            flExcluido = ExcluidoEnum.getById(getSituacao());
        } else {
            situacao = flExcluido.getId();
        }
        return flExcluido;
    }
    
    public void setFlExcluido(ExcluidoEnum flExcluido) {
        this.flExcluido = flExcluido;
    }
    
    public TipoUnidadeEnum getTipoUnidadeEnum() {
        if (!Utils.isNullOrEmpty(this.getTipoUnidade())) {
            tipoUnidadeEnum = TipoUnidadeEnum.getById(this.getTipoUnidade());
        }
        return tipoUnidadeEnum;
    }
    
    public void setTipoUnidadeEnum(TipoUnidadeEnum tipoUnidadeEnum) {
        this.tipoUnidadeEnum = tipoUnidadeEnum;
    }
    
    public TipoTrocaEnum getTipoTrocaEnum() {
        if (!Utils.isNullOrEmpty(this.getTipoTroca())) {
            tipoTrocaEnum = TipoTrocaEnum.getById(this.getTipoTroca());
        }
        return tipoTrocaEnum;
    }
    
    public void setTipoTrocaEnum(TipoTrocaEnum tipoTrocaEnum) {
        this.tipoTrocaEnum = tipoTrocaEnum;
    }
    
    public List<ProdutoImposto> getImpostos() {
        return impostos;
    }
    
    public void setImpostos(List<ProdutoImposto> impostos) {
        this.impostos = impostos;
    }
    
    public List<ProdutoPrecoRota> getRotas() {
        if (rotas == null) {
            rotas = new ArrayList<>();
        }
        return rotas;
    }
    
    public void setRotas(List<ProdutoPrecoRota> rotas) {
        this.rotas = rotas;
    }
    
    public List<MetaVendedorProduto> getMetasVendedores() {
        if (metasVendedores == null) {
            metasVendedores = new ArrayList<>();
        }
        return metasVendedores;
    }
    
    public void setMetasVendedores(List<MetaVendedorProduto> metasVendedores) {
        this.metasVendedores = metasVendedores;
    }
    
    public Paginate getPaginate() {
        if (paginate == null) {
            paginate = new Paginate();
        }
        return paginate;
    }
    
    public Long getQtdeLimitVenda() {
        return qtdeLimitVenda;
    }
    
    public void setQtdeLimitVenda(Long qtdeLimitVenda) {
        this.qtdeLimitVenda = qtdeLimitVenda;
    }
    
    public void setPaginate(Paginate paginate) {
        this.paginate = paginate;
    }
    
    public Double getPrecoVenda() {
        return precoVenda;
    }
    
    public void setPrecoVenda(Double precoVenda) {
        this.precoVenda = precoVenda;
    }
    
    public Long getQtdeProduto() {
        return qtdeProduto;
    }
    
    public Long getQtdeTroca() {
        return qtdeTroca == null ? 0 : qtdeTroca;
    }
    
    public void setQtdeTroca(Long qtdeTroca) {
        this.qtdeTroca = qtdeTroca;
    }
    
    public void setQtdeProduto(Long qtdeProduto) {
        this.qtdeProduto = qtdeProduto;
    }
    
    public Long getDesconto() {
        return desconto == null ? 0 : desconto;
    }
    
    public void setDesconto(Long desconto) {
        this.desconto = desconto;
    }
    
    public Double getVlUnitario() {
        return vlUnitario;
    }
    
    public void setVlUnitario(Double vlUnitario) {
        this.vlUnitario = vlUnitario;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Produto produto = (Produto) o;
        return Objects.equals(idCodProduto, produto.idCodProduto) &&
                Objects.equals(nome, produto.nome) &&
                Objects.equals(tipoUnidade, produto.tipoUnidade) &&
                Objects.equals(grupoProduto, produto.grupoProduto) &&
                Objects.equals(cf, produto.cf) &&
                Objects.equals(fornecedor, produto.fornecedor) &&
                Objects.equals(valorComissao, produto.valorComissao) &&
                Objects.equals(peso, produto.peso) &&
                Objects.equals(percDesconto, produto.percDesconto) &&
                Objects.equals(quantidadeMinVenda, produto.quantidadeMinVenda) &&
                Objects.equals(quantidadeEstoqueAtual, produto.quantidadeEstoqueAtual) &&
                Objects.equals(flgLiberadoVenda, produto.flgLiberadoVenda) &&
                Objects.equals(codigoBarras, produto.codigoBarras) &&
                Objects.equals(dataAlteracao, produto.dataAlteracao) &&
                Objects.equals(situacao, produto.situacao) &&
                Objects.equals(tipoTroca, produto.tipoTroca) &&
                Objects.equals(st, produto.st) &&
                Objects.equals(qtdeLimitVenda, produto.qtdeLimitVenda) &&
                Objects.equals(precoVenda, produto.precoVenda) &&
                Objects.equals(qtdeProduto, produto.qtdeProduto) &&
                Objects.equals(qtdeTroca, produto.qtdeTroca) &&
                Objects.equals(desconto, produto.desconto) &&
                Objects.equals(vlUnitario, produto.vlUnitario) &&
                Objects.equals(impostos, produto.impostos) &&
                Objects.equals(rotas, produto.rotas) &&
                Objects.equals(metasVendedores, produto.metasVendedores) &&
                Objects.equals(periodoDescricao, produto.periodoDescricao) &&
                Objects.equals(idGrupoProdutoList, produto.idGrupoProdutoList) &&
                Objects.equals(dataAlteracaoFormatada, produto.dataAlteracaoFormatada) &&
                flExcluido == produto.flExcluido &&
                tipoUnidadeEnum == produto.tipoUnidadeEnum &&
                tipoTrocaEnum == produto.tipoTrocaEnum &&
                Objects.equals(paginate, produto.paginate);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), idCodProduto, nome, tipoUnidade, grupoProduto, cf, fornecedor, valorComissao, peso, percDesconto, quantidadeMinVenda, quantidadeEstoqueAtual, flgLiberadoVenda, codigoBarras, dataAlteracao, situacao, tipoTroca, st, qtdeLimitVenda, precoVenda, qtdeProduto, qtdeTroca, desconto, vlUnitario, impostos, rotas, metasVendedores, periodoDescricao, idGrupoProdutoList, dataAlteracaoFormatada, flExcluido, tipoUnidadeEnum, tipoTrocaEnum, paginate);
    }
    
    @Override
    public String toString() {
        return "Produto{" +
                "idCodProduto='" + idCodProduto + '\'' +
                ", nome='" + nome + '\'' +
                ", tipoUnidade='" + tipoUnidade + '\'' +
                ", grupoProduto=" + grupoProduto +
                ", cf='" + cf + '\'' +
                ", fornecedor=" + fornecedor +
                ", valorComissao=" + valorComissao +
                ", peso=" + peso +
                ", percDesconto=" + percDesconto +
                ", quantidadeMinVenda=" + quantidadeMinVenda +
                ", quantidadeEstoqueAtual=" + quantidadeEstoqueAtual +
                ", flgLiberadoVenda='" + flgLiberadoVenda + '\'' +
                ", codigoBarras='" + codigoBarras + '\'' +
                ", dataAlteracao=" + dataAlteracao +
                ", situacao=" + situacao +
                ", tipoTroca='" + tipoTroca + '\'' +
                ", st='" + st + '\'' +
                ", qtdeLimitVenda=" + qtdeLimitVenda +
                ", precoVenda=" + precoVenda +
                ", qtdeProduto=" + qtdeProduto +
                ", qtdeTroca=" + qtdeTroca +
                ", desconto=" + desconto +
                ", vlUnitario=" + vlUnitario +
                ", impostos=" + impostos +
                ", rotas=" + rotas +
                ", metasVendedores=" + metasVendedores +
                ", periodoDescricao='" + periodoDescricao + '\'' +
                ", idGrupoProdutoList=" + idGrupoProdutoList +
                ", dataAlteracaoFormatada='" + dataAlteracaoFormatada + '\'' +
                ", flExcluido=" + flExcluido +
                ", tipoUnidadeEnum=" + tipoUnidadeEnum +
                ", tipoTrocaEnum=" + tipoTrocaEnum +
                ", paginate=" + paginate +
                '}';
    }
    
    
}
