package br.gerbellait.sistema.entity;

import java.util.Date;

import br.gerbellait.arquitetura.entities.AbstractEntity;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;

public class Rota extends AbstractEntity {
	private static final long serialVersionUID = 1L;
	private String descricao;
	private Date dataAlteracao;
	private Integer flExcluido;

	private transient ExcluidoEnum situacao;

	public Rota() {
		// empty
	}

	public Rota(Long id) {
		this.setId(id);
	}

	public Rota(Long id, String descricao) {
		this.setId(id);
		this.setDescricao(descricao);
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDataAlteracao() {
		return dataAlteracao;
	}

	public void setDataAlteracao(Date dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

	public Integer getFlExcluido() {
		return flExcluido;
	}

	public void setFlExcluido(Integer flExcluido) {
		this.flExcluido = flExcluido;
	}

	public ExcluidoEnum getSituacao() {
		if (getFlExcluido() != null) {
			situacao = ExcluidoEnum.getById(getFlExcluido());
		}
		return situacao;
	}

	public void setSituacao(ExcluidoEnum situacao) {
		this.situacao = situacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((dataAlteracao == null) ? 0 : dataAlteracao.hashCode());
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((flExcluido == null) ? 0 : flExcluido.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rota other = (Rota) obj;
		if (dataAlteracao == null) {
			if (other.dataAlteracao != null)
				return false;
		} else if (!dataAlteracao.equals(other.dataAlteracao))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (flExcluido == null) {
			if (other.flExcluido != null)
				return false;
		} else if (!flExcluido.equals(other.flExcluido))
			return false;
		return true;
	}

}