package br.gerbellait.sistema.entity;

import java.util.Date;

import br.gerbellait.arquitetura.entities.AbstractEntity;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.util.DateUtil;
import br.gerbellait.sistema.util.Paginate;

public class Msn extends AbstractEntity {
	private static final long serialVersionUID = 1L;
	private String msg;
	private String responsavel;
	private Date data;
	private String destinatario;
	private Date dataAlteracao;
	private Integer situacao;

	private transient String dataFormatada;
	private transient String dataAlteracaoFormatada;
	private transient ExcluidoEnum flExcluido;
	private transient Paginate paginate;

	public Msn() {
		// empty
	}

	public Msn(Long id) {
		this.setId(id);
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	public Date getDataAlteracao() {
		return dataAlteracao;
	}

	public void setDataAlteracao(Date dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

	public Integer getSituacao() {
		return situacao;
	}

	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}

	public String getDataFormatada() {
		this.dataFormatada = DateUtil.dateToFormatDefault(this.getData());
		return this.dataFormatada;
	}

	public void setDataFormatada(String dataFormatada) {
		this.dataFormatada = dataFormatada;
	}

	public String getDataAlteracaoFormatada() {
		this.dataAlteracaoFormatada = DateUtil.dateToFormatDefault(this.getDataAlteracao());
		return this.dataAlteracaoFormatada;
	}

	public void setDataAlteracaoFormatada(String dataAlteracaoFormatada) {
		this.dataAlteracaoFormatada = dataAlteracaoFormatada;
	}

	public ExcluidoEnum getFlExcluido() {
		if (flExcluido == null) {
			flExcluido = ExcluidoEnum.ATIVO;
		}
		if (getSituacao() != null) {
			flExcluido = ExcluidoEnum.getById(getSituacao());
		} else {
			situacao = flExcluido.getId();
		}
		return flExcluido;
	}

	public void setFlExcluido(ExcluidoEnum flExcluido) {
		this.flExcluido = flExcluido;
	}

	public Paginate getPaginate() {
		if (paginate == null) {
			paginate = new Paginate();
		}
		return paginate;
	}

	public void setPaginate(Paginate paginate) {
		this.paginate = paginate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((dataAlteracao == null) ? 0 : dataAlteracao.hashCode());
		result = prime * result + ((destinatario == null) ? 0 : destinatario.hashCode());
		result = prime * result + ((msg == null) ? 0 : msg.hashCode());
		result = prime * result + ((responsavel == null) ? 0 : responsavel.hashCode());
		result = prime * result + ((situacao == null) ? 0 : situacao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Msn other = (Msn) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (dataAlteracao == null) {
			if (other.dataAlteracao != null)
				return false;
		} else if (!dataAlteracao.equals(other.dataAlteracao))
			return false;
		if (destinatario == null) {
			if (other.destinatario != null)
				return false;
		} else if (!destinatario.equals(other.destinatario))
			return false;
		if (msg == null) {
			if (other.msg != null)
				return false;
		} else if (!msg.equals(other.msg))
			return false;
		if (responsavel == null) {
			if (other.responsavel != null)
				return false;
		} else if (!responsavel.equals(other.responsavel))
			return false;
		if (situacao == null) {
			if (other.situacao != null)
				return false;
		} else if (!situacao.equals(other.situacao))
			return false;
		return true;
	}
}