package br.gerbellait.sistema.entity;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.gerbellait.arquitetura.entities.AbstractEntity;
import br.gerbellait.arquitetura.util.Utils;
import br.gerbellait.sistema.bean.UltimasVendasBean;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.dao.enumerador.GeraNfEnum;
import br.gerbellait.sistema.dao.enumerador.StatusPedidoEnum;
import br.gerbellait.sistema.util.DateUtil;
import br.gerbellait.sistema.util.Paginate;

public class Pedido extends AbstractEntity {
    private static final long serialVersionUID = 1L;
    private Long idPedidoCliente;
    private Pessoa cliente;
    private Vendedor vendedor;
    private FormaPagamento formaPagamento;
    private String situacaoPedido;
    private BigDecimal valorDesconto;
    private Date dataPedido;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date dataVencimento;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date dataEntrega;
    private String flGeraNf;
    private String observacao;
    private Date dataAlteracao;
    private Integer situacao;
    private BigDecimal valorPedido;
    private String usuario;
    private List<Produto> produtoList;
    private Long idCodCliente;
    private boolean hasLoadMap;
    private List<UltimasVendasBean> ultimasVendas;
    
    private transient BigDecimal TotalVenda;
    private transient BigDecimal TotalTroca;
    private transient BigDecimal TotalLiquido;
    private transient String nomeCliente;
    private transient String nomeVendedor;
    private transient String descricaoFormaPagamento;
    private transient Usuario usuarioLogado;
    private transient Date dataPeriodoVendaIni;
    private transient Date dataPeriodoVendaFim;
    private transient Date dataPeriodoEntregaIni;
    private transient Date dataPeriodoEntregaFim;
    private transient Long idProduto;
    private transient Long idFornecedor;
    private transient ExcluidoEnum flExcluido;
    private transient StatusPedidoEnum flgStatus;
    private transient GeraNfEnum geraNfEnum;
    private transient String dataAlteracaoFormatada;
    private transient Paginate paginate;
    
    public Pedido() {
        // empty
    }
    
    public Pedido(Long id) {
        this.setId(id);
    }
    
    public BigDecimal getTotalVenda() {
        return TotalVenda;
    }
    
    public void setTotalVenda(BigDecimal totalVenda) {
        TotalVenda = totalVenda;
    }
    
    public BigDecimal getTotalTroca() {
        return TotalTroca;
    }
    
    public void setTotalTroca(BigDecimal totalTroca) {
        TotalTroca = totalTroca;
    }
    
    public BigDecimal getTotalLiquido() {
        return TotalLiquido;
    }
    
    public void setTotalLiquido(BigDecimal totalLiquido) {
        TotalLiquido = totalLiquido;
    }
    
    public String getNomeCliente() {
        return nomeCliente;
    }
    
    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }
    
    public String getNomeVendedor() {
        return nomeVendedor;
    }
    
    public void setNomeVendedor(String nomeVendedor) {
        this.nomeVendedor = nomeVendedor;
    }
    
    public String getDescricaoFormaPagamento() {
        return descricaoFormaPagamento;
    }
    
    public void setDescricaoFormaPagamento(String descricaoFormaPagamento) {
        this.descricaoFormaPagamento = descricaoFormaPagamento;
    }
    
    public Usuario getUsuarioLogado() {
        return usuarioLogado;
    }
    
    public void setUsuarioLogado(Usuario usuarioLogado) {
        this.usuarioLogado = usuarioLogado;
    }
    
    public Date getDataPeriodoVendaIni() {
        return dataPeriodoVendaIni;
    }
    
    public void setDataPeriodoVendaIni(Date dataPeriodoVendaIni) {
        this.dataPeriodoVendaIni = dataPeriodoVendaIni;
    }
    
    public Date getDataPeriodoVendaFim() {
        return dataPeriodoVendaFim;
    }
    
    public void setDataPeriodoVendaFim(Date dataPeriodoVendaFim) {
        this.dataPeriodoVendaFim = dataPeriodoVendaFim;
    }
    
    public Date getDataPeriodoEntregaIni() {
        return dataPeriodoEntregaIni;
    }
    
    public void setDataPeriodoEntregaIni(Date dataPeriodoEntregaIni) {
        this.dataPeriodoEntregaIni = dataPeriodoEntregaIni;
    }
    
    public Date getDataPeriodoEntregaFim() {
        return dataPeriodoEntregaFim;
    }
    
    public void setDataPeriodoEntregaFim(Date dataPeriodoEntregaFim) {
        this.dataPeriodoEntregaFim = dataPeriodoEntregaFim;
    }
    
    public Long getIdFornecedor() {
        return idFornecedor;
    }
    
    public void setIdFornecedor(Long idFornecedor) {
        this.idFornecedor = idFornecedor;
    }
    
    public Long getIdProduto() {
        return idProduto;
    }
    
    public void setIdProduto(Long idProduto) {
        this.idProduto = idProduto;
    }
    
    public Long getIdPedidoCliente() {
        return idPedidoCliente;
    }
    
    public void setIdPedidoCliente(Long idPedidoCliente) {
        this.idPedidoCliente = idPedidoCliente;
    }
    
    public Pessoa getCliente() {
        return cliente;
    }
    
    public void setCliente(Pessoa cliente) {
        this.cliente = cliente;
    }
    
    public Vendedor getVendedor() {
        return vendedor;
    }
    
    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }
    
    public FormaPagamento getFormaPagamento() {
        return formaPagamento;
    }
    
    public void setFormaPagamento(FormaPagamento formaPagamento) {
        this.formaPagamento = formaPagamento;
    }
    
    public String getSituacaoPedido() {
        return situacaoPedido;
    }
    
    public void setSituacaoPedido(String situacaoPedido) {
        this.situacaoPedido = situacaoPedido;
    }
    
    public BigDecimal getValorDesconto() {
        return valorDesconto;
    }
    
    public void setValorDesconto(BigDecimal valorDesconto) {
        this.valorDesconto = valorDesconto;
    }
    
    public Date getDataPedido() {
        return dataPedido;
    }
    
    public void setDataPedido(Date dataPedido) {
        this.dataPedido = dataPedido;
    }
    
    public Date getDataVencimento() {
        return dataVencimento;
    }
    
    public void setDataVencimento(Date dataVencimento) {
        this.dataVencimento = dataVencimento;
    }
    
    public Date getDataEntrega() {
        return dataEntrega;
    }
    
    public void setDataEntrega(Date dataEntrega) {
        this.dataEntrega = dataEntrega;
    }
    
    public String getFlGeraNf() {
        return flGeraNf;
    }
    
    public void setFlGeraNf(String flGeraNf) {
        this.flGeraNf = flGeraNf;
    }
    
    public String getObservacao() {
        return observacao;
    }
    
    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
    
    public Date getDataAlteracao() {
        return dataAlteracao;
    }
    
    public void setDataAlteracao(Date dataAlteracao) {
        this.dataAlteracao = dataAlteracao;
    }
    
    public Integer getSituacao() {
        return situacao;
    }
    
    public void setSituacao(Integer situacao) {
        this.situacao = situacao;
    }
    
    public BigDecimal getValorPedido() {
        return valorPedido;
    }
    
    public void setValorPedido(BigDecimal valorPedido) {
        this.valorPedido = valorPedido;
    }
    
    public String getUsuario() {
        return usuario;
    }
    
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    public ExcluidoEnum getFlExcluido() {
        if (flExcluido == null) {
            flExcluido = ExcluidoEnum.ATIVO;
        }
        if (getSituacao() != null) {
            flExcluido = ExcluidoEnum.getById(getSituacao());
        } else {
            situacao = flExcluido.getId();
        }
        return flExcluido;
    }
    
    public void setFlExcluido(ExcluidoEnum flExcluido) {
        this.flExcluido = flExcluido;
    }
    
    public StatusPedidoEnum getFlgStatus() {
        if (!Utils.isNullOrEmpty(this.getSituacaoPedido())) {
            flgStatus = StatusPedidoEnum.getById(this.getSituacaoPedido());
        }
        return flgStatus;
    }
    
    public void setFlgStatus(StatusPedidoEnum flgStatus) {
        this.flgStatus = flgStatus;
    }
    
    public GeraNfEnum getGeraNfEnum() {
        if (!Utils.isNullOrEmpty(this.getFlGeraNf())) {
            geraNfEnum = GeraNfEnum.getById(this.getFlGeraNf());
        }
        return geraNfEnum;
    }
    
    public void setGeraNfEnum(GeraNfEnum geraNfEnum) {
        this.geraNfEnum = geraNfEnum;
    }
    
    public String getDataAlteracaoFormatada() {
        this.dataAlteracaoFormatada = DateUtil.dateToFormatDefault(this.getDataAlteracao());
        return this.dataAlteracaoFormatada;
    }
    
    public void setDataAlteracaoFormatada(String dataAlteracaoFormatada) {
        this.dataAlteracaoFormatada = dataAlteracaoFormatada;
    }
    
    public List<Produto> getProdutoList() {
        if (produtoList == null) {
            return Collections.emptyList();
        }
        return produtoList;
    }
    
    public void setProdutoList(List<Produto> produtoList) {
        this.produtoList = produtoList;
    }
    
    public Paginate getPaginate() {
        return paginate;
    }
    
    public void setPaginate(Paginate paginate) {
        this.paginate = paginate;
    }
    
    public Long getIdCodCliente() {
        return idCodCliente;
    }
    
    public void setIdCodCliente(Long idCodCliente) {
        this.idCodCliente = idCodCliente;
    }
    
    public boolean isHasLoadMap() {
        return hasLoadMap;
    }
    
    public void setHasLoadMap(boolean hasLoadMap) {
        this.hasLoadMap = hasLoadMap;
    }
    
    public List<UltimasVendasBean> getUltimasVendas() {
        return ultimasVendas;
    }
    
    public void setUltimasVendas(List<UltimasVendasBean> ultimasVendas) {
        this.ultimasVendas = ultimasVendas;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Pedido pedido = (Pedido) o;
        return hasLoadMap == pedido.hasLoadMap &&
                Objects.equals(idPedidoCliente, pedido.idPedidoCliente) &&
                Objects.equals(cliente, pedido.cliente) &&
                Objects.equals(vendedor, pedido.vendedor) &&
                Objects.equals(formaPagamento, pedido.formaPagamento) &&
                Objects.equals(situacaoPedido, pedido.situacaoPedido) &&
                Objects.equals(valorDesconto, pedido.valorDesconto) &&
                Objects.equals(dataPedido, pedido.dataPedido) &&
                Objects.equals(dataVencimento, pedido.dataVencimento) &&
                Objects.equals(dataEntrega, pedido.dataEntrega) &&
                Objects.equals(flGeraNf, pedido.flGeraNf) &&
                Objects.equals(observacao, pedido.observacao) &&
                Objects.equals(dataAlteracao, pedido.dataAlteracao) &&
                Objects.equals(situacao, pedido.situacao) &&
                Objects.equals(valorPedido, pedido.valorPedido) &&
                Objects.equals(usuario, pedido.usuario) &&
                Objects.equals(produtoList, pedido.produtoList) &&
                Objects.equals(idCodCliente, pedido.idCodCliente) &&
                Objects.equals(TotalVenda, pedido.TotalVenda) &&
                Objects.equals(TotalTroca, pedido.TotalTroca) &&
                Objects.equals(TotalLiquido, pedido.TotalLiquido) &&
                Objects.equals(nomeCliente, pedido.nomeCliente) &&
                Objects.equals(nomeVendedor, pedido.nomeVendedor) &&
                Objects.equals(descricaoFormaPagamento, pedido.descricaoFormaPagamento) &&
                Objects.equals(usuarioLogado, pedido.usuarioLogado) &&
                Objects.equals(dataPeriodoVendaIni, pedido.dataPeriodoVendaIni) &&
                Objects.equals(dataPeriodoVendaFim, pedido.dataPeriodoVendaFim) &&
                Objects.equals(dataPeriodoEntregaIni, pedido.dataPeriodoEntregaIni) &&
                Objects.equals(dataPeriodoEntregaFim, pedido.dataPeriodoEntregaFim) &&
                Objects.equals(idProduto, pedido.idProduto) &&
                Objects.equals(idFornecedor, pedido.idFornecedor) &&
                flExcluido == pedido.flExcluido &&
                flgStatus == pedido.flgStatus &&
                geraNfEnum == pedido.geraNfEnum &&
                Objects.equals(dataAlteracaoFormatada, pedido.dataAlteracaoFormatada) &&
                Objects.equals(paginate, pedido.paginate);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), idPedidoCliente, cliente, vendedor, formaPagamento, situacaoPedido, valorDesconto, dataPedido, dataVencimento, dataEntrega, flGeraNf, observacao, dataAlteracao, situacao, valorPedido, usuario, produtoList, idCodCliente, hasLoadMap, TotalVenda, TotalTroca, TotalLiquido, nomeCliente, nomeVendedor, descricaoFormaPagamento, usuarioLogado, dataPeriodoVendaIni, dataPeriodoVendaFim, dataPeriodoEntregaIni, dataPeriodoEntregaFim, idProduto, idFornecedor, flExcluido, flgStatus, geraNfEnum, dataAlteracaoFormatada, paginate);
    }
    
    @Override
    public String toString() {
        return "Pedido{" +
                "idPedidoCliente=" + idPedidoCliente +
                ", cliente=" + cliente +
                ", vendedor=" + vendedor +
                ", formaPagamento=" + formaPagamento +
                ", situacaoPedido='" + situacaoPedido + '\'' +
                ", valorDesconto=" + valorDesconto +
                ", dataPedido=" + dataPedido +
                ", dataVencimento=" + dataVencimento +
                ", dataEntrega=" + dataEntrega +
                ", flGeraNf='" + flGeraNf + '\'' +
                ", observacao='" + observacao + '\'' +
                ", dataAlteracao=" + dataAlteracao +
                ", situacao=" + situacao +
                ", valorPedido=" + valorPedido +
                ", usuario='" + usuario + '\'' +
                ", produtoList=" + produtoList +
                ", idCodCliente=" + idCodCliente +
                ", hasLoadMap=" + hasLoadMap +
                ", TotalVenda=" + TotalVenda +
                ", TotalTroca=" + TotalTroca +
                ", TotalLiquido=" + TotalLiquido +
                ", nomeCliente='" + nomeCliente + '\'' +
                ", nomeVendedor='" + nomeVendedor + '\'' +
                ", descricaoFormaPagamento='" + descricaoFormaPagamento + '\'' +
                ", usuarioLogado=" + usuarioLogado +
                ", dataPeriodoVendaIni=" + dataPeriodoVendaIni +
                ", dataPeriodoVendaFim=" + dataPeriodoVendaFim +
                ", dataPeriodoEntregaIni=" + dataPeriodoEntregaIni +
                ", dataPeriodoEntregaFim=" + dataPeriodoEntregaFim +
                ", idProduto=" + idProduto +
                ", idFornecedor=" + idFornecedor +
                ", flExcluido=" + flExcluido +
                ", flgStatus=" + flgStatus +
                ", geraNfEnum=" + geraNfEnum +
                ", dataAlteracaoFormatada='" + dataAlteracaoFormatada + '\'' +
                ", paginate=" + paginate +
                '}';
    }
    
}
