package br.gerbellait.sistema.entity;

import java.util.Date;
import java.util.Objects;

import br.gerbellait.arquitetura.entities.AbstractEntity;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;

public class PedidoProduto extends AbstractEntity {
    
    private static final long serialVersionUID = 5255037561642211732L;
    private Long pedidoProdutoId;
    private Long pedidoId;
    private Long produtoId;
    private int qtdeProduto;
    private Double valorUnitario;
    private Double valorTotal;
    private int qtdeTroca;
    private Double valorTroca;
    private Double desconto;
    private Date alteracao;
    private ExcluidoEnum excluido;
    private String usuario;
    private Date baixaEstoque;
    private Date atualizacaoQtde;
    
    public Long getPedidoProdutoId() {
        return pedidoProdutoId;
    }
    
    public void setPedidoProdutoId(Long pedidoProdutoId) {
        this.pedidoProdutoId = pedidoProdutoId;
    }
    
    public Long getPedidoId() {
        return pedidoId;
    }
    
    public void setPedidoId(Long pedidoId) {
        this.pedidoId = pedidoId;
    }
    
    public Long getProdutoId() {
        return produtoId;
    }
    
    public void setProdutoId(Long produtoId) {
        this.produtoId = produtoId;
    }
    
    public int getQtdeProduto() {
        return qtdeProduto;
    }
    
    public void setQtdeProduto(int qtdeProduto) {
        this.qtdeProduto = qtdeProduto;
    }
    
    public Double getValorUnitario() {
        return valorUnitario;
    }
    
    public void setValorUnitario(Double valorUnitario) {
        this.valorUnitario = valorUnitario;
    }
    
    public Double getValorTotal() {
        return valorTotal;
    }
    
    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }
    
    public int getQtdeTroca() {
        return qtdeTroca;
    }
    
    public void setQtdeTroca(int qtdeTroca) {
        this.qtdeTroca = qtdeTroca;
    }
    
    public Double getValorTroca() {
        return valorTroca;
    }
    
    public void setValorTroca(Double valorTroca) {
        this.valorTroca = valorTroca;
    }
    
    public Double getDesconto() {
        return desconto;
    }
    
    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }
    
    public Date getAlteracao() {
        return alteracao;
    }
    
    public void setAlteracao(Date alteracao) {
        this.alteracao = alteracao;
    }
    
    public ExcluidoEnum getExcluido() {
        return excluido;
    }
    
    public void setExcluido(ExcluidoEnum excluido) {
        this.excluido = excluido;
    }
    
    public String getUsuario() {
        return usuario;
    }
    
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    public Date getBaixaEstoque() {
        return baixaEstoque;
    }
    
    public void setBaixaEstoque(Date baixaEstoque) {
        this.baixaEstoque = baixaEstoque;
    }
    
    public Date getAtualizacaoQtde() {
        return atualizacaoQtde;
    }
    
    public void setAtualizacaoQtde(Date atualizacaoQtde) {
        this.atualizacaoQtde = atualizacaoQtde;
    }
    
    @Override
    public String toString() {
        return "PedidoProduto{" +
                "pedidoProdutoId=" + pedidoProdutoId +
                ", pedidoId=" + pedidoId +
                ", produtoId=" + produtoId +
                ", qtdeProduto=" + qtdeProduto +
                ", valorUnitario=" + valorUnitario +
                ", valorTotal=" + valorTotal +
                ", qtdeTroca=" + qtdeTroca +
                ", valorTroca=" + valorTroca +
                ", desconto=" + desconto +
                ", alteracao=" + alteracao +
                ", excluido=" + excluido +
                ", usuario='" + usuario + '\'' +
                ", baixaEstoque=" + baixaEstoque +
                ", atualizacaoQtde=" + atualizacaoQtde +
                '}';
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PedidoProduto that = (PedidoProduto) o;
        return qtdeProduto == that.qtdeProduto &&
                qtdeTroca == that.qtdeTroca &&
                Objects.equals(pedidoProdutoId, that.pedidoProdutoId) &&
                Objects.equals(pedidoId, that.pedidoId) &&
                Objects.equals(produtoId, that.produtoId) &&
                Objects.equals(valorUnitario, that.valorUnitario) &&
                Objects.equals(valorTotal, that.valorTotal) &&
                Objects.equals(valorTroca, that.valorTroca) &&
                Objects.equals(desconto, that.desconto) &&
                Objects.equals(alteracao, that.alteracao) &&
                excluido == that.excluido &&
                Objects.equals(usuario, that.usuario) &&
                Objects.equals(baixaEstoque, that.baixaEstoque) &&
                Objects.equals(atualizacaoQtde, that.atualizacaoQtde);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), pedidoProdutoId, pedidoId, produtoId, qtdeProduto, valorUnitario, valorTotal, qtdeTroca, valorTroca, desconto, alteracao, excluido, usuario, baixaEstoque, atualizacaoQtde);
    }
}
