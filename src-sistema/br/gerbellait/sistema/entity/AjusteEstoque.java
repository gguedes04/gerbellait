package br.gerbellait.sistema.entity;

import java.math.BigDecimal;
import java.util.Date;

import br.gerbellait.arquitetura.entities.AbstractEntity;
import br.gerbellait.sistema.util.DateUtil;
import br.gerbellait.sistema.util.Paginate;

public class AjusteEstoque extends AbstractEntity {
	private static final long serialVersionUID = 1L;

	private Produto produto;
	private String tpRubrica;
	private Date dataAjusteEstoque;
	private BigDecimal quantidadeAjusteEstoque;
	private String observacao;
	private String tipoAjusteEstoque;
	private String idLocal;

	private transient Date dataInicial;
	private transient Date dataFinal;
	private transient String dataAjusteEstoqueFormatada;
	private transient Paginate paginate;

	public AjusteEstoque() {
		// empty
	}

	public AjusteEstoque(Long id) {
		this.setId(id);
	}

	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public String getTpRubrica() {
		return tpRubrica;
	}

	public void setTpRubrica(String tpRubrica) {
		this.tpRubrica = tpRubrica;
	}

	public Date getDataAjusteEstoque() {
		return dataAjusteEstoque;
	}

	public void setDataAjusteEstoque(Date dataAjusteEstoque) {
		this.dataAjusteEstoque = dataAjusteEstoque;
	}

	public BigDecimal getQuantidadeAjusteEstoque() {
		return quantidadeAjusteEstoque;
	}

	public void setQuantidadeAjusteEstoque(BigDecimal quantidadeAjusteEstoque) {
		this.quantidadeAjusteEstoque = quantidadeAjusteEstoque;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getTipoAjusteEstoque() {
		return tipoAjusteEstoque;
	}

	public void setTipoAjusteEstoque(String tipoAjusteEstoque) {
		this.tipoAjusteEstoque = tipoAjusteEstoque;
	}

	public String getIdLocal() {
		return idLocal;
	}

	public void setIdLocal(String idLocal) {
		this.idLocal = idLocal;
	}

	public String getDataAjusteEstoqueFormatada() {
		this.dataAjusteEstoqueFormatada = DateUtil.dateToFormatDefault(this.getDataAjusteEstoque());
		return this.dataAjusteEstoqueFormatada;
	}

	public Paginate getPaginate() {
		if (paginate == null) {
			paginate = new Paginate();
		}
		return paginate;
	}

	public void setPaginate(Paginate paginate) {
		this.paginate = paginate;
	}
}