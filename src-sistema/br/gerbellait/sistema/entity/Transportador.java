package br.gerbellait.sistema.entity;

import br.gerbellait.arquitetura.entities.AbstractEntity;
import br.gerbellait.sistema.util.Paginate;

public class Transportador extends AbstractEntity {
	private static final long serialVersionUID = 1L;
	private String nome;
	private String cnpjcpf;
	private String inscricao;
	private String endereco;
	private String bairro;
	private String cep;
	private String ibge;
	private String placa;
	private Uf uf;
	private String motorista;
	private Cidade cidade;
	private transient Paginate paginate;

	public Transportador() {
		// empty
	}

	public Transportador(Long id) {
		this.setId(id);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCnpjcpf() {
		return cnpjcpf;
	}

	public void setCnpjcpf(String cnpjcpf) {
		this.cnpjcpf = cnpjcpf;
	}

	public String getInscricao() {
		return inscricao;
	}

	public void setInscricao(String inscricao) {
		this.inscricao = inscricao;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getIbge() {
		return ibge;
	}

	public void setIbge(String ibge) {
		this.ibge = ibge;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Uf getUf() {
		return uf;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	public String getMotorista() {
		return motorista;
	}

	public void setMotorista(String motorista) {
		this.motorista = motorista;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public Paginate getPaginate() {
		if (paginate == null) {
			paginate = new Paginate();
		}
		return paginate;
	}

	public void setPaginate(Paginate paginate) {
		this.paginate = paginate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((cnpjcpf == null) ? 0 : cnpjcpf.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transportador other = (Transportador) obj;
		if (cnpjcpf == null) {
			if (other.cnpjcpf != null)
				return false;
		} else if (!cnpjcpf.equals(other.cnpjcpf))
			return false;
		return true;
	}
}