package br.gerbellait.sistema.entity;

import br.gerbellait.arquitetura.entities.AbstractEntity;

public class GrupoProduto extends AbstractEntity {
	private static final long serialVersionUID = 1L;
	private String descricao;

	public GrupoProduto() {
		// empty
	}

	public GrupoProduto(Long id) {
		this.setId(id);
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		GrupoProduto other = (GrupoProduto) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return this.getDescricao();
	}
}