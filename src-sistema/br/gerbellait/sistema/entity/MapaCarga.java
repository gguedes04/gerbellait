package br.gerbellait.sistema.entity;

import java.util.Date;
import java.util.Objects;

import br.gerbellait.arquitetura.entities.AbstractEntity;

public class MapaCarga extends AbstractEntity {
    
    private static final long serialVersionUID = -2616221910022440819L;
    private Long id;
    private Pedido pedido;
    private Long pedidocodecli;
    private Date dtPedido;
    private Date dtInclusaoMapaCarga;
    private Rota rota;
    private Long nrMapaCarga;
    
    @Override
    public Long getId() {
        return id;
    }
    
    @Override
    public void setId(Long id) {
        this.id = id;
    }
    
    public Pedido getPedido() {
        return pedido;
    }
    
    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }
    
    public Long getPedidoCodeCli() {
        return pedidocodecli;
    }
    
    public void setPedidoCodeCli(Long pedidocodecli) {
        this.pedidocodecli = pedidocodecli;
    }
    
    
    public Date getDtPedido() {
        return dtPedido;
    }
    
    public void setDtPedido(Date dtPedido) {
        this.dtPedido = dtPedido;
    }
    
    public Date getDtInclusaoMapaCarga() {
        return dtInclusaoMapaCarga;
    }
    
    public void setDtInclusaoMapaCarga(Date dtInclusaoMapaCarga) {
        this.dtInclusaoMapaCarga = dtInclusaoMapaCarga;
    }
    
    public Rota getRota() {
        return rota;
    }
    
    public void setRota(Rota rota) {
        this.rota = rota;
    }
    
    public Long getPedidocodecli() {
        return pedidocodecli;
    }
    
    public void setPedidocodecli(Long pedidocodecli) {
        this.pedidocodecli = pedidocodecli;
    }
    
    public Long getNrMapaCarga() {
        return nrMapaCarga;
    }
    
    public void setNrMapaCarga(Long nrMapaCarga) {
        this.nrMapaCarga = nrMapaCarga;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MapaCarga mapaCarga = (MapaCarga) o;
        return Objects.equals(id, mapaCarga.id) &&
                Objects.equals(pedido, mapaCarga.pedido) &&
                Objects.equals(pedidocodecli, mapaCarga.pedidocodecli) &&
                Objects.equals(dtPedido, mapaCarga.dtPedido) &&
                Objects.equals(dtInclusaoMapaCarga, mapaCarga.dtInclusaoMapaCarga) &&
                Objects.equals(rota, mapaCarga.rota) &&
                Objects.equals(nrMapaCarga, mapaCarga.nrMapaCarga);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, pedido, pedidocodecli, dtPedido, dtInclusaoMapaCarga, rota, nrMapaCarga);
    }
    
    @Override
    public String toString() {
        return "MapaCarga{" +
                "id=" + id +
                ", pedido=" + pedido +
                ", pedidocodecli=" + pedidocodecli +
                ", dtPedido=" + dtPedido +
                ", dtInclusaoMapaCarga=" + dtInclusaoMapaCarga +
                ", rota=" + rota +
                ", nrMapaCarga=" + nrMapaCarga +
                '}';
    }
    
}
