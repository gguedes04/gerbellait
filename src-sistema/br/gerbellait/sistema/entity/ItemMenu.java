package br.gerbellait.sistema.entity;

import br.gerbellait.arquitetura.entities.AbstractEntity;

public class ItemMenu extends AbstractEntity {
	private static final long serialVersionUID = 1L;
	private String nome;
	private ItemMenu menuPai;

	public ItemMenu() {
		// empty
	}

	public ItemMenu(Long id) {
		this.setId(id);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public ItemMenu getMenuPai() {
		return menuPai;
	}

	public void setMenuPai(ItemMenu menuPai) {
		this.menuPai = menuPai;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemMenu other = (ItemMenu) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}
}