package br.gerbellait.sistema.entity;

import java.math.BigDecimal;

import br.gerbellait.arquitetura.entities.AbstractEntity;

public class ProdutoImposto extends AbstractEntity {
	private static final long serialVersionUID = 1L;

	private Produto produto;
	private BigDecimal icmsEntrada;
	private BigDecimal icmsSaida;
	private BigDecimal mva;
	private String uf;
	private String flgPossuiSt;
	private BigDecimal percentualSimples;

	// excluir, alterar, remover, novo
	private transient String acao;

	public ProdutoImposto() {
		// empty
	}

	public ProdutoImposto(Long id) {
		this.setId(id);
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public BigDecimal getIcmsEntrada() {
		return icmsEntrada;
	}

	public void setIcmsEntrada(BigDecimal icmsEntrada) {
		this.icmsEntrada = icmsEntrada;
	}

	public BigDecimal getIcmsSaida() {
		return icmsSaida;
	}

	public void setIcmsSaida(BigDecimal icmsSaida) {
		this.icmsSaida = icmsSaida;
	}

	public BigDecimal getMva() {
		return mva;
	}

	public void setMva(BigDecimal mva) {
		this.mva = mva;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getFlgPossuiSt() {
		return flgPossuiSt;
	}

	public void setFlgPossuiSt(String flgPossuiSt) {
		this.flgPossuiSt = flgPossuiSt;
	}

	public BigDecimal getPercentualSimples() {
		return percentualSimples;
	}

	public void setPercentualSimples(BigDecimal percentualSimples) {
		this.percentualSimples = percentualSimples;
	}

	public String getAcao() {
		return acao;
	}

	public void setAcao(String acao) {
		this.acao = acao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((flgPossuiSt == null) ? 0 : flgPossuiSt.hashCode());
		result = prime * result + ((icmsEntrada == null) ? 0 : icmsEntrada.hashCode());
		result = prime * result + ((icmsSaida == null) ? 0 : icmsSaida.hashCode());
		result = prime * result + ((mva == null) ? 0 : mva.hashCode());
		result = prime * result + ((percentualSimples == null) ? 0 : percentualSimples.hashCode());
		result = prime * result + ((produto == null) ? 0 : produto.hashCode());
		result = prime * result + ((uf == null) ? 0 : uf.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProdutoImposto other = (ProdutoImposto) obj;
		if (flgPossuiSt == null) {
			if (other.flgPossuiSt != null)
				return false;
		} else if (!flgPossuiSt.equals(other.flgPossuiSt))
			return false;
		if (icmsEntrada == null) {
			if (other.icmsEntrada != null)
				return false;
		} else if (!icmsEntrada.equals(other.icmsEntrada))
			return false;
		if (icmsSaida == null) {
			if (other.icmsSaida != null)
				return false;
		} else if (!icmsSaida.equals(other.icmsSaida))
			return false;
		if (mva == null) {
			if (other.mva != null)
				return false;
		} else if (!mva.equals(other.mva))
			return false;
		if (percentualSimples == null) {
			if (other.percentualSimples != null)
				return false;
		} else if (!percentualSimples.equals(other.percentualSimples))
			return false;
		if (produto == null) {
			if (other.produto != null)
				return false;
		} else if (!produto.equals(other.produto))
			return false;
		if (uf == null) {
			if (other.uf != null)
				return false;
		} else if (!uf.equals(other.uf))
			return false;
		return true;
	}

}