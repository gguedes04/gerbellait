package br.gerbellait.sistema.entity;

import java.util.Date;

import br.gerbellait.arquitetura.entities.AbstractEntity;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.util.DateUtil;

public class PeriodoMeta extends AbstractEntity {
	private static final long serialVersionUID = 1L;

	private Date dataInicialMeta;
	private Date dataFinalMeta;
	private Date dataAlteracao;
	private Integer situacao;

	// excluir, alterar, remover, novo
	private transient String acao;

	private transient String dataAlteracaoFormatada;
	private transient ExcluidoEnum flExcluido;

	public PeriodoMeta() {
		// empty
	}

	public PeriodoMeta(Long id) {
		this.setId(id);
	}

	public Date getDataInicialMeta() {
		return dataInicialMeta;
	}

	public void setDataInicialMeta(Date dataInicialMeta) {
		this.dataInicialMeta = dataInicialMeta;
	}

	public Date getDataFinalMeta() {
		return dataFinalMeta;
	}

	public void setDataFinalMeta(Date dataFinalMeta) {
		this.dataFinalMeta = dataFinalMeta;
	}

	public Date getDataAlteracao() {
		return dataAlteracao;
	}

	public void setDataAlteracao(Date dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

	public Integer getSituacao() {
		return situacao;
	}

	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}

	public String getAcao() {
		return acao;
	}

	public void setAcao(String acao) {
		this.acao = acao;
	}

	public String getDataAlteracaoFormatada() {
		this.dataAlteracaoFormatada = DateUtil.dateToFormatDefault(this.getDataAlteracao());
		return this.dataAlteracaoFormatada;
	}

	public void setDataAlteracaoFormatada(String dataAlteracaoFormatada) {
		this.dataAlteracaoFormatada = dataAlteracaoFormatada;
	}

	public ExcluidoEnum getFlExcluido() {
		if (flExcluido == null) {
			flExcluido = ExcluidoEnum.ATIVO;
		}
		if (getSituacao() != null) {
			flExcluido = ExcluidoEnum.getById(getSituacao());
		} else {
			situacao = flExcluido.getId();
		}
		return flExcluido;
	}

	public void setFlExcluido(ExcluidoEnum flExcluido) {
		this.flExcluido = flExcluido;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((dataAlteracao == null) ? 0 : dataAlteracao.hashCode());
		result = prime * result + ((dataFinalMeta == null) ? 0 : dataFinalMeta.hashCode());
		result = prime * result + ((dataInicialMeta == null) ? 0 : dataInicialMeta.hashCode());
		result = prime * result + ((situacao == null) ? 0 : situacao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PeriodoMeta other = (PeriodoMeta) obj;
		if (dataAlteracao == null) {
			if (other.dataAlteracao != null)
				return false;
		} else if (!dataAlteracao.equals(other.dataAlteracao))
			return false;
		if (dataFinalMeta == null) {
			if (other.dataFinalMeta != null)
				return false;
		} else if (!dataFinalMeta.equals(other.dataFinalMeta))
			return false;
		if (dataInicialMeta == null) {
			if (other.dataInicialMeta != null)
				return false;
		} else if (!dataInicialMeta.equals(other.dataInicialMeta))
			return false;
		if (situacao == null) {
			if (other.situacao != null)
				return false;
		} else if (!situacao.equals(other.situacao))
			return false;
		return true;
	}
}