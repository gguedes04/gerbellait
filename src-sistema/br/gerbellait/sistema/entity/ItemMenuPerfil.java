package br.gerbellait.sistema.entity;

import br.gerbellait.arquitetura.entities.AbstractEntity;

public class ItemMenuPerfil extends AbstractEntity {
	private static final long serialVersionUID = 1L;
	private Perfil perfil;
	private ItemMenu itemMenu;

	public ItemMenuPerfil() {
		// empty
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public ItemMenu getItemMenu() {
		return itemMenu;
	}

	public void setItemMenu(ItemMenu itemMenu) {
		this.itemMenu = itemMenu;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((itemMenu == null) ? 0 : itemMenu.hashCode());
		result = prime * result + ((perfil == null) ? 0 : perfil.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemMenuPerfil other = (ItemMenuPerfil) obj;
		if (itemMenu == null) {
			if (other.itemMenu != null)
				return false;
		} else if (!itemMenu.equals(other.itemMenu))
			return false;
		if (perfil == null) {
			if (other.perfil != null)
				return false;
		} else if (!perfil.equals(other.perfil))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return this.getItemMenu().getNome();
	}
}