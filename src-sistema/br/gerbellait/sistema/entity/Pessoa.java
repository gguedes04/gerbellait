package br.gerbellait.sistema.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import br.gerbellait.arquitetura.entities.AbstractEntity;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.dao.enumerador.RegimeTributarioEnum;
import br.gerbellait.sistema.util.DateUtil;
import br.gerbellait.sistema.util.Paginate;

public class Pessoa extends AbstractEntity {
	private static final long serialVersionUID = 1L;
	private Long idCodPessoa;
	private TipoPessoa tipoPessoa;
	private String nome;
	private String nomeFantasia;
	private String cnpj;
	private String inscricaoEstadual;
	private Integer prazo;
	private FormaPagamento formaPagamento;
	private Integer idRota;
	private Long idVendedorRepresentante;
	private String cnae;
	private String flgOptanteSimples;
	private String nomeContato;
	private BigDecimal percDescPadrao;
	private String nomeRepresentante;
	private String email;
	private Date dataAlteracao;
	private Integer situacao;
	private String observacao;
	private String idTipoEmpresa;
	private List<Endereco> enderecos;
	private String idRegimeTributario;

	private transient ExcluidoEnum flExcluido;
	private transient String dataAlteracaoFormatada;
	private transient Endereco enderecoComercial;
	private transient RegimeTributarioEnum regimeTributario;
	private transient Paginate paginate;

	public Pessoa() {
		// empty
	}

	public Pessoa(Long id) {
		this.setId(id);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getIdCodPessoa() {
		return idCodPessoa;
	}

	public void setIdCodPessoa(Long idCodPessoa) {
		this.idCodPessoa = idCodPessoa;
	}

	public TipoPessoa getTipoPessoa() {
		if (tipoPessoa == null) {
			tipoPessoa = new TipoPessoa();
		}
		return tipoPessoa;
	}

	public void setTipoPessoa(TipoPessoa tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}

	public Integer getPrazo() {
		return prazo;
	}

	public void setPrazo(Integer prazo) {
		this.prazo = prazo;
	}

	public FormaPagamento getFormaPagamento() {
		if (formaPagamento == null) {
			formaPagamento = new FormaPagamento();
		}
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public Integer getIdRota() {
		return idRota;
	}

	public void setIdRota(Integer idRota) {
		this.idRota = idRota;
	}

	public Long getIdVendedorRepresentante() {
		return idVendedorRepresentante;
	}

	public void setIdVendedorRepresentante(Long idVendedorRepresentante) {
		this.idVendedorRepresentante = idVendedorRepresentante;
	}

	public String getCnae() {
		return cnae;
	}

	public void setCnae(String cnae) {
		this.cnae = cnae;
	}

	public String getFlgOptanteSimples() {
		return flgOptanteSimples;
	}

	public void setFlgOptanteSimples(String flgOptanteSimples) {
		this.flgOptanteSimples = flgOptanteSimples;
	}

	public String getNomeContato() {
		return nomeContato;
	}

	public void setNomeContato(String nomeContato) {
		this.nomeContato = nomeContato;
	}

	public BigDecimal getPercDescPadrao() {
		return percDescPadrao;
	}

	public void setPercDescPadrao(BigDecimal percDescPadrao) {
		this.percDescPadrao = percDescPadrao;
	}

	public String getNomeRepresentante() {
		return nomeRepresentante;
	}

	public void setNomeRepresentante(String nomeRepresentante) {
		this.nomeRepresentante = nomeRepresentante;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDataAlteracao() {
		return dataAlteracao;
	}

	public void setDataAlteracao(Date dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

	public String getDataAlteracaoFormatada() {
		this.dataAlteracaoFormatada = DateUtil.dateToFormatDefault(this.getDataAlteracao());
		return this.dataAlteracaoFormatada;
	}

	public void setDataAlteracaoFormatada(String dataAlteracaoFormatada) {
		this.dataAlteracaoFormatada = dataAlteracaoFormatada;
	}

	public ExcluidoEnum getFlExcluido() {
		if (flExcluido == null) {
			flExcluido = ExcluidoEnum.ATIVO;
		}
		if (getSituacao() != null) {
			flExcluido = ExcluidoEnum.getById(getSituacao());
		} else {
			situacao = flExcluido.getId();
		}
		return flExcluido;
	}

	public void setFlExcluido(ExcluidoEnum flExcluido) {
		this.flExcluido = flExcluido;
	}

	public String getIdRegimeTributario() {
		return idRegimeTributario;
	}

	public void setIdRegimeTributario(String idRegimeTributario) {
		this.idRegimeTributario = idRegimeTributario;
	}

	public RegimeTributarioEnum getRegimeTributario() {
		if (getIdRegimeTributario() != null) {
			regimeTributario = RegimeTributarioEnum.getById(getIdRegimeTributario());
		}
		return regimeTributario;
	}

	public void setRegimeTributario(RegimeTributarioEnum regimeTributario) {
		this.regimeTributario = regimeTributario;
	}

	public Integer getSituacao() {
		return situacao;
	}

	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

	public Endereco getEnderecoComercial() {
		if (enderecoComercial == null) {
			enderecoComercial = new Endereco();
		}
		return enderecoComercial;
	}

	public void setEnderecoComercial(Endereco enderecoComercial) {
		this.enderecoComercial = enderecoComercial;
	}

	public Paginate getPaginate() {
		if (paginate == null) {
			paginate = new Paginate();
		}
		return paginate;
	}

	public void setPaginate(Paginate paginate) {
		this.paginate = paginate;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getIdTipoEmpresa() {
		return idTipoEmpresa;
	}

	public void setIdTipoEmpresa(String idTipoEmpresa) {
		this.idTipoEmpresa = idTipoEmpresa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((idCodPessoa == null) ? 0 : idCodPessoa.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pessoa other = (Pessoa) obj;
		if (idCodPessoa == null) {
			if (other.idCodPessoa != null)
				return false;
		} else if (!idCodPessoa.equals(other.idCodPessoa))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return this.getNome();
	}
}
