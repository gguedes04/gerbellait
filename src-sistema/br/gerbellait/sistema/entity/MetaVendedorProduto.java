package br.gerbellait.sistema.entity;

import java.math.BigDecimal;
import java.util.Date;

import br.gerbellait.arquitetura.entities.AbstractEntity;

public class MetaVendedorProduto extends AbstractEntity {
	private static final long serialVersionUID = 1L;

	private Vendedor vendedor;
	private Produto produto;
	private String nome;
	private Date dataInicial;
	private Date dataFinal;
	private BigDecimal quantidadeVenda;
	private BigDecimal valorPrecoMedioVenda;
	private BigDecimal quantidadeLimiteVenda;
	private BigDecimal percentualDesconto;
	private BigDecimal quantidadeMinima;
	private Date dataAlteracao;
	private BigDecimal quantidadeLimiteVendaInicial;

	// excluir, alterar, remover, novo
	private transient String acao;

	public MetaVendedorProduto() {
		// empty
	}

	public MetaVendedorProduto(Long id) {
		this.setId(id);
	}

	public Vendedor getVendedor() {
		return vendedor;
	}

	public void setVendedor(Vendedor vendedor) {
		this.vendedor = vendedor;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public BigDecimal getQuantidadeVenda() {
		return quantidadeVenda;
	}

	public void setQuantidadeVenda(BigDecimal quantidadeVenda) {
		this.quantidadeVenda = quantidadeVenda;
	}

	public BigDecimal getValorPrecoMedioVenda() {
		return valorPrecoMedioVenda;
	}

	public void setValorPrecoMedioVenda(BigDecimal valorPrecoMedioVenda) {
		this.valorPrecoMedioVenda = valorPrecoMedioVenda;
	}

	public BigDecimal getQuantidadeLimiteVenda() {
		return quantidadeLimiteVenda;
	}

	public void setQuantidadeLimiteVenda(BigDecimal quantidadeLimiteVenda) {
		this.quantidadeLimiteVenda = quantidadeLimiteVenda;
	}

	public BigDecimal getPercentualDesconto() {
		return percentualDesconto;
	}

	public void setPercentualDesconto(BigDecimal percentualDesconto) {
		this.percentualDesconto = percentualDesconto;
	}

	public BigDecimal getQuantidadeMinima() {
		return quantidadeMinima;
	}

	public void setQuantidadeMinima(BigDecimal quantidadeMinima) {
		this.quantidadeMinima = quantidadeMinima;
	}

	public Date getDataAlteracao() {
		return dataAlteracao;
	}

	public void setDataAlteracao(Date dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

	public BigDecimal getQuantidadeLimiteVendaInicial() {
		return quantidadeLimiteVendaInicial;
	}

	public void setQuantidadeLimiteVendaInicial(BigDecimal quantidadeLimiteVendaInicial) {
		this.quantidadeLimiteVendaInicial = quantidadeLimiteVendaInicial;
	}

	public String getAcao() {
		return acao;
	}

	public void setAcao(String acao) {
		this.acao = acao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((produto == null) ? 0 : produto.hashCode());
		result = prime * result + ((vendedor == null) ? 0 : vendedor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MetaVendedorProduto other = (MetaVendedorProduto) obj;
		if (produto == null) {
			if (other.produto != null)
				return false;
		} else if (!produto.equals(other.produto))
			return false;
		if (vendedor == null) {
			if (other.vendedor != null)
				return false;
		} else if (!vendedor.equals(other.vendedor))
			return false;
		return true;
	}
}