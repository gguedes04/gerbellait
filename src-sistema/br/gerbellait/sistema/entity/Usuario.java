package br.gerbellait.sistema.entity;

import java.util.ArrayList;
import java.util.List;

import br.gerbellait.arquitetura.entities.AbstractEntity;
import br.gerbellait.arquitetura.util.Utils;

public class Usuario extends AbstractEntity {
	private static final long serialVersionUID = 1L;

	private String login;
	private String senha;
	private String nome;
	private String email;
	private Integer situacao;
	private Boolean icPrimeiroAcesso;
	private Vendedor vendedor;
	private Perfil perfil;
	private List<PerfilUsuario> perfis;

	public Usuario() {
		// empty
	}

	public Usuario(Long id) {
		this.setId(id);
	}

	public Vendedor getVendedor() {
		if (vendedor == null) {
			vendedor = new Vendedor();
		}
		return vendedor;
	}

	public void setVendedor(Vendedor vendedor) {
		this.vendedor = vendedor;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getSituacao() {
		return situacao;
	}

	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}

	public Boolean getIcPrimeiroAcesso() {
		return icPrimeiroAcesso;
	}

	public void setIcPrimeiroAcesso(Boolean icPrimeiroAcesso) {
		this.icPrimeiroAcesso = icPrimeiroAcesso;
	}

	public List<PerfilUsuario> getPerfis() {
		if (perfis == null) {
			perfis = new ArrayList<>();
		}
		return perfis;
	}

	public void setPerfis(List<PerfilUsuario> perfis) {
		this.perfis = perfis;
	}

	public Perfil getPerfil() {
		if (perfil == null && !Utils.isNullOrEmpty(this.getPerfis())) {
			perfil = this.getPerfis().get(0).getPerfil();
		}
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return this.getLogin();
	}

}