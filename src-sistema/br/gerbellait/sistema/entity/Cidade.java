package br.gerbellait.sistema.entity;

import br.gerbellait.arquitetura.entities.AbstractEntity;

public class Cidade extends AbstractEntity {
	private static final long serialVersionUID = 1L;
	private Integer idCidadeIbge;
	private String nome;
	private Uf uf;

	public Cidade() {
		// empty
	}

	public Cidade(Long id) {
		this.setId(id);
	}

	public Cidade(String nome) {
		this.setNome(nome);
	}

	public Integer getIdCidadeIbge() {
		return idCidadeIbge;
	}

	public void setIdCidadeIbge(Integer idCidadeIbge) {
		this.idCidadeIbge = idCidadeIbge;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Uf getUf() {
		return uf;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((idCidadeIbge == null) ? 0 : idCidadeIbge.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((uf == null) ? 0 : uf.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cidade other = (Cidade) obj;
		if (idCidadeIbge == null) {
			if (other.idCidadeIbge != null)
				return false;
		} else if (!idCidadeIbge.equals(other.idCidadeIbge))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (uf == null) {
			if (other.uf != null)
				return false;
		} else if (!uf.equals(other.uf))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return this.getNome();
	}
}