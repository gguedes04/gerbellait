package br.gerbellait.sistema.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import br.gerbellait.arquitetura.entities.AbstractEntity;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.dao.enumerador.TipoVendaEnum;
import br.gerbellait.sistema.util.DateUtil;
import br.gerbellait.sistema.util.Paginate;

public class Vendedor extends AbstractEntity {
	private static final long serialVersionUID = 1L;
	private String nome;
	private String tipoVenda;
	private BigDecimal valorVendaGeral;
	private BigDecimal percentualTroca;
	private Date dataAlteracao;
	private Integer situacao;

	private List<RotaVendedor> rotas;
	private List<ComissaoVendedor> comissoes;

	private transient String dataAlteracaoFormatada;
	private transient Rota rota;
	private ExcluidoEnum flExcluido;
	private TipoVendaEnum tipo;
	private transient Paginate paginate;

	public Vendedor() {
		// empty
	}

	public Vendedor(Long id) {
		this.setId(id);
	}

	public Vendedor(Long id, String nome) {
		this.setId(id);
		this.setNome(nome);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTipoVenda() {
		return tipoVenda;
	}

	public void setTipoVenda(String tipoVenda) {
		this.tipoVenda = tipoVenda;
	}

	public BigDecimal getValorVendaGeral() {
		return valorVendaGeral;
	}

	public void setValorVendaGeral(BigDecimal valorVendaGeral) {
		this.valorVendaGeral = valorVendaGeral;
	}

	public Paginate getPaginate() {
		if (paginate == null) {
			paginate = new Paginate();
		}
		return paginate;
	}

	public void setPaginate(Paginate paginate) {
		this.paginate = paginate;
	}

	public BigDecimal getPercentualTroca() {
		return percentualTroca;
	}

	public void setPercentualTroca(BigDecimal percentualTroca) {
		this.percentualTroca = percentualTroca;
	}

	public Date getDataAlteracao() {
		return dataAlteracao;
	}

	public void setDataAlteracao(Date dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

	public String getDataAlteracaoFormatada() {
		this.dataAlteracaoFormatada = DateUtil.dateToFormatDefault(this.getDataAlteracao());
		return this.dataAlteracaoFormatada;
	}

	public void setDataAlteracaoFormatada(String dataAlteracaoFormatada) {
		this.dataAlteracaoFormatada = dataAlteracaoFormatada;
	}

	public Integer getSituacao() {
		return situacao;
	}

	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}

	public Rota getRota() {
		return rota;
	}

	public void setRota(Rota rota) {
		this.rota = rota;
	}

	public List<ComissaoVendedor> getComissoes() {
		return comissoes;
	}

	public void setComissoes(List<ComissaoVendedor> comissoes) {
		this.comissoes = comissoes;
	}

	public ExcluidoEnum getFlExcluido() {
		if (flExcluido == null) {
			flExcluido = ExcluidoEnum.ATIVO;
		}
		if (getSituacao() != null) {
			flExcluido = ExcluidoEnum.getById(getSituacao());
		} else {
			situacao = flExcluido.getId();
		}
		return flExcluido;
	}

	public void setFlExcluido(ExcluidoEnum flExcluido) {
		this.flExcluido = flExcluido;
	}

	public TipoVendaEnum getTipo() {
		if (tipo == null) {
			tipo = TipoVendaEnum.DIRETA;
		}
		if (getTipoVenda() != null) {
			tipo = TipoVendaEnum.getById(getTipoVenda());
		} else {
			tipoVenda = tipo.getId();
		}
		return tipo;
	}

	public void setTipo(TipoVendaEnum tipo) {
		this.tipo = tipo;
	}

	public List<RotaVendedor> getRotas() {
		return rotas;
	}

	public void setRotas(List<RotaVendedor> rotas) {
		this.rotas = rotas;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((dataAlteracao == null) ? 0 : dataAlteracao.hashCode());
		result = prime * result + ((flExcluido == null) ? 0 : flExcluido.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((percentualTroca == null) ? 0 : percentualTroca.hashCode());
		result = prime * result + ((situacao == null) ? 0 : situacao.hashCode());
		result = prime * result + ((tipoVenda == null) ? 0 : tipoVenda.hashCode());
		result = prime * result + ((valorVendaGeral == null) ? 0 : valorVendaGeral.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vendedor other = (Vendedor) obj;
		if (dataAlteracao == null) {
			if (other.dataAlteracao != null)
				return false;
		} else if (!dataAlteracao.equals(other.dataAlteracao))
			return false;
		if (flExcluido != other.flExcluido)
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (percentualTroca == null) {
			if (other.percentualTroca != null)
				return false;
		} else if (!percentualTroca.equals(other.percentualTroca))
			return false;
		if (situacao == null) {
			if (other.situacao != null)
				return false;
		} else if (!situacao.equals(other.situacao))
			return false;
		if (tipoVenda == null) {
			if (other.tipoVenda != null)
				return false;
		} else if (!tipoVenda.equals(other.tipoVenda))
			return false;
		if (valorVendaGeral == null) {
			if (other.valorVendaGeral != null)
				return false;
		} else if (!valorVendaGeral.equals(other.valorVendaGeral))
			return false;
		return true;
	}
}