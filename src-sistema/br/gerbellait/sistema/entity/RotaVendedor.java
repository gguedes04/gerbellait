package br.gerbellait.sistema.entity;

import java.util.Date;

import br.gerbellait.arquitetura.entities.AbstractEntity;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.util.DateUtil;

public class RotaVendedor extends AbstractEntity {
	private static final long serialVersionUID = 1L;
	private Vendedor vendedor;
	private Rota rota;
	private Date dataAlteracao;
	private Integer situacao;

	// excluir, alterar, remover, novo
	private transient String acao;

	private transient String dataAlteracaoFormatada;
	private ExcluidoEnum flExcluido;

	public RotaVendedor() {
		// empty
	}

	public RotaVendedor(Long id) {
		this.setId(id);
	}

	public String getAcao() {
		return acao;
	}

	public void setAcao(String acao) {
		this.acao = acao;
	}

	public Vendedor getVendedor() {
		return vendedor;
	}

	public void setVendedor(Vendedor vendedor) {
		this.vendedor = vendedor;
	}

	public Rota getRota() {
		return rota;
	}

	public void setRota(Rota rota) {
		this.rota = rota;
	}

	public Date getDataAlteracao() {
		return dataAlteracao;
	}

	public void setDataAlteracao(Date dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

	public String getDataAlteracaoFormatada() {
		this.dataAlteracaoFormatada = DateUtil.dateToFormatDefault(this.getDataAlteracao());
		return this.dataAlteracaoFormatada;
	}

	public void setDataAlteracaoFormatada(String dataAlteracaoFormatada) {
		this.dataAlteracaoFormatada = dataAlteracaoFormatada;
	}

	public Integer getSituacao() {
		return situacao;
	}

	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}

	public ExcluidoEnum getFlExcluido() {
		if (flExcluido == null) {
			flExcluido = ExcluidoEnum.ATIVO;
		}
		if (getSituacao() != null) {
			flExcluido = ExcluidoEnum.getById(getSituacao());
		} else {
			situacao = flExcluido.getId();
		}
		return flExcluido;
	}

	public void setFlExcluido(ExcluidoEnum flExcluido) {
		this.flExcluido = flExcluido;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((dataAlteracao == null) ? 0 : dataAlteracao.hashCode());
		result = prime * result + ((flExcluido == null) ? 0 : flExcluido.hashCode());
		result = prime * result + ((rota == null) ? 0 : rota.hashCode());
		result = prime * result + ((situacao == null) ? 0 : situacao.hashCode());
		result = prime * result + ((vendedor == null) ? 0 : vendedor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RotaVendedor other = (RotaVendedor) obj;
		if (dataAlteracao == null) {
			if (other.dataAlteracao != null)
				return false;
		} else if (!dataAlteracao.equals(other.dataAlteracao))
			return false;
		if (flExcluido != other.flExcluido)
			return false;
		if (rota == null) {
			if (other.rota != null)
				return false;
		} else if (!rota.equals(other.rota))
			return false;
		if (situacao == null) {
			if (other.situacao != null)
				return false;
		} else if (!situacao.equals(other.situacao))
			return false;
		if (vendedor == null) {
			if (other.vendedor != null)
				return false;
		} else if (!vendedor.equals(other.vendedor))
			return false;
		return true;
	}
}