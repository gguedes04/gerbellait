package br.gerbellait.sistema.entity;

import java.util.ArrayList;
import java.util.Collection;

public class UsuarioToken {

	private Long id;
	private String login;
	private String token;
	private Collection<String> funcoes = new ArrayList<>();
	private String grupo;
	private String nome;
	private Integer situacao;
	private Boolean icPrimeiroAcesso;
	private Long idVendedor;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdVendedor() {
		return idVendedor;
	}

	public void setIdVendedor(Long idVendedor) {
		this.idVendedor = idVendedor;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setFuncoes(Collection<String> funcoes) {
		this.funcoes = funcoes;
	}

	public Collection<String> getFuncoes() {
		return funcoes;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getLogin() {
		return login;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getToken() {
		return token;
	}

	public Integer getSituacao() {
		return situacao;
	}

	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}

	public Boolean getIcPrimeiroAcesso() {
		return icPrimeiroAcesso;
	}

	public void setIcPrimeiroAcesso(Boolean icPrimeiroAcesso) {
		this.icPrimeiroAcesso = icPrimeiroAcesso;
	}
}