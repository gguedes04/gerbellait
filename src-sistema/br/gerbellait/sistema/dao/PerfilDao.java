package br.gerbellait.sistema.dao;

import java.util.List;

import br.gerbellait.arquitetura.dao.Dao;
import br.gerbellait.sistema.entity.Perfil;

public interface PerfilDao extends Dao<Perfil> {

	public List<Perfil> getAll();

}