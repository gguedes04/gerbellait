package br.gerbellait.sistema.dao;

import java.util.List;

import br.gerbellait.arquitetura.dao.Dao;
import br.gerbellait.sistema.entity.Produto;
import br.gerbellait.sistema.entity.ProdutoPrecoRota;

public interface ProdutoPrecoRotaDao extends Dao<ProdutoPrecoRota> {

	public List<ProdutoPrecoRota> getBy(Produto produto);

	boolean deletar(ProdutoPrecoRota produtoPrecoRota);

	List<ProdutoPrecoRota> all();

	ProdutoPrecoRota getBy(ProdutoPrecoRota produtoPrecoRota);

}