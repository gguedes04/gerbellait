package br.gerbellait.sistema.dao;

import java.util.List;

import br.gerbellait.arquitetura.dao.Dao;
import br.gerbellait.sistema.entity.PeriodoMeta;

public interface PeriodoMetaDao extends Dao<PeriodoMeta> {

	List<PeriodoMeta> all();

	PeriodoMeta getPeriodoAtual();
}