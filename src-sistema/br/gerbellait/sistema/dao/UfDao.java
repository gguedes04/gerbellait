package br.gerbellait.sistema.dao;

import java.util.List;

import br.gerbellait.arquitetura.dao.Dao;
import br.gerbellait.sistema.entity.Uf;

public interface UfDao extends Dao<Uf> {
	public List<Uf> all();
}
