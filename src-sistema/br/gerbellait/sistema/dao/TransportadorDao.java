package br.gerbellait.sistema.dao;

import java.util.List;

import br.gerbellait.arquitetura.dao.Dao;
import br.gerbellait.sistema.entity.Transportador;

public interface TransportadorDao extends Dao<Transportador> {

	List<Transportador> getAll();

	List<Transportador> getByDto(Transportador transportador);
}