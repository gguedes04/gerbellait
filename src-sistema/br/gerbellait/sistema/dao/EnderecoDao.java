package br.gerbellait.sistema.dao;

import java.util.List;

import br.gerbellait.arquitetura.dao.Dao;
import br.gerbellait.sistema.entity.Endereco;
import br.gerbellait.sistema.entity.Pessoa;

public interface EnderecoDao extends Dao<Endereco> {
	public List<Endereco> getByPessoa(Pessoa pessoa);
}