package br.gerbellait.sistema.dao;

import java.util.List;

import br.gerbellait.arquitetura.dao.Dao;
import br.gerbellait.sistema.entity.ItemMenuPerfil;
import br.gerbellait.sistema.entity.Perfil;

public interface ItemMenuPerfilDao extends Dao<ItemMenuPerfil> {

	List<ItemMenuPerfil> allByPerfil(Perfil perfil);
}
