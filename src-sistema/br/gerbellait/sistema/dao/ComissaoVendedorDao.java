package br.gerbellait.sistema.dao;

import java.util.List;

import br.gerbellait.arquitetura.dao.Dao;
import br.gerbellait.sistema.entity.ComissaoVendedor;
import br.gerbellait.sistema.entity.Vendedor;

public interface ComissaoVendedorDao extends Dao<ComissaoVendedor> {

	public List<ComissaoVendedor> getBy(Vendedor vendedor);

	boolean deletar(ComissaoVendedor comissaoVendedor);

	List<ComissaoVendedor> all();
}