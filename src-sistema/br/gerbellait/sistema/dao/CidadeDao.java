package br.gerbellait.sistema.dao;

import java.util.List;

import br.gerbellait.arquitetura.dao.Dao;
import br.gerbellait.sistema.entity.Cidade;
import br.gerbellait.sistema.entity.Uf;

public interface CidadeDao extends Dao<Cidade> {

	public List<Cidade> getByUf(Uf uf);

}