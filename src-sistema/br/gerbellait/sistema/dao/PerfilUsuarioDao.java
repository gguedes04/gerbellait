package br.gerbellait.sistema.dao;

import java.util.List;

import br.gerbellait.arquitetura.dao.Dao;
import br.gerbellait.sistema.entity.PerfilUsuario;
import br.gerbellait.sistema.entity.Usuario;

public interface PerfilUsuarioDao extends Dao<PerfilUsuario> {

	List<PerfilUsuario> allByUser(Usuario usuario);

	boolean deleteByUser(Usuario usuario);
}
