package br.gerbellait.sistema.dao;

import java.util.List;

import br.gerbellait.arquitetura.dao.Dao;
import br.gerbellait.sistema.entity.Msn;

public interface MsnDao extends Dao<Msn> {

	List<Msn> all();

	boolean deletar(Msn msn);

	List<Msn> getByDto(Msn msn);

}