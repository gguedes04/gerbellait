package br.gerbellait.sistema.dao;

import java.util.List;

import br.gerbellait.arquitetura.dao.Dao;
import br.gerbellait.sistema.bean.VendedorFilterBean;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.entity.Vendedor;

public interface VendedorDao extends Dao<Vendedor> {

	List<Vendedor> allBySituacao(ExcluidoEnum flexcluido);
	
	List<VendedorFilterBean> allFilterSellers();

	List<Vendedor> all();

	boolean deletar(Vendedor vendedor);

	List<Vendedor> getByDto(Vendedor vendedor);

	List<Vendedor> getExport(Vendedor vendedor);
}
