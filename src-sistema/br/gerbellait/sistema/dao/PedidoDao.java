package br.gerbellait.sistema.dao;

import java.util.List;

import br.gerbellait.arquitetura.dao.Dao;
import br.gerbellait.sistema.bean.PedidoImpBean;
import br.gerbellait.sistema.bean.TableState;
import br.gerbellait.sistema.bean.UltimasVendasBean;
import br.gerbellait.sistema.entity.Pedido;

public interface PedidoDao extends Dao<Pedido> {
    
    boolean deletar(Pedido pedido);

    List<Pedido> all();
    
    List<Pedido> getByDto(TableState tableState) throws Exception;
    
    List<PedidoImpBean> relatorio(Pedido pedido);
    
    List<UltimasVendasBean> getUltimasVendas(Pedido pedido);
    
}
