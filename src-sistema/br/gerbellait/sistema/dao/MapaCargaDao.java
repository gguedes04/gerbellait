package br.gerbellait.sistema.dao;

import java.sql.SQLException;
import java.util.List;

import br.gerbellait.sistema.bean.MapaCargaFilterBean;
import br.gerbellait.sistema.bean.MapaCargaPedidoBean;
import br.gerbellait.sistema.bean.MapaCargaProdutoBean;
import br.gerbellait.sistema.entity.MapaCarga;

public interface MapaCargaDao {
    
    List<MapaCarga> findLoadMapReport() throws SQLException;
    
    List<MapaCargaPedidoBean> getPedidos(MapaCargaFilterBean filter) throws SQLException;
    
    List<MapaCargaProdutoBean> getProdutos(MapaCargaFilterBean filter) throws SQLException;
    
    Long save(List<MapaCargaPedidoBean> list) throws SQLException;
    
    List<MapaCarga> getLoadMapByFilter(MapaCargaFilterBean filter) throws SQLException;
    
    void deleteLoadMap(Long id) throws SQLException;
    
    List<MapaCargaPedidoBean> getPedidosVinculados(Long mapaCargaId) throws SQLException;
    
    List<MapaCargaPedidoBean> getPedidosNaoVinculados(MapaCargaFilterBean filter) throws SQLException;
    
    boolean hasLinkedPedido(Long pedido);
}
