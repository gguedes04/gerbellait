package br.gerbellait.sistema.dao;

import java.sql.SQLException;
import java.util.List;

import br.gerbellait.arquitetura.dao.Dao;
import br.gerbellait.sistema.bean.HistoricoEstoqueBean;
import br.gerbellait.sistema.bean.TableState;
import br.gerbellait.sistema.entity.Pedido;
import br.gerbellait.sistema.entity.Pessoa;
import br.gerbellait.sistema.entity.Produto;

public interface ProdutoDao extends Dao<Produto> {
    
    public List<Produto> all();
    
    List<Produto> getByDto(Produto produto);
    
    List<Produto> getExport(Produto produto);
    
    boolean deletar(Produto produto);
    
    Produto getByCodigo(Produto produto);
    
    Integer nextId();
    
    List<Produto> getByDesc(Pessoa cliente, String text) throws SQLException;
    
    List<Produto> getByPedido(Pedido pedido) throws SQLException;
    
    boolean updateQtEstoque(Produto produto) throws SQLException;
    
    List<HistoricoEstoqueBean> getHistoricoEstoque(TableState tableState) throws SQLException;
}
