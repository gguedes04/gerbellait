package br.gerbellait.sistema.dao;

import java.util.List;

import br.gerbellait.arquitetura.dao.Dao;
import br.gerbellait.sistema.entity.RotaVendedor;
import br.gerbellait.sistema.entity.Vendedor;

public interface RotaVendedorDao extends Dao<RotaVendedor> {

	public List<RotaVendedor> getBy(Vendedor vendedor);

	boolean deletar(RotaVendedor rotaVendedor);

	List<RotaVendedor> all();
}