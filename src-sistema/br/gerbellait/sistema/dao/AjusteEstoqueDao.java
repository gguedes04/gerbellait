package br.gerbellait.sistema.dao;

import java.util.List;

import br.gerbellait.arquitetura.dao.Dao;
import br.gerbellait.sistema.entity.AjusteEstoque;
import br.gerbellait.sistema.entity.Produto;

public interface AjusteEstoqueDao extends Dao<AjusteEstoque> {

	public List<AjusteEstoque> getAll();

	public List<AjusteEstoque> getByProduto(Produto produto);

	List<AjusteEstoque> getBy(AjusteEstoque filtro, boolean isPaginate);

}