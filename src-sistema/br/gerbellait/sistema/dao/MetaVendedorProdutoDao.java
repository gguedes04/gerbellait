package br.gerbellait.sistema.dao;

import java.util.List;

import br.gerbellait.arquitetura.dao.Dao;
import br.gerbellait.sistema.entity.MetaVendedorProduto;

public interface MetaVendedorProdutoDao extends Dao<MetaVendedorProduto> {

	List<MetaVendedorProduto> all();

	MetaVendedorProduto getBy(MetaVendedorProduto filtro);
}