package br.gerbellait.sistema.dao;

import java.util.List;

import br.gerbellait.arquitetura.dao.Dao;
import br.gerbellait.sistema.entity.Pessoa;

public interface PessoaDao extends Dao<Pessoa> {

	public List<Pessoa> all();

	List<Pessoa> getByDto(Pessoa pessoa);

	List<Pessoa> getByDtoPedido(Pessoa pessoa);

	List<Pessoa> getExport(Pessoa pessoa);

	boolean deletar(Pessoa pessoa);

	Pessoa getByCodigo(Pessoa pessoa);

	Long nextId();
}
