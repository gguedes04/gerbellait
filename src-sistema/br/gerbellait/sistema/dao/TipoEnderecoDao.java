package br.gerbellait.sistema.dao;

import java.util.List;

import br.gerbellait.arquitetura.dao.Dao;
import br.gerbellait.sistema.entity.TipoEndereco;

public interface TipoEnderecoDao extends Dao<TipoEndereco> {
	public List<TipoEndereco> all();
}
