package br.gerbellait.sistema.dao;

import java.sql.SQLException;

import br.gerbellait.arquitetura.dao.Dao;
import br.gerbellait.sistema.entity.Pedido;
import br.gerbellait.sistema.entity.PedidoProduto;
import br.gerbellait.sistema.entity.Produto;

public interface PedidoProdutoDao extends Dao<PedidoProduto> {
    
    boolean save(Pedido pedido, Produto produto, boolean notExists);
    
    boolean contains(Pedido pedido, Produto produto) throws SQLException;
    
}
