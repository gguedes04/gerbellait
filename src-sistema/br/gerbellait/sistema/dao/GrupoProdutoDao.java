package br.gerbellait.sistema.dao;

import java.util.List;

import br.gerbellait.arquitetura.dao.Dao;
import br.gerbellait.sistema.entity.GrupoProduto;

public interface GrupoProdutoDao extends Dao<GrupoProduto> {
	public List<GrupoProduto> all();
}
