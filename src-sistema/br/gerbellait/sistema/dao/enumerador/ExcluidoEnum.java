package br.gerbellait.sistema.dao.enumerador;

public enum ExcluidoEnum {

	ATIVO(0), EXCLUIDO(1);

	private final Integer id;

	ExcluidoEnum(Integer id) {
		this.id = id;
	}

	public static ExcluidoEnum getById(Integer id) {
		if (id.equals(ATIVO.id)) {
			return ATIVO;
		} else {
			return EXCLUIDO;
		}
	}

	public int getId() {
		return this.id;
	}
    
    @Override
    public String toString() {
        return "ExcluidoEnum{" +
                "id=" + id +
                '}';
    }
}
