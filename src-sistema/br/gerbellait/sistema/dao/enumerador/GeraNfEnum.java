package br.gerbellait.sistema.dao.enumerador;

public enum GeraNfEnum {

	S("Sim"), N("Não");

	private final String id;

	GeraNfEnum(String id) {
		this.id = id;
	}

	public static GeraNfEnum getById(String id) {
		if (id.equals(S.id)) {
			return S;
		} else {
			return N;
		}
	}

	public String getId() {
		return this.id;
	}
}