package br.gerbellait.sistema.dao.enumerador;

import java.util.ArrayList;
import java.util.List;

import br.gerbellait.sistema.dto.ObjetoDto;

public enum RegimeTributarioEnum {

	SN("SN", "Simples Nacional"), 
	LP("LP", "Lucro Presumido"), 
	LR("LR", "Lucro Real"),
	ME("ME", "Micro Empresa Individual");

	private final String id;
	private String descricao;

	RegimeTributarioEnum(String id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}

	public static RegimeTributarioEnum getById(String id) {
		if (id.equalsIgnoreCase(SN.id)) {
			return SN;
			
		} else if (id.equalsIgnoreCase(LP.id)) {
			return LP;
			
		} else if (id.equalsIgnoreCase(LR.id)) {
			return LR;
			
		} else if (id.equalsIgnoreCase(ME.id)) {
			return ME;
		}
		return null;
	}

	public String getId() {
		return this.id;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public static List<ObjetoDto> getTipoTrocaList() {
		List<ObjetoDto> retorno = new ArrayList<>();
		retorno.add(new ObjetoDto(SN.id, SN.descricao));
		retorno.add(new ObjetoDto(LP.id, LP.descricao));
		retorno.add(new ObjetoDto(LR.id, LR.descricao));
		retorno.add(new ObjetoDto(ME.id, ME.descricao));
		return retorno;
	}
}