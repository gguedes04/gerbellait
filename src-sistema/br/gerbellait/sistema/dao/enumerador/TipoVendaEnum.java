package br.gerbellait.sistema.dao.enumerador;

import java.util.ArrayList;
import java.util.List;

import br.gerbellait.sistema.dto.ObjetoDto;

public enum TipoVendaEnum {

	DIRETA("D", "Direta"), DISTRIBUICAO("I", "Distribuição");

	private final String id;
	private final String descricao;

	TipoVendaEnum(String id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}

	public static TipoVendaEnum getById(String id) {
		if (id.equalsIgnoreCase(DIRETA.getId())) {
			return DIRETA;
		} else {
			return DISTRIBUICAO;
		}
	}

	public String getId() {
		return this.id;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public static List<ObjetoDto> getTiposVenda() {
		List<ObjetoDto> retorno = new ArrayList<>();
		retorno.add(new ObjetoDto(DIRETA.getId(), DIRETA.getDescricao()));
		retorno.add(new ObjetoDto(DISTRIBUICAO.getId(), DISTRIBUICAO.getDescricao()));
		return retorno;
	}

	@Override
	public String toString() {
		return getDescricao();
	}
}