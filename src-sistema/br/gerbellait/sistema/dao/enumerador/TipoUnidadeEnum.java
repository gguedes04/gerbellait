package br.gerbellait.sistema.dao.enumerador;

import java.util.ArrayList;
import java.util.List;

public enum TipoUnidadeEnum {

	KILO("KG"), UNITARIO("UN");

	private final String id;

	TipoUnidadeEnum(String id) {
		this.id = id;
	}

	public static TipoUnidadeEnum getById(String id) {
		if (id.equalsIgnoreCase(KILO.id)) {
			return KILO;
		} else {
			return UNITARIO;
		}
	}

	public String getId() {
		return this.id;
	}

	public static List<String> getTiposUnidade() {
		List<String> retorno = new ArrayList<>();
		retorno.add(KILO.id);
		retorno.add(UNITARIO.id);
		return retorno;
	}
}