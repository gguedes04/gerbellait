package br.gerbellait.sistema.dao.enumerador;

import java.util.ArrayList;
import java.util.List;

import br.gerbellait.sistema.dto.ObjetoDto;

public enum TipoTrocaEnum {

	BELLA_ITALIA("B", "Bella Italia"), FORNECEDOR("F", "Fornecedor");

	private final String id;
	private String descricao;

	TipoTrocaEnum(String id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}

	public static TipoTrocaEnum getById(String id) {
		if (id.equalsIgnoreCase(BELLA_ITALIA.id)) {
			return BELLA_ITALIA;
		} else {
			return FORNECEDOR;
		}
	}

	public String getId() {
		return this.id;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public static List<ObjetoDto> getTipoTrocaList() {
		List<ObjetoDto> retorno = new ArrayList<>();
		retorno.add(new ObjetoDto(BELLA_ITALIA.id, BELLA_ITALIA.descricao));
		retorno.add(new ObjetoDto(FORNECEDOR.id, FORNECEDOR.descricao));
		return retorno;
	}
}