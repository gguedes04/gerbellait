package br.gerbellait.sistema.dao.enumerador;

public enum StatusPedidoEnum {

	ABERTO("A"), FECHADO("F");

	private final String id;

	StatusPedidoEnum(String id) {
		this.id = id;
	}

	public static StatusPedidoEnum getById(String id) {
		if (id.equals(ABERTO.id)) {
			return ABERTO;
		} else {
			return FECHADO;
		}
	}

	public String getId() {
		return this.id;
	}
}