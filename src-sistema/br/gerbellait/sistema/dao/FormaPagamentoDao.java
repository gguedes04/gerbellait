package br.gerbellait.sistema.dao;

import java.util.List;

import br.gerbellait.arquitetura.dao.Dao;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.entity.FormaPagamento;

public interface FormaPagamentoDao extends Dao<FormaPagamento> {

	List<FormaPagamento> all();

	List<FormaPagamento> allBySituacao(ExcluidoEnum flExcluido);
}
