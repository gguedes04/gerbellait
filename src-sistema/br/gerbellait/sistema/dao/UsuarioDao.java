package br.gerbellait.sistema.dao;

import java.util.List;

import br.gerbellait.arquitetura.dao.Dao;
import br.gerbellait.sistema.entity.Usuario;

public interface UsuarioDao extends Dao<Usuario> {

	List<Usuario> all();

	Usuario insert(Usuario usuario);

	Usuario getBy(String login, String senha);

	List<Usuario> getByDto(Usuario usuarioFitro);

	List<Usuario> getVendedores();
}
