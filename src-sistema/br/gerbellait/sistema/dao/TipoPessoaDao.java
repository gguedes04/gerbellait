package br.gerbellait.sistema.dao;

import java.util.List;

import br.gerbellait.arquitetura.dao.Dao;
import br.gerbellait.sistema.entity.TipoPessoa;

public interface TipoPessoaDao extends Dao<TipoPessoa> {
	public List<TipoPessoa> all();
}