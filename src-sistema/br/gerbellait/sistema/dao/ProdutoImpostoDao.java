package br.gerbellait.sistema.dao;

import java.util.List;

import br.gerbellait.arquitetura.dao.Dao;
import br.gerbellait.sistema.entity.Produto;
import br.gerbellait.sistema.entity.ProdutoImposto;

public interface ProdutoImpostoDao extends Dao<ProdutoImposto> {

	public List<ProdutoImposto> getBy(Produto produto);

	boolean deletar(ProdutoImposto produtoImposto);

	List<ProdutoImposto> all();
}