package br.gerbellait.sistema.dao;

import java.util.List;

import br.gerbellait.arquitetura.dao.Dao;
import br.gerbellait.sistema.entity.Rota;

public interface RotaDao extends Dao<Rota> {

	List<Rota> getByVendedor(Long idVendedor);

}