package br.gerbellait.sistema.dao.impl;

import java.util.List;

import br.gerbellait.arquitetura.dao.AbstractDao;
import br.gerbellait.arquitetura.interceptors.annotations.Repository;
import br.gerbellait.sistema.dao.GrupoProdutoDao;
import br.gerbellait.sistema.entity.GrupoProduto;

@Repository
public class GrupoProdutoDaoImpl extends AbstractDao<GrupoProduto> implements GrupoProdutoDao {
	private static final long serialVersionUID = 1L;

	static {
		banco.put("grupoproduto", "gpm_grupo_produto");
		banco.put("grupoproduto.id", "ID_GRUPO_PRODUTO");
		banco.put("grupoproduto.descricao", "DSC_GRUPO_PRODUTO");
	}

	@Override
	public List<GrupoProduto> all() {
		return (List<GrupoProduto>) this.getAll(GrupoProduto.class);
	}
}