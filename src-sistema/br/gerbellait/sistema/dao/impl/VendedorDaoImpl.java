package br.gerbellait.sistema.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.gerbellait.arquitetura.dao.AbstractDao;
import br.gerbellait.arquitetura.datasource.PreparedStatementWrapper;
import br.gerbellait.arquitetura.exceptions.DaoLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Repository;
import br.gerbellait.arquitetura.util.Utils;
import br.gerbellait.sistema.bean.VendedorFilterBean;
import br.gerbellait.sistema.dao.VendedorDao;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.dao.enumerador.TipoVendaEnum;
import br.gerbellait.sistema.entity.Vendedor;

@Repository
public class VendedorDaoImpl extends AbstractDao<Vendedor> implements VendedorDao {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(VendedorDaoImpl.class);
    private static final String ALL = "vendedor.all";
    private static final String ALL_COUNT = "vendedor.all.count";
    private static final String FL_EXCLUIDO = "vendedor.flExcluido";
    private static final String EXCLUIR = "vendedor.excluir";
    private static final String ORDER_BY = "vendedor.orderBy";
    private static final String BY_CODIGO = "vendedor.codigo";
    private static final String BY_NOME = "vendedor.nome";
    private static final String BY_TIPO = "vendedor.tipo";
    private static final String BY_ROTA = "vendedor.rota";
    private static final String PAGINATE_LIMIT_OFFSET = "paginacao.limit.offset";
    
    static {
        banco.put("vendedor", "gpm_vendedor");
        banco.put("vendedor.id", "ID_VENDEDOR");
        banco.put("vendedor.nome", "NM_VENDEDOR");
        banco.put("vendedor.tipoVenda", "TP_VENDA");
        banco.put("vendedor.valorVendaGeral", "VL_VENDA_GERAL_VENDEDOR");
        banco.put("vendedor.percentualTroca", "PERC_TROCA_VENDEDOR");
        banco.put("vendedor.dataAlteracao", "DT_ALTERACAO");
        banco.put("vendedor.situacao", "FL_EXCLUIDO");
    }
    
    @Override
    public List<Vendedor> all() {
        return (List<Vendedor>) this.getAll(Vendedor.class);
    }
    
    private void where(StringBuilder sql, Vendedor vendedor) {
        if (vendedor.getId() != null) {
            sql.append(" ").append(super.getProperty(BY_CODIGO));
        }
        if (!Utils.isNullOrEmpty(vendedor.getNome())) {
            sql.append(" ").append(super.getProperty(BY_NOME));
        }
        if (!Utils.isNullOrEmpty(vendedor.getTipoVenda())) {
            sql.append(" ").append(super.getProperty(BY_TIPO));
        }
        if (vendedor.getRota() != null && vendedor.getRota().getId() != null) {
            sql.append(" ").append(super.getProperty(BY_ROTA));
        }
    }
    
    private void parameters(PreparedStatementWrapper pstmt, Vendedor vendedor, boolean isPaginate) throws SQLException {
        Integer parameterIndex = 0;
        if (vendedor.getId() != null) {
            pstmt.setLong(++parameterIndex, vendedor.getId());
        }
        if (!Utils.isNullOrEmpty(vendedor.getNome())) {
            pstmt.setString(++parameterIndex, "%" + vendedor.getNome() + "%");
        }
        if (!Utils.isNullOrEmpty(vendedor.getTipoVenda())) {
            pstmt.setString(++parameterIndex, vendedor.getTipoVenda());
        }
        if (vendedor.getRota() != null && vendedor.getRota().getId() != null) {
            pstmt.setLong(++parameterIndex, vendedor.getRota().getId());
        }
        if (isPaginate) {
            pstmt.setInt(++parameterIndex, vendedor.getPaginate().getQuantidadePorPagina());
            pstmt.setInt(++parameterIndex, vendedor.getPaginate().getOffset());
        }
    }
    
    public Integer totalAmount(Vendedor vendedor) {
        Integer retorno = null;
        PreparedStatementWrapper pstmt = null;
        ResultSet rs = null;
        try {
            StringBuilder sql = new StringBuilder(super.getProperty(ALL_COUNT));
            this.where(sql, vendedor);
            pstmt = getTransaction().prepareStatement(sql);
            this.parameters(pstmt, vendedor, false);
            logger.debug(pstmt);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                retorno = rs.getInt(1);
            }
            return retorno;
            
        } catch (SQLException e) {
            throw new DaoLayerException(e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                logger.info(e);
            }
        }
    }
    
    private Vendedor getByResultSet(ResultSet rs) throws SQLException {
        int columnIndex = 0;
        Vendedor retorno = new Vendedor();
        retorno.setId(rs.getLong(++columnIndex));
        retorno.setNome(rs.getString(++columnIndex));
        retorno.setTipoVenda(rs.getString(++columnIndex));
        retorno.setTipo(TipoVendaEnum.getById(retorno.getTipoVenda()));
        retorno.setValorVendaGeral(rs.getBigDecimal(++columnIndex));
        retorno.setPercentualTroca(rs.getBigDecimal(++columnIndex));
        retorno.setDataAlteracao(rs.getTimestamp(++columnIndex));
        retorno.setSituacao(rs.getInt(++columnIndex));
        retorno.setFlExcluido(ExcluidoEnum.getById(retorno.getSituacao()));
        return retorno;
    }
    
    @Override
    public List<Vendedor> getByDto(Vendedor vendedor) {
        vendedor.getPaginate().setNumTotalRegistros(this.totalAmount(vendedor));
        List<Vendedor> retorno = new ArrayList<>();
        PreparedStatementWrapper pstmt = null;
        ResultSet rs = null;
        try {
            StringBuilder sql = new StringBuilder(super.getProperty(ALL));
            this.where(sql, vendedor);
            sql.append(" ").append(super.getProperty(ORDER_BY));
            sql.append(" ").append(super.getProperty(PAGINATE_LIMIT_OFFSET));
            pstmt = getTransaction().prepareStatement(sql);
            this.parameters(pstmt, vendedor, true);
            logger.debug(pstmt);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                retorno.add(this.getByResultSet(rs));
            }
            return retorno;
            
        } catch (SQLException e) {
            throw new DaoLayerException(e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                logger.info(e);
            }
        }
    }
    
    @Override
    public List<Vendedor> getExport(Vendedor vendedor) {
        vendedor.getPaginate().setNumTotalRegistros(this.totalAmount(vendedor));
        List<Vendedor> retorno = new ArrayList<>();
        PreparedStatementWrapper pstmt = null;
        ResultSet rs = null;
        try {
            StringBuilder sql = new StringBuilder(super.getProperty(ALL));
            this.where(sql, vendedor);
            sql.append(" ").append(super.getProperty(ORDER_BY));
            pstmt = getTransaction().prepareStatement(sql);
            this.parameters(pstmt, vendedor, false);
            logger.debug(pstmt);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                retorno.add(this.getByResultSet(rs));
            }
            return retorno;
            
        } catch (SQLException e) {
            throw new DaoLayerException(e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                logger.info(e);
            }
        }
    }
    
    @Override
    public boolean deletar(Vendedor vendedor) {
        PreparedStatementWrapper pstmt = null;
        try {
            StringBuilder sql = new StringBuilder(super.getProperty(EXCLUIR));
            pstmt = getTransaction().prepareStatement(sql);
            pstmt.setInt(1, ExcluidoEnum.EXCLUIDO.getId());
            pstmt.setLong(2, vendedor.getId());
            logger.debug(pstmt);
            pstmt.executeUpdate();
            return true;
            
        } catch (SQLException e) {
            throw new DaoLayerException(e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (SQLException e) {
                logger.info(e);
            }
        }
    }
    
    @Override
    public List<Vendedor> allBySituacao(ExcluidoEnum flExcluido) {
        List<Vendedor> retorno = new ArrayList<>();
        PreparedStatementWrapper pstmt = null;
        ResultSet rs = null;
        try {
            StringBuilder sql = new StringBuilder(super.getProperty(ALL));
            sql.append(" ").append(super.getProperty(FL_EXCLUIDO));
            sql.append(" ").append(super.getProperty(ORDER_BY));
            pstmt = getTransaction().prepareStatement(sql);
            pstmt.setInt(1, flExcluido.getId());
            logger.debug(pstmt);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                int columnIndex = 0;
                Vendedor vendedor = new Vendedor();
                vendedor.setId(rs.getLong(++columnIndex));
                vendedor.setNome(rs.getString(++columnIndex));
                vendedor.setTipoVenda(rs.getString(++columnIndex));
                vendedor.setValorVendaGeral(rs.getBigDecimal(++columnIndex));
                vendedor.setPercentualTroca(rs.getBigDecimal(++columnIndex));
                vendedor.setDataAlteracao(rs.getDate(++columnIndex));
                vendedor.setFlExcluido(ExcluidoEnum.getById(rs.getInt(++columnIndex)));
                retorno.add(vendedor);
            }
            return retorno;
            
        } catch (SQLException e) {
            throw new DaoLayerException(e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                logger.info(e);
            }
        }
    }
    
    @Override
    public List<VendedorFilterBean> allFilterSellers() {
        List<VendedorFilterBean> result = new ArrayList<>();
        List<Vendedor> all = allBySituacao(ExcluidoEnum.ATIVO);
        all.forEach(vendedor -> result.add(new VendedorFilterBean(vendedor.getId(), vendedor.getNome())));
        return result;
    }
}
