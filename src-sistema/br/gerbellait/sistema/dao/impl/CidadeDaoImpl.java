package br.gerbellait.sistema.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.gerbellait.arquitetura.dao.AbstractDao;
import br.gerbellait.arquitetura.datasource.PreparedStatementWrapper;
import br.gerbellait.arquitetura.exceptions.DaoLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Repository;
import br.gerbellait.sistema.dao.CidadeDao;
import br.gerbellait.sistema.entity.Cidade;
import br.gerbellait.sistema.entity.Uf;

@Repository
public class CidadeDaoImpl extends AbstractDao<Cidade> implements CidadeDao {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(CidadeDaoImpl.class);

	private static final String ALL = "cidade.all";
	private static final String GET_BY_UF = "cidade.getByUf";
	private static final String ORDER_BY = "cidade.orderBy";

	static {
		banco.put("cidade", "gpm_cidade");
		banco.put("cidade.id", "ID_CIDADE");
		banco.put("cidade.idCidadeIbge", "ID_CIDADE_IBGE");
		banco.put("cidade.nome", "NM_CIDADE");
		banco.put("cidade.uf", "ID_UF");
	}

	@Override
	public List<Cidade> getByUf(Uf uf) {
		List<Cidade> retorno = new ArrayList<>();
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(ALL));
			if (uf != null && uf.getId() != null) {
				sql.append(" ").append(super.getProperty(GET_BY_UF));
			}
			sql.append(" ").append(super.getProperty(ORDER_BY));
			pstmt = getTransaction().prepareStatement(sql);
			int parameterIndex = 0;
			if (uf != null && uf.getId() != null) {
				pstmt.setLong(++parameterIndex, uf.getId());
			}
			logger.debug(pstmt);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Cidade cidade = new Cidade();
				cidade.setId(rs.getLong(1));
				cidade.setIdCidadeIbge(rs.getInt(2));
				cidade.setNome(rs.getString(3));
				cidade.setUf(new Uf(rs.getLong(4)));
				retorno.add(cidade);
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}
}