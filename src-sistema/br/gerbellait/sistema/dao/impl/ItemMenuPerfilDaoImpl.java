package br.gerbellait.sistema.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.gerbellait.arquitetura.dao.AbstractDao;
import br.gerbellait.arquitetura.datasource.PreparedStatementWrapper;
import br.gerbellait.arquitetura.exceptions.DaoLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Repository;
import br.gerbellait.sistema.dao.ItemMenuPerfilDao;
import br.gerbellait.sistema.entity.ItemMenu;
import br.gerbellait.sistema.entity.ItemMenuPerfil;
import br.gerbellait.sistema.entity.Perfil;

@Repository
public class ItemMenuPerfilDaoImpl extends AbstractDao<ItemMenuPerfil> implements ItemMenuPerfilDao {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(ItemMenuPerfilDaoImpl.class);
	private static final String ALL_BY = "item.menu.perfil.all";

	static {
		banco.put("itemmenuperfil", "gpm_item_menu_perfil");
		banco.put("itemmenuperfil.id", "co_item_menu_perfil");
		banco.put("itemmenuperfil.perfil.id", "co_perfil");
		banco.put("itemmenuperfil.itemMenu", "co_item_menu");
	}

	@Override
	public List<ItemMenuPerfil> allByPerfil(Perfil perfil) {
		List<ItemMenuPerfil> retorno = new ArrayList<>();
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = getTransaction().prepareStatement(new StringBuilder(super.getProperty(ALL_BY)));
			pstmt.setLong(1, perfil.getId());
			logger.debug(pstmt);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				ItemMenuPerfil itemMenuPerfil = new ItemMenuPerfil();
				itemMenuPerfil.setId(rs.getLong(1));
				itemMenuPerfil.setPerfil(new Perfil(perfil.getId()));
				ItemMenu itemMenu = new ItemMenu();
				itemMenu.setId(rs.getLong(2));
				itemMenu.setNome(rs.getString(3));
				itemMenu.setMenuPai(new ItemMenu(rs.getLong(4)));
				itemMenuPerfil.setItemMenu(itemMenu);
				retorno.add(itemMenuPerfil);
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.info(e.getMessage());
			}
		}
	}
}