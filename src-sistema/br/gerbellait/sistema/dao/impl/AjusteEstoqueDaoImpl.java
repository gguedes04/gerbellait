package br.gerbellait.sistema.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.gerbellait.arquitetura.dao.AbstractDao;
import br.gerbellait.arquitetura.datasource.PreparedStatementWrapper;
import br.gerbellait.arquitetura.exceptions.DaoLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Repository;
import br.gerbellait.sistema.dao.AjusteEstoqueDao;
import br.gerbellait.sistema.entity.AjusteEstoque;
import br.gerbellait.sistema.entity.Produto;

@Repository
public class AjusteEstoqueDaoImpl extends AbstractDao<AjusteEstoque> implements AjusteEstoqueDao {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(AjusteEstoqueDaoImpl.class);

	private static final String ALL = "ajusteEstoque.all";
	private static final String ALL_COUNT = "ajusteEstoque.all.count";
	private static final String GET_BY_PRODUTO = "ajusteEstoque.getByProduto";
	private static final String GET_BY_DATAS = "ajusteEstoque.getByDatas";
	private static final String ORDER_BY = "ajusteEstoque.orderBy";
	private static final String PAGINATE_LIMIT_OFFSET = "paginacao.limit.offset";

	static {
		banco.put("ajusteestoque", "gpm_ajuste_estoque");
		banco.put("ajusteestoque.id", "co_ajuste_estoque");
		banco.put("ajusteestoque.produto", "id_produto");
		banco.put("ajusteestoque.tpRubrica", "tp_rubrica");
		banco.put("ajusteestoque.dataAjusteEstoque", "dt_ajuste_estoque");
		banco.put("ajusteestoque.quantidadeAjusteEstoque", "qtde_ajuste_estoque");
		banco.put("ajusteestoque.observacao", "observacao");
		banco.put("ajusteestoque.tipoAjusteEstoque", "tp_ajuste_estoque");
		banco.put("ajusteestoque.idLocal", "id_local");
	}

	@Override
	public List<AjusteEstoque> getAll() {
		return (List<AjusteEstoque>) this.getAll(AjusteEstoque.class);
	}

	private AjusteEstoque getByResultSet(ResultSet rs) throws SQLException {
		int columnIndex = 0;
		AjusteEstoque retorno = new AjusteEstoque();
		retorno.setId(rs.getLong(++columnIndex));
		retorno.setProduto(new Produto(rs.getLong(++columnIndex)));
		retorno.setTpRubrica(rs.getString(++columnIndex));
		retorno.setDataAjusteEstoque(rs.getDate(++columnIndex));
		retorno.setQuantidadeAjusteEstoque(rs.getBigDecimal(++columnIndex));
		retorno.setObservacao(rs.getString(++columnIndex));
		retorno.setTipoAjusteEstoque(rs.getString(++columnIndex));
		retorno.setIdLocal(rs.getString(++columnIndex));
		return retorno;
	}

	@Override
	public List<AjusteEstoque> getByProduto(Produto produto) {
		List<AjusteEstoque> retorno = new ArrayList<>();
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(ALL));
			if (produto != null && produto.getId() != null) {
				sql.append(" ").append(super.getProperty(GET_BY_PRODUTO));
			}
			sql.append(" ").append(super.getProperty(ORDER_BY));
			pstmt = getTransaction().prepareStatement(sql);
			int parameterIndex = 0;
			if (produto != null && produto.getId() != null) {
				pstmt.setLong(++parameterIndex, produto.getId());
			}
			logger.debug(pstmt);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				retorno.add(this.getByResultSet(rs));
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}

	public Integer totalAmount(AjusteEstoque filtro) {
		Integer retorno = null;
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(ALL_COUNT));
			if (filtro.getProduto() != null && filtro.getProduto().getId() != null) {
				sql.append(" ").append(super.getProperty(GET_BY_PRODUTO));
			}
			if (filtro.getDataInicial() != null && filtro.getDataFinal() != null) {
				sql.append(" ").append(super.getProperty(GET_BY_DATAS));
			}
			pstmt = getTransaction().prepareStatement(sql);
			int parameterIndex = 0;
			if (filtro.getProduto() != null && filtro.getProduto().getId() != null) {
				pstmt.setLong(++parameterIndex, filtro.getProduto().getId());
			}
			if (filtro.getDataInicial() != null && filtro.getDataFinal() != null) {
				pstmt.setDate(++parameterIndex, new java.sql.Date(filtro.getDataInicial().getTime()));
				pstmt.setDate(++parameterIndex, new java.sql.Date(filtro.getDataFinal().getTime()));
			}
			logger.debug(pstmt);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				retorno = rs.getInt(1);
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}

	@Override
	public List<AjusteEstoque> getBy(AjusteEstoque filtro, boolean isPaginate) {
		if (isPaginate) {
			filtro.getPaginate().setNumTotalRegistros(this.totalAmount(filtro));
		}
		List<AjusteEstoque> retorno = new ArrayList<>();
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(ALL));
			if (filtro.getProduto() != null && filtro.getProduto().getId() != null) {
				sql.append(" ").append(super.getProperty(GET_BY_PRODUTO));
			}
			if (filtro.getDataInicial() != null && filtro.getDataFinal() != null) {
				sql.append(" ").append(super.getProperty(GET_BY_DATAS));
			}
			sql.append(" ").append(super.getProperty(ORDER_BY));
			if (isPaginate) {
				sql.append(" ").append(super.getProperty(PAGINATE_LIMIT_OFFSET));
			}
			pstmt = getTransaction().prepareStatement(sql);
			int parameterIndex = 0;
			if (filtro.getProduto() != null && filtro.getProduto().getId() != null) {
				pstmt.setLong(++parameterIndex, filtro.getProduto().getId());
			}
			if (filtro.getDataInicial() != null && filtro.getDataFinal() != null) {
				pstmt.setDate(++parameterIndex, new java.sql.Date(filtro.getDataInicial().getTime()));
				pstmt.setDate(++parameterIndex, new java.sql.Date(filtro.getDataFinal().getTime()));
			}
			if (isPaginate) {
				pstmt.setInt(++parameterIndex, filtro.getPaginate().getQuantidadePorPagina());
				pstmt.setInt(++parameterIndex, filtro.getPaginate().getOffset());
			}
			logger.debug(pstmt);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				retorno.add(this.getByResultSet(rs));
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}
}