package br.gerbellait.sistema.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.gerbellait.arquitetura.dao.AbstractDao;
import br.gerbellait.arquitetura.datasource.PreparedStatementWrapper;
import br.gerbellait.arquitetura.exceptions.DaoLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Repository;
import br.gerbellait.sistema.dao.ProdutoImpostoDao;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.entity.Produto;
import br.gerbellait.sistema.entity.ProdutoImposto;

@Repository
public class ProdutoImpostoDaoImpl extends AbstractDao<ProdutoImposto> implements ProdutoImpostoDao {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(ProdutoImpostoDaoImpl.class);
	private static final String ALL = "produtoImposto.all";
	private static final String BY_ID_PRODUTO = "produtoImposto.codigoProduto";
	private static final String ORDER_BY = "produtoImposto.orderBy";
	private static final String EXCLUIR = "produtoImposto.excluir";

	static {
		banco.put("produtoimposto", "gpm_produto_imposto");
		banco.put("produtoimposto.id", "ID_PROD_IMP");
		banco.put("produtoimposto.produto", "ID_PRODUTO");
		banco.put("produtoimposto.icmsEntrada", "ICMS_ENTRADA");
		banco.put("produtoimposto.icmsSaida", "ICMS_SAIDA");
		banco.put("produtoimposto.mva", "MVA");
		banco.put("produtoimposto.uf", "UF");
		banco.put("produtoimposto.flgPossuiSt", "FLG_POSSUIST");
		banco.put("produtoimposto.percentualSimples", "PERC_SIMPLES");
	}

	@Override
	public List<ProdutoImposto> all() {
		return (List<ProdutoImposto>) this.getAll(ProdutoImposto.class);
	}

	@Override
	public boolean deletar(ProdutoImposto produtoImposto) {
		PreparedStatementWrapper pstmt = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(EXCLUIR));
			pstmt = getTransaction().prepareStatement(sql);
			pstmt.setInt(1, ExcluidoEnum.EXCLUIDO.getId());
			pstmt.setLong(2, produtoImposto.getId());
			logger.debug(pstmt);
			pstmt.executeUpdate();
			return true;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}

	private ProdutoImposto getByResultSet(ResultSet rs) throws SQLException {
		int columnIndex = 0;
		ProdutoImposto retorno = new ProdutoImposto();
		retorno.setId(rs.getLong(++columnIndex));
		retorno.setProduto(new Produto(rs.getLong(++columnIndex)));
		retorno.setIcmsEntrada(rs.getBigDecimal(++columnIndex));
		retorno.setIcmsSaida(rs.getBigDecimal(++columnIndex));
		retorno.setMva(rs.getBigDecimal(++columnIndex));
		retorno.setUf(rs.getString(++columnIndex));
		retorno.setFlgPossuiSt(rs.getString(++columnIndex));
		retorno.setPercentualSimples(rs.getBigDecimal(++columnIndex));
		return retorno;
	}

	@Override
	public List<ProdutoImposto> getBy(Produto produto) {
		List<ProdutoImposto> retorno = new ArrayList<>();
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(ALL));
			sql.append(" ").append(super.getProperty(BY_ID_PRODUTO));
			sql.append(" ").append(super.getProperty(ORDER_BY));
			pstmt = getTransaction().prepareStatement(sql);
			pstmt.setLong(1, produto.getId());
			logger.debug(pstmt);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				retorno.add(this.getByResultSet(rs));
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}
}