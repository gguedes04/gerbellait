package br.gerbellait.sistema.dao.impl;

import java.util.List;

import br.gerbellait.arquitetura.dao.AbstractDao;
import br.gerbellait.arquitetura.interceptors.annotations.Repository;
import br.gerbellait.sistema.dao.TipoPessoaDao;
import br.gerbellait.sistema.entity.TipoPessoa;

@Repository
public class TipoPessoaDaoImpl extends AbstractDao<TipoPessoa> implements TipoPessoaDao {
	private static final long serialVersionUID = 1L;

	static {
		banco.put("tipopessoa", "gpm_tipo_pessoa");
		banco.put("tipopessoa.id", "ID_TIPO_PESSOA");
		banco.put("tipopessoa.nome", "DSC_TIPO_PESSOA");
	}

	@Override
	public List<TipoPessoa> all() {
		return (List<TipoPessoa>) this.getAll(TipoPessoa.class);
	}
}