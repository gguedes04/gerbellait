package br.gerbellait.sistema.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.gerbellait.arquitetura.dao.AbstractDao;
import br.gerbellait.arquitetura.datasource.PreparedStatementWrapper;
import br.gerbellait.arquitetura.exceptions.DaoLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Repository;
import br.gerbellait.sistema.dao.ComissaoVendedorDao;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.entity.ComissaoVendedor;
import br.gerbellait.sistema.entity.GrupoProduto;
import br.gerbellait.sistema.entity.Vendedor;

@Repository
public class ComissaoVendedorDaoImpl extends AbstractDao<ComissaoVendedor> implements ComissaoVendedorDao {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(ComissaoVendedorDaoImpl.class);
	private static final String ALL = "comissaoVendedor.all";
	private static final String BY_ID_VENDEDOR = "comissaoVendedor.codigoVendedor";
	private static final String ORDER_BY = "comissaoVendedor.orderBy";
	private static final String EXCLUIR = "comissaoVendedor.excluir";

	static {
		banco.put("comissaovendedor", "gpm_comissao_vendedor");
		banco.put("comissaovendedor.id", "ID_COMISSAO");
		banco.put("comissaovendedor.vendedor", "ID_VENDEDOR");
		banco.put("comissaovendedor.grupoProduto", "ID_GRUPO_PRODUTO");
		banco.put("comissaovendedor.percentualComissao", "PERC_COMISSAO");
	}

	@Override
	public List<ComissaoVendedor> all() {
		return (List<ComissaoVendedor>) this.getAll(ComissaoVendedor.class);
	}

	@Override
	public boolean deletar(ComissaoVendedor comissaoVendedor) {
		PreparedStatementWrapper pstmt = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(EXCLUIR));
			pstmt = getTransaction().prepareStatement(sql);
			pstmt.setInt(1, ExcluidoEnum.EXCLUIDO.getId());
			pstmt.setLong(2, comissaoVendedor.getId());
			logger.debug(pstmt);
			pstmt.executeUpdate();
			return true;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}

	private ComissaoVendedor getByResultSet(ResultSet rs) throws SQLException {
		int columnIndex = 0;
		ComissaoVendedor retorno = new ComissaoVendedor();
		retorno.setId(rs.getLong(++columnIndex));
		retorno.setVendedor(new Vendedor(rs.getLong(++columnIndex)));
		retorno.setGrupoProduto(new GrupoProduto(rs.getLong(++columnIndex)));
		retorno.getGrupoProduto().setDescricao(rs.getString(++columnIndex));
		retorno.setPercentualComissao(rs.getBigDecimal(++columnIndex));
		return retorno;
	}

	@Override
	public List<ComissaoVendedor> getBy(Vendedor vendedor) {
		List<ComissaoVendedor> retorno = new ArrayList<>();
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(ALL));
			sql.append(" ").append(super.getProperty(BY_ID_VENDEDOR));
			sql.append(" ").append(super.getProperty(ORDER_BY));
			pstmt = getTransaction().prepareStatement(sql);
			pstmt.setLong(1, vendedor.getId());
			logger.debug(pstmt);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				retorno.add(this.getByResultSet(rs));
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}
}