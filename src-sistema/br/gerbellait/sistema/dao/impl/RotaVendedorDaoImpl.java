package br.gerbellait.sistema.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.gerbellait.arquitetura.dao.AbstractDao;
import br.gerbellait.arquitetura.datasource.PreparedStatementWrapper;
import br.gerbellait.arquitetura.exceptions.DaoLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Repository;
import br.gerbellait.sistema.dao.RotaVendedorDao;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.entity.Rota;
import br.gerbellait.sistema.entity.RotaVendedor;
import br.gerbellait.sistema.entity.Vendedor;

@Repository
public class RotaVendedorDaoImpl extends AbstractDao<RotaVendedor> implements RotaVendedorDao {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(RotaVendedorDaoImpl.class);
	private static final String ALL = "rotaVendedor.all";
	private static final String BY_ID_VENDEDOR = "rotaVendedor.codigoVendedor";
	private static final String ORDER_BY = "rotaVendedor.orderBy";
	private static final String EXCLUIR = "rotaVendedor.excluir";

	static {
		banco.put("rotavendedor", "gpm_rota_vendedor");
		banco.put("rotavendedor.id", "ID_ROTA_VENDEDOR");
		banco.put("rotavendedor.vendedor", "ID_VENDEDOR");
		banco.put("rotavendedor.rota", "ID_ROTA");
		banco.put("rotavendedor.dataAlteracao", "DT_ALTERACAO");
		banco.put("rotavendedor.situacao", "FL_EXCLUIDO");
	}

	@Override
	public List<RotaVendedor> all() {
		return (List<RotaVendedor>) this.getAll(RotaVendedor.class);
	}

	@Override
	public boolean deletar(RotaVendedor produtoImposto) {
		PreparedStatementWrapper pstmt = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(EXCLUIR));
			pstmt = getTransaction().prepareStatement(sql);
			pstmt.setInt(1, ExcluidoEnum.EXCLUIDO.getId());
			pstmt.setLong(2, produtoImposto.getId());
			logger.debug(pstmt);
			pstmt.executeUpdate();
			return true;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}

	private RotaVendedor getByResultSet(ResultSet rs) throws SQLException {
		int columnIndex = 0;
		RotaVendedor retorno = new RotaVendedor();
		retorno.setId(rs.getLong(++columnIndex));
		retorno.setVendedor(new Vendedor(rs.getLong(++columnIndex)));
		retorno.setRota(new Rota(rs.getLong(++columnIndex)));
		retorno.getRota().setDescricao(rs.getString(++columnIndex));
		retorno.setDataAlteracao(rs.getTimestamp(++columnIndex));
		retorno.setSituacao(rs.getInt(++columnIndex));
		retorno.setFlExcluido(ExcluidoEnum.getById(retorno.getSituacao()));
		return retorno;
	}

	@Override
	public List<RotaVendedor> getBy(Vendedor vendedor) {
		List<RotaVendedor> retorno = new ArrayList<>();
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(ALL));
			sql.append(" ").append(super.getProperty(BY_ID_VENDEDOR));
			sql.append(" ").append(super.getProperty(ORDER_BY));
			pstmt = getTransaction().prepareStatement(sql);
			pstmt.setLong(1, vendedor.getId());
			logger.debug(pstmt);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				retorno.add(this.getByResultSet(rs));
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}
}