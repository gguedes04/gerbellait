package br.gerbellait.sistema.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.gerbellait.arquitetura.dao.AbstractDao;
import br.gerbellait.arquitetura.datasource.PreparedStatementWrapper;
import br.gerbellait.arquitetura.exceptions.DaoLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Repository;
import br.gerbellait.sistema.dao.ProdutoPrecoRotaDao;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.entity.Produto;
import br.gerbellait.sistema.entity.ProdutoPrecoRota;
import br.gerbellait.sistema.entity.Rota;

@Repository
public class ProdutoPrecoRotaDaoImpl extends AbstractDao<ProdutoPrecoRota> implements ProdutoPrecoRotaDao {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(ProdutoPrecoRotaDaoImpl.class);
	private static final String ALL = "produtoPrecoRota.all";
	private static final String BY_ID_PRODUTO = "produtoPrecoRota.codigoProduto";
	private static final String BY_ID_ROTA = "produtoPrecoRota.codigoRota";
	private static final String BY_SITUACAO = "produtoPrecoRota.situacao";
	private static final String ORDER_BY = "produtoPrecoRota.orderBy";
	private static final String EXCLUIR = "produtoPrecoRota.excluir";

	static {
		banco.put("produtoprecorota", "gpm_prod_preco_rota");
		banco.put("produtoprecorota.id", "ID_PROD_PRECO_ROTA");
		banco.put("produtoprecorota.produto", "ID_PRODUTO");
		banco.put("produtoprecorota.rota", "ID_ROTA");
		banco.put("produtoprecorota.valorPrecoVendaProduto", "VL_PRECO_VENDA_PROD");
		banco.put("produtoprecorota.dataAlteracao", "DT_ALTERACAO");
		banco.put("produtoprecorota.flExcluido", "FL_EXCLUIDO");
	}

	@Override
	public List<ProdutoPrecoRota> all() {
		return (List<ProdutoPrecoRota>) this.getAll(ProdutoPrecoRota.class);
	}

	@Override
	public boolean deletar(ProdutoPrecoRota produtoImposto) {
		PreparedStatementWrapper pstmt = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(EXCLUIR));
			pstmt = getTransaction().prepareStatement(sql);
			pstmt.setInt(1, ExcluidoEnum.EXCLUIDO.getId());
			pstmt.setLong(2, produtoImposto.getId());
			logger.debug(pstmt);
			pstmt.executeUpdate();
			return true;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}

	private ProdutoPrecoRota getByResultSet(ResultSet rs) throws SQLException {
		int columnIndex = 0;
		ProdutoPrecoRota retorno = new ProdutoPrecoRota();
		retorno.setId(rs.getLong(++columnIndex));
		retorno.setProduto(new Produto(rs.getLong(++columnIndex)));
		retorno.setRota(new Rota(rs.getLong(++columnIndex)));
		retorno.setValorPrecoVendaProduto(rs.getBigDecimal(++columnIndex));
		retorno.setDataAlteracao(rs.getTimestamp(++columnIndex));
		retorno.setFlExcluido(rs.getInt(++columnIndex));
		retorno.setSituacao(ExcluidoEnum.getById(retorno.getFlExcluido()));
		return retorno;
	}

	private void where(Produto produto, StringBuilder sql) {
		if (produto.getId() != null) {
			sql.append(" ").append(super.getProperty(BY_ID_PRODUTO));
		}
		if (produto.getSituacao() != null) {
			sql.append(" ").append(super.getProperty(BY_SITUACAO));
		}
	}

	private void parameters(Produto produto, PreparedStatementWrapper pstmt) throws SQLException {
		Integer parameterIndex = 0;
		if (produto.getId() != null) {
			pstmt.setLong(++parameterIndex, produto.getId());
		}
		if (produto.getSituacao() != null) {
			pstmt.setInt(++parameterIndex, produto.getSituacao());
		}
	}

	@Override
	public List<ProdutoPrecoRota> getBy(Produto produto) {
		List<ProdutoPrecoRota> retorno = new ArrayList<>();
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(ALL));
			this.where(produto, sql);
			sql.append(" ").append(super.getProperty(ORDER_BY));
			pstmt = getTransaction().prepareStatement(sql);
			this.parameters(produto, pstmt);
			logger.debug(pstmt);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				retorno.add(this.getByResultSet(rs));
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}

	@Override
	public ProdutoPrecoRota getBy(ProdutoPrecoRota produtoPrecoRota) {
		ProdutoPrecoRota retorno = null;
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(ALL));
			if (produtoPrecoRota.getProduto() != null && produtoPrecoRota.getProduto().getId() != null) {
				sql.append(" ").append(super.getProperty(BY_ID_PRODUTO));
			}
			if (produtoPrecoRota.getRota() != null && produtoPrecoRota.getRota().getId() != null) {
				sql.append(" ").append(super.getProperty(BY_ID_ROTA));
			}
			sql.append(" ").append(super.getProperty(ORDER_BY));
			pstmt = getTransaction().prepareStatement(sql);
			Integer parameterIndex = 0;
			if (produtoPrecoRota.getProduto() != null && produtoPrecoRota.getProduto().getId() != null) {
				pstmt.setLong(++parameterIndex, produtoPrecoRota.getProduto().getId());
			}
			if (produtoPrecoRota.getRota() != null && produtoPrecoRota.getRota().getId() != null) {
				pstmt.setLong(++parameterIndex, produtoPrecoRota.getRota().getId());
			}
			logger.debug(pstmt);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				retorno = this.getByResultSet(rs);
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}
}