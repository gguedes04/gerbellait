package br.gerbellait.sistema.dao.impl;

import java.util.List;

import br.gerbellait.arquitetura.dao.AbstractDao;
import br.gerbellait.arquitetura.interceptors.annotations.Repository;
import br.gerbellait.sistema.dao.PerfilDao;
import br.gerbellait.sistema.entity.Perfil;

@Repository
public class PerfilDaoImpl extends AbstractDao<Perfil> implements PerfilDao {
	private static final long serialVersionUID = 1L;

	static {
		banco.put("perfil", "gpm_perfil");
		banco.put("perfil.id", "co_perfil");
		banco.put("perfil.nome", "nm_perfil");
	}

	@Override
	public List<Perfil> getAll() {
		return (List<Perfil>) this.getAll(Perfil.class);
	}
}