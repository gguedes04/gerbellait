package br.gerbellait.sistema.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.gerbellait.arquitetura.dao.AbstractDao;
import br.gerbellait.arquitetura.datasource.PreparedStatementWrapper;
import br.gerbellait.arquitetura.exceptions.DaoLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Repository;
import br.gerbellait.sistema.dao.RotaDao;
import br.gerbellait.sistema.entity.Rota;

@Repository
public class RotaDaoImpl extends AbstractDao<Rota> implements RotaDao {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(RotaDaoImpl.class);
	private static final String ALL_BY_VENDEDOR = "rota.vendedor.all";

	static {
		banco.put("rota", "gpm_rota");
		banco.put("rota.id", "ID_ROTA");
		banco.put("rota.descricao", "DSC_ROTA");
		banco.put("rota.dataAlteracao", "DT_ALTERACAO");
		banco.put("rota.flExcluido", "FL_EXCLUIDO");
	}

	private Rota getByResultSet(ResultSet rs) throws SQLException {
		int columnIndex = 0;
		Rota retorno = new Rota();
		retorno.setId(rs.getLong(++columnIndex));
		retorno.setDescricao(rs.getString(++columnIndex));
		retorno.setDataAlteracao(rs.getTimestamp(++columnIndex));
		retorno.setFlExcluido(rs.getInt(++columnIndex));
		return retorno;
	}

	@Override
	public List<Rota> getByVendedor(Long idVendedor) {
		List<Rota> retorno = new ArrayList<>();
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(ALL_BY_VENDEDOR));
			pstmt = getTransaction().prepareStatement(sql);
			pstmt.setLong(1, idVendedor);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				retorno.add(this.getByResultSet(rs));
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}
}