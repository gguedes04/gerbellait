package br.gerbellait.sistema.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.gerbellait.arquitetura.dao.AbstractDao;
import br.gerbellait.arquitetura.datasource.PreparedStatementWrapper;
import br.gerbellait.arquitetura.exceptions.DaoLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Repository;
import br.gerbellait.arquitetura.util.Utils;
import br.gerbellait.sistema.dao.MsnDao;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.entity.Msn;

@Repository
public class MsnDaoImpl extends AbstractDao<Msn> implements MsnDao {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(MsnDaoImpl.class);
	private static final String ALL = "msn.all";
	private static final String ALL_COUNT = "msn.all.count";
	private static final String BY_CODIGO = "msn.codigo";
	private static final String BY_RESPONSAVEL = "msn.responsavel";
	private static final String BY_DESTINATARIO = "msn.destinatario";
	private static final String BY_SITUACAO = "msn.situacao";
	private static final String BY_DATA = "msn.data";
	private static final String ORDER_BY = "msn.orderBy";
	private static final String PAGINATE_LIMIT_OFFSET = "paginacao.limit.offset";
	private static final String EXCLUIR = "msn.excluir";

	static {
		banco.put("msn", "msn");
		banco.put("msn.id", "id");
		banco.put("msn.msg", "msg");
		banco.put("msn.responsavel", "responsavel");
		banco.put("msn.data", "data");
		banco.put("msn.destinatario", "destinatario");
		banco.put("msn.dataAlteracao", "DT_ALTERACAO");
		banco.put("msn.situacao", "FL_EXCLUIDO");
	}

	private void where(StringBuilder sql, Msn msn) {
		if (msn.getId() != null) {
			sql.append(" ").append(super.getProperty(BY_CODIGO));
		}
		if (!Utils.isNullOrEmpty(msn.getResponsavel())) {
			sql.append(" ").append(super.getProperty(BY_RESPONSAVEL));
		}
		if (!Utils.isNullOrEmpty(msn.getDestinatario())) {
			sql.append(" ").append(super.getProperty(BY_DESTINATARIO));
		}
		if (msn.getFlExcluido() != null) {
			sql.append(" ").append(super.getProperty(BY_SITUACAO));
		}
		if (msn.getData() != null) {
			sql.append(" ").append(super.getProperty(BY_DATA));
		}
	}

	private void parameters(PreparedStatementWrapper pstmt, Msn msn, boolean isPaginate) throws SQLException {
		Integer parameterIndex = 0;
		if (msn.getId() != null) {
			pstmt.setLong(++parameterIndex, msn.getId());
		}
		if (!Utils.isNullOrEmpty(msn.getResponsavel())) {
			pstmt.setString(++parameterIndex, "%" + msn.getResponsavel() + "%");
		}
		if (!Utils.isNullOrEmpty(msn.getDestinatario())) {
			pstmt.setString(++parameterIndex, "%" + msn.getDestinatario() + "%");
		}
		if (msn.getFlExcluido() != null) {
			pstmt.setInt(++parameterIndex, msn.getFlExcluido().getId());
		}
		if (msn.getData() != null) {
			pstmt.setDate(++parameterIndex, new java.sql.Date(msn.getData().getTime()));
		}
		if (isPaginate) {
			pstmt.setInt(++parameterIndex, msn.getPaginate().getQuantidadePorPagina());
			pstmt.setInt(++parameterIndex, msn.getPaginate().getOffset());
		}
	}

	public Integer totalAmount(Msn msn) {
		Integer retorno = null;
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(ALL_COUNT));
			this.where(sql, msn);
			pstmt = getTransaction().prepareStatement(sql);
			this.parameters(pstmt, msn, false);
			logger.debug(pstmt);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				retorno = rs.getInt(1);
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}

	private Msn getByResultSet(ResultSet rs) throws SQLException {
		int columnIndex = 0;
		Msn retorno = new Msn();
		retorno.setId(rs.getLong(++columnIndex));
		retorno.setMsg(rs.getString(++columnIndex));
		retorno.setResponsavel(rs.getString(++columnIndex));
		retorno.setData(rs.getTimestamp(++columnIndex));
		retorno.setDestinatario(rs.getString(++columnIndex));
		retorno.setDataAlteracao(rs.getTimestamp(++columnIndex));
		retorno.setSituacao(rs.getInt(++columnIndex));
		retorno.setFlExcluido(ExcluidoEnum.getById(retorno.getSituacao()));
		return retorno;
	}

	@Override
	public List<Msn> getByDto(Msn msn) {
		msn.getPaginate().setNumTotalRegistros(this.totalAmount(msn));
		List<Msn> retorno = new ArrayList<>();
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(ALL));
			this.where(sql, msn);
			sql.append(" ").append(super.getProperty(ORDER_BY));
			sql.append(" ").append(super.getProperty(PAGINATE_LIMIT_OFFSET));
			pstmt = getTransaction().prepareStatement(sql);
			this.parameters(pstmt, msn, true);
			logger.debug(pstmt);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				retorno.add(this.getByResultSet(rs));
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}

	@Override
	public List<Msn> all() {
		return (List<Msn>) this.getAll(Msn.class);
	}

	@Override
	public boolean deletar(Msn msn) {
		PreparedStatementWrapper pstmt = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(EXCLUIR));
			pstmt = getTransaction().prepareStatement(sql);
			pstmt.setInt(1, ExcluidoEnum.EXCLUIDO.getId());
			pstmt.setLong(2, msn.getId());
			logger.debug(pstmt);
			pstmt.executeUpdate();
			return true;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}
}