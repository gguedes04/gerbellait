package br.gerbellait.sistema.dao.impl;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.gerbellait.arquitetura.dao.AbstractDao;
import br.gerbellait.arquitetura.datasource.PreparedStatementWrapper;
import br.gerbellait.arquitetura.exceptions.DaoLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Repository;
import br.gerbellait.arquitetura.util.StringUtils;
import br.gerbellait.sistema.bean.PedidoImpBean;
import br.gerbellait.sistema.bean.PedidosFilterBean;
import br.gerbellait.sistema.bean.TableState;
import br.gerbellait.sistema.bean.UltimasVendasBean;
import br.gerbellait.sistema.dao.PedidoDao;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.entity.FormaPagamento;
import br.gerbellait.sistema.entity.Pedido;
import br.gerbellait.sistema.entity.Pessoa;
import br.gerbellait.sistema.entity.Vendedor;
import br.gerbellait.sistema.service.ProdutoService;
import br.gerbellait.sistema.service.UsuarioService;
import br.gerbellait.sistema.util.Constants;

@Repository
public class PedidoDaoImpl extends AbstractDao<Pedido> implements PedidoDao {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(PedidoDaoImpl.class);
    
    private static final String ALL_COUNT = "pedido.all.count";
    private static final String ALL = "pedido.all";
    private static final String BY_PRODUTO = "pedido.getByProduto";
    private static final String BY_FORNECEDOR = "pedido.getByFornecedor";
    private static final String BY_CLIENTE = "pedido.getByCliente";
    private static final String BY_VENDEDOR = "pedido.getByVendedor";
    private static final String BY_IDCODPEDIDO = "pedido.getByIdCodPedido";
    private static final String BY_PERIODO_VENDA = "pedido.getByPeriodoVenda";
    private static final String BY_PERIODO_ENTREGA = "pedido.getByPeriodoEntrega";
    private static final String BY_ROTA = "pedido.getByRota";
    private static final String BY_USUARIO_VENDEDOR = "pedido.getByUsuarioVendedor";
    private static final String ORDER_BY = "pedido.orderBy";
    private static final String GROUP_BY = "pedido.getGroupBY";
    private static final String EXCLUIR = "pedido.excluir";
    private static final String RELATORIO = "pedido.relatorio";
    private static final String ULTIMAS_VENDAS = "pedido.ultimas.vendas";
    private static final String INTEGRA_META_PEDIDO = "integra.meta.pedido";
    private static final String PAGINATE_LIMIT_OFFSET = "paginacao.limit.offset";
    
    @Inject
    private UsuarioService usuarioService;
    
    @Inject
    private ProdutoService produtoService;
    
    static {
        banco.put("pedido", "gpm_pedido");
        banco.put("pedido.id", "ID_PEDIDO");
        banco.put("pedido.idPedidoCliente", "ID_PEDIDO_CLIENTE");
        banco.put("pedido.cliente", "ID_CLIENTE");
        banco.put("pedido.vendedor", "ID_VENDEDOR");
        banco.put("pedido.formaPagamento", "ID_FORMA_PAGAMENTO");
        banco.put("pedido.situacaoPedido", "FLG_STATUS");
        banco.put("pedido.valorDesconto", "VL_DESCONTO");
        banco.put("pedido.dataPedido", "DT_PEDIDO");
        banco.put("pedido.dataVencimento", "DT_VENCIMENTO");
        banco.put("pedido.dataEntrega", "DT_ENTREGA");
        banco.put("pedido.flGeraNf", "FLG_GERA_NF");
        banco.put("pedido.observacao", "OBSERVACAO");
        banco.put("pedido.dataAlteracao", "DT_ALTERACAO");
        banco.put("pedido.situacao", "FL_EXCLUIDO");
        banco.put("pedido.valorPedido", "VL_PEDIDO");
        banco.put("pedido.usuario", "USUARIO");
    }
    
    @Override
    public List<Pedido> all() {
        return (List<Pedido>) this.getAll(Pedido.class);
    }
    
    private void where(StringBuilder sql, PedidosFilterBean filter) throws IOException {
        if (filter.getPedido() != null && filter.getPedido().getIdPedidoCliente() != null) {
            sql.append(" ").append(super.getProperty(BY_IDCODPEDIDO));
        }
        if (filter.getProduto() != null && filter.getProduto().getId() != null) {
            sql.append(" ").append(super.getProperty(BY_PRODUTO));
        }
        if (filter.getFornecedor() != null) {
            sql.append(" ").append(super.getProperty(BY_FORNECEDOR));
        }
        if (filter.getCliente() != null && filter.getCliente().getId() != null) {
            sql.append(" ").append(super.getProperty(BY_CLIENTE));
        }
        if (filter.getVendedor() != null && filter.getVendedor().getId() != null) {
            sql.append(" ").append(super.getProperty(BY_VENDEDOR));
        }
        if (!StringUtils.isNullOrEmpty(filter.getVendaInicial())) {
            sql.append(" ").append(super.getProperty(BY_PERIODO_VENDA));
        }
        if (!StringUtils.isNullOrEmpty(filter.getEntregaInicial())) {
            sql.append(" ").append(super.getProperty(BY_PERIODO_ENTREGA));
        }
        if (filter.getRota() != null) {
            sql.append(" ").append(super.getProperty(BY_ROTA));
        }
        
/*        if (filter.getUsuarioLogado() != null && usuarioService.isVendedor(filter.getUsuarioLogado())) {
            sql.append(" ").append(super.getProperty(BY_USUARIO_VENDEDOR));
        }*/
    }
    
    private void parameters(PreparedStatementWrapper pstmt, PedidosFilterBean filter, TableState tableState, boolean isPaginate) throws SQLException, IOException {
        Integer parameterIndex = 0;
        if (filter.getPedido() != null
                && filter.getPedido().getIdPedidoCliente() != null) {
            pstmt.setLong(++parameterIndex, filter.getPedido().getIdPedidoCliente());
        }
        if (filter.getProduto() != null
                && filter.getProduto().getId() != null) {
            pstmt.setLong(++parameterIndex, filter.getProduto().getId());
        }
        
        if (filter.getFornecedor() != null) {
            pstmt.setLong(++parameterIndex, filter.getFornecedor().getId());
        }
        if (filter.getCliente() != null && filter.getCliente().getId() != null) {
            pstmt.setLong(++parameterIndex, filter.getCliente().getId());
        }
        if (filter.getVendedor() != null && filter.getVendedor().getId() != null) {
            pstmt.setLong(++parameterIndex, filter.getVendedor().getId());
        }
        
        if (!StringUtils.isNullOrEmpty(filter.getVendaInicial())
                && !StringUtils.isNullOrEmpty(filter.getVendaFinal())) {
            
            pstmt.setString(++parameterIndex, filter.getVendaInicial());
            pstmt.setString(++parameterIndex, filter.getVendaFinal());
            
        } else if (!StringUtils.isNullOrEmpty(filter.getVendaInicial())
                && StringUtils.isNullOrEmpty(filter.getVendaFinal())) {
            
            pstmt.setString(++parameterIndex, filter.getVendaInicial());
            LocalDate localDate = LocalDate.now();
            localDate = localDate.plusYears(10);
            pstmt.setString(++parameterIndex, localDate.format(DateTimeFormatter.ofPattern(Constants.DATE_FORMAT_DEFAULT)));
        }
        
        if (!StringUtils.isNullOrEmpty(filter.getEntregaInicial())
                && !StringUtils.isNullOrEmpty(filter.getEntregaFinal())) {
            
            pstmt.setString(++parameterIndex, filter.getEntregaInicial());
            pstmt.setString(++parameterIndex, filter.getEntregaFinal());
            
        } else if (!StringUtils.isNullOrEmpty(filter.getEntregaInicial())
                && StringUtils.isNullOrEmpty(filter.getEntregaFinal())) {
            
            pstmt.setString(++parameterIndex, filter.getEntregaInicial());
            
            LocalDate localDate = LocalDate.now();
            localDate = localDate.plusYears(10);
            pstmt.setString(++parameterIndex, localDate.format(DateTimeFormatter.ofPattern(Constants.DATE_FORMAT_DEFAULT)));
        }
        if (filter.getRota() != null) {
            pstmt.setLong(++parameterIndex, filter.getRota().getId());
        }
        
/*        if (filter.getUsuarioLogado() != null && usuarioService.isVendedor(filter.getUsuarioLogado())) {
            pstmt.setLong(++parameterIndex, filter.getUsuarioLogado());
        }*/
        
        if (isPaginate) {
            pstmt.setInt(++parameterIndex, tableState.getPagination().getNumber());
            pstmt.setInt(++parameterIndex, tableState.getPagination().getStart());
        }
    }
    
    private Pedido getByResultSet(ResultSet rs) throws SQLException {
        int columnIndex = 0;
        Pedido retorno = new Pedido();
        retorno.setId(rs.getLong(++columnIndex));
        retorno.setIdPedidoCliente(rs.getLong(++columnIndex));
        retorno.setCliente(new Pessoa(rs.getLong(++columnIndex)));
        retorno.setVendedor(new Vendedor(rs.getLong(++columnIndex)));
        retorno.setFormaPagamento(new FormaPagamento(rs.getLong(++columnIndex)));
        retorno.setSituacaoPedido(rs.getString(++columnIndex));
        retorno.setValorDesconto(rs.getBigDecimal(++columnIndex));
        retorno.setDataPedido(rs.getDate(++columnIndex));
        retorno.setDataVencimento(rs.getDate(++columnIndex));
        retorno.setDataEntrega(rs.getDate(++columnIndex));
        retorno.setFlGeraNf(rs.getString(++columnIndex));
        retorno.setObservacao(rs.getString(++columnIndex));
        retorno.setDataAlteracao(rs.getDate(++columnIndex));
        retorno.setSituacao(rs.getInt(++columnIndex));
        retorno.setValorPedido(rs.getBigDecimal(++columnIndex));
        retorno.setUsuario(rs.getString(++columnIndex));
        retorno.setNomeCliente(rs.getString(++columnIndex));
        retorno.setNomeVendedor(rs.getString(++columnIndex));
        retorno.setDescricaoFormaPagamento(rs.getString(++columnIndex));
        retorno.setTotalVenda(rs.getBigDecimal(++columnIndex));
        retorno.setTotalTroca(rs.getBigDecimal(++columnIndex));
        retorno.setTotalLiquido(rs.getBigDecimal(++columnIndex));
        retorno.setIdCodCliente(rs.getLong(++columnIndex));
        return retorno;
    }
    
    private Integer totalAmount(TableState tableState, PedidosFilterBean filter) throws SQLException, IOException {
        Integer retorno = null;
        PreparedStatementWrapper pstmt = null;
        ResultSet rs = null;
        try {
            StringBuilder sql = new StringBuilder(super.getProperty(ALL_COUNT));
            sql.insert(0, "select count(1) from (");
            this.where(sql, filter);
            sql.append(" ").append(super.getProperty(GROUP_BY));
            sql.append(") total");
            pstmt = getTransaction().prepareStatement(sql);
            this.parameters(pstmt, filter, tableState, false);
            logger.debug(pstmt);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                retorno = rs.getInt(1);
            }
            return retorno;
            
        } catch (Exception e) {
            logger.error(e);
            throw e;
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    
    @Override
    public List<Pedido> getByDto(TableState tableState) throws Exception {
        PreparedStatementWrapper pstmt = null;
        ResultSet rs = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            PedidosFilterBean filter = mapper.convertValue(tableState.getSearch().getPredicateObject(), PedidosFilterBean.class);
            tableState.getPagination().setNumberOfPages(this.totalAmount(tableState, filter));
            List<Pedido> retorno = new ArrayList<>();
            StringBuilder sql = new StringBuilder(super.getProperty(ALL));
            this.where(sql, filter);
            sql.append(" ").append(super.getProperty(GROUP_BY));
            if (!StringUtils.isNullOrEmpty(tableState.getSort().getPredicate()) && tableState.getSort().isReverse()) {
                sql.append(" ").append(" order by pedido.").append(tableState.getSort().getPredicate()).append(" desc");
            } else if (!StringUtils.isNullOrEmpty(tableState.getSort().getPredicate()) && !tableState.getSort().isReverse()) {
                sql.append(" ").append(" order by pedido.").append(tableState.getSort().getPredicate()).append(" asc");
            } else {
                sql.append(" ").append(super.getProperty(ORDER_BY));
            }
            sql.append(" ").append(super.getProperty(PAGINATE_LIMIT_OFFSET));
            pstmt = getTransaction().prepareStatement(sql);
            this.parameters(pstmt, filter, tableState, true);
            logger.debug(pstmt);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                retorno.add(this.getByResultSet(rs));
            }
            return retorno;
            
        } catch (Exception e) {
            logger.error(e);
            throw e;
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    @Override
    public boolean deletar(Pedido pedido) {
        PreparedStatementWrapper pstmt = null;
        try {
            StringBuilder sql = new StringBuilder(super.getProperty(EXCLUIR));
            pstmt = getTransaction().prepareStatement(sql);
            pstmt.setInt(1, ExcluidoEnum.EXCLUIDO.getId());
            pstmt.setLong(2, pedido.getId());
            logger.debug(pstmt);
            pstmt.executeUpdate();
            return true;
            
        } catch (SQLException e) {
            throw new DaoLayerException(e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (SQLException e) {
                logger.info(e);
            }
        }
    }
    
    @Override
    public List<PedidoImpBean> relatorio(Pedido pedido) {
        List<PedidoImpBean> result = new ArrayList<>();
        if (pedido.getIdPedidoCliente() == null) {
            return Collections.emptyList();
        }
        StringBuilder sql = new StringBuilder(super.getProperty(RELATORIO));
        try (PreparedStatementWrapper pstmt = getTransaction().prepareStatement(sql)) {
            
            pstmt.setLong(1, pedido.getIdPedidoCliente());
            
            logger.debug(pstmt);
            ResultSet rs = pstmt.executeQuery();
            
            while (rs.next()) {
                PedidoImpBean bean = new PedidoImpBean();
                bean.setVlTotalProduto(rs.getDouble("VL_TOTAL"));
                bean.setVlUnitario(rs.getDouble("VL_UNITARIO"));
                bean.setQtTrocaProduto(rs.getDouble("QTDE_TROCA"));
                bean.setQtProduto(rs.getDouble("QTDE_PRODUTO"));
                bean.setNmProduto(rs.getString("NM_PRODUTO"));
                bean.setCodProduto(rs.getLong("id_cod_produto"));
                bean.setIdTipoPessoa(rs.getLong("id_tipo_empresa"));
                bean.setPossuiSt(rs.getString("flg_possuist"));
                bean.setIcmsSaida(rs.getDouble("ICMSSAIDA"));
                bean.setIcmsEntrada(rs.getDouble("ICMSENTRADA"));
                bean.setPercSimples(rs.getInt("PERCSIMPLES"));
                bean.setMva(rs.getDouble("MVA"));
                bean.setNumero(rs.getString("numero"));
                bean.setGeraNf(rs.getString("FLG_GERA_NF"));
                bean.setObservacao(rs.getString("OBSERVACAO"));
                bean.setDesconto(rs.getDouble("VL_DESCONTO"));
                bean.setUnidade(rs.getString("TP_UNIDADE"));
                bean.setPrazo(rs.getInt("PRAZO"));
                bean.setNmFantasia(rs.getString("NM_FANTASIA"));
                bean.setDtPedido(rs.getDate("dt_pedido"));
                bean.setFormaPagamento(rs.getString("DSC_FORMA_PAGAMENTO"));
                bean.setVlTotal(rs.getDouble("vl_total"));
                bean.setCep(rs.getString("CEP"));
                bean.setTelefone(rs.getString("TELEFONE"));
                bean.setInscrEstadual(rs.getString("INSCR_ESTADUAL"));
                bean.setUf(rs.getString("DSC_UF"));
                bean.setCidade(rs.getString("NM_CIDADE"));
                bean.setBairro(rs.getString("BAIRRO"));
                bean.setLogradouro(rs.getString("LOGRADOURO"));
                bean.setOptanteSimples(rs.getString("FLG_OPTANTE_SIMPLES"));
                bean.setCnpj(rs.getString("cnpj"));
                bean.setNmPessoa(rs.getString("NM_PESSOA"));
                bean.setCodPessoa(rs.getLong("ID_COD_PESSOA"));
                bean.setPedidoCliente(rs.getLong("ID_PEDIDO_CLIENTE"));
                bean.setVlPedido(rs.getDouble("VL_PEDIDO"));
                result.add(bean);
            }
            
        } catch (SQLException e) {
            throw new DaoLayerException(e);
        }
        return result;
    }
    
    
    @Override
    public List<UltimasVendasBean> getUltimasVendas(Pedido pedido) {
        List<UltimasVendasBean> result = new ArrayList<>();
        
        StringBuilder sql = new StringBuilder(super.getProperty(ULTIMAS_VENDAS));
        try (PreparedStatementWrapper pstmt = getTransaction().prepareStatement(sql)) {
            
            pstmt.setLong(1, pedido.getCliente().getId());
            
            logger.debug(pstmt);
            ResultSet rs = pstmt.executeQuery();
            
            while (rs.next()) {
                UltimasVendasBean bean = new UltimasVendasBean();
                bean.setIdPedido(rs.getLong("id_pedid"));
                bean.setIdPedidc(rs.getLong("id_pedidc"));
                bean.setIdVendedor(rs.getLong("id_vendedor"));
                bean.setIdClien(rs.getLong("id_clien"));
                bean.setDtPedid(rs.getDate("dt_pedid"));
                bean.setDtEntre(rs.getDate("dt_entre"));
                bean.setVlDesco(rs.getDouble("vl_desco"));
                bean.setIdForma(rs.getLong("id_forma"));
                bean.setFlgGera(rs.getString("flg_gera"));
                bean.setFlExclu(rs.getString("fl_exclu"));
                bean.setObserva(rs.getString("observa"));
                bean.setFlgSta(rs.getString("flg_sta"));
                bean.setDtAlte(rs.getDate("dt_alte"));
                bean.setVlPedi(rs.getDouble("vl_pedi"));
                bean.setDtVenc(rs.getDate("dt_venc"));
                bean.setIdVend(rs.getLong("id_vend"));
                bean.setIdProduto(rs.getLong("ID_PRODUTO"));
                bean.setIdCodProduto(rs.getLong("ID_COD_PRODUTO"));
                bean.setNmProduto(rs.getString("NM_PRODUTO"));
                bean.setTpUnidade(rs.getString("TP_UNIDADE"));
                bean.setQtdeProduto(rs.getDouble("QTDE_PRODUTO"));
                bean.setVlUnitario(rs.getDouble("VL_UNITARIO"));
                bean.setVlTotal(rs.getDouble("VL_TOTAL"));
                bean.setQtdeTroca(rs.getDouble("QTDE_TROCA"));
                bean.setVlTotalTroca(rs.getDouble("VL_TOTAL_TROCA"));
                bean.setVlDesconto(rs.getDouble("VL_DESCONTO"));
                bean.setDtAlteracao(rs.getDate("DT_ALTERACAO"));
                result.add(bean);
            }
            
        } catch (SQLException e) {
            logger.error("erro ao buscasr dados  ----------------------> ", e);
            throw new DaoLayerException(e);
        }
        return result;
    }
}
