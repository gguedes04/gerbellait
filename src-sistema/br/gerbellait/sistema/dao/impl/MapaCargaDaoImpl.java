package br.gerbellait.sistema.dao.impl;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.jboss.logging.Logger;

import br.gerbellait.arquitetura.dao.AbstractDao;
import br.gerbellait.arquitetura.datasource.PreparedStatementWrapper;
import br.gerbellait.arquitetura.exceptions.DaoLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Repository;
import br.gerbellait.arquitetura.util.StringUtils;
import br.gerbellait.sistema.bean.MapaCargaFilterBean;
import br.gerbellait.sistema.bean.MapaCargaPedidoBean;
import br.gerbellait.sistema.bean.MapaCargaProdutoBean;
import br.gerbellait.sistema.dao.MapaCargaDao;
import br.gerbellait.sistema.entity.MapaCarga;

@Repository
public class MapaCargaDaoImpl extends AbstractDao<MapaCarga> implements MapaCargaDao {
    private static final Logger logger = Logger.getLogger(MapaCargaDaoImpl.class);
    private static final String FIND_ALL_REPORTS = "mapacarga.findMapaCargaBySearch";
    private static final String GET_PEDIDOS = "mapacarga.getPedidos";
    private static final String GET_PRODUTOS = "mapacarga.getProdutos";
    private static final String INSERT = "mapacarga.insert";
    private static final String DELETE = "mapacarga.delete";
    private static final String GET_PEDIDOS_VINCULADOS = "mapacarga.pedidos.vinculados";
    private static final String GET_PEDIDOS_NAO_VINCULADOS = "mapacarga.pedidos.naoVinculados";
    private static final String GET_MAX_NR_MAPA_CARGA = "mapacarga.maxNrMapaCarga";
    
    @Override
    public List<MapaCarga> findLoadMapReport() throws SQLException {
        List<MapaCarga> result = new ArrayList<>();
        PreparedStatementWrapper pstmt = null;
        ResultSet rs = null;
        try {
            StringBuilder sql = new StringBuilder(super.getProperty(FIND_ALL_REPORTS));
            pstmt = getTransaction().prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                MapaCarga mapaCarga = new MapaCarga();
                mapaCarga.setId(rs.getLong(1));
                result.add(mapaCarga);
            }
            return result;
            
        } catch (SQLException e) {
            throw new DaoLayerException(e);
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    
    @Override
    public List<MapaCargaPedidoBean> getPedidos(MapaCargaFilterBean filter) throws SQLException {
        List<MapaCargaPedidoBean> result = new ArrayList<>();
        PreparedStatementWrapper pstmt = null;
        ResultSet rs = null;
        try {
            StringBuilder sql = new StringBuilder(super.getProperty(GET_PEDIDOS));
            
            addSqlWhere(filter, sql);
            
            sql.append(" order by p.dt_pedido DESC, p.id_pedido_cliente");
            
            pstmt = getTransaction().prepareStatement(sql);
            rs = pstmt.executeQuery();
            
            while (rs.next()) {
                MapaCargaPedidoBean bean = new MapaCargaPedidoBean();
                bean.setCliente(rs.getLong("id_cliente"));
                bean.setPedido(rs.getLong("id_pedido"));
                bean.setPedidoCodeCli(rs.getLong("id_pedido_cliente"));
                bean.setData(rs.getDate("dt_pedido"));
                bean.setNome(rs.getString("nm_pessoa"));
                bean.setValor(rs.getDouble("vl_pedido"));
                bean.setRota(rs.getLong("id_rota"));
                result.add(bean);
            }
            return result;
            
        } catch (Exception e) {
            throw new DaoLayerException(e);
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    
    @Override
    public List<MapaCargaProdutoBean> getProdutos(MapaCargaFilterBean filter) throws SQLException {
        List<MapaCargaProdutoBean> result = new ArrayList<>();
        PreparedStatementWrapper pstmt = null;
        ResultSet rs = null;
        try {
            StringBuilder sql = new StringBuilder(super.getProperty(GET_PRODUTOS));
            
            addSqlWhere(filter, sql);
            
            if (filter.getSelectedOrders() != null && !filter.getSelectedOrders().isEmpty()) {
                sql.append("and p.id_pedido in(")
                        .append(filter.getSelectedOrders().stream()
                                .map(mapaCargaPedidoBean -> String.valueOf(mapaCargaPedidoBean.getPedido()))
                                .collect(Collectors.joining(", "))
                        )
                        .append(")");
            }
            
            sql.append(" group by pr.ID_PRODUTO, pr.ID_COD_PRODUTO, pr.NM_PRODUTO, pr.TP_UNIDADE ");
            sql.append(" order by pr.ID_COD_PRODUTO");
            
            pstmt = getTransaction().prepareStatement(sql);
            rs = pstmt.executeQuery();
            
            while (rs.next()) {
                MapaCargaProdutoBean bean = new MapaCargaProdutoBean();
                bean.setCustomerOrder(rs.getLong("id_pedido_cliente"));
                bean.setProductId(rs.getLong("ID_PRODUTO"));
                bean.setProductIdCli(rs.getString("ID_COD_PRODUTO"));
                bean.setProductDesc(rs.getString("NM_PRODUTO"));
                bean.setUnit(rs.getString("TP_UNIDADE"));
                bean.setTotalSelles(rs.getDouble("TotalVenda"));
                bean.setTotalChanges(rs.getDouble("TotalTroca"));
                bean.setTotalLoad(rs.getDouble("TotalCarga"));
                bean.setAveragePrice(rs.getDouble("precomedio"));
                bean.setMinPrice(rs.getDouble("precomin"));
                bean.setMaxPrice(rs.getDouble("precomax"));
                bean.setTotal(rs.getDouble("TotalFaturado"));
                result.add(bean);
            }
            return result;
            
        } catch (Exception e) {
            throw new DaoLayerException(e);
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    
    private String transformList(List<Long> list) {
        return list.stream().map(String::valueOf).collect(Collectors.joining(", "));
    }
    
    private void addSqlWhere(MapaCargaFilterBean filter, StringBuilder sql) {
        if (filter.getSelectedSellers() != null && !filter.getSelectedSellers().isEmpty()) {
            sql.append(" and v1.id_vendedor in(").append(transformList(filter.getSelectedSellers())).append(")");
        }
        
        if (filter.getSelectedRoutes() != null && !filter.getSelectedRoutes().isEmpty()) {
            sql.append(" and r.id_rota in(").append(transformList(filter.getSelectedRoutes())).append(")");
        }
        
        if (!StringUtils.isNullOrEmpty(filter.getDataInicial())
                && !StringUtils.isNullOrEmpty(filter.getDataFinal())) {
            sql.append(" and p.dt_pedido between STR_TO_DATE('")
                    .append(filter.getDataInicial())
                    .append("', '%d/%m/%Y') and STR_TO_DATE('")
                    .append(filter.getDataFinal())
                    .append("', '%d/%m/%Y')");
        }
        
        if (filter.getLoadMapNumber() == null) {
            sql.append(" and not exists(select 0 from gpm_mapa_carga as mc where mc.id_pedido = p.id_pedido)");
        } else {
            sql.append(" and exists(select 0 from gpm_mapa_carga as mc where mc.id_pedido = p.id_pedido)");
        }
    }
    
    private Long getMaxNrMapaCarga() {
        StringBuilder sql = new StringBuilder(super.getProperty(GET_MAX_NR_MAPA_CARGA));
        try (PreparedStatementWrapper pstmt = getTransaction().prepareStatement(sql);
             ResultSet rs = pstmt.executeQuery()) {
            rs.next();
            return rs.getLong("value") + 1;
        } catch (SQLException e) {
            throw new DaoLayerException(e);
        }
    }
    
    @Override
    public Long save(List<MapaCargaPedidoBean> list) throws SQLException {
        Long nrMapaCarga = getMaxNrMapaCarga();
        for (MapaCargaPedidoBean mapaCargaPedidoBean : list) {
            StringBuilder sql = new StringBuilder(super.getProperty(INSERT));
            PreparedStatement pstmt = getTransaction().prepareStatement(sql);
            pstmt.setLong(1, mapaCargaPedidoBean.getPedido());
            pstmt.setDate(2, new Date(mapaCargaPedidoBean.getData().getTime()));
            pstmt.setDate(3, new Date(new java.util.Date().getTime()));
            pstmt.setLong(4, mapaCargaPedidoBean.getRota());
            pstmt.setDouble(5, mapaCargaPedidoBean.getValor());
            pstmt.setLong(6, mapaCargaPedidoBean.getPedidoCodeCli());
            pstmt.setLong(7, nrMapaCarga);
            pstmt.executeUpdate();
            pstmt.close();
        }
        return nrMapaCarga;
    }
    
    
    @Override
    public List<MapaCarga> getLoadMapByFilter(MapaCargaFilterBean filter) {
        try {
            StringBuilder sql = new StringBuilder(super.getProperty(FIND_ALL_REPORTS));
            sql.append(addSqlWhereLoadMapByFilter(filter));
            
            return getMapaCargaList(sql);
            
        } catch (Exception e) {
            throw new DaoLayerException(e);
        }
    }
    
    private List<MapaCarga> getMapaCargaList(StringBuilder sql) throws SQLException {
        List<MapaCarga> result = new ArrayList<>();
        PreparedStatementWrapper pstmt = null;
        ResultSet rs = null;
        pstmt = getTransaction().prepareStatement(sql);
        rs = pstmt.executeQuery();
        try {
            while (rs.next()) {
                MapaCarga mapaCarga = new MapaCarga();
                mapaCarga.setId(rs.getLong(1));
                mapaCarga.setDtInclusaoMapaCarga(rs.getTimestamp(2));
                result.add(mapaCarga);
            }
            return result;
            
        } catch (SQLException e) {
            throw new DaoLayerException(e);
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    
    private StringBuilder addSqlWhereLoadMapByFilter(MapaCargaFilterBean filter) {
        StringBuilder sqlWhere = new StringBuilder();
        sqlWhere.append(" where 1=1 ");
        
        if (!StringUtils.isNullOrEmpty(filter.getDataInicial())
                && !StringUtils.isNullOrEmpty(filter.getDataFinal())) {
            sqlWhere.append(" and mc.dt_pedido between ")
                    .append(" STR_TO_DATE('")
                    .append(filter.getDataInicial())
                    .append("', '%d/%m/%Y') and STR_TO_DATE('")
                    .append(filter.getDataFinal())
                    .append("', '%d/%m/%Y')");
        }
        
        if (filter.getSelectedRoutes() != null && !filter.getSelectedRoutes().isEmpty()) {
            sqlWhere.append(" and mc.id_rota in(").append(transformList(filter.getSelectedRoutes())).append(")");
        }
        
        if (!Objects.isNull(filter.getLoadMapNumber())) {
            MapaCarga mapa = filter.getLoadMapNumber();
            sqlWhere.append(" and  mc.nr_mapa_carga = ").append(mapa.getId());
        }
        
        return sqlWhere;
    }
    
    public void deleteLoadMap(Long id) throws SQLException {
        StringBuilder sql = new StringBuilder(super.getProperty(DELETE));
        PreparedStatementWrapper pstmt = getTransaction().prepareStatement(sql);
        pstmt.setLong(1, id);
        try {
            pstmt.execute();
        } catch (Exception e) {
            logger.debug("Erro ao deletar o mapa de carga", e);
        }
    }
    
    public List<MapaCargaPedidoBean> getPedidosNaoVinculados(MapaCargaFilterBean filter) throws SQLException {
        List<MapaCargaPedidoBean> result = new ArrayList<>();
        PreparedStatementWrapper pstmt = null;
        ResultSet rs = null;
        StringBuilder sql = new StringBuilder(super.getProperty(GET_PEDIDOS_NAO_VINCULADOS));
        if (filter.getSelectedSellers() != null && !filter.getSelectedSellers().isEmpty()) {
            sql.append(" and v1.id_vendedor in(").append(transformList(filter.getSelectedSellers())).append(")");
        }
        
        if (filter.getSelectedRoutes() != null && !filter.getSelectedRoutes().isEmpty()) {
            sql.append(" and r.id_rota in(").append(transformList(filter.getSelectedRoutes())).append(")");
        }
        
        if (!StringUtils.isNullOrEmpty(filter.getDataInicial())
                && !StringUtils.isNullOrEmpty(filter.getDataFinal())) {
            sql.append(" and p.dt_pedido between STR_TO_DATE('")
                    .append(filter.getDataInicial())
                    .append("', '%d/%m/%Y') and STR_TO_DATE('")
                    .append(filter.getDataFinal())
                    .append("', '%d/%m/%Y')");
        }
        
        sql.append(" and not exists(select 0 ")
                .append(" from gpm_mapa_carga as mc ")
                .append(" where mc.id_pedido = p.id_pedido) ")
                .append(" order by p.dt_pedido DESC, p.id_pedido_cliente ");
        
        try {
            
            pstmt = getTransaction().prepareStatement(sql);
            rs = pstmt.executeQuery();
            
            while (rs.next()) {
                MapaCargaPedidoBean bean = new MapaCargaPedidoBean();
                bean.setCliente(rs.getLong("id_cliente"));
                bean.setPedido(rs.getLong("id_pedido"));
                bean.setPedidoCodeCli(rs.getLong("id_pedido_cliente"));
                bean.setData(rs.getDate("dt_pedido"));
                bean.setNome(rs.getString("nm_pessoa"));
                bean.setValor(rs.getDouble("vl_pedido"));
                bean.setRota(rs.getLong("id_rota"));
                result.add(bean);
            }
            return result;
            
        } catch (Exception e) {
            throw new DaoLayerException(e);
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    
    public List<MapaCargaPedidoBean> getPedidosVinculados(Long mapaCargaId) throws SQLException {
        List<MapaCargaPedidoBean> result = new ArrayList<>();
        PreparedStatementWrapper pstmt = null;
        ResultSet rs = null;
        StringBuilder sql = new StringBuilder(super.getProperty(GET_PEDIDOS_VINCULADOS));
        sql.append(" and mc.nr_mapa_carga = ").append(mapaCargaId);
        sql.append(" order by p.id_pedido_cliente ");
        
        try {
            
            pstmt = getTransaction().prepareStatement(sql);
            rs = pstmt.executeQuery();
            
            while (rs.next()) {
                MapaCargaPedidoBean bean = new MapaCargaPedidoBean();
                bean.setCliente(rs.getLong("id_cliente"));
                bean.setPedido(rs.getLong("id_pedido"));
                bean.setPedidoCodeCli(rs.getLong("id_pedido_cliente"));
                bean.setData(rs.getDate("dt_pedido"));
                bean.setNome(rs.getString("nm_pessoa"));
                bean.setValor(rs.getDouble("vl_pedido"));
                result.add(bean);
            }
            return result;
            
        } catch (Exception e) {
            throw new DaoLayerException(e);
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    
    @Override
    public boolean hasLinkedPedido(Long pedido) {
        StringBuilder sql = new StringBuilder(super.getProperty(FIND_ALL_REPORTS));
        sql.insert(0, "select count(1) value from (");
        sql.append(" where mc.id_pedido = ").append(pedido);
        sql.append(") c");
        try (PreparedStatementWrapper pstmt = getTransaction().prepareStatement(sql);
             ResultSet rs = pstmt.executeQuery()) {
            rs.next();
            return rs.getInt("value") > 0;
        } catch (SQLException e) {
            throw new DaoLayerException(e);
        }
    }
}
