package br.gerbellait.sistema.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import br.gerbellait.arquitetura.dao.AbstractDao;
import br.gerbellait.arquitetura.datasource.PreparedStatementWrapper;
import br.gerbellait.arquitetura.exceptions.DaoLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Repository;
import br.gerbellait.sistema.dao.MetaVendedorProdutoDao;
import br.gerbellait.sistema.entity.MetaVendedorProduto;
import br.gerbellait.sistema.entity.Produto;
import br.gerbellait.sistema.entity.Vendedor;

@Repository
public class MetaVendedorProdutoDaoImpl extends AbstractDao<MetaVendedorProduto> implements MetaVendedorProdutoDao {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(MetaVendedorProdutoDaoImpl.class);

	private static final String ALL = "metaVendedorProduto.all";
	private static final String BY_ID_PRODUTO = "metaVendedorProduto.codigoProduto";
	private static final String BY_ID_VENDEDOR = "metaVendedorProduto.codigoVendedor";
	private static final String BY_PERIODO = "metaVendedorProduto.periodo";
	private static final String ORDER_BY = "metaVendedorProduto.orderBy";

	static {
		banco.put("metavendedorproduto", "gpm_meta_vendedor_prod");
		banco.put("metavendedorproduto.id", "ID_META_VENDEDOR_PROD");
		banco.put("metavendedorproduto.vendedor", "ID_VENDEDOR");
		banco.put("metavendedorproduto.produto", "ID_PRODUTO");
		banco.put("metavendedorproduto.dataInicial", "DT_INICIAL");
		banco.put("metavendedorproduto.dataFinal", "DT_FINAL");
		banco.put("metavendedorproduto.quantidadeVenda", "QTDE_VENDA");
		banco.put("metavendedorproduto.valorPrecoMedioVenda", "VL_PRECO_MEDIO_VENDA");
		banco.put("metavendedorproduto.quantidadeLimiteVenda", "QTDE_LIMITE_VENDA");
		banco.put("metavendedorproduto.percentualDesconto", "PERC_DESC");
		banco.put("metavendedorproduto.quantidadeMinima", "QTDE_MINIMA");
		banco.put("metavendedorproduto.dataAlteracao", "DT_ALTERACAO");
		banco.put("metavendedorproduto.quantidadeLimiteVendaInicial", "QTDE_LIMITE_VENDA_INICIAL");
	}

	@Override
	public List<MetaVendedorProduto> all() {
		return (List<MetaVendedorProduto>) this.getAll(MetaVendedorProduto.class);
	}

	private MetaVendedorProduto getByResultSet(ResultSet rs) throws SQLException {
		int columnIndex = 0;
		MetaVendedorProduto retorno = new MetaVendedorProduto();
		retorno.setId(rs.getLong(++columnIndex));
		retorno.setVendedor(new Vendedor(rs.getLong(++columnIndex)));
		retorno.setProduto(new Produto(rs.getLong(++columnIndex)));
		retorno.setDataInicial(rs.getTimestamp(++columnIndex));
		retorno.setDataFinal(rs.getTimestamp(++columnIndex));
		retorno.setQuantidadeVenda(rs.getBigDecimal(++columnIndex));
		retorno.setValorPrecoMedioVenda(rs.getBigDecimal(++columnIndex));
		retorno.setQuantidadeLimiteVenda(rs.getBigDecimal(++columnIndex));
		retorno.setPercentualDesconto(rs.getBigDecimal(++columnIndex));
		retorno.setQuantidadeMinima(rs.getBigDecimal(++columnIndex));
		retorno.setDataAlteracao(rs.getTimestamp(++columnIndex));
		retorno.setQuantidadeLimiteVendaInicial(rs.getBigDecimal(++columnIndex));
		return retorno;
	}

	@Override
	public MetaVendedorProduto getBy(MetaVendedorProduto filtro) {
		MetaVendedorProduto retorno = null;
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(ALL));
			if (filtro.getProduto() != null && filtro.getProduto().getId() != null) {
				sql.append(" ").append(super.getProperty(BY_ID_PRODUTO));
			}
			if (filtro.getVendedor() != null && filtro.getVendedor().getId() != null) {
				sql.append(" ").append(super.getProperty(BY_ID_VENDEDOR));
			}
			if (filtro.getDataInicial() != null && filtro.getDataFinal() != null) {
				sql.append(" ").append(super.getProperty(BY_PERIODO));
			}
			sql.append(" ").append(super.getProperty(ORDER_BY));
			pstmt = getTransaction().prepareStatement(sql);
			Integer parameterIndex = 0;
			if (filtro.getProduto() != null && filtro.getProduto().getId() != null) {
				pstmt.setLong(++parameterIndex, filtro.getProduto().getId());
			}
			if (filtro.getVendedor() != null && filtro.getVendedor().getId() != null) {
				pstmt.setLong(++parameterIndex, filtro.getVendedor().getId());
			}
			if (filtro.getDataInicial() != null && filtro.getDataFinal() != null) {
				pstmt.setDate(++parameterIndex, new java.sql.Date(filtro.getDataInicial().getTime()));
				pstmt.setDate(++parameterIndex, new java.sql.Date(filtro.getDataFinal().getTime()));
			}
			logger.debug(pstmt);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				retorno = this.getByResultSet(rs);
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}
}