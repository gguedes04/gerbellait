package br.gerbellait.sistema.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.gerbellait.arquitetura.dao.AbstractDao;
import br.gerbellait.arquitetura.datasource.PreparedStatementWrapper;
import br.gerbellait.arquitetura.exceptions.DaoLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Repository;
import br.gerbellait.arquitetura.util.Utils;
import br.gerbellait.sistema.dao.PessoaDao;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.entity.Cidade;
import br.gerbellait.sistema.entity.FormaPagamento;
import br.gerbellait.sistema.entity.Pessoa;
import br.gerbellait.sistema.entity.TipoPessoa;

@Repository
public class PessoaDaoImpl extends AbstractDao<Pessoa> implements PessoaDao {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(PessoaDaoImpl.class);
	private static final String ALL = "pessoa.all";
	private static final String ALL_COUNT = "pessoa.all.count";
	private static final String BY_CODIGO = "pessoa.codigo";
	private static final String BY_CODIGOGER = "pessoa.codigoger";
	private static final String BY_NOME = "pessoa.nome";
	private static final String BY_NOME_FANTASIA = "pessoa.nomeFantasia";
	private static final String BY_CNPJ = "pessoa.cnpj";
	private static final String BY_FORMA_PAGAMENTO = "pessoa.formaPagamento";
	private static final String BY_VENDEDOR = "pessoa.vendedor";
	private static final String BY_BAIRRO = "pessoa.bairro";
	private static final String BY_UF = "pessoa.uf";
	private static final String BY_CIDADE = "pessoa.cidade";
	private static final String BY_TIPO_PESSOA = "pessoa.tipo.pessoa";
	private static final String BY_TIPO_ENDERECO = "pessoa.tipo.endereco";
	private static final String ORDER_BY = "pessoa.orderBy";
	private static final String PAGINATE_LIMIT_OFFSET = "paginacao.limit.offset";
	private static final String EXCLUIR = "pessoa.excluir";
	private static final String NEXT_ID = "pessoa.nextId";

	static {
		banco.put("pessoa", "gpm_pessoa");
		banco.put("pessoa.id", "ID_PESSOA");
		banco.put("pessoa.idCodPessoa", "ID_COD_PESSOA");
		banco.put("pessoa.tipoPessoa", "ID_TIPO_PESSOA");
		banco.put("pessoa.nome", "NM_PESSOA");
		banco.put("pessoa.nomeFantasia", "NM_FANTASIA");
		banco.put("pessoa.cnpj", "CNPJ");
		banco.put("pessoa.inscricaoEstadual", "INSCR_ESTADUAL");
		banco.put("pessoa.prazo", "PRAZO");
		banco.put("pessoa.formaPagamento", "ID_FORMA_PAGAMENTO");
		banco.put("pessoa.idRota", "ID_ROTA");
		banco.put("pessoa.idVendedorRepresentante", "ID_VENDEDOR_REPRESENTANTE");
		banco.put("pessoa.cnae", "CNAE");
		banco.put("pessoa.flgOptanteSimples", "FLG_OPTANTE_SIMPLES");
		banco.put("pessoa.nomeContato", "NM_CONTATO");
		banco.put("pessoa.percDescPadrao", "PERC_DESC_PADRAO");
		banco.put("pessoa.nomeRepresentante", "NM_REPRESENTANTE");
		banco.put("pessoa.email", "EMAIL");
		banco.put("pessoa.dataAlteracao", "DT_ALTERACAO");
		banco.put("pessoa.situacao", "FL_EXCLUIDO");
		banco.put("pessoa.observacao", "OBSERVACAO");
		banco.put("pessoa.idTipoEmpresa", "ID_TIPO_EMPRESA");
		banco.put("pessoa.idRegimeTributario", "IC_REGIME_TRIBUTARIO");
	}

	@Override
	public List<Pessoa> all() {
		return (List<Pessoa>) this.getAll(Pessoa.class);
	}

	private void where(StringBuilder sql, Pessoa pessoa) {
		if (pessoa.getId() != null) {
			sql.append(" ").append(super.getProperty(BY_CODIGO));
		}
		if (!Utils.isNullOrEmpty(pessoa.getNome())) {
			sql.append(" ").append(super.getProperty(BY_NOME));
		}
		if (!Utils.isNullOrEmpty(pessoa.getNomeFantasia())) {
			sql.append(" ").append(super.getProperty(BY_NOME_FANTASIA));
		}
		if (!Utils.isNullOrEmpty(pessoa.getCnpj())) {
			sql.append(" ").append(super.getProperty(BY_CNPJ));
		}
		if (pessoa.getFormaPagamento() != null && pessoa.getFormaPagamento().getId() != null) {
			sql.append(" ").append(super.getProperty(BY_FORMA_PAGAMENTO));
		}
		if (pessoa.getIdVendedorRepresentante() != null) {
			sql.append(" ").append(super.getProperty(BY_VENDEDOR));
		}
		if (pessoa.getEnderecoComercial() != null && !Utils.isNullOrEmpty(pessoa.getEnderecoComercial().getBairro())) {
			sql.append(" ").append(super.getProperty(BY_BAIRRO));
		}
		if (pessoa.getEnderecoComercial() != null && pessoa.getEnderecoComercial().getCidade() != null
				&& pessoa.getEnderecoComercial().getCidade().getUf() != null
				&& pessoa.getEnderecoComercial().getCidade().getUf().getId() != null) {
			sql.append(" ").append(super.getProperty(BY_UF));
		}
		if (pessoa.getEnderecoComercial() != null && pessoa.getEnderecoComercial().getCidade() != null
				&& pessoa.getEnderecoComercial().getCidade().getId() != null) {
			sql.append(" ").append(super.getProperty(BY_CIDADE));
		}
		if (pessoa.getTipoPessoa() != null && pessoa.getTipoPessoa().getId() != null) {
			sql.append(" ").append(super.getProperty(BY_TIPO_PESSOA));
		}
		if (pessoa.getEnderecoComercial() != null && pessoa.getEnderecoComercial().getTipoEndereco() != null
				&& pessoa.getEnderecoComercial().getTipoEndereco().getId() != null) {
			sql.append(" ").append(super.getProperty(BY_TIPO_ENDERECO));
		}
	}

	private void parameters(PreparedStatementWrapper pstmt, Pessoa pessoa, boolean isPaginate) throws SQLException {
		Integer parameterIndex = 0;
		if (pessoa.getId() != null) {
			pstmt.setLong(++parameterIndex, pessoa.getId());
		}
		if (!Utils.isNullOrEmpty(pessoa.getNome())) {
			pstmt.setString(++parameterIndex, "%" + pessoa.getNome() + "%");
		}
		if (!Utils.isNullOrEmpty(pessoa.getNomeFantasia())) {
			pstmt.setString(++parameterIndex, "%" + pessoa.getNomeFantasia() + "%");
		}
		if (!Utils.isNullOrEmpty(pessoa.getCnpj())) {
			pstmt.setString(++parameterIndex, "%" + pessoa.getCnpj() + "%");
		}
		if (pessoa.getFormaPagamento() != null && pessoa.getFormaPagamento().getId() != null) {
			pstmt.setLong(++parameterIndex, pessoa.getFormaPagamento().getId());
		}
		if (pessoa.getIdVendedorRepresentante() != null) {
			pstmt.setLong(++parameterIndex, pessoa.getIdVendedorRepresentante());
		}
		if (pessoa.getEnderecoComercial() != null && !Utils.isNullOrEmpty(pessoa.getEnderecoComercial().getBairro())) {
			pstmt.setString(++parameterIndex, "%" + pessoa.getEnderecoComercial().getBairro() + "%");
		}
		if (pessoa.getEnderecoComercial() != null && pessoa.getEnderecoComercial().getCidade() != null
				&& pessoa.getEnderecoComercial().getCidade().getUf() != null
				&& pessoa.getEnderecoComercial().getCidade().getUf().getId() != null) {
			pstmt.setLong(++parameterIndex, pessoa.getEnderecoComercial().getCidade().getUf().getId());
		}
		if (pessoa.getEnderecoComercial() != null && pessoa.getEnderecoComercial().getCidade() != null
				&& pessoa.getEnderecoComercial().getCidade().getId() != null) {
			pstmt.setLong(++parameterIndex, pessoa.getEnderecoComercial().getCidade().getId());
		}
		if (pessoa.getTipoPessoa() != null && pessoa.getTipoPessoa().getId() != null) {
			pstmt.setLong(++parameterIndex, pessoa.getTipoPessoa().getId());
		}
		if (pessoa.getEnderecoComercial() != null && pessoa.getEnderecoComercial().getTipoEndereco() != null
				&& pessoa.getEnderecoComercial().getTipoEndereco().getId() != null) {
			pstmt.setLong(++parameterIndex, pessoa.getEnderecoComercial().getTipoEndereco().getId());
		}
		if (isPaginate) {
			pstmt.setInt(++parameterIndex, pessoa.getPaginate().getQuantidadePorPagina());
			pstmt.setInt(++parameterIndex, pessoa.getPaginate().getOffset());
		}
	}

	@Override
	public boolean deletar(Pessoa pessoa) {
		PreparedStatementWrapper pstmt = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(EXCLUIR));
			pstmt = getTransaction().prepareStatement(sql);
			pstmt.setInt(1, ExcluidoEnum.EXCLUIDO.getId());
			pstmt.setLong(2, pessoa.getId());
			logger.debug(pstmt);
			pstmt.executeUpdate();
			return true;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}

	@Override
	public Long nextId() {
		Long retorno = null;
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(NEXT_ID));
			pstmt = getTransaction().prepareStatement(sql);
			logger.debug(pstmt);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				retorno = rs.getLong(1);
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}

	public Integer totalAmount(Pessoa pessoa) {
		Integer retorno = null;
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(ALL_COUNT));
			this.where(sql, pessoa);
			pstmt = getTransaction().prepareStatement(sql);
			this.parameters(pstmt, pessoa, false);
			logger.debug(pstmt);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				retorno = rs.getInt(1);
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}

	private Pessoa getByResultSet(ResultSet rs) throws SQLException {
		int columnIndex = 0;
		Pessoa retorno = new Pessoa();
		retorno.setId(rs.getLong(++columnIndex));
		retorno.setIdCodPessoa(rs.getLong(++columnIndex));
		retorno.setTipoPessoa(new TipoPessoa(rs.getLong(++columnIndex)));
		retorno.setNome(rs.getString(++columnIndex));
		retorno.setNomeFantasia(rs.getString(++columnIndex));
		retorno.setCnpj(rs.getString(++columnIndex));
		retorno.setInscricaoEstadual(rs.getString(++columnIndex));
		retorno.setPrazo(rs.getInt(++columnIndex));
		retorno.setFormaPagamento(new FormaPagamento(rs.getLong(++columnIndex)));
		retorno.setIdRota(rs.getInt(++columnIndex));
		retorno.setIdVendedorRepresentante(rs.getLong(++columnIndex));
		retorno.setCnae(rs.getString(++columnIndex));
		retorno.setFlgOptanteSimples(rs.getString(++columnIndex));
		retorno.setNomeContato(rs.getString(++columnIndex));
		retorno.setPercDescPadrao(rs.getBigDecimal(++columnIndex));
		retorno.setNomeRepresentante(rs.getString(++columnIndex));
		retorno.setEmail(rs.getString(++columnIndex));
		retorno.setDataAlteracao(rs.getTimestamp(++columnIndex));
		retorno.setFlExcluido(ExcluidoEnum.getById(rs.getInt(++columnIndex)));
		retorno.getEnderecoComercial().setBairro(rs.getString(++columnIndex));
		retorno.getEnderecoComercial().setCidade(new Cidade(rs.getString(++columnIndex)));
		FormaPagamento formaPagamento = new FormaPagamento();
		formaPagamento.setId(rs.getLong(++columnIndex));
		formaPagamento.setDescricao(rs.getString(++columnIndex));
		retorno.setFormaPagamento(formaPagamento);
		retorno.setObservacao(rs.getString(++columnIndex));
		retorno.setIdTipoEmpresa(rs.getString(++columnIndex));
		retorno.setIdRegimeTributario(rs.getString(++columnIndex));
		return retorno;
	}

	@Override
	public Pessoa getByCodigo(Pessoa pessoa) {
		Pessoa retorno = null;
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(ALL));
			sql.append(" ").append(super.getProperty(BY_CODIGOGER));
			pstmt = getTransaction().prepareStatement(sql);
			pstmt.setLong(1, pessoa.getId());
			logger.debug(pstmt);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				retorno = this.getByResultSet(rs);
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}

	@Override
	public List<Pessoa> getByDto(Pessoa pessoa) {
		pessoa.getPaginate().setNumTotalRegistros(this.totalAmount(pessoa));
		List<Pessoa> retorno = new ArrayList<>();
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(ALL));
			this.where(sql, pessoa);
			sql.append(" ").append(super.getProperty(ORDER_BY));
			sql.append(" ").append(super.getProperty(PAGINATE_LIMIT_OFFSET));
			pstmt = getTransaction().prepareStatement(sql);
			this.parameters(pstmt, pessoa, true);
			logger.debug(pstmt);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				retorno.add(this.getByResultSet(rs));
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}

    @Override
    public List<Pessoa> getByDtoPedido(Pessoa pessoa) {
        pessoa.getPaginate().setNumTotalRegistros(this.totalAmount(pessoa));
        List<Pessoa> retorno = new ArrayList<>();
        PreparedStatementWrapper pstmt = null;
        ResultSet rs = null;
        try {
            StringBuilder sql = new StringBuilder(super.getProperty(ALL));
            this.where(sql, pessoa);
            sql.append(" ").append(super.getProperty(ORDER_BY));
            // sql.append(" ").append(super.getProperty(PAGINATE_LIMIT_OFFSET));
            pstmt = getTransaction().prepareStatement(sql);
            this.parameters(pstmt, pessoa, false);
            logger.debug(pstmt);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                retorno.add(this.getByResultSet(rs));
            }
            return retorno;

        } catch (SQLException e) {
            throw new DaoLayerException(e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                logger.info(e);
            }
        }
    }


	@Override
	public List<Pessoa> getExport(Pessoa pessoa) {
		pessoa.getPaginate().setNumTotalRegistros(this.totalAmount(pessoa));
		List<Pessoa> retorno = new ArrayList<>();
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(ALL));
			this.where(sql, pessoa);
			sql.append(" ").append(super.getProperty(ORDER_BY));
			pstmt = getTransaction().prepareStatement(sql);
			this.parameters(pstmt, pessoa, false);
			logger.debug(pstmt);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				retorno.add(this.getByResultSet(rs));
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}
}