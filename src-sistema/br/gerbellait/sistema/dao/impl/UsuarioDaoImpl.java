package br.gerbellait.sistema.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sun.xml.internal.ws.org.objectweb.asm.Type;

import br.gerbellait.arquitetura.dao.AbstractDao;
import br.gerbellait.arquitetura.datasource.PreparedStatementWrapper;
import br.gerbellait.arquitetura.exceptions.DaoLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Repository;
import br.gerbellait.arquitetura.util.Utils;
import br.gerbellait.sistema.dao.UsuarioDao;
import br.gerbellait.sistema.entity.Usuario;
import br.gerbellait.sistema.entity.Vendedor;

@Repository
public class UsuarioDaoImpl extends AbstractDao<Usuario> implements UsuarioDao {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(UsuarioDaoImpl.class);
	private static final String INSERT = "usuario.insert";
	private static final String ALL = "usuario.all";
	private static final String LOGIN = "usuario.login";
	private static final String NOME = "usuario.nome";
	private static final String SITUACAO = "usuario.situacao";
	private static final String PERFIL = "usuario.perfil";
	private static final String VENDEDOR = "usuario.vendedor";
	private static final String IS_VENDEDOR = "usuario.isVendedor";
	private static final String GET_BY = "usuario.getBy";
	private static final String ORDER_BY = "usuario.orderBy";

	static {
		banco.put("usuario", "gpm_usuario");
		banco.put("usuario.id", "co_usuario");
		banco.put("usuario.login", "login");
		banco.put("usuario.nome", "nome");
		banco.put("usuario.senha", "senha");
		banco.put("usuario.email", "email");
		banco.put("usuario.situacao", "situacao");
		banco.put("usuario.icPrimeiroAcesso", "ic_primeiro_acesso");
		banco.put("usuario.vendedor", "id_vendedor");
	}

	@Override
	public Usuario insert(Usuario entity) {
		PreparedStatementWrapper pstmt = null;
		ResultSet genKey = null;
		try {
			pstmt = getTransaction().prepareInsertStatement(new StringBuilder(super.getProperty(INSERT)));
			pstmt.setString(1, entity.getLogin());
			pstmt.setString(2, entity.getNome());
			pstmt.setString(3, entity.getSenha());
			pstmt.setString(4, entity.getEmail());
			pstmt.setInt(5, entity.getSituacao());
			if (entity.getVendedor() != null && entity.getVendedor().getId() != null) {
				pstmt.setLong(6, entity.getVendedor().getId());
			} else {
				pstmt.setNull(6, Type.LONG);
			}
			logger.debug(pstmt);
			pstmt.executeUpdate();
			genKey = pstmt.getGeneratedKeys();
			if (genKey.next()) {
				entity.setId(genKey.getLong(1));
			}
		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (SQLException e) {
				logger.debug(e);
			}
		}
		return entity;
	}

	@Override
	public List<Usuario> all() {
		List<Usuario> retorno = new ArrayList<>();
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(ALL));
			sql.append(" ").append(super.getProperty(ORDER_BY));
			pstmt = getTransaction().prepareStatement(sql);
			logger.debug(pstmt);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Usuario usuario = new Usuario();
				usuario.setId(rs.getLong(1));
				usuario.setLogin(rs.getString(2));
				usuario.setSenha(rs.getString(3));
				usuario.setNome(rs.getString(4));
				usuario.setEmail(rs.getString(5));
				usuario.setSituacao(rs.getInt(6));
				usuario.setIcPrimeiroAcesso(rs.getBoolean(7));
				usuario.setVendedor(new Vendedor(rs.getLong(8)));
				retorno.add(usuario);
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}

	public void where(Usuario usuarioFitro, StringBuilder sql) {
		if (!Utils.isNullOrEmpty(usuarioFitro.getLogin())) {
			sql.append(" ").append(super.getProperty(LOGIN));
		}
		if (!Utils.isNullOrEmpty(usuarioFitro.getNome())) {
			sql.append(" ").append(super.getProperty(NOME));
		}
		if (usuarioFitro.getSituacao() != null) {
			sql.append(" ").append(super.getProperty(SITUACAO));
		}
		if (usuarioFitro.getPerfil() != null && usuarioFitro.getPerfil().getId() != null) {
			sql.append(" ").append(super.getProperty(PERFIL));
		}
		if (usuarioFitro.getVendedor() != null && usuarioFitro.getVendedor().getId() != null) {
			sql.append(" ").append(super.getProperty(VENDEDOR));
		}
	}

	public void setParameters(Usuario usuarioFitro, PreparedStatementWrapper pstmt) throws SQLException {
		int parameterIndex = 0;
		if (!Utils.isNullOrEmpty(usuarioFitro.getLogin())) {
			pstmt.setString(++parameterIndex, "%" + usuarioFitro.getLogin() + "%");
		}
		if (!Utils.isNullOrEmpty(usuarioFitro.getNome())) {
			pstmt.setString(++parameterIndex, "%" + usuarioFitro.getNome() + "%");
		}
		if (usuarioFitro.getSituacao() != null) {
			pstmt.setInt(++parameterIndex, usuarioFitro.getSituacao());
		}
		if (usuarioFitro.getPerfil() != null && usuarioFitro.getPerfil().getId() != null) {
			pstmt.setLong(++parameterIndex, usuarioFitro.getPerfil().getId());
		}
		if (usuarioFitro.getVendedor() != null && usuarioFitro.getVendedor().getId() != null) {
			pstmt.setLong(++parameterIndex, usuarioFitro.getVendedor().getId());
		}
	}

	@Override
	public List<Usuario> getByDto(Usuario usuarioFitro) {
		List<Usuario> retorno = new ArrayList<>();
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(ALL));
			this.where(usuarioFitro, sql);
			sql.append(" ").append(super.getProperty(ORDER_BY));
			pstmt = getTransaction().prepareStatement(sql);
			this.setParameters(usuarioFitro, pstmt);
			logger.debug(pstmt);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Usuario usuario = new Usuario();
				usuario.setId(rs.getLong(1));
				usuario.setLogin(rs.getString(2));
				usuario.setSenha(rs.getString(3));
				usuario.setNome(rs.getString(4));
				usuario.setEmail(rs.getString(5));
				usuario.setSituacao(rs.getInt(6));
				usuario.setIcPrimeiroAcesso(rs.getBoolean(7));
				usuario.setVendedor(new Vendedor(rs.getLong(8)));
				retorno.add(usuario);
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}
	
	@Override
	public List<Usuario> getVendedores() {
		List<Usuario> retorno = new ArrayList<>();
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(ALL));
			sql.append(" ").append(super.getProperty(IS_VENDEDOR));
			sql.append(" ").append(super.getProperty(ORDER_BY));
			pstmt = getTransaction().prepareStatement(sql);
			logger.debug(pstmt);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Usuario usuario = new Usuario();
				usuario.setId(rs.getLong(1));
				usuario.setLogin(rs.getString(2));
				usuario.setSenha(rs.getString(3));
				usuario.setNome(rs.getString(4));
				usuario.setEmail(rs.getString(5));
				usuario.setSituacao(rs.getInt(6));
				usuario.setIcPrimeiroAcesso(rs.getBoolean(7));
				usuario.setVendedor(new Vendedor(rs.getLong(8)));
				retorno.add(usuario);
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}

	@Override
	public Usuario getBy(String login, String senha) {
		Usuario retorno = null;
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = getTransaction().prepareStatement(new StringBuilder(super.getProperty(GET_BY)));
			pstmt.setString(1, login);
			pstmt.setString(2, senha);
			logger.debug(pstmt);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				retorno = new Usuario();
				retorno.setId(rs.getLong(1));
				retorno.setLogin(rs.getString(2));
				retorno.setSenha(rs.getString(3));
				retorno.setNome(rs.getString(4));
				retorno.setEmail(rs.getString(5));
				retorno.setSituacao(rs.getInt(6));
				retorno.setIcPrimeiroAcesso(rs.getBoolean(7));
				retorno.setVendedor(new Vendedor(rs.getLong(8)));
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}
}