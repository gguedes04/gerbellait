package br.gerbellait.sistema.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.gerbellait.arquitetura.dao.AbstractDao;
import br.gerbellait.arquitetura.datasource.PreparedStatementWrapper;
import br.gerbellait.arquitetura.exceptions.DaoLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Repository;
import br.gerbellait.sistema.dao.PerfilUsuarioDao;
import br.gerbellait.sistema.entity.Perfil;
import br.gerbellait.sistema.entity.PerfilUsuario;
import br.gerbellait.sistema.entity.Usuario;

@Repository
public class PerfilUsuarioDaoImpl extends AbstractDao<PerfilUsuario> implements PerfilUsuarioDao {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(PerfilUsuarioDaoImpl.class);
	private static final String ALL_BY_USER = "perfil.usuario.all";
	private static final String DELETE_BY_USER = "perfil.delete.by.usuario";

	static {
		banco.put("perfilusuario", "gpm_perfil_usuario");
		banco.put("perfilusuario.id", "co_perfil_usuario");
		banco.put("perfilusuario.usuario", "co_usuario");
		banco.put("perfilusuario.perfil", "co_perfil");
	}

	@Override
	public List<PerfilUsuario> allByUser(Usuario usuarioFiltro) {
		List<PerfilUsuario> retorno = new ArrayList<>();
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = getTransaction().prepareStatement(new StringBuilder(super.getProperty(ALL_BY_USER)));
			pstmt.setLong(1, usuarioFiltro.getId());
			logger.debug(pstmt);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				PerfilUsuario perfilUsuario = new PerfilUsuario();
				perfilUsuario.setId(rs.getLong(1));
				Usuario usuario = new Usuario();
				usuario.setId(rs.getLong(2));
				perfilUsuario.setUsuario(usuario);
				Perfil perfil = new Perfil();
				perfil.setId(rs.getLong(3));
				perfil.setNome(rs.getString(4));
				perfilUsuario.setPerfil(perfil);
				retorno.add(perfilUsuario);
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}

	@Override
	public boolean deleteByUser(Usuario usuario) {
		PreparedStatementWrapper pstmt = null;
		try {
			pstmt = getTransaction().prepareStatement(new StringBuilder(super.getProperty(DELETE_BY_USER)));
			pstmt.setLong(1, usuario.getId());
			logger.debug(pstmt);
			pstmt.executeUpdate();
			return true;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}
}