package br.gerbellait.sistema.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import br.gerbellait.arquitetura.dao.AbstractDao;
import br.gerbellait.arquitetura.datasource.PreparedStatementWrapper;
import br.gerbellait.arquitetura.exceptions.DaoLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Repository;
import br.gerbellait.arquitetura.util.Utils;
import br.gerbellait.sistema.dao.TransportadorDao;
import br.gerbellait.sistema.entity.Cidade;
import br.gerbellait.sistema.entity.Transportador;
import br.gerbellait.sistema.entity.Uf;

@Repository
public class TransportadorDaoImpl extends AbstractDao<Transportador> implements TransportadorDao {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(TransportadorDaoImpl.class);

	private static final String ALL = "transportador.all";
	private static final String ALL_COUNT = "transportador.all.count";
	private static final String GET_TRANSPORTADOR = "transportador.getById";
	private static final String GET_NOME = "transportador.getByNome";
	private static final String GET_CPF_CNPJ = "transportador.getByCpfCnpj";
	private static final String ORDER_BY = "transportador.orderBy";
	private static final String PAGINATE_LIMIT_OFFSET = "paginacao.limit.offset";

	static {
		banco.put("transportador", "gpm_transportador");
		banco.put("transportador.id", "codigo");
		banco.put("transportador.nome", "nome");
		banco.put("transportador.cnpjcpf", "cnpjcpf");
		banco.put("transportador.inscricao", "inscricao");
		banco.put("transportador.endereco", "endereco");
		banco.put("transportador.bairro", "bairro");
		banco.put("transportador.cep", "cep");
		banco.put("transportador.ibge", "ibge");
		banco.put("transportador.placa", "placa");
		banco.put("transportador.uf", "id_uf");
		banco.put("transportador.motorista", "motorista");
		banco.put("transportador.cidade", "ID_CIDADE");
	}

	@Override
	public List<Transportador> getAll() {
		return (List<Transportador>) this.getAll(Transportador.class);
	}

	private void where(StringBuilder sql, Transportador transportador) {
		if (transportador.getId() != null) {
			sql.append(" ").append(super.getProperty(GET_TRANSPORTADOR));
		}
		if (!Utils.isNullOrEmpty(transportador.getNome())) {
			sql.append(" ").append(super.getProperty(GET_NOME));
		}
		if (!Utils.isNullOrEmpty(transportador.getCnpjcpf())) {
			sql.append(" ").append(super.getProperty(GET_CPF_CNPJ));
		}
	}

	private void parameters(PreparedStatementWrapper pstmt, Transportador transportador, boolean isPaginate)
			throws SQLException {
		Integer parameterIndex = 0;
		if (transportador.getId() != null) {
			pstmt.setLong(++parameterIndex, transportador.getId());
		}
		if (!Utils.isNullOrEmpty(transportador.getNome())) {
			pstmt.setString(++parameterIndex, "%" + transportador.getNome() + "%");
		}
		if (!Utils.isNullOrEmpty(transportador.getCnpjcpf())) {
			pstmt.setString(++parameterIndex, transportador.getCnpjcpf());
		}

		if (isPaginate) {
			pstmt.setInt(++parameterIndex, transportador.getPaginate().getQuantidadePorPagina());
			pstmt.setInt(++parameterIndex, transportador.getPaginate().getOffset());
		}
	}

	public Integer totalAmount(Transportador transportador) {
		Integer retorno = null;
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(ALL_COUNT));
			this.where(sql, transportador);
			pstmt = getTransaction().prepareStatement(sql);
			this.parameters(pstmt, transportador, false);
			LOGGER.debug(pstmt);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				retorno = rs.getInt(1);
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				LOGGER.info(e);
			}
		}
	}

	private Transportador getByResultSet(ResultSet rs) throws SQLException {
		int columnIndex = 0;
		Transportador retorno = new Transportador();
		retorno.setId(rs.getLong(++columnIndex));
		retorno.setNome(rs.getString(++columnIndex));
		retorno.setCnpjcpf(rs.getString(++columnIndex));
		retorno.setInscricao(rs.getString(++columnIndex));
		retorno.setEndereco(rs.getString(++columnIndex));
		retorno.setBairro(rs.getString(++columnIndex));
		retorno.setCep(rs.getString(++columnIndex));
		retorno.setIbge(rs.getString(++columnIndex));
		retorno.setPlaca(rs.getString(++columnIndex));
		retorno.setUf(new Uf(rs.getLong(++columnIndex)));
		retorno.setMotorista(rs.getString(++columnIndex));
		retorno.setCidade(new Cidade(rs.getLong(++columnIndex)));
		return retorno;
	}

	@Override
	public List<Transportador> getByDto(Transportador transportador) {
		transportador.getPaginate().setNumTotalRegistros(this.totalAmount(transportador));
		List<Transportador> retorno = new ArrayList<>();
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(ALL));
			this.where(sql, transportador);
			sql.append(" ").append(super.getProperty(ORDER_BY));
			sql.append(" ").append(super.getProperty(PAGINATE_LIMIT_OFFSET));
			pstmt = getTransaction().prepareStatement(sql);
			this.parameters(pstmt, transportador, true);
			LOGGER.debug(pstmt);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				retorno.add(this.getByResultSet(rs));
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				LOGGER.info(e);
			}
		}
	}
}