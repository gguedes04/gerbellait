package br.gerbellait.sistema.dao.impl;

import java.util.List;

import br.gerbellait.arquitetura.dao.AbstractDao;
import br.gerbellait.arquitetura.interceptors.annotations.Repository;
import br.gerbellait.sistema.dao.TipoEnderecoDao;
import br.gerbellait.sistema.entity.TipoEndereco;

@Repository
public class TipoEnderecoDaoImpl extends AbstractDao<TipoEndereco> implements TipoEnderecoDao {
	private static final long serialVersionUID = 1L;

	static {
		banco.put("tipoendereco", "gpm_tp_endereco");
		banco.put("tipoendereco.id", "ID_TP_ENDERECO");
		banco.put("tipoendereco.nome", "DSC_TP_ENDERECO");
	}

	@Override
	public List<TipoEndereco> all() {
		return (List<TipoEndereco>) this.getAll(TipoEndereco.class);
	}
}