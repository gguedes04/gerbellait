package br.gerbellait.sistema.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;

import br.gerbellait.arquitetura.dao.AbstractDao;
import br.gerbellait.arquitetura.datasource.PreparedStatementWrapper;
import br.gerbellait.arquitetura.exceptions.DaoLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Repository;
import br.gerbellait.arquitetura.util.Utils;
import br.gerbellait.sistema.bean.HistoricoEstoqueBean;
import br.gerbellait.sistema.bean.PedidoProdutoBean;
import br.gerbellait.sistema.bean.TableState;
import br.gerbellait.sistema.dao.ProdutoDao;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.entity.GrupoProduto;
import br.gerbellait.sistema.entity.Pedido;
import br.gerbellait.sistema.entity.Pessoa;
import br.gerbellait.sistema.entity.Produto;
import br.gerbellait.sistema.service.GrupoProdutoService;
import br.gerbellait.sistema.service.ProdutoImpostoService;
import br.gerbellait.sistema.service.ProdutoPrecoRotaService;

@Repository
public class ProdutoDaoImpl extends AbstractDao<Produto> implements ProdutoDao {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(ProdutoDaoImpl.class);
    private static final String ALL = "produto.all";
    private static final String ALL_COUNT = "produto.all.count";
    private static final String BY_CODIGO = "produto.codigo";
    private static final String BY_NOME = "produto.nome";
    private static final String BY_UNIDADE = "produto.unidade";
    private static final String BY_GRUPO = "produto.grupo";
    private static final String BY_GRUPOS = "produto.grupos";
    private static final String ORDER_BY = "produto.orderBy";
    private static final String PAGINATE_LIMIT_OFFSET = "paginacao.limit.offset";
    private static final String EXCLUIR = "produto.excluir";
    private static final String NEXT_ID = "produto.nextId";
    private static final String PRODUTO_PEDIDO_ALL = "produto.pedido.all";
    private static final String PRODUTO_PEDIDO_DATA = "produto.pedido.data";
    private static final String PRODUTO_UPDATE_ESTOQUE = "produto.update.qt.estoque";
    private static final String PRODUTO_RELATORIO_ESTOQUE = "produto.relatorios.estoque";
    private static final String PRODUTO_RELATORIO_COUNT = "produto.relatorios.count";
    
    static {
        banco.put("produto", "gpm_produto");
        banco.put("produto.id", "ID_PRODUTO");
        banco.put("produto.idCodProduto", "ID_COD_PRODUTO");
        banco.put("produto.nome", "NM_PRODUTO");
        banco.put("produto.tipoUnidade", "TP_UNIDADE");
        banco.put("produto.grupoProduto", "ID_GRUPO_PRODUTO");
        banco.put("produto.cf", "CF");
        banco.put("produto.fornecedor", "ID_FORNECEDOR");
        banco.put("produto.valorComissao", "VL_COMISSAO");
        banco.put("produto.peso", "PESO");
        banco.put("produto.percDesconto", "PERC_DESCONTO");
        banco.put("produto.quantidadeMinVenda", "QTDE_MIN_VENDA");
        banco.put("produto.quantidadeEstoqueAtual", "QTDE_ESTOQUE_ATUAL");
        banco.put("produto.flgLiberadoVenda", "FLG_LIBERADO_VENDA");
        banco.put("produto.codigoBarras", "CODIGO_BARRAS");
        banco.put("produto.dataAlteracao", "DT_ALTERACAO");
        banco.put("produto.situacao", "FL_EXCLUIDO");
        banco.put("produto.tipoTroca", "TP_TROCA");
        banco.put("produto.st", "ST");
    }
    
    @Override
    public List<Produto> all() {
        return (List<Produto>) this.getAll(Produto.class);
    }
    
    private void where(StringBuilder sql, Produto produto) {
        if (produto.getId() != null) {
            sql.append(" ").append(super.getProperty(BY_CODIGO));
        }
        if (!Utils.isNullOrEmpty(produto.getNome())) {
            sql.append(" ").append(super.getProperty(BY_NOME));
        }
        if (!Utils.isNullOrEmpty(produto.getTipoUnidade())) {
            sql.append(" ").append(super.getProperty(BY_UNIDADE));
        }
        if (produto.getGrupoProduto() != null && produto.getGrupoProduto().getId() != null) {
            sql.append(" ").append(super.getProperty(BY_GRUPO));
        }
        if (!Utils.isNullOrEmpty(produto.getIdGrupoProdutoList())) {
            sql.append(" ").append(super.getProperty(BY_GRUPOS));
        }
    }
    
    private void parameters(PreparedStatementWrapper pstmt, Produto produto, boolean isPaginate) throws SQLException {
        Integer parameterIndex = 0;
        if (produto.getId() != null) {
            pstmt.setLong(++parameterIndex, produto.getId());
        }
        if (!Utils.isNullOrEmpty(produto.getNome())) {
            pstmt.setString(++parameterIndex, "%" + produto.getNome() + "%");
        }
        if (!Utils.isNullOrEmpty(produto.getTipoUnidade())) {
            pstmt.setString(++parameterIndex, produto.getTipoUnidade());
        }
        if (produto.getGrupoProduto() != null && produto.getGrupoProduto().getId() != null) {
            pstmt.setLong(++parameterIndex, produto.getGrupoProduto().getId());
        }
        if (!Utils.isNullOrEmpty(produto.getIdGrupoProdutoList())) {
            pstmt.setInt(++parameterIndex, produto.getIdGrupoProdutoList().get(0));
            pstmt.setInt(++parameterIndex, produto.getIdGrupoProdutoList().get(1));
        }
        
        if (isPaginate) {
            pstmt.setInt(++parameterIndex, produto.getPaginate().getQuantidadePorPagina());
            pstmt.setInt(++parameterIndex, produto.getPaginate().getOffset());
        }
    }
    
    @Override
    public boolean deletar(Produto produto) {
        PreparedStatementWrapper pstmt = null;
        try {
            StringBuilder sql = new StringBuilder(super.getProperty(EXCLUIR));
            pstmt = getTransaction().prepareStatement(sql);
            pstmt.setInt(1, ExcluidoEnum.EXCLUIDO.getId());
            pstmt.setLong(2, produto.getId());
            logger.debug(pstmt);
            pstmt.executeUpdate();
            return true;
            
        } catch (SQLException e) {
            throw new DaoLayerException(e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (SQLException e) {
                logger.info(e);
            }
        }
    }
    
    @Override
    public Integer nextId() {
        Integer retorno = null;
        PreparedStatementWrapper pstmt = null;
        ResultSet rs = null;
        try {
            StringBuilder sql = new StringBuilder(super.getProperty(NEXT_ID));
            pstmt = getTransaction().prepareStatement(sql);
            logger.debug(pstmt);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                retorno = rs.getInt(1);
            }
            return retorno;
            
        } catch (SQLException e) {
            throw new DaoLayerException(e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                logger.info(e);
            }
        }
    }
    
    public Integer totalAmount(Produto produto) {
        Integer retorno = null;
        PreparedStatementWrapper pstmt = null;
        ResultSet rs = null;
        try {
            StringBuilder sql = new StringBuilder(super.getProperty(ALL_COUNT));
            this.where(sql, produto);
            pstmt = getTransaction().prepareStatement(sql);
            this.parameters(pstmt, produto, false);
            logger.debug(pstmt);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                retorno = rs.getInt(1);
            }
            return retorno;
            
        } catch (SQLException e) {
            throw new DaoLayerException(e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                logger.info(e);
            }
        }
    }
    
    private Produto getByResultSet(ResultSet rs) throws SQLException {
        int columnIndex = 0;
        Produto retorno = new Produto();
        retorno.setId(rs.getLong(++columnIndex));
        retorno.setIdCodProduto(rs.getString(++columnIndex));
        retorno.setNome(rs.getString(++columnIndex));
        retorno.setTipoUnidade(rs.getString(++columnIndex));
        retorno.setGrupoProduto(new GrupoProduto(rs.getLong(++columnIndex)));
        retorno.setCf(rs.getString(++columnIndex));
        retorno.setFornecedor(new Pessoa(rs.getLong(++columnIndex)));
        retorno.setValorComissao(rs.getBigDecimal(++columnIndex));
        retorno.setPeso(rs.getBigDecimal(++columnIndex));
        retorno.setPercDesconto(rs.getBigDecimal(++columnIndex));
        retorno.setQuantidadeMinVenda(rs.getBigDecimal(++columnIndex));
        retorno.setQuantidadeEstoqueAtual(rs.getBigDecimal(++columnIndex));
        retorno.setFlgLiberadoVenda(rs.getString(++columnIndex));
        retorno.setCodigoBarras(rs.getString(++columnIndex));
        retorno.setDataAlteracao(rs.getTimestamp(++columnIndex));
        retorno.setFlExcluido(ExcluidoEnum.getById(rs.getInt(++columnIndex)));
        retorno.setTipoTroca(rs.getString(++columnIndex));
        GrupoProduto grupo = new GrupoProduto();
        grupo.setId(rs.getLong(++columnIndex));
        grupo.setDescricao(rs.getString(++columnIndex));
        retorno.setGrupoProduto(grupo);
        retorno.setSt(rs.getString(++columnIndex));
        retorno.setQtdeLimitVenda(rs.getLong(++columnIndex));
        return retorno;
    }
    
    @Override
    public Produto getByCodigo(Produto produto) {
        Produto retorno = null;
        PreparedStatementWrapper pstmt = null;
        ResultSet rs = null;
        try {
            StringBuilder sql = new StringBuilder(super.getProperty(ALL));
            sql.append(" ").append(super.getProperty(BY_CODIGO));
            pstmt = getTransaction().prepareStatement(sql);
            pstmt.setLong(1, produto.getId());
            logger.debug(pstmt);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                retorno = this.getByResultSet(rs);
            }
            return retorno;
            
        } catch (SQLException e) {
            throw new DaoLayerException(e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                logger.info(e);
            }
        }
    }
    
    @Override
    public List<Produto> getByDto(Produto produto) {
        produto.getPaginate().setNumTotalRegistros(this.totalAmount(produto));
        List<Produto> retorno = new ArrayList<>();
        PreparedStatementWrapper pstmt = null;
        ResultSet rs = null;
        try {
            StringBuilder sql = new StringBuilder(super.getProperty(ALL));
            this.where(sql, produto);
            sql.append(" ").append(super.getProperty(ORDER_BY));
            sql.append(" ").append(super.getProperty(PAGINATE_LIMIT_OFFSET));
            pstmt = getTransaction().prepareStatement(sql);
            this.parameters(pstmt, produto, true);
            logger.debug(pstmt);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                retorno.add(this.getByResultSet(rs));
            }
            return retorno;
            
        } catch (SQLException e) {
            throw new DaoLayerException(e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                logger.info(e);
            }
        }
    }
    
    @Override
    public List<Produto> getExport(Produto produto) {
        produto.getPaginate().setNumTotalRegistros(this.totalAmount(produto));
        List<Produto> retorno = new ArrayList<>();
        PreparedStatementWrapper pstmt = null;
        ResultSet rs = null;
        try {
            StringBuilder sql = new StringBuilder(super.getProperty(ALL));
            this.where(sql, produto);
            sql.append(" ").append(super.getProperty(ORDER_BY));
            pstmt = getTransaction().prepareStatement(sql);
            this.parameters(pstmt, produto, false);
            logger.debug(pstmt);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                retorno.add(this.getByResultSet(rs));
            }
            return retorno;
            
        } catch (SQLException e) {
            throw new DaoLayerException(e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                logger.info(e);
            }
        }
    }
    
    @Override
    public List<Produto> getByDesc(Pessoa cliente, String text) throws SQLException {
        List<Produto> result = new ArrayList<>();
        PreparedStatementWrapper pstmt = null;
        ResultSet rs = null;
        try {
            StringBuilder sql = new StringBuilder(super.getProperty(ALL));

            sql.append(" and (UPPER(produto.`NM_PRODUTO`) like UPPER('%").append(text).append("%')");
            sql.append(" or produto.`ID_COD_PRODUTO` like '%").append(text).append("%')");
            sql.append(" and produto.FL_EXCLUIDO = ").append(ExcluidoEnum.ATIVO.getId());
            
            pstmt = getTransaction().prepareStatement(sql);
            
            
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Produto product = this.getByResultSet(rs);
                PedidoProdutoBean pedidoProdutoBean = getProdutoPedido(cliente, product);
                product.setPrecoVenda(pedidoProdutoBean.getSellPrice());
                result.add(product);
            }
            return result;
            
        } catch (SQLException e) {
            throw new DaoLayerException(e);
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    
    private PedidoProdutoBean getProdutoPedido(Pessoa cliente, Produto produto) throws SQLException {
        PreparedStatementWrapper pstmt = null;
        ResultSet rs = null;
        try {
            StringBuilder sql = new StringBuilder(super.getProperty(PRODUTO_PEDIDO_DATA));
            
            pstmt = getTransaction().prepareStatement(sql);
            pstmt.setLong(1, cliente.getId());
            pstmt.setLong(2, produto.getId());
            
            
            rs = pstmt.executeQuery();
            rs.next();
            PedidoProdutoBean bean = new PedidoProdutoBean();
            bean.setProductCode(rs.getString("ID_COD_PRODUTO"));
            bean.setProductId(rs.getLong("id_produto"));
            bean.setProductDesc(rs.getString("NM_PRODUTO"));
            bean.setUnit(rs.getString("TP_UNIDADE"));
            bean.setWeight(rs.getDouble("PESO"));
            bean.setSellRelease(rs.getBoolean("FLG_LIBERADO_VENDA"));
            bean.setBarCode(rs.getString("CODIGO_BARRAS"));
            bean.setSellPrice(rs.getDouble("PRECO_VENDA"));
            bean.setRoute(rs.getLong("id_rota"));
            bean.setSellLimitCount(rs.getInt("QTDE_LIMITE_VENDA"));
            bean.setPromotionalPrice(rs.getDouble("Preco_Promocao"));
            bean.setSellMinCount(rs.getInt("QTDE_MIN_VENDA"));
            return bean;
            
        } catch (SQLException e) {
            throw new DaoLayerException(e);
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    
    @Override
    public List<Produto> getByPedido(Pedido pedido) throws SQLException {
        List<Produto> result = new ArrayList<>();
        PreparedStatementWrapper pstmt = null;
        ResultSet rs = null;
        try {
            StringBuilder sql = new StringBuilder(super.getProperty(PRODUTO_PEDIDO_ALL));
            
            sql.append(" and pedido.id_pedido = ").append(pedido.getId());
            
            pstmt = getTransaction().prepareStatement(sql);
            
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Produto product = this.getByResultSet(rs);
                product.setQtdeProduto(rs.getLong("QTDE_PRODUTO"));
                product.setQtdeTroca(rs.getLong("QTDE_TROCA"));
                product.setDesconto(rs.getLong("VL_DESCONTO"));
                product.setVlUnitario(rs.getDouble("VL_UNITARIO"));
                PedidoProdutoBean pedidoProdutoBean = getProdutoPedido(pedido.getCliente(), product);
                product.setPrecoVenda(pedidoProdutoBean.getSellPrice());
                product.setFornecedor(null);
                result.add(product);
            }
            return result;
            
        } catch (SQLException e) {
            throw new DaoLayerException(e);
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    
    @Override
    public boolean updateQtEstoque(Produto produto) throws SQLException {
        PreparedStatementWrapper pstmt = null;
        try {
            StringBuilder sql = new StringBuilder(super.getProperty(PRODUTO_UPDATE_ESTOQUE));
            pstmt = getTransaction().prepareStatement(sql);
            pstmt.setLong(1, produto.getFlExcluido().getId());
            pstmt.setLong(2, produto.getQtdeProduto());
            pstmt.setLong(3, produto.getQtdeProduto());
            pstmt.setString(4, produto.getIdCodProduto());
            
            pstmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DaoLayerException(e);
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
        }
    }
    
    @Override
    public List<HistoricoEstoqueBean> getHistoricoEstoque(TableState tableState) throws SQLException {
        List<HistoricoEstoqueBean> result = new ArrayList<>();
        PreparedStatementWrapper pstmt = null;
        ResultSet rs = null;
        try {
            StringBuilder sqlCount = new StringBuilder(super.getProperty(PRODUTO_RELATORIO_COUNT));
            pstmt = getTransaction().prepareStatement(sqlCount);
            rs = pstmt.executeQuery();
            rs.next();
            tableState.getPagination().setNumberOfPages(rs.getInt("total"));
            
            
            StringBuilder sql = new StringBuilder(super.getProperty(PRODUTO_RELATORIO_ESTOQUE));
            sql.append(" ").append(super.getProperty(PAGINATE_LIMIT_OFFSET));
            
            pstmt = getTransaction().prepareStatement(sql);
            pstmt.setInt(1, tableState.getPagination().getNumber());
            pstmt.setInt(2, tableState.getPagination().getStart());
            
            rs = pstmt.executeQuery();
            while (rs.next()) {
                HistoricoEstoqueBean bean = new HistoricoEstoqueBean();
                bean.setProductCli(rs.getString("ID_COD_PRODUTO"));
                bean.setProductDesc(rs.getString("NM_PRODUTO"));
                bean.setUnit(rs.getString("TP_UNIDADE"));
                bean.setCurrentStock(rs.getInt("QTDE_ESTOQUE_ATUAL"));
                bean.setDay1(rs.getDouble("day1"));
                bean.setDay2(rs.getDouble("day2"));
                bean.setDay3(rs.getDouble("day3"));
                bean.setDay4(rs.getDouble("day4"));
                bean.setDay5(rs.getDouble("day5"));
                bean.setTotal(rs.getDouble("total"));
                result.add(bean);
            }
            return result;
            
        } catch (Exception e) {
            logger.error(e);
            throw new DaoLayerException(e);
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
}
