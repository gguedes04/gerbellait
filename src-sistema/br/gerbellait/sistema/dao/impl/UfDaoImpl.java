package br.gerbellait.sistema.dao.impl;

import java.util.List;

import br.gerbellait.arquitetura.dao.AbstractDao;
import br.gerbellait.arquitetura.interceptors.annotations.Repository;
import br.gerbellait.sistema.dao.UfDao;
import br.gerbellait.sistema.entity.Uf;

@Repository
public class UfDaoImpl extends AbstractDao<Uf> implements UfDao {
	private static final long serialVersionUID = 1L;

	static {
		banco.put("uf", "gpm_uf");
		banco.put("uf.id", "id_uf");
		banco.put("uf.nome", "dsc_uf");
		banco.put("uf.sigla", "sigla_uf");
	}

	@Override
	public List<Uf> all() {
		return (List<Uf>) this.getAll(Uf.class);
	}
}