package br.gerbellait.sistema.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.gerbellait.arquitetura.dao.AbstractDao;
import br.gerbellait.arquitetura.datasource.PreparedStatementWrapper;
import br.gerbellait.arquitetura.exceptions.DaoLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Repository;
import br.gerbellait.sistema.dao.EnderecoDao;
import br.gerbellait.sistema.entity.Cidade;
import br.gerbellait.sistema.entity.Endereco;
import br.gerbellait.sistema.entity.Pessoa;
import br.gerbellait.sistema.entity.TipoEndereco;

@Repository
public class EnderecoDaoImpl extends AbstractDao<Endereco> implements EnderecoDao {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(EnderecoDaoImpl.class);

	private static final String ALL = "endereco.all";
	private static final String GET_BY_PESSOA = "endereco.getByPessoa";
	private static final String ORDER_BY = "endereco.orderBy";

	static {
		banco.put("endereco", "gpm_endereco");
		banco.put("endereco.id", "ID_ENDERECO");
		banco.put("endereco.cidade", "ID_CIDADE");
		banco.put("endereco.pessoa", "ID_PESSOA");
		banco.put("endereco.logradouro", "LOGRADOURO");
		banco.put("endereco.numero", "NUMERO");
		banco.put("endereco.bairro", "BAIRRO");
		banco.put("endereco.cep", "CEP");
		banco.put("endereco.tipoEndereco", "ID_TP_ENDERECO");
		banco.put("endereco.telefone", "TELEFONE");
		banco.put("endereco.complemento", "COMPLEMENTO");
	}

	@Override
	public List<Endereco> getByPessoa(Pessoa pessoa) {
		List<Endereco> retorno = new ArrayList<>();
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(ALL));
			if (pessoa.getId() != null) {
				sql.append(" ").append(super.getProperty(GET_BY_PESSOA));
			}
			sql.append(" ").append(super.getProperty(ORDER_BY));
			pstmt = getTransaction().prepareStatement(sql);
			int parameterIndex = 0;
			if (pessoa.getId() != null) {
				pstmt.setLong(++parameterIndex, pessoa.getId());
			}
			logger.debug(pstmt);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Endereco endereco = new Endereco();
				endereco.setId(rs.getLong(1));
				endereco.setCidade(new Cidade(rs.getLong(2)));
				endereco.setPessoa(new Pessoa(rs.getLong(3)));
				endereco.setLogradouro(rs.getString(4));
				endereco.setNumero(rs.getInt(5));
				endereco.setBairro(rs.getString(6));
				endereco.setCep(rs.getString(7));
				endereco.setTipoEndereco(new TipoEndereco(rs.getLong(8)));
				endereco.setTelefone(rs.getString(9));
				endereco.setComplemento(rs.getString(10));
				retorno.add(endereco);
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}
}