package br.gerbellait.sistema.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import br.gerbellait.arquitetura.dao.AbstractDao;
import br.gerbellait.arquitetura.datasource.PreparedStatementWrapper;
import br.gerbellait.arquitetura.exceptions.DaoLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Repository;
import br.gerbellait.sistema.dao.PeriodoMetaDao;
import br.gerbellait.sistema.entity.PeriodoMeta;

@Repository
public class PeriodoMetaDaoImpl extends AbstractDao<PeriodoMeta> implements PeriodoMetaDao {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(ProdutoDaoImpl.class);
	private static final String PERIODO_DATAS_ATUAL = "periodo.datas.atual";

	static {
		banco.put("periodometa", "gpm_periodo_meta");
		banco.put("periodometa.id", "id_periodo_meta");
		banco.put("periodometa.dataInicialMeta", "dt_inicial_meta");
		banco.put("periodometa.dataFinalMeta", "dt_final_meta");
		banco.put("periodometa.dataAlteracao", "DT_ALTERACAO");
		banco.put("periodometa.situacao", "FL_EXCLUIDO");
	}

	@Override
	public List<PeriodoMeta> all() {

		return (List<PeriodoMeta>) this.getAll(PeriodoMeta.class);
	}

	@Override
	public PeriodoMeta getPeriodoAtual() {
		PeriodoMeta retorno = new PeriodoMeta();
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(PERIODO_DATAS_ATUAL));
			pstmt = getTransaction().prepareStatement(sql);
			logger.debug(pstmt);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				retorno.setDataInicialMeta(rs.getDate(1));
				retorno.setDataFinalMeta(rs.getDate(2));
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}
}