package br.gerbellait.sistema.dao.impl;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.apache.james.mime4j.field.datetime.DateTime;
import org.jboss.logging.Logger;

import br.gerbellait.arquitetura.dao.AbstractDao;
import br.gerbellait.arquitetura.datasource.PreparedStatementWrapper;
import br.gerbellait.arquitetura.exceptions.DaoLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Repository;
import br.gerbellait.sistema.dao.PedidoProdutoDao;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.dao.enumerador.TipoUnidadeEnum;
import br.gerbellait.sistema.entity.Pedido;
import br.gerbellait.sistema.entity.PedidoProduto;
import br.gerbellait.sistema.entity.Produto;
import br.gerbellait.sistema.service.ProdutoService;

import javax.inject.Inject;

@Repository
public class PedidoProdutoDaoImpl extends AbstractDao<PedidoProduto> implements PedidoProdutoDao {
    private static final Logger logger = Logger.getLogger(PedidoProdutoDaoImpl.class);
    private static final String PEDIDO_META_VENDEDOR = "pedido.meta.vendedor";
    private static final String PEDIDO_PRODUTO_INSERT = "pedido.produto.insert";
    private static final String PEDIDO_PRODUTO_UPDATE = "pedido.produto.update";
    private static final String PEDIDO_PRODUTO_CONTAINS = "pedido.produto.contains";

    @Inject
    private ProdutoService produtoService;

    @Override
    public boolean contains(Pedido pedido, Produto produto) throws SQLException {
        PreparedStatementWrapper pstmt = null;
        ResultSet rs = null;
        try {
            StringBuilder sql = new StringBuilder(super.getProperty(PEDIDO_PRODUTO_CONTAINS));
            pstmt = getTransaction().prepareStatement(sql);
            pstmt.setLong(1, produto.getId());
            pstmt.setLong(2, pedido.getId());
            logger.debug(pstmt);
            rs = pstmt.executeQuery();
            
            rs.next();
            return rs.getInt("TOTAL") > 0;
            
        } catch (SQLException e) {
            throw new DaoLayerException(e);
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    
    private boolean updateMetaVendedor(Pedido pedido, Produto produto) {
        PreparedStatementWrapper pstmt = null;
        try {
            StringBuilder sql = new StringBuilder(super.getProperty(PEDIDO_META_VENDEDOR));
            pstmt = getTransaction().prepareStatement(sql);
            pstmt.setLong(1, produto.getFlExcluido().getId());
            pstmt.setLong(2, produto.getQtdeProduto());
            pstmt.setLong(3, produto.getQtdeProduto());
            pstmt.setLong(4, pedido.getCliente().getIdVendedorRepresentante());
            pstmt.setString(5, produto.getIdCodProduto());
            
            logger.debug(pstmt);
            pstmt.executeUpdate();
            
            return true;
        } catch (SQLException e) {
            throw new DaoLayerException(e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (SQLException e) {
                logger.info(e);
            }
        }
    }
    
    @Override
    public boolean save(Pedido pedido, Produto produto, boolean notExists) {
        PreparedStatementWrapper pstmt = null;
        try {
            double count = 0d;
            if (TipoUnidadeEnum.UNITARIO.getId().equals(produto.getTipoUnidade())) {
                count = produto.getPrecoVenda() * produto.getQtdeProduto();
            } else if (TipoUnidadeEnum.KILO.getId().equals(produto.getTipoUnidade())) {
                count = produto.getPrecoVenda() * produto.getPeso().doubleValue();
            }
            final double valorTotal = count - (count * (produto.getDesconto() / 100f));
            if (!contains(pedido, produto)) {
                StringBuilder sql = new StringBuilder(super.getProperty(PEDIDO_PRODUTO_INSERT));
                pstmt = getTransaction().prepareStatement(sql);
                pstmt.setLong(1, pedido.getId());
                pstmt.setLong(2, produto.getId());
                pstmt.setLong(3, produto.getQtdeProduto());
                pstmt.setDouble(4, produto.getVlUnitario());
                pstmt.setDouble(5, valorTotal);
                pstmt.setDouble(6, produto.getQtdeTroca());
                pstmt.setDouble(7, 0D);
                pstmt.setDouble(8, produto.getDesconto());
                pstmt.setTimestamp(9, new Timestamp(new java.util.Date().getTime()));
                pstmt.setInt(10, ExcluidoEnum.ATIVO.getId());
                pstmt.setString(11, pedido.getUsuario());
                logger.debug(pstmt);
                pstmt.executeUpdate();
                updateMetaVendedor(pedido, produto);
                produtoService.updateQtEstoque(produto);
                // pstmt.setDate(12, new Date(new java.util.Date().getTime()));
                // pstmt.setDate(13, new Date(new java.util.Date().getTime()));
            }
            else {
                if (produto.getFlExcluido().getId() == 1) {
                    StringBuilder sql = new StringBuilder(super.getProperty(PEDIDO_PRODUTO_UPDATE));
                    pstmt = getTransaction().prepareStatement(sql);
                    pstmt.setLong(1, produto.getQtdeProduto());
                    pstmt.setDouble(2, produto.getVlUnitario());
                    pstmt.setDouble(3, valorTotal);
                    pstmt.setDouble(4, produto.getQtdeTroca());
                    pstmt.setDouble(5, 0D);
                    pstmt.setDouble(6, produto.getDesconto());
                    pstmt.setDate(7, new Date(new java.util.Date().getTime()));
                    pstmt.setInt(8, produto.getFlExcluido().getId());
                    pstmt.setString(9, pedido.getUsuario());
                    // pstmt.setDate(10, new Date(new java.util.Date().getTime()));
                    // pstmt.setDate(11, new Date(new java.util.Date().getTime()));
                    pstmt.setLong(10, pedido.getId());
                    pstmt.setLong(11, produto.getId());
                    logger.debug(pstmt);
                    pstmt.executeUpdate();
                    updateMetaVendedor(pedido, produto);
                    produtoService.updateQtEstoque(produto);
                }

            }

            return true;
        } catch (Exception e) {
            logger.error(e);
            throw new DaoLayerException(e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (SQLException e) {
                logger.info(e);
            }
        }
    }
}
