package br.gerbellait.sistema.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.gerbellait.arquitetura.dao.AbstractDao;
import br.gerbellait.arquitetura.datasource.PreparedStatementWrapper;
import br.gerbellait.arquitetura.exceptions.DaoLayerException;
import br.gerbellait.arquitetura.interceptors.annotations.Repository;
import br.gerbellait.sistema.dao.FormaPagamentoDao;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.entity.FormaPagamento;

@Repository
public class FormaPagamentoDaoImpl extends AbstractDao<FormaPagamento> implements FormaPagamentoDao {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(FormaPagamentoDaoImpl.class);

	private static final String ALL = "formapagamento.all";
	private static final String FL_EXCLUIDO = "formapagamento.flExcluido";
	private static final String ORDER_BY = "formapagamento.orderBy";

	static {
		banco.put("formapagamento", "gpm_forma_pagamento");
		banco.put("formapagamento.id", "ID_FORMA_PAGAMENTO");
		banco.put("formapagamento.descricao", "DSC_FORMA_PAGAMENTO");
		banco.put("formapagamento.prazo", "PRAZO");
		banco.put("formapagamento.valorMinimoPedido", "VL_MINIMO_PEDIDO");
		banco.put("formapagamento.dataAlteracao", "DT_ALTERACAO");
		banco.put("formapagamento.flExcluido", "FL_EXCLUIDO");
		banco.put("formapagamento.permiteDesconto", "FL_PERMITE_DESCONTO");
	}

	@Override
	public List<FormaPagamento> all() {
		return (List<FormaPagamento>) this.getAll(FormaPagamento.class);
	}

	@Override
	public List<FormaPagamento> allBySituacao(ExcluidoEnum flExcluido) {
		List<FormaPagamento> retorno = new ArrayList<>();
		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder(super.getProperty(ALL));
			sql.append(" ").append(super.getProperty(FL_EXCLUIDO));
			sql.append(" ").append(super.getProperty(ORDER_BY));
			pstmt = getTransaction().prepareStatement(sql);
			pstmt.setInt(1, flExcluido.getId());
			logger.debug(pstmt);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				int columnIndex = 0;
				FormaPagamento formaPagamento = new FormaPagamento();
				formaPagamento.setId(rs.getLong(++columnIndex));
				formaPagamento.setDescricao(rs.getString(++columnIndex));
				formaPagamento.setPrazo(rs.getInt(++columnIndex));
				formaPagamento.setValorMinimoPedido(rs.getBigDecimal(++columnIndex));
				formaPagamento.setDataAlteracao(rs.getDate(++columnIndex));
				formaPagamento.setFlExcluido(ExcluidoEnum.getById(rs.getInt(++columnIndex)));
				formaPagamento.setPermiteDesconto(rs.getString(++columnIndex));
				retorno.add(formaPagamento);
			}
			return retorno;

		} catch (SQLException e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.info(e);
			}
		}
	}
}