package br.gerbellait.sistema.util;

import java.math.BigDecimal;

public class Paginate {

	private Integer paginaAtual;
	private Integer quantidadePorPagina;
	private Integer numTotalRegistros;
	private Integer numTotalPaginas;
	private Integer offset;

	public Paginate() {
		// empty
	}

	public Integer getQuantidadePorPagina() {
		if (quantidadePorPagina == null) {
			quantidadePorPagina = Constants.QUANTIDADE_POR_PAGINA_DEFAULT;
		}
		return quantidadePorPagina;
	}

	public void setQuantidadePorPagina(Integer quantidadePorPagina) {
		this.quantidadePorPagina = quantidadePorPagina;
	}

	public Integer getPaginaAtual() {
		if (paginaAtual == null) {
			paginaAtual = 0;
		}
		return paginaAtual;
	}

	public void setPaginaAtual(Integer pagina) {
		this.paginaAtual = pagina;
	}

	public Integer getNumTotalRegistros() {
		if (numTotalRegistros == null) {
			numTotalRegistros = 0;
		}
		return numTotalRegistros;
	}

	public void setNumTotalRegistros(Integer numTotalRegistros) {
		this.numTotalRegistros = numTotalRegistros;
	}

	public Integer getNumTotalPaginas() {
		if (this.getNumTotalRegistros() > 0 && this.getQuantidadePorPagina() > 0) {
			BigDecimal retorno = BigDecimal.valueOf(this.getNumTotalRegistros())
					.divide(BigDecimal.valueOf(this.getQuantidadePorPagina()), BigDecimal.ROUND_CEILING);
			numTotalPaginas = retorno.intValue();
		}
		return numTotalPaginas;
	}

	public void setNumTotalPaginas(Integer numTotalPaginas) {
		this.numTotalPaginas = numTotalPaginas;
	}

	public Integer getOffset() {
		offset = this.getPaginaAtual() * this.getQuantidadePorPagina();
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}
}