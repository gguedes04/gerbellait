package br.gerbellait.sistema.util;

import java.io.IOException;

import org.jboss.resteasy.util.Base64;

import br.gerbellait.arquitetura.util.StringUtils;

public class CryptoUtil {

	private CryptoUtil() {
		throw new IllegalStateException("CryptoUtil class");
	}

	/**
	 * Codifica a String.
	 *
	 * @return string codificada.
	 */
	public static String toEncoding(String value) {
		String retorno = null;
		if (StringUtils.isNullOrEmpty(value)) {
			return retorno;
		}
		retorno = Base64.encodeBytes(value.getBytes());
		for (int i = 0; i < Constants.NUM_CRYPTO_UTIL; i++) {
			retorno = Base64.encodeBytes(retorno.getBytes());
		}
		return retorno;
	}

	/**
	 * Decodifica a String.
	 *
	 * @return string codificada.
	 */
	public static String toDecoding(String value) throws IOException {
		String retorno = null;
		if (StringUtils.isNullOrEmpty(value)) {
			return retorno;
		}
		retorno = new String(Base64.decode(value));
		for (int i = 0; i < Constants.NUM_CRYPTO_UTIL; i++) {
			retorno = new String(Base64.decode(retorno));
		}
		return retorno;
	}
}
