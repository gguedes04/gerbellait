package br.gerbellait.sistema.util;

public class Constants {

	public static final Integer NUM_CRYPTO_UTIL = 3;

	public static final Integer ATIVO = 1;
	public static final Integer INATIVO = 0;

	public static final Integer GRUPO_PRODUTO_MATERIA_PRIMA = 5;
	public static final Integer GRUPO_PRODUTO_TERCEIROS = 6;

	public static final String ALTERAR = "alterar";
	public static final String REMOVER = "excluir";

	public static final String USUARIO = "usuario";
	public static final String PERFIS = "perfis";
	public static final String SITUACOES = "situacoes";
	public static final String VENDEDORES = "vendedores";

	public static final Integer QUANTIDADE_POR_PAGINA_DEFAULT = 30;

	public static final Long TIPO_PESSOA_CLIENTE = 1L;
	public static final Long TIPO_PESSOA_FORNECEDOR = 2L;
	public static final Long TIPO_ENDERECO_COMERCIAL = 1L;

	public static final String DATE_FORMAT_DEFAULT = "dd/MM/yyyy";

	public static final String ADMINISTRADOR = "ADMINISTRADOR";
	public static final String CADASTRO = "CADASTRO";
	public static final String VENDEDOR = "VENDEDOR";

	public static final Long ID_PERFIL_ADMINISTRADOR = 1L;
	public static final Long ID_PERFIL_CADASTRO = 2L;
	public static final Long ID_PERFIL_VENDEDOR = 3L;

	public static final String TIPO_ESTOQUE_TRANSFERENCIA = "T";
	public static final String TIPO_ESTOQUE_SAIDA = "S";
	public static final String TIPO_ESTOQUE_ENTRADA = "E";

	private Constants() {
		throw new IllegalStateException("Constants class");
	}

}