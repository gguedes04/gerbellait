package br.gerbellait.sistema.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

	private DateUtil() {
		throw new IllegalStateException("DateUtil class");
	}

	public static String dateToFormatDefault(Date date) {
		if (date == null) {
			return "";
		}
		return new SimpleDateFormat(Constants.DATE_FORMAT_DEFAULT).format(date);
	}

}
