package br.gerbellait.sistema.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.gerbellait.sistema.entity.Produto;
import br.gerbellait.sistema.entity.ProdutoPrecoRota;
import br.gerbellait.sistema.entity.Rota;
import br.gerbellait.sistema.service.ProdutoPrecoRotaService;
import br.gerbellait.sistema.service.RotaService;

@Path("/gerbellait/precovendarota")
@Produces(MediaType.APPLICATION_JSON)
public class PrecoVendaRotaResource {

	@Inject
	private ProdutoPrecoRotaService produtoPrecoRotaService;

	@Inject
	private RotaService rotaService;

	@POST
	@Path("init")
	public Response init(Produto produto) {
		try {
			if (produto == null) {
				produto = new Produto();
			}
			Map<String, Object> response = new HashMap<>();
			List<Rota> rotas = rotaService.getAll();
			response.put("rotas", rotas);
			response.put("lista", produtoPrecoRotaService.getPrecoVendaRotaResource(produto, rotas));
			response.put("filtro", produto);
			return Response.ok(response).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@POST
	@Path("salvar")
	public Response salvar(List<ProdutoPrecoRota> rotas) {
		try {
			produtoPrecoRotaService.saveList(rotas);
			return Response.ok(true).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}
}