package br.gerbellait.sistema.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.gerbellait.sistema.bean.MapaCargaFilterBean;
import br.gerbellait.sistema.bean.MapaCargaPedidoBean;
import br.gerbellait.sistema.service.MapaCargaService;
import br.gerbellait.sistema.service.RotaService;
import br.gerbellait.sistema.service.VendedorService;

@Path("/gerbellait/mapacarga")
@Produces(MediaType.APPLICATION_JSON)
public class MapaCargaResource {
    
    @Inject
    private RotaService rotaService;
    @Inject
    private VendedorService vendedorService;
    @Inject
    private MapaCargaService mapaCargaService;
    
    @POST
    @Path("init")
    public Response init() {
        try {
            Map<String, Object> response = new HashMap<>();
            response.put("routes", rotaService.getAll());
            response.put("sellers", vendedorService.getAll());
            response.put("reports", mapaCargaService.findLoadMapReport());
            return Response.ok(response).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
    
    @POST
    @Path("pedidos")
    public Response getPedidos(MapaCargaFilterBean filter) {
        try {
            return Response.ok(mapaCargaService.getPedidos(filter)).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
    
    @POST
    @Path("produtos")
    public Response getProdutos(MapaCargaFilterBean filter) {
        try {
            return Response.ok(mapaCargaService.getProdutos(filter)).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
    
    @POST
    public Response save(List<MapaCargaPedidoBean> list) {
        try {
            return Response.ok(mapaCargaService.save(list)).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
    
    @POST
    @Path("relatorio/carga")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response relatorioCarga(MapaCargaFilterBean filter) {
        try {
            return Response.ok(mapaCargaService.relatorioCargaExport(filter)).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
    }
    
    @POST
    @Path("relatorio/cobranca")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response relatorioCobranca(MapaCargaFilterBean filter) {
        try {
            return Response.ok(mapaCargaService.relatorioCobrancaExport(filter)).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
    }
    
    @POST
    @Path("loadmap")
    public Response getLoadMapByFilter(MapaCargaFilterBean filter) {
        try {
            return Response.ok(mapaCargaService.getLoadMapByFilter(filter)).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
    
    @DELETE
    @Path("/{id}")
    public Response deleteLoadMap(@PathParam("id") Long id) {
        try {
            mapaCargaService.deleteLoadMap(id);
            return Response.ok().build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
    
    @POST
    @Path("pedidos/getallorders")
    public Response getAllOrders(MapaCargaFilterBean filter) {
        try {
            Map<String, Object> response = new HashMap<>();
            Long id = filter.getLoadMapNumber().getId();
            response.put("unlinkedOrders", mapaCargaService.getPedidosNaoVinculados(filter));
            response.put("linkedOrders", mapaCargaService.getPedidosVinculados(id));
            return Response.ok(response).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
    }
}
