
package br.gerbellait.sistema.resource;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.gerbellait.arquitetura.ApplicationContext;
import br.gerbellait.arquitetura.util.Utils;
import br.gerbellait.sistema.entity.Usuario;
import br.gerbellait.sistema.service.PerfilService;
import br.gerbellait.sistema.service.UsuarioService;
import br.gerbellait.sistema.service.VendedorService;
import br.gerbellait.sistema.util.Constants;

@Path("/gerbellait/usuario")
@Produces(MediaType.APPLICATION_JSON)
public class UsuarioResource {

	@Inject
	private UsuarioService usuarioService;

	@Inject
	private PerfilService perfilService;

	@Inject
	private VendedorService vendedorService;

	@Inject
	private ApplicationContext context;

	@GET
	@Path("editar/{id}")
	public Response editar(@PathParam("id") Long id) {
		try {
			Map<String, Object> response = new HashMap<>();
			Usuario usuario = usuarioService.getById(id);
			usuario.setSenha(null);
			response.put(Constants.USUARIO, usuario);
			response.put(Constants.PERFIS, perfilService.getAll());
			response.put(Constants.SITUACOES, Utils.getSituacoes());
			response.put(Constants.VENDEDORES, vendedorService.getAll());
			return Response.ok(response).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@GET
	@Path("novo")
	public Response novo() {
		try {
			Map<String, Object> response = new HashMap<>();
			Usuario usuario = new Usuario();
			usuario.setSituacao(1);
			response.put(Constants.USUARIO, usuario);
			response.put(Constants.PERFIS, perfilService.getAll());
			response.put(Constants.SITUACOES, Utils.getSituacoes());
			response.put(Constants.VENDEDORES, vendedorService.getAll());
			return Response.ok(response).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@GET
	@Path("init")
	public Response init() {
		try {
			Map<String, Object> response = new HashMap<>();
			response.put(Constants.SITUACOES, Utils.getSituacoes());
			response.put(Constants.PERFIS, perfilService.getAll());
			response.put("lista", usuarioService.getByDto(new Usuario()));
			return Response.ok(response).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@POST
	@Path("listar")
	public Response listar(Usuario usuario) {
		try {
			return Response.ok(usuarioService.getByDto(usuario)).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@POST
	public Response save(Usuario usuario) {
		try {
			if (usuario.getSituacao() == null) {
				usuario.setSituacao(1);
			}
			return Response.ok(usuarioService.save(usuario)).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@PUT
	public Response delete(Usuario usuario) {
		try {
			return Response.ok(usuarioService.deletar(usuario)).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@POST
	@Path("reset")
	public Response reset(Usuario usuario) {
		try {
			return Response.ok(usuarioService.emailEsqueceuSenha(context, usuario)).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@POST
	@Path("resetsenha")
	public Response resetsenha(Usuario usuario) {
		try {
			return Response.ok(usuarioService.salvaNovaSenha(context, usuario)).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

}