package br.gerbellait.sistema.resource;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.gerbellait.sistema.entity.Transportador;
import br.gerbellait.sistema.entity.Uf;
import br.gerbellait.sistema.service.CidadeService;
import br.gerbellait.sistema.service.TransportadorService;
import br.gerbellait.sistema.service.UfService;

@Path("/gerbellait/transportador")
@Produces(MediaType.APPLICATION_JSON)
public class TransportadorResource {

	@Inject
	private TransportadorService service;

	@Inject
	private UfService ufService;

	@Inject
	private CidadeService cidadeService;

	@POST
	@Path("init")
	public Response init() {
		try {
			Map<String, Object> response = new HashMap<>();
			Transportador transportador = new Transportador();
			response.put("lista", service.getByDto(transportador));
			response.put("filtro", transportador);
			return Response.ok(response).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@POST
	@Path("listar")
	public Response listar(Transportador transportador) {
		try {
			Map<String, Object> response = new HashMap<>();
			response.put("lista", service.getByDto(transportador));
			response.put("filtro", transportador);
			return Response.ok(response).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@GET
	@Path("novo")
	public Response novo() {
		try {
			Map<String, Object> response = new HashMap<>();
			response.put("ufs", ufService.getAll());
			response.put("transportador", new Transportador());
			return Response.ok(response).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@GET
	@Path("editar/{id}")
	public Response editar(@PathParam("id") Long id) {
		try {
			Map<String, Object> response = new HashMap<>();
			Transportador transportador = service.getById(new Transportador(id));
			response.put("transportador", transportador);
			response.put("ufs", ufService.getAll());
			response.put("cidades", cidadeService.getByUf(transportador.getCidade().getUf()));
			return Response.ok(response).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}
	
	@GET
	@Path("cidades/{uf}")
	public Response getCidades(@PathParam("uf") Long uf) {
		try {
			return Response.ok(cidadeService.getByUf(new Uf(uf))).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@POST
	public Response save(Transportador transportador) {
		try {
			return Response.ok(service.save(transportador)).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@PUT
	public Response delete(Transportador transportador) {
		try {
			return Response.ok(service.deletar(transportador)).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}
}