package br.gerbellait.sistema.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.gerbellait.sistema.entity.MetaVendedorProduto;
import br.gerbellait.sistema.entity.PeriodoMeta;
import br.gerbellait.sistema.entity.Produto;
import br.gerbellait.sistema.entity.Vendedor;
import br.gerbellait.sistema.service.MetaVendedorProdutoService;
import br.gerbellait.sistema.service.PeriodoMetaService;
import br.gerbellait.sistema.service.VendedorService;
import br.gerbellait.sistema.util.DateUtil;

@Path("/gerbellait/precomediovenda")
@Produces(MediaType.APPLICATION_JSON)
public class PrecoMedioVendaResource {

	@Inject
	private MetaVendedorProdutoService metaVendedorProdutoService;

	@Inject
	private VendedorService vendedorService;

	@Inject
	private PeriodoMetaService periodoMetaService;

	@POST
	@Path("init")
	public Response init(MetaVendedorProduto filtro) {
		try {
			Map<String, Object> response = new HashMap<>();
			if (filtro == null) {
				filtro = new MetaVendedorProduto();
			}
			if (filtro.getProduto() == null) {
				filtro.setProduto(new Produto());
			}
			if (filtro.getDataInicial() == null || filtro.getDataFinal() == null) {
				PeriodoMeta periodoMeta = periodoMetaService.getPeriodoAtual();
				if (periodoMeta != null) {
					filtro.setDataInicial(periodoMeta.getDataInicialMeta());
					filtro.setDataFinal(periodoMeta.getDataFinalMeta());
					response.put("dataInicialStr", DateUtil.dateToFormatDefault(periodoMeta.getDataInicialMeta()));
					response.put("dataFinalStr", DateUtil.dateToFormatDefault(periodoMeta.getDataFinalMeta()));
				}
			}
			List<Vendedor> vendedores = vendedorService.getAll();
			response.put("vendedores", vendedores);
			if (filtro.getDataInicial() != null && filtro.getDataFinal() != null) {
				response.put("lista", metaVendedorProdutoService.getPrecoMedioVendaResource(filtro, vendedores));
			}
			response.put("filtro", filtro);
			return Response.ok(response).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@POST
	@Path("salvar")
	public Response salvar(List<MetaVendedorProduto> limites) {
		try {
			metaVendedorProdutoService.saveList(limites);
			return Response.ok(true).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}
}