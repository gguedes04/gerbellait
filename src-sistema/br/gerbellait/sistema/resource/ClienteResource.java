package br.gerbellait.sistema.resource;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.gerbellait.arquitetura.util.PerfilUsuario;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.dao.enumerador.RegimeTributarioEnum;
import br.gerbellait.sistema.entity.Pessoa;
import br.gerbellait.sistema.entity.TipoEndereco;
import br.gerbellait.sistema.entity.TipoPessoa;
import br.gerbellait.sistema.entity.Uf;
import br.gerbellait.sistema.entity.Usuario;
import br.gerbellait.sistema.service.CidadeService;
import br.gerbellait.sistema.service.FormaPagamentoService;
import br.gerbellait.sistema.service.PessoaService;
import br.gerbellait.sistema.service.RotaService;
import br.gerbellait.sistema.service.TipoEnderecoService;
import br.gerbellait.sistema.service.UfService;
import br.gerbellait.sistema.service.UsuarioService;
import br.gerbellait.sistema.service.VendedorService;
import br.gerbellait.sistema.util.Constants;

@Path("/gerbellait/cliente")
@Produces(MediaType.APPLICATION_JSON)
public class ClienteResource {

	public static final String VENDEDORES = "vendedores";
	public static final String FORMAS_PAGAMENTO = "formasPagamento";

	@Inject
	private PessoaService pessoaService;

	@Inject
	private FormaPagamentoService formaPagamentoService;

	@Inject
	private VendedorService vendedorService;

	@Inject
	private UfService ufService;

	@Inject
	private CidadeService cidadeService;

	@Inject
	private RotaService rotaService;

	@Inject
	private TipoEnderecoService tipoEnderecoService;

	@Inject
	private UsuarioService usuarioService;

	@POST
	@Path("init")
	public Response init(Long idUsuario) {
		try {
			Usuario usuarioLogado = usuarioService.getById(idUsuario);
			Map<String, Object> response = new HashMap<>();
			response.put(FORMAS_PAGAMENTO, formaPagamentoService.allBySituacao(ExcluidoEnum.ATIVO));
			response.put(VENDEDORES, vendedorService.allBySituacao(ExcluidoEnum.ATIVO));
			response.put("ufs", ufService.getAll());
			Pessoa pessoa = new Pessoa();
			if (PerfilUsuario.isVendedor(usuarioLogado)) {
				pessoa.setIdVendedorRepresentante(usuarioLogado.getVendedor().getId());
				response.put("isVendedor", true);
			}
			response.put("filtro", pessoa);
			response.put("lista", pessoaService.getClientesByDto(pessoa));
			return Response.ok(response).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@POST
	@Path("listar")
	public Response listar(Pessoa pessoa) {
		try {
			Map<String, Object> response = new HashMap<>();
			response.put("lista", pessoaService.getClientesByDto(pessoa));
			response.put("filtro", pessoa);
			return Response.ok(response).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@POST
	@Path("exportar")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response exportar(Pessoa pessoa) {
		try {
			return Response.ok(pessoaService.toExport(pessoa)).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@GET
	@Path("cidades/{uf}")
	public Response getCidades(@PathParam("uf") Long uf) {
		try {
			return Response.ok(cidadeService.getByUf(new Uf(uf))).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@GET
	@Path("novo/{idUsuario}")
	public Response novo(@PathParam("idUsuario") Long idUsuario) {
		try {
			Usuario usuarioLogado = usuarioService.getById(idUsuario);
			Map<String, Object> response = new HashMap<>();
			Pessoa pessoa = new Pessoa();
			if (usuarioLogado.getVendedor() != null && usuarioLogado.getVendedor().getId() != null) {
				pessoa.setIdVendedorRepresentante(usuarioLogado.getVendedor().getId());
				response.put("rotas", rotaService.getByVendedor(pessoa.getIdVendedorRepresentante()));
			}
			pessoa.setFlExcluido(ExcluidoEnum.ATIVO);
			pessoa.setTipoPessoa(new TipoPessoa(Constants.TIPO_PESSOA_CLIENTE));
			pessoa.getEnderecoComercial().setTipoEndereco(new TipoEndereco(Constants.TIPO_ENDERECO_COMERCIAL));
			response.put("pessoa", pessoa);
			response.put("situacoes", ExcluidoEnum.values());
			response.put(VENDEDORES, vendedorService.allBySituacao(ExcluidoEnum.ATIVO));
			response.put(FORMAS_PAGAMENTO, formaPagamentoService.allBySituacao(ExcluidoEnum.ATIVO));
			response.put("tiposEndereco", tipoEnderecoService.getAll());
			response.put("ufs", ufService.getAll());
			response.put("regimesTributarios", RegimeTributarioEnum.getTipoTrocaList());
			return Response.ok(response).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@GET
	@Path("editar/{id}")
	public Response editar(@PathParam("id") Long id) {
		try {
			Map<String, Object> response = new HashMap<>();
			Pessoa pessoa = pessoaService.getById(id);
			response.put("pessoa", pessoa);
			response.put("situacoes", ExcluidoEnum.values());
			response.put(VENDEDORES, vendedorService.allBySituacao(ExcluidoEnum.ATIVO));
			response.put("rotas", rotaService.getByVendedor(pessoa.getIdVendedorRepresentante()));
			response.put(FORMAS_PAGAMENTO, formaPagamentoService.allBySituacao(ExcluidoEnum.ATIVO));
			response.put("tiposEndereco", tipoEnderecoService.getAll());
			response.put("ufs", ufService.getAll());
			response.put("regimesTributarios", RegimeTributarioEnum.getTipoTrocaList());
			return Response.ok(response).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@GET
	@Path("rotas/{idVendedor}")
	public Response getRotaByVendedor(@PathParam("idVendedor") Long idVendedor) {
		try {
			return Response.ok(rotaService.getByVendedor(idVendedor)).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@POST
	public Response save(Pessoa pessoa) {
		try {
			pessoa.getFlExcluido();
			return Response.ok(pessoaService.save(pessoa)).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@PUT
	public Response delete(Pessoa pessoa) {
		try {
			return Response.ok(pessoaService.deletar(pessoa)).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}
}