package br.gerbellait.sistema.resource;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.gerbellait.sistema.bean.TableState;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.dao.enumerador.TipoTrocaEnum;
import br.gerbellait.sistema.dao.enumerador.TipoUnidadeEnum;
import br.gerbellait.sistema.entity.Pessoa;
import br.gerbellait.sistema.entity.Produto;
import br.gerbellait.sistema.service.GrupoProdutoService;
import br.gerbellait.sistema.service.PessoaService;
import br.gerbellait.sistema.service.ProdutoService;
import br.gerbellait.sistema.service.RotaService;
import br.gerbellait.sistema.service.UfService;

@Path("/gerbellait/produto")
@Produces(MediaType.APPLICATION_JSON)
public class ProdutoResource {
    
    public static final String UNIDADES = "unidades";
    public static final String GRUPOS = "grupos";
    
    @Inject
    private GrupoProdutoService grupoProdutoService;
    
    @Inject
    private ProdutoService produtoService;
    
    @Inject
    private UfService ufService;
    
    @Inject
    private RotaService rotaService;
    
    @Inject
    private PessoaService pessoaService;
    
    @POST
    @Path("init")
    public Response init() {
        try {
            Map<String, Object> response = new HashMap<>();
            response.put(GRUPOS, grupoProdutoService.getAll());
            response.put(UNIDADES, TipoUnidadeEnum.getTiposUnidade());
            Produto produto = new Produto();
            response.put("filtro", produto);
            response.put("lista", produtoService.getByDto(produto));
            return Response.ok(response).build();
        } catch (Exception e) {
            return Response.status(Status.FORBIDDEN).build();
        }
    }
    
    @POST
    @Path("listar")
    public Response listar(Produto produto) {
        try {
            Map<String, Object> response = new HashMap<>();
            response.put("lista", produtoService.getByDto(produto));
            response.put("filtro", produto);
            return Response.ok(response).build();
        } catch (Exception e) {
            return Response.status(Status.FORBIDDEN).build();
        }
    }
    
    @GET
    @Path("getByText/{customerId}/{text}")
    public Response getByText(@PathParam("customerId") Long customerId, @PathParam("text") String text) {
        try {
            return Response.ok(produtoService.getByDesc(customerId, text)).build();
        } catch (Exception e) {
            return Response.status(Status.BAD_REQUEST).build();
        }
    }
    
    @GET
    @Path("relatorio/estoque/{tableState}")
    public Response getHistorioEstoque(@PathParam("tableState") String json) {
        try {
            TableState tableState = new ObjectMapper().readValue(json, TableState.class);
            Map map = new HashMap();
            map.put("records", produtoService.getHistoricoEstoque(tableState));
            map.put("tableState", tableState);
            return Response.ok(map).build();
        } catch (Exception e) {
            return Response.status(Status.BAD_REQUEST).build();
        }
    }
    
    @POST
    @Path("relatorio/estoque/exportar")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response relatorioExportar(TableState tableState) {
        try {
            return Response.ok(produtoService.relatorioEstoqueExport(tableState)).build();
        } catch (Exception e) {
            return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
    }
    
    @POST
    @Path("exportar")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response exportar(Produto produto) {
        try {
            return Response.ok(produtoService.toExport(produto)).build();
        } catch (Exception e) {
            return Response.status(Status.FORBIDDEN).build();
        }
    }
    
    @GET
    @Path("novo")
    public Response novo() {
        try {
            Map<String, Object> response = new HashMap<>();
            Produto produto = new Produto();
            produto.setFlExcluido(ExcluidoEnum.ATIVO);
            response.put("produto", produto);
            response.put("situacoes", ExcluidoEnum.values());
            response.put(GRUPOS, grupoProdutoService.getAll());
            response.put(UNIDADES, TipoUnidadeEnum.getTiposUnidade());
            response.put("tiposTroca", TipoTrocaEnum.getTipoTrocaList());
            response.put("ufs", ufService.getAll());
            response.put("rotas", rotaService.getAll());
            response.put("fornecedores", pessoaService.getFornecedoresByDto(new Pessoa()));
            return Response.ok(response).build();
        } catch (Exception e) {
            return Response.status(Status.FORBIDDEN).build();
        }
    }
    
    @GET
    @Path("editar/{id}")
    public Response editar(@PathParam("id") Long id) {
        try {
            Map<String, Object> response = new HashMap<>();
            Produto produto = produtoService.getById(id);
            response.put("produto", produto);
            response.put("situacoes", ExcluidoEnum.values());
            response.put(GRUPOS, grupoProdutoService.getAll());
            response.put(UNIDADES, TipoUnidadeEnum.getTiposUnidade());
            response.put("tiposTroca", TipoTrocaEnum.getTipoTrocaList());
            response.put("ufs", ufService.getAll());
            response.put("rotas", rotaService.getAll());
            response.put("fornecedores", pessoaService.getFornecedoresByDto(new Pessoa()));
            return Response.ok(response).build();
        } catch (Exception e) {
            return Response.status(Status.FORBIDDEN).build();
        }
    }
    
    @POST
    public Response save(Produto produto) {
        try {
            produto.getFlExcluido();
            return Response.ok(produtoService.save(produto)).build();
        } catch (Exception e) {
            return Response.status(Status.FORBIDDEN).build();
        }
    }
    
    @PUT
    public Response delete(Produto produto) {
        try {
            return Response.ok(produtoService.deletar(produto)).build();
        } catch (Exception e) {
            return Response.status(Status.FORBIDDEN).build();
        }
    }
}
