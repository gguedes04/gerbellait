package br.gerbellait.sistema.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.gerbellait.arquitetura.util.Utils;
import br.gerbellait.sistema.entity.MetaVendedorProduto;
import br.gerbellait.sistema.entity.PeriodoMeta;
import br.gerbellait.sistema.service.PeriodoMetaService;
import br.gerbellait.sistema.util.DateUtil;

@Path("/gerbellait/periodometa")
@Produces(MediaType.APPLICATION_JSON)
public class PeriodoMetaResource {

	@Inject
	private PeriodoMetaService service;

	@POST
	@Path("init")
	public Response init() {
		try {
			Map<String, Object> response = new HashMap<>();
			PeriodoMeta periodos = service.getPeriodoAtual();
			//filtro.setDataInicial(periodos.getDataInicialMeta());
			//filtro.setDataFinal(periodos.getDataFinalMeta());
			response.put("dataInicialStr", DateUtil.dateToFormatDefault(periodos.getDataInicialMeta()));
			response.put("dataFinalStr", DateUtil.dateToFormatDefault(periodos.getDataFinalMeta()));

			return Response.ok(response).build();

			/* Response.ok(!Utils.isNullOrEmpty(periodos) ? periodos.getDataInicialMeta() : null).build(); */
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@POST
	@Path("salvar")
	public Response salvar(PeriodoMeta periodoMeta) {
		try {
			return Response.ok(service.save(periodoMeta)).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}
}