package br.gerbellait.sistema.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.entity.Msn;
import br.gerbellait.sistema.service.MsnService;
import br.gerbellait.sistema.service.UsuarioService;

@Path("/gerbellait/recadovendedor")
@Produces(MediaType.APPLICATION_JSON)
public class MsnResource {

	@Inject
	private MsnService service;
	
	@Inject
	private UsuarioService usuarioService;

	@POST
	@Path("init")
	public Response init() {
		try {
			Map<String, Object> response = new HashMap<>();
			Msn msn = new Msn();
			msn.setFlExcluido(ExcluidoEnum.ATIVO);
			response.put("filtro", msn);
			response.put("lista", service.getByDto(msn));
			return Response.ok(response).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@POST
	@Path("listar")
	public Response listar(Msn msn) {
		try {
			Map<String, Object> response = new HashMap<>();
			response.put("lista", service.getByDto(msn));
			response.put("filtro", msn);
			return Response.ok(response).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@GET
	@Path("novo")
	public Response novo() {
		try {
			Map<String, Object> response = new HashMap<>();
			Msn msn = new Msn();
			msn.setFlExcluido(ExcluidoEnum.ATIVO);
			msn.setDataAlteracao(new Date());
			msn.setData(new Date());
			response.put("msn", msn);
			response.put("loginVendedores", usuarioService.getVendedores());
			return Response.ok(response).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@GET
	@Path("editar/{id}")
	public Response editar(@PathParam("id") Long id) {
		try {
			Map<String, Object> response = new HashMap<>();
			Msn msn = service.getById(id);
			msn.setDataAlteracao(new Date());
			response.put("msn", msn);
			response.put("loginVendedores", usuarioService.getVendedores());
			return Response.ok(response).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@POST
	public Response save(Msn msn) {
		try {
			msn.getFlExcluido();
			return Response.ok(service.save(msn)).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@PUT
	public Response delete(Msn msn) {
		try {
			return Response.ok(service.deletar(msn)).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}
}