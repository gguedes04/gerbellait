package br.gerbellait.sistema.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.dao.enumerador.TipoVendaEnum;
import br.gerbellait.sistema.entity.Vendedor;
import br.gerbellait.sistema.service.GrupoProdutoService;
import br.gerbellait.sistema.service.RotaService;
import br.gerbellait.sistema.service.VendedorService;

@Path("/gerbellait/vendedor")
@Produces(MediaType.APPLICATION_JSON)
public class VendedorResource {

	public static final String TIPOS = "tipos";
	public static final String ROTAS = "rotas";

	@Inject
	private VendedorService vendedorService;

	@Inject
	private RotaService rotaService;

	@Inject
	private GrupoProdutoService grupoProdutoService;

	@POST
	@Path("init")
	public Response init() {
		try {
			Map<String, Object> response = new HashMap<>();
			response.put(ROTAS, rotaService.getAll());
			response.put(TIPOS, TipoVendaEnum.getTiposVenda());
			Vendedor vendedor = new Vendedor();
			response.put("filtro", vendedor);
			response.put("lista", vendedorService.getByDto(vendedor));
			return Response.ok(response).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@POST
	@Path("listar")
	public Response listar(Vendedor vendedor) {
		try {
			Map<String, Object> response = new HashMap<>();
			response.put("lista", vendedorService.getByDto(vendedor));
			response.put("filtro", vendedor);
			return Response.ok(response).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@POST
	@Path("exportar")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response exportar(Vendedor vendedor) {
		try {
			return Response.ok(vendedorService.toExport(vendedor)).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@GET
	@Path("novo")
	public Response novo() {
		try {
			Map<String, Object> response = new HashMap<>();
			Vendedor vendedor = new Vendedor();
			vendedor.setFlExcluido(ExcluidoEnum.ATIVO);
			response.put("vendedor", vendedor);
			response.put("situacoes", ExcluidoEnum.values());
			response.put(ROTAS, rotaService.getAll());
			response.put(TIPOS, TipoVendaEnum.getTiposVenda());
			response.put("grupos", grupoProdutoService.getAll());
			return Response.ok(response).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@GET
	@Path("editar/{id}")
	public Response editar(@PathParam("id") Long id) {
		try {
			Map<String, Object> response = new HashMap<>();
			Vendedor vendedor = vendedorService.getById(id);
			response.put("vendedor", vendedor);
			response.put("situacoes", ExcluidoEnum.values());
			response.put(ROTAS, rotaService.getAll());
			response.put(TIPOS, TipoVendaEnum.getTiposVenda());
			response.put("grupos", grupoProdutoService.getAll());
			return Response.ok(response).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@POST
	public Response save(Vendedor vendedor) {
		try {
			vendedor.getFlExcluido();
			return Response.ok(vendedorService.save(vendedor)).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@POST
	@Path("saveList")
	public Response salvar(List<Vendedor> vendedores) {
		try {
			for (Vendedor vendedor : vendedores) {
				vendedorService.save(vendedor);
			}
			return Response.ok(true).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@PUT
	public Response delete(Vendedor vendedor) {
		try {
			return Response.ok(vendedorService.deletar(vendedor)).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}
}