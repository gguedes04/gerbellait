package br.gerbellait.sistema.resource;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.gerbellait.sistema.bean.TableState;
import br.gerbellait.sistema.dao.enumerador.ExcluidoEnum;
import br.gerbellait.sistema.entity.Pedido;
import br.gerbellait.sistema.entity.Pessoa;
import br.gerbellait.sistema.service.FormaPagamentoService;
import br.gerbellait.sistema.service.PedidoService;
import br.gerbellait.sistema.service.PessoaService;
import br.gerbellait.sistema.service.ProdutoService;
import br.gerbellait.sistema.service.RotaService;
import br.gerbellait.sistema.service.VendedorService;

@Path("/gerbellait/pedido")
@Produces(MediaType.APPLICATION_JSON)
public class PedidoResource {
    
    private static final String FILTRO = "filtro";
    private static final String PEDIDOS = "pedidos";
    private static final String PRODUTOS = "produtos";
    private static final String FORNECEDORES = "fornecedores";
    private static final String CLIENTES = "clientes";
    private static final String VENDEDORES = "vendedores";
    private static final String ROUTES = "rotas";
    private static final String FORMAS_PARAGAMENTO = "formasPagamento";
    
    @Inject
    private PedidoService pedidoService;
    
    @Inject
    private ProdutoService produtoService;
    
    @Inject
    private PessoaService pessoaService;
    
    @Inject
    private VendedorService vendedorService;
    
    @Inject
    private RotaService rotaService;
    
    @Inject
    private FormaPagamentoService formaPagamentoService;
    
    @POST
    @Path("init")
    public Response init(Long idUsuario) {
        try {
            Map<String, Object> response = new HashMap<>();
            Pedido pedido = new Pedido();
            response.put(FILTRO, pedido);
            response.put(PRODUTOS, produtoService.getAll());
            response.put(FORNECEDORES, pessoaService.getFornecedoresByDto(new Pessoa()));
            response.put(CLIENTES, pessoaService.getClientesPedByDto(new Pessoa()));
            response.put(VENDEDORES, vendedorService.allFilterSellers());
            response.put(ROUTES, rotaService.getAll());
            response.put(FORMAS_PARAGAMENTO, formaPagamentoService.getAll());
            return Response.ok(response).build();
        } catch (Exception e) {
            return Response.status(Status.FORBIDDEN).build();
        }
    }
    
    @POST
    @Path("listar")
    public Response listar(TableState tableState) {
        try {
            Map<String, Object> response = new HashMap<>();
            response.put("tableState", tableState);
            response.put(PEDIDOS, pedidoService.getByDto(tableState));
            return Response.ok(response).type(MediaType.APPLICATION_JSON_TYPE).build();
        } catch (Exception e) {
            return Response.status(Status.FORBIDDEN).build();
        }
    }
    
    @GET
    @Path("novo/{idUsuario}")
    public Response novo(@PathParam("idUsuario") Long idUsuario) {
        try {
            Map<String, Object> response = new HashMap<>();
            Pedido pedido = new Pedido();
            response.put("pedido", pedido);
            return Response.ok(response).build();
        } catch (Exception e) {
            return Response.status(Status.FORBIDDEN).build();
        }
    }
    
    @GET
    @Path("/ultimas/vendas/{idCliente}")
    public Response getUltimasVendas(@PathParam("idCliente") Long idCliente) {
        try {
            Map<String, Object> response = new HashMap<>();
            Pedido pedido = new Pedido();
            pedido.setCliente(new Pessoa(idCliente));
            response.put("ultimasVendas", pedidoService.getUltimasVendas(pedido));
            return Response.ok(response).build();
        } catch (Exception e) {
            return Response.status(Status.FORBIDDEN).build();
        }
    }
    
    @GET
    @Path("editar/{id}")
    public Response editar(@PathParam("id") Long id) {
        try {
            Map<String, Object> response = new HashMap<>();
            Pedido pedido = pedidoService.getById(id);
            response.put("pedido", pedido);
            return Response.ok(response).build();
        } catch (Exception e) {
            return Response.status(Status.FORBIDDEN).build();
        }
    }
    
    @POST
    public Response save(Pedido pedido) {
        try {
            pedido.getFlExcluido();
            pedido.getFlgStatus();
            pedido.getGeraNfEnum();
            return Response.ok(pedidoService.save(pedido)).build();
        } catch (Exception e) {
            return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
    }

    @DELETE
    @Path("/{pedido}")
    public Response delete(@PathParam("pedido") Long id) {
        try {
            Pedido pedido = new Pedido(id);
            pedido.setSituacao(ExcluidoEnum.EXCLUIDO.getId());
            pedido.setFlExcluido(ExcluidoEnum.EXCLUIDO);
            return Response.ok(pedidoService.deletar(pedido)).build();
        } catch (Exception e) {
            return Response.status(Status.FORBIDDEN).build();
        }
    }
    
    @POST
    @Path("/relatorio")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response relatorioCobranca(Pedido pedido) {
        try {
            return Response.ok(pedidoService.relatorio(pedido)).build();
        } catch (Exception e) {
            if (e.getMessage().equals("Não ha dados para este pedido")) {
                return Response.status(Status.LENGTH_REQUIRED).type(MediaType.TEXT_PLAIN_TYPE).entity(e.getMessage()).build();
            } else {
                return Response.status(Response.Status.BAD_REQUEST).type(MediaType.TEXT_PLAIN_TYPE).entity(e.getMessage()).build();
            }
        }
    }
}
