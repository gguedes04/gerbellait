package br.gerbellait.sistema.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.gerbellait.sistema.entity.AjusteEstoque;
import br.gerbellait.sistema.service.AjusteEstoqueService;
import br.gerbellait.sistema.service.ProdutoService;

@Path("/gerbellait/ajusteestoque")
@Produces(MediaType.APPLICATION_JSON)
public class AjusteEstoqueResource {

	private static final String AJUSTEESTOQUE = "ajusteEstoque";
	private static final String PRODUTOS = "produtos";

	@Inject
	private AjusteEstoqueService service;

	@Inject
	private ProdutoService produtoService;

	@POST
	@Path("init")
	public Response init() {
		try {
			Map<String, Object> response = new HashMap<>();
			AjusteEstoque ajusteEstoque = new AjusteEstoque();
			response.put(PRODUTOS, produtoService.getAll());
			response.put("lista", service.getBy(ajusteEstoque, true));
			response.put(AJUSTEESTOQUE, ajusteEstoque);
			return Response.ok(response).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@POST
	@Path("listar")
	public Response listar(AjusteEstoque filtro) {
		try {
			return Response.ok(service.getBy(filtro, true)).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@GET
	@Path("novo")
	public Response novo() {
		try {
			Map<String, Object> response = new HashMap<>();
			AjusteEstoque ajusteEstoque = new AjusteEstoque();
			ajusteEstoque.setDataAjusteEstoque(new Date());
			response.put(PRODUTOS, produtoService.getAll());
			response.put(AJUSTEESTOQUE, ajusteEstoque);
			return Response.ok(response).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@GET
	@Path("editar/{id}")
	public Response editar(@PathParam("id") Long id) {
		try {
			Map<String, Object> response = new HashMap<>();
			response.put(PRODUTOS, produtoService.getAll());
			response.put(AJUSTEESTOQUE, service.getById(id));
			return Response.ok(response).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@POST
	public Response save(AjusteEstoque ajusteEstoque) {
		try {
			return Response.ok(service.save(ajusteEstoque)).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@PUT
	public Response delete(AjusteEstoque ajusteEstoque) {
		try {
			return Response.ok(service.deletar(ajusteEstoque)).build();
		} catch (Exception e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}
}