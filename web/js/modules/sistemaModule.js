(function() {
	'use strict';

	angular.module('appBella.sistema.services', []);
	angular.module('appBella.sistema.run', []);
	angular.module('appBella.sistema.config', []);
	angular.module('appBella.sistema.factories', []);
	angular.module('appBella.sistema.controllers', []);

	angular.module('appBella.sistema', [ 'appBella.sistema.services',
			'appBella.sistema.factories', 'appBella.sistema.controllers',
			'appBella.sistema.run', 'appBella.sistema.config' ]);

})();
