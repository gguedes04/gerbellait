(function() {
	'use strict';
	angular.module('appBella.utils.directives', []);
	angular.module('appBella.utils.constants', []);
	angular.module('appBella.utils.services', []);
	angular.module('appBella.utils.factories', []);
	angular.module('appBella.utils.filters', []);
	angular.module('appBella.utils', [ 'appBella.utils.directives',
			'appBella.utils.factories', 'appBella.utils.constants',
			'appBella.utils.services', 'appBella.utils.filters' ]);
})();
