(function() {
	'use strict';

	function ControllerFactory($resource) {
		return $resource('rest/gerbellait/:action/:json', {}, {
			update : {
				method : 'PUT'
			}
		});
	}

	function AutenticaFactory($resource) {
		return $resource('rest/gerbellait/login');
	}

	ControllerFactory.$inject = [ '$resource' ];
	AutenticaFactory.$inject = [ '$resource' ];

	angular.module('appBella.utils.factories').factory('ControllerFactory',
			ControllerFactory).factory('AutenticaFactory', AutenticaFactory);

})();
