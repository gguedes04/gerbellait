(function() {
	'use strict';

	function ControllerService(ControllerFactory) {

		this.all = function(action) {
			var rest = {
				action : '',
				json : '{}'
			};
			rest.action = action;
			return this.find(rest);
		};

		this.find = function(objectRest) {
			return ControllerFactory.query(objectRest);
		};

		this.createUpdateGet = function(objectRest) {
			var rest = {
				action : '',
				json : ''
			};
			rest.action = objectRest.action;
			rest.json = JSON.stringify(objectRest.json);
			return rest;
		};

		this.getByIdRemove = function(objectRest) {
			var rest = {
				action : '',
				json : ''
			};
			rest.action = objectRest.action;
			rest.json = '{ id: ' + objectRest.json + '}';
			return rest;
		};

		this.post = function(objectRest) {
			return ControllerFactory.save(this.createUpdateGet(objectRest));
		};

		this.update = function(objectRest) {
			return ControllerFactory.update(this.createUpdateGet(objectRest));
		};

		this.get = function(objectRest) {
			var rest = {
				action : '',
				json : ''
			};
			rest.action = objectRest.action;
			rest.json = objectRest.json;
			return ControllerFactory.get(rest);
		};

		this.getById = function(objectRest) {
			return ControllerFactory.get(this.getByIdRemove(objectRest));
		};

		this.remove = function(objectRest) {
			return ControllerFactory.remove(this.getByIdRemove(objectRest));
		};

	}

	ControllerService.$inject = [ 'ControllerFactory' ];

	angular.module('appBella.utils.services').service('ControllerService',
			ControllerService);

})();
