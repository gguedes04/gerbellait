(function() {
	'use strict';

	var w = window.innerWidth || document.documentElement.clientWidth
			|| document.body.clientWidth;

	var TIMEZONE = {
		timezone : 'UTC',
		dateOptions : {
			changeYear: true,
		    changeMonth: true,
			dateFormat : "dd/mm/yy",
			dayNames : [ "Domingo", "Segunda", "Ter\u00e7a", "Quarta",
					"Quinta", "Sexta", "S\u00e1bado" ],
			dayNamesMin : [ "Do", "Sg", "Te", "Qa", "Qi", "Se", "Sa" ],
			dayNamesShort : [ "Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab" ],
			monthNames : [ "janeiro", "fevereiro", "mar\u00e7o", "abril",
					"maio", "junho", "julho", "agosto", "setembro", "outubro",
					"novembro", "dezembro" ],
			monthNamesShort : [ "jan", "fev", "mar", "abr", "mai", "jun",
					"jul", "ago", "set", "out", "nov", "dez" ]
		}
	};

	angular.module('appBella.utils.constants').constant('mobile', (w <= 992))
			.constant('TIMEZONE', TIMEZONE);

})();