(function() {
	'use strict';

	var FileUploadDirective = function() {
		return {
			restrict : 'A',
			require : "ngModel",
			link : function($scope, el, attrs, ngModel) {
				el.bind('change', function(event) {
					var files = event.target.files;
					var file = files[0];

					var size = file.size;
					if (size > 20000000) {
						ngModel.$setViewValue(undefined);
						var snackbarContainer = document
								.querySelector('#demo-toast-example');
						snackbarContainer.MaterialSnackbar.showSnackbar({
							message : 'Limite excedido'
						});
						return;
					}

					var reader = new FileReader();

					reader.onload = function(e) {
						ngModel.$setViewValue(reader.result);
					};

					reader.readAsDataURL(file);

					$scope.$apply();
				});
			}
		};
	};

	var FileUploadPdfDirective = function() {
		return {
			restrict : 'A',
			require : "ngModel",
			link : function($scope, el, attrs, ngModel) {
				el.bind('change', function(event) {
					var files = event.target.files;
					var file = files[0];

					if (file.type !== 'application/pdf') {
						ngModel.$setViewValue(undefined);
						var snackbarContainer = document
								.querySelector('#demo-toast-example');
						snackbarContainer.MaterialSnackbar.showSnackbar({
							message : 'Extensão diferente de pdf'
						});
						return;
					}

					var size = file.size;
					if (size > 20000000) {
						ngModel.$setViewValue(undefined);
						var snackbarContainer = document
								.querySelector('#demo-toast-example');
						snackbarContainer.MaterialSnackbar.showSnackbar({
							message : 'Limite excedido'
						});
						return;
					}

					var reader = new FileReader();

					reader.onload = function(e) {
						ngModel.$setViewValue(reader.result);
					};

					reader.readAsDataURL(file);

					$scope.$apply();
				});
			}
		};
	};

	var CurrencyInputDirective = function() {
		return {
			restrict : 'C',
			link : function($scope, $element, $attrs) {
				$element.maskMoney({
					thousands : '.',
					decimal : ','
				});
			}
		};
	};

	var PercentualInputDirective = function() {
		return {
			restrict : 'C',
			link : function($scope, $element, $attrs) {
				$element.maskMoney({
					thousands : '.',
					decimal : ',',
					precision : 5
				});
			}
		};
	};

	var CapitalizeDirective = function(uppercaseFilter, $parse) {
		return {
			restrict : 'C',
			require : 'ngModel',
			link : function(scope, element, attrs, modelCtrl) {
				var capitalize = function(inputValue) {
					if (inputValue) {
						var capitalized = inputValue.charAt(0).toUpperCase()
								+ inputValue.charAt(1).toUpperCase()
								+ inputValue.substring(2);
						if (capitalized !== inputValue) {
							modelCtrl.$setViewValue(capitalized);
							modelCtrl.$render();
						}
						return capitalized;
					}
				};
				var model = $parse(attrs.ngModel);
				modelCtrl.$parsers.push(capitalize);
				capitalize(model(scope));
			}
		};
	};

	var ZeroDireitaDirective = function(uppercaseFilter, $parse) {
		return {
			restrict : 'C',
			require : 'ngModel',
			link : function(scope, element, attrs, modelCtrl) {
				var capitalize = function(inputValue) {
					if (inputValue) {
						var capitalized;
						var h = parseInt(inputValue);
						if (new Number(h).toString().length === 1)
							capitalized = "00" + h;
						else if (new Number(h).toString().length === 2)
							capitalized = '0' + h;
						else if (new Number(h).toString().length === 3)
							capitalized = h;
						else {
							var str = new Number(h).toString();
							capitalized = str.charAt(0) + str.charAt(1)
									+ str.charAt(2);
						}

						if (capitalized !== inputValue) {
							modelCtrl.$setViewValue(capitalized);
							modelCtrl.$render();
						}
						return capitalized;
					}
				};
				var model = $parse(attrs.ngModel);
				modelCtrl.$parsers.push(capitalize);
				capitalize(model(scope));
			}
		};
	};

	var ZeroDireita4DigitoDirective = function(uppercaseFilter, $parse) {
		return {
			restrict : 'C',
			require : 'ngModel',
			link : function(scope, element, attrs, modelCtrl) {
				var capitalize = function(inputValue) {
					if (inputValue) {
						var capitalized;
						var h = parseInt(inputValue);
						if (new Number(h).toString().length === 1)
							capitalized = "00000" + h;
						else if (new Number(h).toString().length === 2)
							capitalized = "0000" + h;
						else if (new Number(h).toString().length === 3)
							capitalized = "000" + h;
						else if (new Number(h).toString().length === 4)
							capitalized = '00' + h;
						else if (new Number(h).toString().length === 5)
							capitalized = '0' + h;
						else if (new Number(h).toString().length === 6)
							capitalized = h;
						else {
							var str = new Number(h).toString();
							capitalized = str.charAt(0) + str.charAt(1)
									+ str.charAt(2) + str.charAt(3)
									+ str.charAt(4) + str.charAt(5);
						}

						if (capitalized !== inputValue) {
							modelCtrl.$setViewValue(capitalized);
							modelCtrl.$render();
						}
						return capitalized;
					}
				};
				var model = $parse(attrs.ngModel);
				modelCtrl.$parsers.push(capitalize);
				capitalize(model(scope));
			}
		};
	};

	var InputDateMaskDirective = function() {
		return {
			restrict : 'C',
			link : function($scope, $element, $attrs) {
				$element.mask("99/99/9999");
			}
		};
	};

	var ReorderDirective = function() {
		return {
			restrict : 'A',
			scope : {
				arrays : "=",
				index : "="
			},
			link : function($scope, $element, $attrs) {

				$element
						.on(
								'click',
								function() {

									Array.prototype.move = function(from, to) {
										this.splice(to, 0,
												this.splice(from, 1)[0]);
									};

									var ind = undefined;
									var taxa = $scope.index;

									for (var i = 0; i < $scope.arrays.length; i++) {
										var temp = $scope.arrays[i];
										if (taxa.tipo === temp.tipo
												&& taxa.descricao === temp.descricao
												&& taxa.faixaInicial === temp.faixaInicial
												&& taxa.faixaFinal === temp.faixaFinal
												&& taxa.prazoInicial === temp.prazoInicial
												&& taxa.prazoFinal === temp.prazoFinal
												&& taxa.taxaJuro === temp.taxaJuro
												&& taxa.taxaEfetiva === temp.taxaEfetiva
												&& taxa.quota === temp.quota
												&& taxa.tipoGarantia === temp.tipoGarantia
												&& taxa.prazoMinimoGarantia === temp.prazoMinimoGarantia
												&& taxa.prazoMaximoGarantia === temp.prazoMaximoGarantia
												&& taxa.prazoMinimoGarantia === temp.prazoMinimoGarantia
												&& taxa.faturamentoMinimo === temp.faturamentoMinimo
												&& taxa.faturamentoMaximo === temp.faturamentoMaximo
												&& taxa.porteEmpresa === temp.porteEmpresa
												&& taxa.faixaInicialPontuacao === temp.faixaInicialPontuacao
												&& taxa.faixaFinalPontuacao === temp.faixaFinalPontuacao
												&& taxa.ratings === temp.ratings) {
											ind = i;
											break;
										}
									}

									if (ind === undefined)
										return;

									$scope.arrays.move(ind,
											$(this).is(".up") ? (ind - 1)
													: (ind + 1));

									if ($(this).is(".up")) {
										$(this).parent().parent().insertBefore(
												$(this).parent().parent()
														.prev());
									} else {
										$(this).parent().parent().insertAfter(
												$(this).parent().parent()
														.next());
									}
									$scope.$digest();
								});

			}
		};
	};

	CapitalizeDirective.$inject = [ 'uppercaseFilter', '$parse' ];
	ZeroDireitaDirective.$inject = [ 'uppercaseFilter', '$parse' ];
	ZeroDireita4DigitoDirective.$inject = [ 'uppercaseFilter', '$parse' ];

	angular.module('appBella.utils.directives').directive('img',
			FileUploadDirective).directive('file', FileUploadPdfDirective)
			.directive('currencyInput', CurrencyInputDirective).directive(
					'percentualInput', PercentualInputDirective).directive(
					'capitalize', CapitalizeDirective).directive('zeros',
					ZeroDireitaDirective).directive('maskDate',
					InputDateMaskDirective).directive('reorder',
					ReorderDirective).directive('zeros4Digito',
					ZeroDireita4DigitoDirective);
})();
