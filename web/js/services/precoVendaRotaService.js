(function() {
	'use strict';

	PrecoVendaRotaService.$inject = [ '$http' ];

	angular.module('appBella.sistema.services').service(
			'PrecoVendaRotaService', PrecoVendaRotaService);

	function PrecoVendaRotaService($http) {

		var url = 'rest/gerbellait/precovendarota';

		return ({
			init : init,
			saveList : saveList
		});

		function init(filtro) {
			return $http.post(url + '/init', filtro);
		}
		
		function saveList(list) {
			return $http.post(url + '/salvar', list);
		}
	}
})();
