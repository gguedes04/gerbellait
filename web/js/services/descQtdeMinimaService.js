(function() {
	'use strict';

	DescontoQuantidadeMinimaService.$inject = [ '$http' ];

	angular.module('appBella.sistema.services').service(
			'DescontoQuantidadeMinimaService', DescontoQuantidadeMinimaService);

	function DescontoQuantidadeMinimaService($http) {

		var url = 'rest/gerbellait/percdescqtdeminima';

		return ({
			init : init,
			saveList : saveList
		});

		function init(filtro) {
			return $http.post(url + '/init', filtro);
		}

		function saveList(list) {
			return $http.post(url + '/salvar', list);
		}
	}
})();
