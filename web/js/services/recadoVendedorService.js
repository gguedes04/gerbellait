(function() {
	'use strict';

	RecadoVendedorService.$inject = [ '$http' ];

	angular.module('appBella.sistema.services').service(
			'RecadoVendedorService', RecadoVendedorService);

	function RecadoVendedorService($http) {

		var url = 'rest/gerbellait/recadovendedor';

		return ({
			init : init,
			listar : listar,
			editar : editar,
			excluir : excluir,
			salvar : salvar,
			novo : novo
		});

		function init() {
			return $http.post(url + '/init');
		}

		function listar(recado) {
			return $http.post(url + '/listar', recado);
		}

		function editar(id) {
			return $http.get(url + '/editar/' + id);
		}

		function excluir(recado) {
			return $http.put(url, recado);
		}

		function salvar(recado) {
			return $http.post(url, recado);
		}

		function novo() {
			return $http.get(url + '/novo');
		}
	}
})();
