(function() {
	'use strict';

	angular.module('appBella.sistema.services').service('DownloadService',
			DownloadService);

	function DownloadService() {

		return ({
			download : download
		});

		function download(data, fileName, type) {
			if (data) {
				var blob = new Blob([ data ], {
					encoding : "UTF-8",
					type : type + ";charset=UTF-8"
				});
				if (window.navigator.msSaveOrOpenBlob) {
					window.navigator.msSaveBlob(blob, fileName);

				} else {
					var a = window.document.createElement("a");
					a.href = window.URL.createObjectURL(blob, {
						type : type
					});
					a.download = "" + fileName;
					document.body.appendChild(a);
					a.click();
					document.body.removeChild(a);
				}
			}
		}
	}
})();
