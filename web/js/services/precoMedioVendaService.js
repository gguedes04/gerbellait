(function() {
	'use strict';

	PrecoMedioVendaService.$inject = [ '$http' ];

	angular.module('appBella.sistema.services').service('PrecoMedioVendaService',
			PrecoMedioVendaService);

	function PrecoMedioVendaService($http) {

		var url = 'rest/gerbellait/precomediovenda';

		return ({
			init : init,
			saveList : saveList
		});

		function init(filtro) {
			return $http.post(url + '/init', filtro);
		}

		function saveList(list) {
			return $http.post(url + '/salvar', list);
		}
	}
})();
