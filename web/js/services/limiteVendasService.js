(function() {
	'use strict';

	LimiteVendasService.$inject = [ '$http' ];

	angular.module('appBella.sistema.services').service('LimiteVendasService',
			LimiteVendasService);

	function LimiteVendasService($http) {

		var url = 'rest/gerbellait/limitevendas';

		return ({
			init : init,
			saveList : saveList
		});

		function init(filtro) {
			return $http.post(url + '/init', filtro);
		}

		function saveList(list) {
			return $http.post(url + '/salvar', list);
		}
	}
})();
