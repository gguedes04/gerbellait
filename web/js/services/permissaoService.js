(function() {
	'use strict';

	function PermissaoService(SessionService) {

		/**
		 * Permissão de acesso aos itens principais do menu;
		 */
		var containsPermissionsMain = function(str) {
			if (!isUndefinedOrNull(str)) {
				var usuario = SessionService.getUser();
				if (!usuario) {
					return false;
				}
				return usuario.grupo.indexOf(str) !== -1;
			}
			return false;
		};

		/**
		 * Permissão de acesso aos itens internos do menu;
		 */
		var containsPermissions = function(str) {
			if (!isUndefinedOrNull(str)) {
				var usuario = SessionService.getUser();
				if (!usuario) {
					return false;
				}
				return usuario.funcoes.indexOf(str) !== -1;
			}
			return false;
		};

		function isUndefinedOrNull(str) {
			return angular.isUndefined(str) || str === null;
		}

		return {
			containsPermissions : containsPermissions,
			containsPermissionsMain : containsPermissionsMain,
			isUndefinedOrNull : isUndefinedOrNull
		};
	}

	PermissaoService.$inject = [ 'SessionService' ];

	angular.module('appBella.sistema.services').service('PermissaoService',
			PermissaoService);

})();