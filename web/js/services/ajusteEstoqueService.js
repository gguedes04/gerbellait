(function() {
	'use strict';

	AjusteEstoqueService.$inject = [ '$http' ];

	angular.module('appBella.sistema.services').service('AjusteEstoqueService',
			AjusteEstoqueService);

	function AjusteEstoqueService($http) {

		var url = 'rest/gerbellait/ajusteestoque';

		return ({
			init : init,
			listar : listar,
			editar : editar,
			excluir : excluir,
			salvar : salvar,
			novo : novo
		});

		function init() {
			return $http.post(url + '/init');
		}

		function listar(ajusteEstoque) {
			return $http.post(url + '/listar', ajusteEstoque);
		}
		
		function editar(id) {
			return $http.get(url + '/editar/' + id);
		}

		function novo() {
			return $http.get(url + '/novo');
		}

		function excluir(ajusteEstoque) {
			return $http.put(url, ajusteEstoque);
		}

		function salvar(ajusteEstoque) {
			return $http.post(url, ajusteEstoque);
		}
	}
})();
