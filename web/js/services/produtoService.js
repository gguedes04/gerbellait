(function() {
    'use strict';

    ProdutoService.$inject = ['$http'];

    angular.module('appBella.sistema.services').service('ProdutoService', ProdutoService);

    function ProdutoService($http) {

        const url = 'rest/gerbellait/produto';

        return ({
            init,
            exportar,
            editar,
            excluir,
            salvar,
            novo,
            listar,
            getByDesc,
            getRelatorioEstoque,
            relatorioEstoqueExportar
        })
            ;

        function init() {
            return $http.post(url + '/init');
        }

        function listar(tableState) {
            return $http.post(url + '/listar', tableState);
        }

        function getByDesc(clienteId, text) {
            return $http.get(`${url}/getByText/${clienteId}/${text}`);
        }

        function exportar(produto) {
            return $http.post(url + '/exportar', produto, {
                responseType: 'arraybuffer'
            });
        }

        function editar(id) {
            return $http.get(url + '/editar/' + id);
        }

        function excluir(produto) {
            return $http.put(url, produto);
        }

        function salvar(produto) {
            return $http.post(url, produto);
        }

        function novo() {
            return $http.get(url + '/novo');
        }

        function relatorioEstoqueExportar(tableState) {
            return $http.post(url + '/relatorio/estoque/exportar', tableState, {
                responseType: 'arraybuffer'
            });
        }

        function getRelatorioEstoque(tableState) {
            return $http.get(`${url}/relatorio/estoque/${angular.toJson(tableState)}`);
        }
    }
})();
