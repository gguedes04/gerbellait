(function() {
	'use strict';

	TransportadorService.$inject = [ '$http' ];

	angular.module('appBella.sistema.services').service('TransportadorService',
			TransportadorService);

	function TransportadorService($http) {

		var url = 'rest/gerbellait/transportador';

		return ({
			init : init,
			listar : listar,
			editar : editar,
			excluir : excluir,
			salvar : salvar,
			novo : novo,
			cidades : cidades
		});

		function init() {
			return $http.post(url + '/init');
		}

		function listar(vendedor) {
			return $http.post(url + '/listar', vendedor);
		}

		function editar(id) {
			return $http.get(url + '/editar/' + id);
		}

		function excluir(vendedor) {
			return $http.put(url, vendedor);
		}

		function salvar(vendedor) {
			return $http.post(url, vendedor);
		}

		function novo() {
			return $http.get(url + '/novo');
		}

		function cidades(uf) {
			return $http.get(url + '/cidades/' + uf);
		}
	}
})();
