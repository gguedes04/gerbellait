(function() {
	'use strict';

	PeriodoMetaService.$inject = [ '$http' ];

	angular.module('appBella.sistema.services').service('PeriodoMetaService',
			PeriodoMetaService);

	function PeriodoMetaService($http) {

		var url = 'rest/gerbellait/periodometa';

		return ({
			init : init,
			salvar : salvar
		});

		function init() {
			return $http.post(url + '/init');
		}

		function salvar(periodoMeta) {
			return $http.post(url + '/salvar', periodoMeta);
		}
	}
})();
