(function() {
	'use strict';

	UsuarioService.$inject = [ '$http' ];

	angular.module('appBella.sistema.services').service('UsuarioService',
			UsuarioService);

	function UsuarioService($http) {

		var url = 'rest/gerbellait/usuario';

		return ({
			init : init,
			editar : editar,
			listar : listar,
			excluir : excluir,
			salvar : salvar,
			novo : novo
		});

		function init() {
			return $http.get(url + '/init');
		}

		function editar(id) {
			return $http.get(url + '/editar/' + id);
		}

		function novo() {
			return $http.get(url + '/novo');
		}

		function listar(usuario) {
			return $http.post(url + '/listar', usuario);
		}

		function excluir(usuario) {
			return $http.put(url, usuario);
		}

		function salvar(usuario) {
			return $http.post(url, usuario);
		}
	}
})();
