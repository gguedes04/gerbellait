(function() {
	'use strict';

	QuantidadeVendaService.$inject = [ '$http' ];

	angular.module('appBella.sistema.services').service('QuantidadeVendaService',
			QuantidadeVendaService);

	function QuantidadeVendaService($http) {

		var url = 'rest/gerbellait/quantidadevenda';

		return ({
			init : init,
			saveList : saveList
		});

		function init(filtro) {
			return $http.post(url + '/init', filtro);
		}

		function saveList(list) {
			return $http.post(url + '/salvar', list);
		}
	}
})();
