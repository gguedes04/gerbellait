(function() {
	'use strict';

	AutorizacaoService.$inject = [ '$location', 'PermissaoService', '$http' ];

	angular.module('appBella.sistema.services').service('AutorizacaoService',
			AutorizacaoService);

	function AutorizacaoService($location, PermissaoService, $http) {

		return ({
			autenticar : autenticar,
			access : access,
			autorizarUsuario : autorizarUsuario,
			esqueceuEmail : esqueceuEmail,
			salvaNovaSenha : salvaNovaSenha
		});

		function autorizarUsuario(access) {
			// console.log(access);
		}

		function autenticar(usuario) {
			return $http.post('rest/gerbellait/login', usuario);
		}

		function access(access) {
			if (!access || !access.requiresLogin) {
				return true;
			}
			var result = PermissaoService
					.containsPermissions(access.permissionType);
			if (result === false) {
				$location.path('/');
			}
		}

		function esqueceuEmail(email) {
			return $http.post('rest/gerbellait/usuario/reset', email);
		}
		
		function salvaNovaSenha(salvaNovaSenha) {
			return $http.post('rest/gerbellait/usuario/resetsenha', salvaNovaSenha);
		}
	}
})();
