(function() {
	'use strict';

	function SessionService($window) {

		var getUser = function() {
			var usuario = $window.sessionStorage.getItem('user_token');
			if (!usuario) {
				return undefined;
			}
			usuario = JSON.parse(usuario);
			return usuario;
		};

		var setUser = function(data) {
			$window.sessionStorage.setItem('user_token', JSON.stringify(data));
		};

		var removeUser = function() {
			$window.sessionStorage.removeItem('user_token');
		};

		return {
			getUser : getUser,
			setUser : setUser,
			removeUser : removeUser
		};
	}

	SessionService.$inject = [ '$window' ];

	angular.module('appBella.sistema.services').service('SessionService',
			SessionService);

})();