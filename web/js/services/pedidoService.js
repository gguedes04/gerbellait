(function() {
    'use strict';

    PedidoService.$inject = ['$http'];

    angular.module('appBella.sistema.services').service('PedidoService',
        PedidoService);

    function PedidoService($http) {

        const url = 'rest/gerbellait/pedido';
        let filter = {};
        let alreadyFiltered = false;
        let currentData = {};

        return ({
            filter,
            alreadyFiltered,
            currentData,
            init,
            listar,
            editar,
            excluir,
            salvar,
            novo,
            printOrder,
            getUltimasVendas
        });

        function init(idUsuario) {
            return $http.post(url + '/init', idUsuario);
        }

        function listar(pedido) {
            return $http.post(url + '/listar', pedido);
        }

        function excluir(pedido) {
            return $http.delete(`${ url }/${ pedido }`);
        }

        function salvar(pedido) {
            return $http.post(url, pedido);
        }

        function novo(idUsuario) {
            return $http.get(url + '/novo/' + idUsuario);
        }

        function getUltimasVendas(idCliente) {
            return $http.get(url + '/ultimas/vendas/' + idCliente);
        }

        function editar(id) {
            return $http.get(url + '/editar/' + id);
        }

        function printOrder(order) {
            delete order.dataEntrega;
            delete order.dataVencimento;
            return $http.post(url + '/relatorio', order, { responseType: 'arraybuffer' });
        }
    }
})();
