(function() {
	'use strict';

	VendedorService.$inject = [ '$http' ];

	angular.module('appBella.sistema.services').service('VendedorService',
			VendedorService);

	function VendedorService($http) {

		var url = 'rest/gerbellait/vendedor';

		return ({
			init : init,
			listar : listar,
			exportar : exportar,
			editar : editar,
			excluir : excluir,
			salvar : salvar,
			saveList : saveList,
			novo : novo
		});

		function init() {
			return $http.post(url + '/init');
		}

		function listar(vendedor) {
			return $http.post(url + '/listar', vendedor);
		}

		function exportar(vendedor) {
			return $http.post(url + '/exportar', vendedor, {
				responseType : 'arraybuffer'
			});
		}

		function editar(id) {
			return $http.get(url + '/editar/' + id);
		}

		function excluir(vendedor) {
			return $http.put(url, vendedor);
		}

		function salvar(vendedor) {
			return $http.post(url, vendedor);
		}

		function saveList(list) {
			return $http.post(url + '/saveList', list);
		}

		function novo() {
			return $http.get(url + '/novo');
		}
	}
})();
