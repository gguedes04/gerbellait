(function() {
    'use strict';

    mapCargaService.$inject = ['$http'];

    angular.module('appBella.sistema.services').service('MapCargaService', mapCargaService);

    function mapCargaService($http) {

        const url = 'rest/gerbellait/mapacarga';

        return {
            init: () => $http.post(`${url}/init`),

            getProdutos: filter => $http.post(`${url}/produtos`, filter),

            getPedidos: filter => $http.post(`${url}/pedidos`, filter),

            save: filter => $http.post(url, filter),

            relatorioMapaCobranca: filter => $http.post(url + '/relatorio/cobranca', filter, { responseType: 'arraybuffer' }),

            relatorioMapaCarga: filter => $http.post(url + '/relatorio/carga', filter, { responseType: 'arraybuffer' }),

            getLoadMapByFilter: filter => $http.post(`${url}/loadmap`, filter),

            deleteLoadMap: id => $http.delete(`${url}/${id}`),

            getAllOrders: filter => $http.post(`${url}/pedidos/getallorders`, filter),
        };

    }
})();
