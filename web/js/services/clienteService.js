(function() {
	'use strict';

	ClienteService.$inject = [ '$http' ];

	angular.module('appBella.sistema.services').service('ClienteService',
			ClienteService);

	function ClienteService($http) {

		var url = 'rest/gerbellait/cliente';

		return ({
			init : init,
			listar : listar,
			exportar : exportar,
			cidades : cidades,
			editar : editar,
			excluir : excluir,
			salvar : salvar,
			novo : novo,
			rotasByVendedor : rotasByVendedor
		});

		function init(idUsuario) {
			return $http.post(url + '/init', idUsuario);
		}

		function listar(pessoa) {
			return $http.post(url + '/listar', pessoa);
		}

		function exportar(pessoa) {
			return $http.post(url + '/exportar', pessoa, {
				responseType : 'arraybuffer'
			});
		}

		function cidades(uf) {
			return $http.get(url + '/cidades/' + uf);
		}

		function excluir(cliente) {
			return $http.put(url, cliente);
		}

		function salvar(cliente) {
			return $http.post(url, cliente);
		}

		function novo(idUsuario) {
			return $http.get(url + '/novo/' + idUsuario);
		}

		function editar(id) {
			return $http.get(url + '/editar/' + id);
		}

		function rotasByVendedor(idVendedor) {
			return $http.get(url + '/rotas/' + idVendedor);
		}
	}
})();
