(function() {
	'use strict';

	angular.module('appBella.modules', [ 'appBella.sistema', 'appBella.utils',
			'ui.router' ]);
})();