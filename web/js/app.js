(function() {
    'use strict';

    var app = angular.module('appBella', ['ngRoute', 'ngResource', 'ngSanitize',
        'ui.date', 'ngMaterial', 'appBella.modules', 'smart-table', 'ui.utils.masks']);

    app.constant('funcoes', {
        manterUsuario: 'MANTER USUÁRIOS',
        cliente: 'CLIENTE',
        produto: 'PRODUTO',
        vendedor: 'VENDEDOR',
        fornecedor: 'FORNECEDOR',
        precoVendaRota: 'PREÇO DE VENDA',
        rota: 'ROTA',
        meta: 'META',
        ajusteEstoque: 'AJUSTE DE ESTOQUE',
        cfpo: 'CFPO',
        transportador: 'TRASNPORTADOR',
        limiteVenda: 'LIMITES DE VENDA',
        recadoVendedor: 'RECADO VENDEDOR',
        formaPagamento: 'FORMA PAGAMENTO',
        pedido: 'PEDIDOS'
    });

    app.constant('perfis', {
        ADMINISTRADOR: 'ADMINISTRADOR',
        CADASTRO: 'CADASTRO',
        VENDEDOR: 'VENDEDOR'
    });

    app.constant('pagination', [10, 20, 30, 50]);

})();
