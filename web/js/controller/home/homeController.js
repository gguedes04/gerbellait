(function() {

    function HomeController($scope, $timeout, $interval, $location,
                            SessionService, PermissaoService, TIMEZONE, ControllerService,
                            $filter, funcoes, $window) {

        var vm = this;
        vm.usuario = SessionService.getUser();
        vm.timezone = TIMEZONE;
        vm.funcoes = funcoes;
        vm.criaMenu = criaMenu;
        vm.clicouSair = false;

        vm.goTo = function(url) {
            if (url) {
                $location.path('/' + url);
            } else {
                $location.path('/home');
            }
            hideMenu();
        };

        function hideMenu() {
            var obfuscator = angular.element(document.querySelector('.mdl-layout__obfuscator'));
            if (obfuscator) {
                obfuscator.removeClass('is-visible');
            }
            var drawer = angular.element(document.querySelector('.mdl-layout__drawer'));
            if (drawer) {
                drawer.removeClass('is-visible');
            }
        }

        vm.corrigeMascaraPerc = function(value) {
            Number.prototype.format = function(n, x, s, c) {
                var re = '\\d(?=(\\d{' + (x || 3) + '})+'
                    + (n > 0 ? '\\D' : '$') + ')', num = this.toFixed(Math
                    .max(0, ~~n));

                return (c ? num.replace('.', c) : num).replace(new RegExp(re,
                    'g'), '$&' + (s || ','));
            };
            return value.format(5, 3, '.', ',');
        };

        vm.corrigeMascara = function(value) {
            if (!value.toString().match(/\./g)) {
                value = value.toString() + '.00';
            }
            if (value.toString().match(/\./g)
                && (value.toString().split('\.')[1].length === 1)) {
                value = value.toString() + '0';
            }
            value = vm.formatSemCifrao(new Number(value));
            return value;
        };

        vm.formatSemCifrao = function(value) {
            Number.prototype.format = function(n, x, s, c) {
                var re = '\\d(?=(\\d{' + (x || 3) + '})+'
                    + (n > 0 ? '\\D' : '$') + ')', num = this.toFixed(Math
                    .max(0, ~~n));

                return (c ? num.replace('.', c) : num).replace(new RegExp(re,
                    'g'), '$&' + (s || ','));
            };
            return value.format(2, 3, '.', ',');
        };

        vm.sair = function() {
            SessionService.removeUser();
            delete vm.usuario;
            vm.clicouSair = true;
            $location.path('/');
        };

        vm.containsPermissionsMain = function(permissao) {
            return PermissaoService.containsPermissionsMain(permissao);
        };

        vm.containsPermissions = function(permissao) {
            return PermissaoService.containsPermissions(permissao);
        };

        vm.stopSpinner = function() {
            $timeout(function() {
                delete vm.spinner;
            }, 1000);
        };

        vm.noEdit = function() {
            $timeout(function() {
                delete vm.editHome;
                delete vm.edit;
            }, 1500);
        };

        $scope.$on('$viewContentLoaded', function() {
            if (vm.clicouSair == false && (vm.usuario)) {
                $('#header').show();
                $('#menu').show();
                $('.mdl-layout__drawer-button').show();
            }
        });

        function criaMenu() {
            $('.sidebar-menu > li.have-children a').on('click', function(i) {
                i.preventDefault();
                if (!$(this).parent().hasClass('active')) {
                    $('.sidebar-menu li ul').slideUp();
                    $(this).next().slideToggle();
                    $('.sidebar-menu li').removeClass('active');
                    $(this).parent().addClass('active');
                } else {
                    $(this).next().slideToggle();
                    $('.sidebar-menu li').removeClass('active');
                }
            });
        }

    }

    HomeController.$inject = ['$scope', '$timeout', '$interval', '$location',
        'SessionService', 'PermissaoService', 'TIMEZONE',
        'ControllerService', '$filter', 'funcoes', '$window'];

    angular.module('appBella.sistema.controllers').controller('HomeController',
        HomeController);
})();
