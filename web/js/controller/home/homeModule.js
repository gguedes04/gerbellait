(function() {
	'use strict';

	angular.module('appBella.home.controllers', []);

	angular.module('appBella.home', [ 'appBella.home.controllers' ]);

})();
