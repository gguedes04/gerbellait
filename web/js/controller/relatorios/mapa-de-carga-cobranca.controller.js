(function() {
    'use strict';

    mapCargaCobranca.$inject = ['$location', 'MapCargaService', 'pagination', 'TIMEZONE', '$scope', 'DownloadService'];

    angular.module('appBella.sistema.controllers').controller('MapCargaCobranca', mapCargaCobranca);

    function mapCargaCobranca($location, MapCargaService, pagination, TIMEZONE, $scope, DownloadService) {
        const vm = this;
        init();

        function init() {
            vm.timezone = TIMEZONE;
            vm.loading = false;
            vm.loadingOrders = false;
            vm.filter = {};
            vm.sellers = [];
            vm.routes = [];
            vm.ordersFullList = [];
            vm.productsFullList = [];
            vm.selectedRoutes = [];
            vm.selectedSellers = [];
            vm.selectedOrders = [];
            vm.orders = [];
            vm.paginas = pagination;
            vm.page = 1;
            vm.total = 0;

            vm.addSeller = addSellerToList;
            vm.addRoute = addRouteToList;
            vm.removeRoute = removeRouteFromList;
            vm.removeSeller = removeSellerFromList;
            vm.search = search;
            vm.callServerOrders = callServerOrders;
            vm.callServerProducts = callServerProducts;
            vm.addOrderToList = addOrderToList;
            vm.exportMapaCarga = exportMapaCarga;
            vm.exportCobranca = exportCobranca;

            vm.hasSellerAdded = opt => vm.selectedSellers.some(seller => seller.id === opt.id);
            vm.hasRouteAdded = opt => vm.selectedRoutes.some(seller => seller.id === opt.id);
            vm.save = async () => {
                if (vm.selectedOrders.length) {
                    const { data } = await MapCargaService.save(vm.selectedOrders);
                    showMessage('Registros salvos com sucesso! Numero de carga gerado é: ' + data + '. ');
                    vm.tableStateOrders.pagination.start = 0;
                    search();
                    searchProducts();
                }
            };

            _getData();
        }

        async function exportCobranca() {
            vm.loading = true;
            try {
                const { data } = await MapCargaService.relatorioMapaCobranca(vm.filter);

                DownloadService.download(data, 'relatorio-mapa-cobrança.xls', 'text/xls');
                $scope.$applyAsync();
            } catch (error) {
                showMessage('Falha ao exportar os dados!');
            } finally {
                vm.loading = false;
            }
        }

        async function exportMapaCarga() {
            vm.loading = true;
            try {
                const { data } = await MapCargaService.relatorioMapaCarga(vm.filter);

                DownloadService.download(data, 'relatorio-mapa-carga.xls', 'text/xls');
                $scope.$applyAsync();
            } catch (error) {
                showMessage('Falha ao exportar os dados!');
            } finally {
                vm.loading = false;
            }
        }

        function callServerOrders(tableState) {
            vm.tableStateOrders = tableState;
            vm.loadingOrders = true;

            const pagination = tableState.pagination;

            const start = pagination.start || 0;
            const number = pagination.number || 10;
            const result = getPage(vm.ordersFullList, start, number);

            vm.orders = result.data;
            tableState.pagination.numberOfPages = result.numberOfPages;
            vm.loadingOrders = false;
            $scope.$applyAsync();
        }

        function callServerProducts(tableState) {
            vm.tableStateProducts = tableState;
            vm.loadingOrders = true;

            const pagination = tableState.pagination;

            const start = pagination.start || 0;
            const number = pagination.number || 10;
            const result = getPage(vm.productsFullList, start, number);

            vm.products = result.data;
            tableState.pagination.numberOfPages = result.numberOfPages;
            vm.loadingOrders = false;
            $scope.$applyAsync();
        }

        function getPage(list, start, number) {
            const result = list.slice(start, start + number);

            return {
                data: result,
                numberOfPages: Math.ceil(list.length / number)
            };
        }

        function addOrderToList(order, all = false) {
            if (all) {
                if (vm.checkedAllOrders) {
                    vm.ordersFullList.forEach(order => vm.selectedOrders.push(angular.copy(order, { checked: true })));
                } else {
                    vm.ordersFullList.forEach(order => order.checked = false);
                    vm.selectedOrders = [];
                }
            } else {
                const index = vm.selectedOrders.findIndex(_order => _order.pedido === order.pedido);
                if (index > -1) {
                    vm.selectedOrders.splice(index, 1);
                } else {
                    vm.selectedOrders.push(order);
                }
            }
            searchProducts();
        }

        async function searchProducts() {
            vm.total = 0;
            vm.loadingProducts = true;
            if (vm.selectedOrders.length > 0 && !vm.filter.loadMapNumber) {
                vm.filter.selectedOrders = vm.selectedOrders;
                const { data } = await MapCargaService.getProdutos(vm.filter);
                vm.productsFullList = data;
            } else {
                vm.productsFullList = [];
            }
            vm.productsFullList.forEach(product => vm.total += product.total);
            callServerProducts(vm.tableStateProducts);
            vm.loadingProducts = false;
        }

        async function search() {
            vm.selectedOrders = [];
            searchProducts();
            if (vm.filter.dataInicial && vm.filter.dataFinal
                && moment(vm.filter.dataInicial, 'DD/MM/YYYY').isAfter(moment(vm.filter.dataFinal, 'DD/MM/YYYY'))) {
                showMessage('Data inicial deve ser menor que a final.');
                return;
            }
            vm.filter.selectedSellers = vm.selectedSellers.map(seller => seller.id);
            vm.filter.selectedRoutes = vm.selectedRoutes.map(seller => seller.id);
            vm.loadingOrders = true;
            const { data } = await MapCargaService.getPedidos(vm.filter);
            vm.loadingOrders = false;
            vm.ordersFullList = data;
            callServerOrders(vm.tableStateOrders);
        }

        async function _getData() {
            try {
                vm.loading = true;
                const { data } = await MapCargaService.init();
                vm.sellers = data.sellers;
                vm.routes = data.routes;
                vm.autocompleteSearcher = autocompleteSearcher.bind(data.reports);
            } catch (e) {
                showMessage('Falha ao carregar!');
            } finally {
                vm.loading = false;
            }
        }

        function showMessage(message) {
            const snackbarContainer = document.querySelector('#message-screen');
            snackbarContainer.MaterialSnackbar.showSnackbar({ message });
        }

        function addSellerToList() {
            if (vm.seller) {
                vm.selectedSellers.push(vm.seller);
                delete vm.seller;
            }
        }

        function addRouteToList() {
            if (vm.route) {
                vm.selectedRoutes.push(vm.route);
                delete vm.route;
            }
        }

        function removeSellerFromList(seller) {
            const index = vm.selectedSellers.findIndex(_seller => _seller.id === seller.id);
            if (index > -1) {
                vm.selectedSellers.splice(index, 1);
            }
        }

        function removeRouteFromList(route) {
            const index = vm.selectedRoutes.findIndex(_route => _route.id === route.id);
            if (index > -1) {
                vm.selectedRoutes.splice(index, 1);
            }
        }

        function autocompleteSearcher(textSearch) {
            if (textSearch) {
                if (!isNaN(textSearch)) {
                    return this.filter(item => item.id.toString().indexOf(textSearch) > -1);
                }
            } else {
                return [];
            }
        }
    }
})();
