(function() {
    'use strict';

    RelatorioEstoqueProduto.$inject = ['ProdutoService', '$scope', 'DownloadService'];

    angular.module('appBella.sistema.controllers').controller('RelatorioEstoqueProdutoController', RelatorioEstoqueProduto);

    function RelatorioEstoqueProduto(ProdutoService, $scope, DownloadService) {

        const vm = this;
        vm.records = [];
        vm.callServer = callServer;
        vm.export = async () => {
            vm.loading = true;
            vm.tableState.pagination.start = 0;
            vm.tableState.pagination.number = 99999999;
            try {
                const { data } = await ProdutoService.relatorioEstoqueExportar(vm.tableState);

                DownloadService.download(data, 'relatorio-estoque-produto.xls', 'text/xls');
                $scope.$applyAsync();
            } catch (error) {
                showMessage('Falha ao exportar os dados!');
            } finally {
                vm.loading = false;
            }
        };

        async function callServer(tableState) {
            try {
                vm.loading = true;
                tableState.pagination.start = tableState.pagination.start || 0;
                tableState.pagination.number = tableState.pagination.number || 10;
                const response = await ProdutoService.getRelatorioEstoque(tableState);
                const { data } = response;
                vm.records = data.records;
                tableState.pagination.numberOfPages = data.tableState.pagination.numberOfPages;
                $scope.$applyAsync();
                vm.tableState = tableState;
            } catch (e) {
                showMessage('Falha ao carregar os dados!');
            } finally {
                vm.loading = false;
            }
        }

        function showMessage(message) {
            const snackbarContainer = document.querySelector('#message-screen');
            snackbarContainer.MaterialSnackbar.showSnackbar({ message });
        }
    }
})();
