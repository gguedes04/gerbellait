(function() {
	'use strict';

	TransportadoresController.$inject = [ '$location', 'TransportadorService',
			'DownloadService', 'PermissaoService', 'funcoes', 'pagination' ];

	angular.module('appBella.sistema.controllers').controller(
			'TransportadoresController', TransportadoresController);

	function TransportadoresController($location, TransportadorService,
			DownloadService, PermissaoService, funcoes, pagination) {

		var vm = this;
		vm.mask = mask;
		vm.listar = listar;
		vm.seleciona = seleciona;
		vm.novo = novo;
		vm.toEdit = toEdit;
		vm.reset = reset;
		vm.toPage = toPage;
		vm.selectPage = selectPage;
		vm.openDialog = openDialog;
		vm.closeDialog = closeDialog;
		vm.excluir = excluir;
		vm.quantidadePorPagina = quantidadePorPagina;
		vm.manterTransportador = {};
		vm.manterTransportador.lista = [];
		vm.manterTransportador.filtro = {};
		vm.transportadorSelecionado = {};
		vm.paginas = pagination;
		vm.page = 1;
		vm.loading = true;
		init();

		function init() {
			validaPermissaoAcesso();
			TransportadorService.init().then(function success(response) {
				vm.manterTransportador.filtro = response.data.filtro;
				vm.manterTransportador.transportadores = response.data.transportadores;
				vm.manterTransportador.lista = response.data.lista;
				vm.loading = false;
			}, function error(response) {
				showMessage('Falha ao carregar os transportadores!');
				vm.loading = false;
			});
		}

		function validaPermissaoAcesso() {
			if (!PermissaoService.containsPermissions(funcoes.transportador)) {
				$location.path('/login');
			}
		}

		function reset() {
			vm.page = 1;
			vm.manterTransportador.filtro.paginate.paginaAtual = 0;
			vm.manterTransportador.lista = [];
		}

		function showMessage(_messsage) {
			var snackbarContainer = document.querySelector('#message-screen');
			snackbarContainer.MaterialSnackbar.showSnackbar({
				message : _messsage
			});
		}

		function mask() {
			$("#cnpjcpf").mask("99.999.999/9999-99");
		}

		function toPage(page) {
			vm.manterTransportador.filtro.paginate.paginaAtual = page;
			vm.page = vm.manterTransportador.filtro.paginate.paginaAtual + 1;
			vm.listar();
		}

		function selectPage() {
			if (vm.page < 1
					|| vm.page > vm.manterTransportador.filtro.paginate.numTotalPaginas) {
				showMessage('Página inválida!');
				vm.page = 1;
				vm.manterTransportador.filtro.paginate.paginaAtual = 0;
				vm.toPage(vm.manterTransportador.filtro.paginate.paginaAtual);
			} else if (vm.page) {
				vm.toPage(vm.page - 1);
			}
		}

		function quantidadePorPagina() {
			vm.page = 1;
			vm.manterTransportador.filtro.paginate.paginaAtual = 0;
			vm.listar();
		}

		function listar() {
			vm.loading = true;
			TransportadorService
					.listar(vm.manterTransportador.filtro)
					.then(
							function success(response) {
								vm.manterTransportador.lista = response.data.lista;
								vm.manterTransportador.filtro.paginate = response.data.filtro.paginate;
								vm.loading = false;
							},
							function error(response) {
								showMessage('Falha ao carregar os transportadores!');
								vm.loading = false;
							});
		}

		function novo() {
			$location.path('/transportador/novo');
		}

		function toEdit(transportador) {
			if (transportador && transportador.id) {
				$location.path('/transportador/' + transportador.id);
			}
		}

		function seleciona(vendedor) {
			vm.transportadorSelecionado = vendedor;
			vm.openDialog();
		}

		function openDialog() {
			document.querySelector('#deleteDialog').showModal();
		}

		function closeDialog() {
			document.querySelector('#deleteDialog').close();
		}

		function excluir() {
			if (vm.transportadorSelecionado) {
				TransportadorService
						.excluir(vm.transportadorSelecionado)
						.then(
								function success(response) {
									closeDialog();
									showMessage('Transportador excluído com sucesso!');
									vm.listar();
									delete vm.transportadorSelecionado;
								},
								function error(response) {
									closeDialog();
									showMessage('Falha ao tentar excluir o transportador!');
									delete vm.transportadorSelecionado;
								});
			}
		}
	}
})();
