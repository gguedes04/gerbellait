(function() {
	TransportadorController.$inject = [ '$location', 'TransportadorService',
			'$routeParams', '$timeout', 'funcoes', 'PermissaoService',
			'$filter' ];

	angular.module('appBella.sistema.controllers').controller(
			'TransportadorController', TransportadorController);

	function TransportadorController($location, TransportadorService,
			$routeParams, $timeout, funcoes, PermissaoService, $filter) {

		var vm = this;
		vm.salvar = salvar;
		vm.voltar = voltar;
		vm.mask = mask;
		vm.cidades = cidades;
		vm.manterTransportador = {};

		init();

		function init() {
			validaPermissaoAcesso();
			if ($routeParams.id > 0) {
				editar($routeParams.id);
			} else {
				novo();
			}
		}

		function mask() {
			$("#cnpjcpf").mask("99.999.999/9999-99");
			$("#cep").mask("99.999-999");
		}

		function validaPermissaoAcesso() {
			if (!PermissaoService.containsPermissions(funcoes.transportador)) {
				$location.path('/login');
			}
		}

		function showMessage(_messsage) {
			var snackbarContainer = document.querySelector('#message-screen');
			snackbarContainer.MaterialSnackbar.showSnackbar({
				message : _messsage
			});
		}

		function editar(id) {
			TransportadorService.editar(id).then(function success(response) {
				vm.manterTransportador.obj = response.data.transportador;
				vm.manterTransportador.ufs = response.data.ufs;
				vm.manterTransportador.cidades = response.data.cidades;
			}, function error(response) {
				showMessage('Falha ao carregar o transportador!');
			});
		}

		function novo() {
			TransportadorService.novo($routeParams.id).then(
					function success(response) {
						vm.manterTransportador.obj = response.data.transportador;
						vm.manterTransportador.ufs = response.data.ufs;
					}, function error(response) {
						showMessage('Falha ao carregar o transportador!');
					});
		}

		function salvar() {
			TransportadorService.salvar(vm.manterTransportador.obj).then(
					function success(response) {
						showMessage('Transportador salvo com sucesso!');
						$timeout(function() {
							vm.voltar();
						}, 1000);
					}, function error(response) {
						showMessage('Falha ao tentar salvar o transportador!');
					});
		}

		function voltar() {
			$location.path('/transportador');
		}
		
		function cidades() {
			if (vm.manterTransportador.obj.cidade.uf.id) {
				TransportadorService.cidades(
						vm.manterTransportador.obj.cidade.uf.id)
						.then(function success(response) {
							vm.manterTransportador.cidades = response.data;
						}, function error(response) {
							showMessage('Falha ao carregar as cidades!');
						});
			} else {
				vm.manterCliente.cidades = [];
			}
		}
	}
})();
