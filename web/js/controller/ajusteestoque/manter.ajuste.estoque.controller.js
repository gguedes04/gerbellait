(function() {
	'use strict';

	ManterAjusteEstoqueController.$inject = [ '$location',
			'AjusteEstoqueService', '$routeParams', '$timeout', 'funcoes',
			'PermissaoService', 'SessionService', 'perfis', 'TIMEZONE', '$filter' ];

	angular.module('appBella.sistema.controllers').controller(
			'ManterAjusteEstoqueController', ManterAjusteEstoqueController);

	function ManterAjusteEstoqueController($location, AjusteEstoqueService,
			$routeParams, $timeout, funcoes, PermissaoService, SessionService,
			perfis, TIMEZONE, $filter) {

		var vm = this;
		vm.usuario = SessionService.getUser();
		vm.timezone = TIMEZONE;
		vm.dataStr = '';
		vm.dateFormat = 'DD/MM/YYYY';
		vm.salvar = salvar;
		vm.voltar = voltar;
		vm.mask = mask;
		vm.textSearch = '';
		vm.produtoSearch = produtoSearch;
		vm.manterAjusteEstoque = {};

		init();

		function init() {
			validaPermissaoAcesso();
			if ($routeParams.id > 0) {
				editar($routeParams.id);
			} else {
				novo();
			}
		}

		function mask() {
		}

		function validaPermissaoAcesso() {
			if (!PermissaoService.containsPermissions(funcoes.ajusteEstoque)) {
				$location.path('/login');
			}
		}

		function showMessage(_messsage) {
			var snackbarContainer = document.querySelector('#message-screen');
			snackbarContainer.MaterialSnackbar.showSnackbar({
				message : _messsage
			});
		}

		function editar(id) {
			AjusteEstoqueService.editar(id).then(function success(response) {
				vm.manterAjusteEstoque.ajusteEstoque = response.data.ajusteEstoque;
				vm.manterAjusteEstoque.produtos = response.data.produtos;
				vm.dataStr = moment(vm.manterAjusteEstoque.ajusteEstoque.dataAjusteEstoque).format(
						vm.dateFormat);
			}, function error(response) {
				showMessage('Falha ao carregar o registro!');
			});
		}

		function novo() {
			AjusteEstoqueService.novo(vm.usuario.id).then(
					function success(response) {
						vm.manterAjusteEstoque.ajusteEstoque = response.data.ajusteEstoque;
						vm.manterAjusteEstoque.produtos = response.data.produtos;
					}, function error(response) {
						showMessage('Falha ao carregar o registro!');
					});
		}

		function salvar() {
			vm.manterAjusteEstoque.ajusteEstoque.dataAjusteEstoque = moment(vm.dataStr, vm.dateFormat).toDate();
			AjusteEstoqueService.salvar(vm.manterAjusteEstoque.ajusteEstoque).then(
					function success(response) {
						showMessage('Registro salvo com sucesso!');
						$timeout(function() {
							vm.voltar();
						}, 1000);
					}, function error(response) {
						showMessage('Falha ao tentar salvar o registro!');
					});
		}

		function voltar() {
			$location.path('/ajusteestoque');
		}

		/**
		 * Autocomplete: filtra produtos pelo id ou nome.
		 */
		function produtoSearch(textSearch) {
			if (textSearch) {
				var itensFound = [];
				if (!isNaN(textSearch)) {
					// busca pelo id
					itensFound = $filter('filter')(
							vm.manterAjusteEstoque.produtos, {
								'id' : parseInt(textSearch)
							});
				} else {
					// busca pelo nome
					itensFound = $filter('filter')(
							vm.manterAjusteEstoque.produtos, {
								'nome' : textSearch
							});
				}
				return itensFound;
			}
			return [];
		}
	}
})();
