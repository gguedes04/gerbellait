(function() {
	'use strict';

	ListaAjusteEstoqueController.$inject = [ '$location', 'funcoes',
			'AjusteEstoqueService', 'PermissaoService', 'TIMEZONE', '$filter',
			'pagination' ];

	angular.module('appBella.sistema.controllers').controller(
			'ListaAjusteEstoqueController', ListaAjusteEstoqueController);

	function ListaAjusteEstoqueController($location, funcoes,
			AjusteEstoqueService, PermissaoService, TIMEZONE, $filter,
			pagination) {
		var vm = this;
		vm.manterAjusteEstoque = {};
		vm.dataInicialStr = '';
		vm.dataFinalStr = '';
		vm.timezone = TIMEZONE;
		vm.dateFormat = 'DD/MM/YYYY';
		vm.textSearch = '';
		vm.produtoSearch = produtoSearch;
		vm.ajusteEstoqueSelecionado = {};
		vm.seleciona = seleciona;
		vm.novo = novo;
		vm.toEdit = toEdit;
		vm.reset = reset;
		vm.toPage = toPage;
		vm.selectPage = selectPage;
		vm.openDialog = openDialog;
		vm.closeDialog = closeDialog;
		vm.excluir = excluir;
		vm.listar = listar;
		vm.paginas = pagination;
		vm.quantidadePorPagina = quantidadePorPagina;
		vm.page = 1;
		vm.loading = true;

		init();

		function init() {
			validaPermissaoAcesso();
			AjusteEstoqueService
					.init()
					.then(
							function success(response) {
								vm.manterAjusteEstoque.produtos = response.data.produtos;
								vm.manterAjusteEstoque.lista = response.data.lista;
								vm.manterAjusteEstoque.filtro = response.data.ajusteEstoque;
								vm.loading = false;
							},
							function error(response) {
								showMessage('Falha ao carregar a lista de produtos do filtro!');
								vm.loading = false;
							});
		}

		function validaPermissaoAcesso() {
			if (!PermissaoService.containsPermissions(funcoes.ajusteEstoque)) {
				$location.path('/login');
			}
		}

		function showMessage(_messsage) {
			var snackbarContainer = document.querySelector('#message-screen');
			snackbarContainer.MaterialSnackbar.showSnackbar({
				message : _messsage
			});
		}

		function reset() {
			vm.page = 1;
			vm.manterAjusteEstoque.filtro.paginate.paginaAtual = 0;
			vm.manterAjusteEstoque.lista = [];
		}

		function listar() {
			if (vm.dataInicialStr) {
				vm.manterAjusteEstoque.filtro.dataInicial = moment(
						vm.dataInicialStr, vm.dateFormat).toDate();
			}
			if (vm.dataFinalStr) {
				vm.manterAjusteEstoque.filtro.dataFinal = moment(
						vm.dataFinalStr, vm.dateFormat).toDate();
			}
			if (vm.manterAjusteEstoque.filtro.dataInicial > vm.manterAjusteEstoque.filtro.dataFinal) {
				showMessage('Período de datas inválido!');
				delete vm.dataFinalStr;
				delete vm.manterAjusteEstoque.dataFinal;

			} else {
				AjusteEstoqueService
						.listar(vm.manterAjusteEstoque.filtro)
						.then(
								function success(response) {
									vm.manterAjusteEstoque.lista = response.data;
								},
								function error(response) {
									showMessage('Falha ao consultar os produtos para ajuste!');
								});
			}
		}

		/**
		 * Autocomplete: filtra produtos pelo id ou nome.
		 */
		function produtoSearch(textSearch) {
			if (textSearch) {
				var itensFound = [];
				if (!isNaN(textSearch)) {
					// busca pelo id
					itensFound = $filter('filter')(
							vm.manterAjusteEstoque.produtos, {
								'id' : parseInt(textSearch)
							});
				} else {
					// busca pelo nome
					itensFound = $filter('filter')(
							vm.manterAjusteEstoque.produtos, {
								'nome' : textSearch
							});
				}
				return itensFound;
			}
			return [];
		}

		function toPage(page) {
			vm.manterAjusteEstoque.filtro.paginate.paginaAtual = page;
			vm.page = vm.manterAjusteEstoque.filtro.paginate.paginaAtual + 1;
			vm.listar();
		}

		function selectPage() {
			if (vm.page < 1
					|| vm.page > vm.manterAjusteEstoque.filtro.paginate.numTotalPaginas) {
				showMessage('Página inválida!');
				vm.page = 1;
				vm.manterAjusteEstoque.filtro.paginate.paginaAtual = 0;
				vm.toPage(vm.manterAjusteEstoque.filtro.paginate.paginaAtual);
			} else if (vm.page) {
				vm.toPage(vm.page - 1);
			}
		}

		function quantidadePorPagina() {
			vm.page = 1;
			vm.manterAjusteEstoque.filtro.paginate.paginaAtual = 0;
			vm.listar();
		}

		function novo() {
			$location.path('/ajusteestoque/novo');
		}

		function toEdit(ajusteEstoque) {
			if (ajusteEstoque && ajusteEstoque.id) {
				$location.path('/ajusteestoque/' + ajusteEstoque.id);
			}
		}

		function seleciona(ajusteEstoque) {
			vm.ajusteEstoqueSelecionado = ajusteEstoque;
			vm.openDialog();
		}

		function openDialog() {
			document.querySelector('#deleteDialog').showModal();
		}

		function closeDialog() {
			document.querySelector('#deleteDialog').close();
		}

		function excluir() {
			if (vm.ajusteEstoqueSelecionado) {
				AjusteEstoqueService.excluir(vm.ajusteEstoqueSelecionado).then(
						function success(response) {
							closeDialog();
							showMessage('Registro excluído com sucesso!');
							vm.listar();
							delete vm.ajusteEstoqueSelecionado;
						}, function error(response) {
							closeDialog();
							showMessage('Falha ao tentar excluir o registro!');
							delete vm.ajusteEstoqueSelecionado;
						});
			}
		}

	}
})();
