(function() {
	'use strict';

	ClientesController.$inject = [ '$location', 'ClienteService',
			'DownloadService', 'PermissaoService', 'funcoes', 'pagination',
			'SessionService' ];

	angular.module('appBella.sistema.controllers').controller(
			'ClientesController', ClientesController);

	function ClientesController($location, ClienteService, DownloadService,
			PermissaoService, funcoes, pagination, SessionService) {

		var vm = this;
		vm.usuario = SessionService.getUser();
		vm.listar = listar;
		vm.exportar = exportar;
		vm.seleciona = seleciona;
		vm.novo = novo;
		vm.toEdit = toEdit;
		vm.mask = mask;
		vm.reset = reset;
		vm.toPage = toPage;
		vm.selectPage = selectPage;
		vm.openDialog = openDialog;
		vm.closeDialog = closeDialog;
		vm.excluir = excluir;
		vm.cidades = cidades;
		vm.quantidadePorPagina = quantidadePorPagina;
		vm.manterCliente = {};
		vm.manterCliente.lista = [];
		vm.manterCliente.filtro = {};
		vm.clienteSelecionado = {};
		vm.paginas = pagination;
		vm.page = 1;
		vm.loading = true;
		init();

		function init() {
			validaPermissaoAcesso();
			ClienteService
					.init(vm.usuario.id)
					.then(
							function success(response) {
								vm.manterCliente.filtro = response.data.filtro;
								vm.manterCliente.formasPagamento = response.data.formasPagamento;
								vm.manterCliente.vendedores = response.data.vendedores;
								vm.manterCliente.ufs = response.data.ufs;
								vm.manterCliente.lista = response.data.lista;
								vm.manterCliente.isVendedor = response.data.isVendedor;
								vm.loading = false;
							}, function error(response) {
								showMessage('Falha ao carregar os clientes!');
								vm.loading = false;
							});
		}

		function mask() {
			$("#cnpj").mask("99.999.999/9999-99");
		}

		function reset() {
			vm.page = 1;
			vm.manterCliente.filtro.paginate.paginaAtual = 0;
			vm.manterCliente.lista = [];
		}

		function showMessage(_messsage) {
			var snackbarContainer = document.querySelector('#message-screen');
			snackbarContainer.MaterialSnackbar.showSnackbar({
				message : _messsage
			});
		}

		function validaPermissaoAcesso() {
			if (!PermissaoService.containsPermissions(funcoes.cliente)) {
				$location.path('/login');
			}
		}

		function toPage(page) {
			vm.manterCliente.filtro.paginate.paginaAtual = page;
			vm.page = vm.manterCliente.filtro.paginate.paginaAtual + 1;
			vm.listar();
		}

		function selectPage() {
			if (vm.page < 1
					|| vm.page > vm.manterCliente.filtro.paginate.numTotalPaginas) {
				showMessage('Página inválida!');
				vm.page = 1;
				vm.manterCliente.filtro.paginate.paginaAtual = 0;
				vm.toPage(vm.manterCliente.filtro.paginate.paginaAtual);
			} else if (vm.page) {
				vm.toPage(vm.page - 1);
			}
		}

		function quantidadePorPagina() {
			vm.page = 1;
			vm.manterCliente.filtro.paginate.paginaAtual = 0;
			vm.listar();
		}

		function listar() {
			vm.loading = true;
			ClienteService
					.listar(vm.manterCliente.filtro)
					.then(
							function success(response) {
								vm.manterCliente.lista = response.data.lista;
								vm.manterCliente.filtro.paginate = response.data.filtro.paginate;
								vm.loading = false;
							}, function error(response) {
								showMessage('Falha ao carregar os clientes!');
								vm.loading = false;
							});
		}

		function exportar() {
			vm.loading = true;
			ClienteService.exportar(vm.manterCliente.filtro).then(
					function success(response) {
						DownloadService.download(response.data, 'clientes.xls',
								'text/xls');
						vm.loading = false;
					}, function error(response) {
						showMessage('Falha ao exportar os clientes!');
						vm.loading = false;
					});
		}

		function cidades() {
			if (vm.manterCliente.filtro.enderecoComercial.cidade.uf.id) {
				vm.reset();
				ClienteService.cidades(
						vm.manterCliente.filtro.enderecoComercial.cidade.uf.id)
						.then(function success(response) {
							vm.manterCliente.cidades = response.data;
						}, function error(response) {
							showMessage('Falha ao carregar as cidades!');
						});
			} else {
				vm.manterCliente.cidades = [];
			}
		}

		function novo() {
			$location.path('/cliente/novo');
		}

		function toEdit(cliente) {
			if (cliente && cliente.id) {
				$location.path('/cliente/' + cliente.id);
			}
		}

		function seleciona(cliente) {
			vm.clienteSelecionado = cliente;
			vm.openDialog();
		}

		function openDialog() {
			document.querySelector('#deleteDialog').showModal();
		}

		function closeDialog() {
			document.querySelector('#deleteDialog').close();
		}

		function excluir() {
			if (vm.clienteSelecionado) {
				ClienteService.excluir(vm.clienteSelecionado).then(
						function success(response) {
							closeDialog();
							showMessage('Cliente excluído com sucesso!');
							vm.listar();
							delete vm.clienteSelecionado;
						}, function error(response) {
							closeDialog();
							showMessage('Falha ao tentar excluir o cliente!');
							delete vm.clienteSelecionado;
						});
			}
		}
	}
})();
