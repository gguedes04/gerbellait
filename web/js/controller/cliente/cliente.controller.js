(function() {
	'use strict';

	ClienteController.$inject = [ '$location', 'ClienteService',
			'$routeParams', '$timeout', 'funcoes', 'PermissaoService', 'SessionService', 'perfis' ];

	angular.module('appBella.sistema.controllers').controller(
			'ClienteController', ClienteController);

	function ClienteController($location, ClienteService, $routeParams,
			$timeout, funcoes, PermissaoService, SessionService, perfis) {
		
		var vm = this;
		vm.usuario = SessionService.getUser();
		vm.salvar = salvar;
		vm.voltar = voltar;
		vm.rotasByVendedor = rotasByVendedor;
		vm.toEditEndereco = toEditEndereco;
		vm.excluir = excluir;
		vm.mask = mask;
		vm.openDialog = openDialog;
		vm.closeDialog = closeDialog;
		vm.salvarEndereco = salvarEndereco;
		vm.novoEndereco = novoEndereco;
		vm.carregaCidades = carregaCidades;
		vm.manterCliente = {};
		vm.situacoes = [];
		vm.vendedores = [];
		vm.rotas = [];
		vm.formasPagamento = [];
		vm.enderecosRemovidos = [];
		vm.enderecoSelecionado = {};
		vm.tiposEndereco = [];
		vm.ufs = [];
		vm.cidades = [];
		vm.regimesTributarios = [];
		vm.formaPagamentoChange = formaPagamentoChange;
		vm.regimeTributarioChange = regimeTributarioChange;
		vm.inscricaoEstadualIsento = false;

		init();

		function init() {
			validaPermissaoAcesso();
			if ($routeParams.id > 0) {
				editar($routeParams.id);
			} else {
				novo();
			}
		}

		function mask() {
			$("#cnpj").mask("99.999.999/9999-99");
			$("#cep").mask("99.999-999");
			$("#telefone").mask("(00) 0000-00009");
		}

		function validaPermissaoAcesso() {
			if (!PermissaoService.containsPermissions(funcoes.cliente)) {
				$location.path('/login');
			}
		}

		function carregaCidades() {
			if (vm.enderecoSelecionado && vm.enderecoSelecionado.cidade
					&& vm.enderecoSelecionado.cidade.uf.id) {
				carregaCidadesByUf(vm.enderecoSelecionado.cidade.uf.id);
			} else {
				vm.cidades = [];
			}
		}

		function carregaCidadesByUf(idUf) {
			ClienteService.cidades(idUf).then(function success(response) {
				vm.cidades = response.data;
			}, function error(response) {
				showMessage('Falha ao carregar as cidades!');
			});
		}

		function openDialog() {
			document.querySelector('#incluirDialog').showModal();
		}

		function closeDialog() {
			vm.enderecoSelecionado = {};
			document.querySelector('#incluirDialog').close();
		}

		function getIndexEnderecoById(id) {
			var retorno = null;
			if (vm.manterCliente.enderecos
					&& vm.manterCliente.enderecos.length > 0) {
				for (var i = 0; i < vm.manterCliente.enderecos.length; i++) {
					if (vm.manterCliente.enderecos[i].id === id) {
						retorno = i;
						break;
					}
				}
			}
			return retorno;
		}

		function getIndexTipoEnderecoById(id) {
			var retorno = null;
			if (id) {
				for (var t = 0; t < vm.tiposEndereco.length; t++) {
					if (vm.tiposEndereco[t].id === id) {
						retorno = t;
						break;
					}
				}
			}
			return retorno;
		}

		function getIndexUfById(id) {
			var retorno = null;
			if (id) {
				for (var u = 0; u < vm.ufs.length; u++) {
					if (vm.ufs[u].id === id) {
						retorno = u;
						break;
					}
				}
			}
			return retorno;
		}

		function getIndexCidadeById(id) {
			var retorno = null;
			if (id) {
				for (var c = 0; c < vm.cidades.length; c++) {
					if (vm.cidades[c].id === id) {
						retorno = c;
						break;
					}
				}
			}
			return retorno;
		}

		function salvarEndereco() {
			if (!vm.manterCliente.enderecos) {
				vm.manterCliente.enderecos = [];
			}
			if (vm.enderecoSelecionado.id) {
				carregaDadosEndereco();
				vm.enderecoSelecionado.acao = 'alterar';
				var index = getIndexEnderecoById(vm.enderecoSelecionado.id);
				vm.manterCliente.enderecos[index] = vm.enderecoSelecionado;
			} else {
				carregaDadosEndereco();
				vm.enderecoSelecionado.acao = 'novo';
				vm.manterCliente.enderecos.push(angular
						.copy(vm.enderecoSelecionado));
			}
			vm.enderecoSelecionado = {};
			closeDialog();
		}

		function carregaDadosEndereco() {

			// tipo de endereço
			var indexTipoEndereco = getIndexTipoEnderecoById(vm.enderecoSelecionado.tipoEndereco.id);
			vm.enderecoSelecionado.tipoEndereco = indexTipoEndereco != null ? angular
					.copy(vm.tiposEndereco[indexTipoEndereco])
					: {};

			// cidade
			var indexCidade = getIndexCidadeById(vm.enderecoSelecionado.cidade.id);
			vm.enderecoSelecionado.cidade = indexCidade != null ? vm.cidades[indexCidade]
					: {};

			// uf
			if (vm.enderecoSelecionado.cidade
					&& vm.enderecoSelecionado.cidade.uf) {
				var indexUf = getIndexUfById(vm.enderecoSelecionado.cidade.uf.id);
				vm.enderecoSelecionado.cidade.uf = indexUf != null ? vm.ufs[indexUf]
						: {};
			}
		}

		function showMessage(_messsage) {
			var snackbarContainer = document.querySelector('#message-screen');
			snackbarContainer.MaterialSnackbar.showSnackbar({
				message : _messsage
			});
		}

		function excluir(index) {
			if (vm.manterCliente.enderecos[index].id) {
				var enderecoRemover = angular
						.copy(vm.manterCliente.enderecos[index]);
				enderecoRemover.acao = 'excluir';
				vm.enderecosRemovidos.push(enderecoRemover);
			}
			vm.manterCliente.enderecos.splice(index, 1);
			showMessage('Endereço removido da lista');
		}

		function novoEndereco() {
			vm.enderecoSelecionado = {};
			vm.openDialog();
		}

		function toEditEndereco(endereco) {
			vm.enderecoSelecionado = endereco;
			carregaCidadesByUf(vm.enderecoSelecionado.cidade.uf.id);
			vm.openDialog();
		}

		function getFormasPagamento(response) {
			if (vm.usuario.grupo === perfis.ADMINISTRADOR
					|| vm.usuario.grupo === perfis.CADASTRO) {
				// todas as formas de pagamento
				return response.data.formasPagamento;
				
			} else if (vm.usuario.grupo === perfis.VENDEDOR) {
				// permitir pagamento à vista
				var aVista = response.data.formasPagamento.filter(_formapagamento => _formapagamento.descricao === 'A VISTA');
				vm.manterCliente.formaPagamento = aVista[0];
				return aVista;
			}
		}

		function editar(id) {
			ClienteService.editar(id).then(function success(response) {
				vm.manterCliente = response.data.pessoa;
				vm.situacoes = response.data.situacoes;
				vm.vendedores = response.data.vendedores;
				vm.rotas = response.data.rotas;
				vm.formasPagamento = getFormasPagamento(response);
				vm.tiposEndereco = response.data.tiposEndereco;
				vm.ufs = response.data.ufs;
				vm.regimesTributarios = response.data.regimesTributarios;
				regimeTributarioChange();
			}, function error(response) {
				showMessage('Falha ao carregar o cliente!');
			});
		}

		function novo() {
			ClienteService.novo(vm.usuario.id).then(
					function success(response) {
						vm.manterCliente = response.data.pessoa;
						vm.situacoes = response.data.situacoes;
						vm.vendedores = response.data.vendedores;
						vm.rotas = response.data.rotas;
						vm.formasPagamento = getFormasPagamento(response);
						vm.tiposEndereco = response.data.tiposEndereco;
						vm.ufs = response.data.ufs;
						vm.regimesTributarios = response.data.regimesTributarios;
					}, function error(response) {
						showMessage('Falha ao carregar o cliente!');
					});
		}

		function salvar() {
			if (vm.manterCliente.enderecos
					&& vm.manterCliente.enderecos.length > 0) {
				var enderecosFull = vm.manterCliente.enderecos
						.concat(vm.enderecosRemovidos);
				vm.manterCliente.enderecos = enderecosFull;
				ClienteService.salvar(vm.manterCliente).then(
						function success(response) {
							showMessage('Cliente salvo com sucesso!');
							$timeout(function() {
								vm.voltar();
							}, 1000);
						}, function error(response) {
							showMessage('Falha ao tentar salvar o cliente!');
						});
			} else {
				showMessage('Ao menos um endereço deve ser incluído.');
			}
		}

		function rotasByVendedor() {
			if (vm.manterCliente.idVendedorRepresentante) {
				ClienteService
						.rotasByVendedor(
								vm.manterCliente.idVendedorRepresentante)
						.then(
								function success(response) {
									vm.rotas = response.data;
								},
								function error(response) {
									showMessage('Falha ao carregar as rotas do vendedor selecionado!');
								});
			} else {
				vm.rotas = [];
			}
		}

		function voltar() {
			$location.path('/clientes');
		}

		function formaPagamentoChange() {
			if (vm.manterCliente.formaPagamento && vm.manterCliente.formaPagamento.id) {
				var formaPagamentoSelecionada = null;
				for (var i = 0; i < vm.formasPagamento.length; i++) {
					if (vm.formasPagamento[i].id === vm.manterCliente.formaPagamento.id) {
						formaPagamentoSelecionada = vm.formasPagamento[i];
						break;
					}
				}
				vm.manterCliente.prazo = formaPagamentoSelecionada.prazo;
			}
		}

		function regimeTributarioChange() {
			if (vm.manterCliente.idRegimeTributario === 'ME') {
				vm.inscricaoEstadualIsento = true;
				vm.manterCliente.inscricaoEstadual = null;
			} else {
				vm.inscricaoEstadualIsento = false;
			}
		}
	}
})();
