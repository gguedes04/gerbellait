(function() {
	'use strict';

	LimiteVendasController.$inject = [ '$location','funcoes', 'LimiteVendasService','PermissaoService', '$scope', 'SessionService','TIMEZONE', 'pagination', 'DownloadService'];


	angular.module('appBella.sistema.controllers').controller(
			'LimiteVendasController', LimiteVendasController);

	function LimiteVendasController($location,funcoes, LimiteVendasService,PermissaoService, $scope, SessionService,TIMEZONE, pagination, DownloadService) {
		var vm = this;
		vm.listar = listar;
		vm.page = 1;
		vm.loading = true;
		vm.toPage = toPage;
		vm.selectPage = selectPage;
		vm.quantidadePorPagina = quantidadePorPagina;
		vm.addItensAlterados = addItensAlterados;
		vm.salvar = salvar;
		vm.manterLimiteVenda = {};
		vm.manterLimiteVenda.filtro = {};
        vm.dataInicialStr = '';
        vm.dataFinalStr = '';
		vm.itensAlterados = [];
		vm.timezone = TIMEZONE;
		vm.dateFormat = 'DD/MM/YYYY';
		vm.paginas = pagination;
		init();

		function init() {
			validaPermissaoAcesso();
			vm.listar();
		}

		function listar() {

			if (vm.dataInicialStr) {
				vm.manterLimiteVenda.filtro.dataInicial = moment(vm.dataInicialStr, vm.dateFormat).toDate();
			}
			if (vm.dataFinalStr) {
				vm.manterLimiteVenda.filtro.dataFinal = moment(vm.dataFinalStr,vm.dateFormat).toDate();
			}

			if (vm.manterLimiteVenda.filtro.dataInicial > vm.manterLimiteVenda.filtro.dataFinal) {
				showMessage('Período de datas inválido!');
				delete vm.dataFinalStr;
				delete vm.manterLimiteVenda.dataFinal;

			} else {

				vm.loading = true;
				LimiteVendasService.init(vm.manterLimiteVenda.filtro).then(
					function success(response) {
						vm.manterLimiteVenda = response.data;
						vm.manterLimiteVenda.filtro.produto.paginate = response.data.filtro.produto.paginate;
						if (vm.manterLimiteVenda.dataInicialStr) {
							vm.dataInicialStr = vm.manterLimiteVenda.dataInicialStr;
						}
						if (vm.manterLimiteVenda.dataFinalStr) {
							vm.dataFinalStr = vm.manterLimiteVenda.dataFinalStr;
						}
						vm.loading = false;
					}, function error(response) {
						showMessage('Falha ao carregar os produtos e seus limites de venda!');
						vm.loading = false;
					});
			}
		}

		function validaPermissaoAcesso() {
			if (!PermissaoService.containsPermissions(funcoes.limiteVenda)) {
				$location.path('/login');
			}
		}

		function showMessage(_messsage) {
			var snackbarContainer = document.querySelector('#message-screen');
			snackbarContainer.MaterialSnackbar.showSnackbar({
				message : _messsage
			});
		}

		function addItensAlterados(plimite) {
			var inserido = false;
			for (var i = 0; i < vm.itensAlterados.length; i++) {
				if (vm.itensAlterados[i].produto.id === plimite.produto.id
						&& vm.itensAlterados[i].vendedor.id === plimite.vendedor.id) {
					vm.itensAlterados[i] = plimite;
					inserido = true;
					break;
				}
			}
			if (!inserido) {
				vm.itensAlterados.push(plimite);
			}
		}

		function salvar() {
			vm.loading = true;
			LimiteVendasService.saveList(vm.itensAlterados).then(
					function success(response) {
						vm.loading = false;
						$scope.limiteVendasForm.$setPristine();
						$scope.limiteVendasForm.$setUntouched();
						showMessage('Alterações salvas com sucesso!');
					}, function error(response) {
						showMessage('Falha ao salvar as alterações!');
						vm.loading = false;
					});
		}

		function toPage(page) {
			vm.manterLimiteVenda.filtro.produto.paginate.paginaAtual = page;
			vm.page = vm.manterLimiteVenda.filtro.produto.paginate.paginaAtual + 1;
			vm.listar();
		}

		function selectPage() {
			if (vm.page < 1
					|| vm.page > vm.manterLimiteVenda.filtro.produto.paginate.numTotalPaginas) {
				showMessage('Página inválida!');
				vm.page = 1;
				vm.manterLimiteVenda.filtro.produto.paginate.paginaAtual = 0;
				vm.toPage(vm.manterLimiteVenda.filtro.produto.paginate.paginaAtual);
			} else if (vm.page) {
				vm.toPage(vm.page - 1);
			}
		}

		function quantidadePorPagina() {
			vm.page = 1;
			vm.manterLimiteVenda.filtro.produto.paginate.paginaAtual = 0;
			vm.listar();
		}
	}
})();
