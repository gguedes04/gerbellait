(function() {
	'use strict';

	RecadosVendedoresController.$inject = [ '$location',
			'RecadoVendedorService', 'DownloadService', 'PermissaoService',
			'funcoes', 'pagination', 'TIMEZONE' ];

	angular.module('appBella.sistema.controllers').controller(
			'RecadosVendedoresController', RecadosVendedoresController);

	function RecadosVendedoresController($location, RecadoVendedorService,
			DownloadService, PermissaoService, funcoes, pagination, TIMEZONE) {

		var vm = this;
		vm.timezone = TIMEZONE;
		vm.dateFormat = 'DD/MM/YYYY';
		vm.recado = recado;
		vm.listar = listar;
		vm.seleciona = seleciona;
		vm.novo = novo;
		vm.toEdit = toEdit;
		vm.reset = reset;
		vm.toPage = toPage;
		vm.selectPage = selectPage;
		vm.openDialog = openDialog;
		vm.closeDialog = closeDialog;
		vm.excluir = excluir;
		vm.quantidadePorPagina = quantidadePorPagina;
		vm.manterRecado = {};
		vm.manterRecado.lista = [];
		vm.manterRecado.filtro = {};
		vm.recadoSelecionado = {};
		vm.paginas = pagination;
		vm.page = 1;
		vm.loading = true;
		vm.dataStr = null;
		vm.limparFiltro = limparFiltro;
		init();

		function init() {
			validaPermissaoAcesso();
			RecadoVendedorService.init().then(function success(response) {
				vm.manterRecado.filtro = response.data.filtro;
				vm.manterRecado.lista = response.data.lista;
				vm.loading = false;
			}, function error(response) {
				showMessage('Falha ao carregar os recados aos vendedores!');
				vm.loading = false;
			});
		}

		function validaPermissaoAcesso() {
			if (!PermissaoService.containsPermissions(funcoes.recadoVendedor)) {
				$location.path('/login');
			}
		}

		function limparFiltro() {
			delete vm.manterRecado.filtro.responsavel;
			delete vm.manterRecado.filtro.destinatario;
			delete vm.manterRecado.filtro.data;
			delete vm.dataStr;
		}

		function reset() {
			vm.page = 1;
			vm.manterRecado.filtro.paginate.paginaAtual = 0;
			vm.manterRecado.lista = [];
		}

		function showMessage(_messsage) {
			var snackbarContainer = document.querySelector('#message-screen');
			snackbarContainer.MaterialSnackbar.showSnackbar({
				message : _messsage
			});
		}

		function limpaData() {
			delete vm.dataStr;
			delete vm.manterRecado.filtro.data;
		}

		function recado(msg) {
			if (msg && msg.length > 70) {
				return msg.substr(0, 70) + '[...]';
			}
			return msg;
		}

		function toPage(page) {
			vm.manterRecado.filtro.paginate.paginaAtual = page;
			vm.page = vm.manterRecado.filtro.paginate.paginaAtual + 1;
			vm.listar();
		}

		function selectPage() {
			if (vm.page < 1
					|| vm.page > vm.manterRecado.filtro.paginate.numTotalPaginas) {
				showMessage('Página inválida!');
				vm.page = 1;
				vm.manterRecado.filtro.paginate.paginaAtual = 0;
				vm.toPage(vm.manterRecado.filtro.paginate.paginaAtual);
			} else if (vm.page) {
				vm.toPage(vm.page - 1);
			}
		}

		function quantidadePorPagina() {
			vm.page = 1;
			vm.manterRecado.filtro.paginate.paginaAtual = 0;
			vm.listar();
		}

		function listar() {
			vm.loading = true;
			if (vm.dataStr) {
				vm.manterRecado.filtro.data = moment(vm.dataStr, vm.dateFormat)
						.toDate();
			}
			RecadoVendedorService
					.listar(vm.manterRecado.filtro)
					.then(
							function success(response) {
								vm.manterRecado.lista = response.data.lista;
								vm.manterRecado.filtro.paginate = response.data.filtro.paginate;
								vm.loading = false;
							},
							function error(response) {
								showMessage('Falha ao carregar os recados aos vededores!');
								vm.loading = false;
							});
		}

		function novo() {
			$location.path('/recadovendedor/novo');
		}

		function toEdit(recado) {
			if (recado && recado.id) {
				$location.path('/recadovendedor/' + recado.id);
			}
		}

		function seleciona(recado) {
			vm.recadoSelecionado = recado;
			vm.openDialog();
		}

		function openDialog() {
			document.querySelector('#deleteDialog').showModal();
		}

		function closeDialog() {
			document.querySelector('#deleteDialog').close();
		}

		function excluir() {
			if (vm.recadoSelecionado) {
				RecadoVendedorService.excluir(vm.recadoSelecionado).then(
						function success(response) {
							closeDialog();
							showMessage('Recado excluído com sucesso!');
							vm.listar();
							delete vm.recadoSelecionado;
						}, function error(response) {
							closeDialog();
							showMessage('Falha ao tentar excluir o recado!');
							delete vm.recadoSelecionado;
						});
			}
		}
	}
})();
