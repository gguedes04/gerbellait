(function() {
	RecadoVendedorController.$inject = [ '$location', 'RecadoVendedorService',
			'$routeParams', '$timeout', 'funcoes', 'PermissaoService',
			'$filter', 'TIMEZONE', 'SessionService' ];

	angular.module('appBella.sistema.controllers').controller(
			'RecadoVendedorController', RecadoVendedorController);

	function RecadoVendedorController($location, RecadoVendedorService,
			$routeParams, $timeout, funcoes, PermissaoService, $filter,
			TIMEZONE, SessionService) {

		var vm = this;
		vm.salvar = salvar;
		vm.voltar = voltar;
		vm.manterRecado = {};
		vm.loginVendedores = [];
		vm.timezone = TIMEZONE;
		vm.dataStr = '';
		vm.dateFormat = 'DD/MM/YYYY';
		vm.usuario = SessionService.getUser();

		init();

		function init() {
			validaPermissaoAcesso();
			if ($routeParams.id > 0) {
				editar($routeParams.id);
			} else {
				novo();
			}
		}

		function validaPermissaoAcesso() {
			if (!PermissaoService.containsPermissions(funcoes.recadoVendedor)) {
				$location.path('/login');
			}
		}

		function showMessage(_messsage) {
			var snackbarContainer = document.querySelector('#message-screen');
			snackbarContainer.MaterialSnackbar.showSnackbar({
				message : _messsage
			});
		}

		function editar(id) {
			RecadoVendedorService.editar(id).then(
					function success(response) {
						vm.manterRecado = response.data.msn;
						vm.dataStr = moment(vm.manterRecado.data).format(
								vm.dateFormat);
						vm.loginVendedores = response.data.loginVendedores;
					}, function error(response) {
						showMessage('Falha ao carregar o recado!');
					});
		}

		function novo() {
			RecadoVendedorService.novo($routeParams.id).then(
					function success(response) {
						vm.manterRecado = response.data.msn;
						vm.dataStr = moment(vm.manterRecado.data).format(
								vm.dateFormat);
						vm.loginVendedores = response.data.loginVendedores;
					}, function error(response) {
						showMessage('Falha ao carregar o recado!');
					});
		}

		function salvar() {
			if (!vm.manterRecado.destinatario) {
				vm.manterRecado.destinatario = 'Todos';
			}
			vm.manterRecado.responsavel = vm.usuario.login;
			vm.manterRecado.data = moment(vm.dataStr, vm.dateFormat).toDate();
			RecadoVendedorService.salvar(vm.manterRecado).then(
					function success(response) {
						showMessage('Recado salvo com sucesso!');
						$timeout(function() {
							vm.voltar();
						}, 1000);
					}, function error(response) {
						showMessage('Falha ao tentar salvar o recado!');
					});
		}

		function voltar() {
			$location.path('/recadovendedor');
		}
	}
})();
