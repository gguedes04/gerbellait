(function() {
	ProdutoController.$inject = [ '$location', 'ProdutoService',
			'$routeParams', '$timeout', 'funcoes', 'PermissaoService', '$filter' ];

	angular.module('appBella.sistema.controllers').controller(
			'ProdutoController', ProdutoController);

	function ProdutoController($location, ProdutoService, $routeParams,
			$timeout, funcoes, PermissaoService, $filter) {

		var vm = this;
		vm.salvar = salvar;
		vm.voltar = voltar;
		vm.mask = mask;
		vm.openDialogImposto = openDialogImposto;
		vm.closeDialogImposto = closeDialogImposto;
		vm.manterProduto = {};
		vm.situacoes = [];
		vm.grupos = [];
		vm.unidades = [];
		vm.tiposTroca = [];
		vm.ufs = [];
		vm.rotas = [];
		vm.novoImposto = novoImposto;
		vm.toEditImposto = toEditImposto;
		vm.impostoSelecionado = {};
		vm.rotaSelecionada = {};
		vm.salvaImposto = salvaImposto;
		vm.excluirImposto = excluirImposto;
		vm.salvaRota = salvaRota;
		vm.excluirRota = excluirRota;
		vm.impostosRemovidos = [];
		vm.rotasRemovidas = [];
		vm.fornecedores = [];
		vm.fornecedorTextSearch = '';
		vm.fornecedorSearch = fornecedorSearch;

		init();

		function init() {
			validaPermissaoAcesso();
			if ($routeParams.id > 0) {
				editar($routeParams.id);
			} else {
				novo();
			}
		}

		function mask() {
		}

		/**
		 * Autocomplete: filtra fornecedores pelo id ou nome.
		 */
		function fornecedorSearch(textSearch) {
			if (textSearch) {
				var itensFound = [];
				if (!isNaN(textSearch)) {
					// busca pelo id
					itensFound = $filter('filter')(vm.fornecedores, {
						'id' : parseInt(textSearch)
					});
				} else {
					// busca pelo nome
					itensFound = $filter('filter')(vm.fornecedores, {
						'nome' : textSearch
					});
				}
				return itensFound;
			}
			return [];
		}

		function novoImposto() {
			vm.impostoSelecionado = {};
			vm.openDialogImposto();
		}

		function toEditImposto(imposto) {
			vm.impostoSelecionado = imposto;
			vm.openDialogImposto();
		}

		function getIndexById(id) {
			var retorno = null;
			if (vm.manterProduto.impostos
					&& vm.manterProduto.impostos.length > 0) {
				for (var i = 0; i < vm.manterProduto.impostos.length; i++) {
					if (vm.manterProduto.impostos[i].id === id) {
						retorno = i;
						break;
					}
				}
			}
			return retorno;
		}

		function permiteAdicionarImposto() {
			var retorno = true;
			for (var i = 0; i < vm.manterProduto.impostos.length; i++) {
				if (vm.impostoSelecionado.uf === vm.manterProduto.impostos[i].uf) {
					retorno = false;
					break;
				}
			}
			return retorno;
		}

		function salvaImposto() {
			if (!vm.manterProduto.impostos) {
				vm.manterProduto.impostos = [];
			}
			if (permiteAdicionarImposto()) {
				if (vm.impostoSelecionado.id) {
					vm.impostoSelecionado.acao = 'alterar';
					var index = getIndexById(vm.impostoSelecionado.id);
					vm.manterProduto.impostos[index] = vm.impostoSelecionado;
				} else {
					vm.impostoSelecionado.acao = 'novo';
					vm.manterProduto.impostos.push(angular
							.copy(vm.impostoSelecionado));
				}
			} else {
				showMessage('Imposto já adicionado para está UF');
			}
			vm.impostoSelecionado = {};
			closeDialogImposto();
		}

		function excluirImposto(index) {
			if (vm.manterProduto.impostos[index].id) {
				var impostoRemover = angular
						.copy(vm.manterProduto.impostos[index]);
				impostoRemover.acao = 'excluir';
				vm.impostosRemovidos.push(impostoRemover);
			}
			vm.manterProduto.impostos.splice(index, 1);
			showMessage('Imposto removido da lista');
		}

		function permiteAdicionarRota() {
			var retorno = true;
			for (var i = 0; i < vm.manterProduto.rotas.length; i++) {
				if (vm.rotaSelecionada.id === vm.manterProduto.rotas[i].rota.id) {
					retorno = false;
					break;
				}
			}
			return retorno;
		}

		function salvaRota() {
			if (!vm.manterProduto.rotas) {
				vm.manterProduto.rotas = [];
			}
			if (permiteAdicionarRota()) {
				var pRotaSelecionada = {};
				pRotaSelecionada.produto = {};
				pRotaSelecionada.produto.id = vm.manterProduto.id;
				pRotaSelecionada.rota = angular.copy(vm.rotaSelecionada);
				pRotaSelecionada.acao = 'novo';
				vm.manterProduto.rotas.push(angular.copy(pRotaSelecionada));
			} else {
				showMessage('Rota já adicionada');
			}
			vm.rotaSelecionada = {};
		}

		function excluirRota(index) {
			if (vm.manterProduto.rotas[index].id) {
				var rotaRemover = angular.copy(vm.manterProduto.rotas[index]);
				rotaRemover.acao = 'excluir';
				vm.rotasRemovidas.push(rotaRemover);
			}
			vm.manterProduto.rotas.splice(index, 1);
			showMessage('Rota removida da lista');
		}

		function validaPermissaoAcesso() {
			if (!PermissaoService.containsPermissions(funcoes.produto)) {
				$location.path('/login');
			}
		}

		function showMessage(_messsage) {
			var snackbarContainer = document.querySelector('#message-screen');
			snackbarContainer.MaterialSnackbar.showSnackbar({
				message : _messsage
			});
		}

		function openDialogImposto() {
			document.querySelector('#impostoDialog').showModal();
		}

		function closeDialogImposto() {
			document.querySelector('#impostoDialog').close();
		}

		function editar(id) {
			ProdutoService.editar(id).then(function success(response) {
				vm.manterProduto = response.data.produto;
				vm.situacoes = response.data.situacoes;
				vm.grupos = response.data.grupos;
				vm.unidades = response.data.unidades;
				vm.tiposTroca = response.data.tiposTroca;
				vm.ufs = response.data.ufs;
				vm.rotas = response.data.rotas;
				vm.fornecedores = response.data.fornecedores;
			}, function error(response) {
				showMessage('Falha ao carregar o produto!');
			});
		}

		function novo() {
			ProdutoService.novo($routeParams.id).then(
					function success(response) {
						vm.manterProduto = response.data.produto;
						vm.situacoes = response.data.situacoes;
						vm.grupos = response.data.grupos;
						vm.unidades = response.data.unidades;
						vm.tiposTroca = response.data.tiposTroca;
						vm.ufs = response.data.ufs;
						vm.rotas = response.data.rotas;
						vm.fornecedores = response.data.fornecedores;
					}, function error(response) {
						showMessage('Falha ao carregar o produto!');
					});
		}

		function salvar() {
			if (vm.manterProduto.impostos && vm.manterProduto.rotas
					&& vm.manterProduto.impostos.length > 0
					&& vm.manterProduto.rotas.length > 0) {

				// add impostos com id para remover da base
				var impostosFull = vm.manterProduto.impostos
						.concat(vm.impostosRemovidos);
				vm.manterProduto.impostos = impostosFull;

				// add rotas com id para remover da base
				var rotasFull = vm.manterProduto.rotas
						.concat(vm.rotasRemovidas);
				vm.manterProduto.rotas = rotasFull;

				ProdutoService.salvar(vm.manterProduto).then(
						function success(response) {
							showMessage('Produto salvo com sucesso!');
							$timeout(function() {
								vm.voltar();
							}, 1000);
						}, function error(response) {
							showMessage('Falha ao tentar salvar o produto!');
						});
			} else {
				if (!(vm.manterProduto.impostos && vm.manterProduto.impostos.length > 0)) {
					showMessage('Ao menos um imposto deve ser incluído.');
				}
				if (!(vm.manterProduto.rotas && vm.manterProduto.rotas.length > 0)) {
					showMessage('Ao menos uma rota deve ser incluída.');
				}
			}
		}

		function voltar() {
			$location.path('/produtos');
		}
	}
})();
