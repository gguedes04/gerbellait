(function() {
	'use strict';

	ProdutosController.$inject = [ '$location', 'ProdutoService',
			'DownloadService', 'PermissaoService', 'funcoes', 'pagination' ];

	angular.module('appBella.sistema.controllers').controller(
			'ProdutosController', ProdutosController);

	function ProdutosController($location, ProdutoService, DownloadService,
			PermissaoService, funcoes, pagination) {

		var vm = this;
		vm.listar = listar;
		vm.exportar = exportar;
		vm.seleciona = seleciona;
		vm.novo = novo;
		vm.toEdit = toEdit;
		vm.reset = reset;
		vm.toPage = toPage;
		vm.selectPage = selectPage;
		vm.openDialog = openDialog;
		vm.closeDialog = closeDialog;
		vm.excluir = excluir;
		vm.quantidadePorPagina = quantidadePorPagina;
		vm.manterProduto = {};
		vm.manterProduto.lista = [];
		vm.manterProduto.filtro = {};
		vm.produtoSelecionado = {};
		vm.paginas = pagination;
		vm.page = 1;
		vm.loading = true;
		init();

		function init() {
			validaPermissaoAcesso();
			ProdutoService.init().then(function success(response) {
				vm.manterProduto.filtro = response.data.filtro;
				vm.manterProduto.unidades = response.data.unidades;
				vm.manterProduto.grupos = response.data.grupos;
				vm.manterProduto.lista = response.data.lista;
				vm.loading = false;
			}, function error(response) {
				showMessage('Falha ao carregar os produtos!');
				vm.loading = false;
			});
		}

		function validaPermissaoAcesso() {
			if (!PermissaoService.containsPermissions(funcoes.produto)) {
				$location.path('/login');
			}
		}

		function reset() {
			vm.page = 1;
			vm.manterProduto.filtro.paginate.paginaAtual = 0;
			vm.manterProduto.lista = [];
		}

		function showMessage(_messsage) {
			var snackbarContainer = document.querySelector('#message-screen');
			snackbarContainer.MaterialSnackbar.showSnackbar({
				message : _messsage
			});
		}

		function toPage(page) {
			vm.manterProduto.filtro.paginate.paginaAtual = page;
			vm.page = vm.manterProduto.filtro.paginate.paginaAtual + 1;
			vm.listar();
		}

		function selectPage() {
			if (vm.page < 1
					|| vm.page > vm.manterProduto.filtro.paginate.numTotalPaginas) {
				showMessage('Página inválida!');
				vm.page = 1;
				vm.manterProduto.filtro.paginate.paginaAtual = 0;
				vm.toPage(vm.manterProduto.filtro.paginate.paginaAtual);
			} else if (vm.page) {
				vm.toPage(vm.page - 1);
			}
		}

		function quantidadePorPagina() {
			vm.page = 1;
			vm.manterProduto.filtro.paginate.paginaAtual = 0;
			vm.listar();
		}

		function listar() {
			vm.loading = true;
			ProdutoService
					.listar(vm.manterProduto.filtro)
					.then(
							function success(response) {
								vm.manterProduto.lista = response.data.lista;
								vm.manterProduto.filtro.paginate = response.data.filtro.paginate;
								vm.loading = false;
							}, function error(response) {
								showMessage('Falha ao carregar os produtos!');
								vm.loading = false;
							});
		}

		function exportar() {
			vm.loading = true;
			ProdutoService.exportar(vm.manterProduto.filtro).then(
					function success(response) {
						DownloadService.download(response.data, 'produtos.xls',
								'text/xls');
						vm.loading = false;
					}, function error(response) {
						showMessage('Falha ao exportar os produtos!');
						vm.loading = false;
					});
		}

		function novo() {
			$location.path('/produto/novo');
		}

		function toEdit(produto) {
			if (produto && produto.id) {
				$location.path('/produto/' + produto.id);
			}
		}

		function seleciona(produto) {
			vm.produtoSelecionado = produto;
			vm.openDialog();
		}

		function openDialog() {
			document.querySelector('#deleteDialog').showModal();
		}

		function closeDialog() {
			document.querySelector('#deleteDialog').close();
		}

		function excluir() {
			if (vm.produtoSelecionado) {
				ProdutoService.excluir(vm.produtoSelecionado).then(
						function success(response) {
							closeDialog();
							showMessage('Produto excluído com sucesso!');
							vm.listar();
							delete vm.produtoSelecionado;
						}, function error(response) {
							closeDialog();
							showMessage('Falha ao tentar excluir o produto!');
							delete vm.produtoSelecionado;
						});
			}
		}
	}
})();
