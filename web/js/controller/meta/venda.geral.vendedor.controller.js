(function() {
	'use strict';

	VendaGeralVendedorController.$inject = [ '$location', 'VendedorService',
			'PermissaoService', 'funcoes', 'pagination', '$scope' ];

	angular.module('appBella.sistema.controllers').controller(
			'VendaGeralVendedorController', VendaGeralVendedorController);

	function VendaGeralVendedorController($location, VendedorService,
			PermissaoService, funcoes, pagination, $scope) {

		var vm = this;
		vm.salvar = salvar;
		vm.listar = listar;
		vm.reset = reset;
		vm.toPage = toPage;
		vm.selectPage = selectPage;
		vm.quantidadePorPagina = quantidadePorPagina;
		vm.addItensAlterados = addItensAlterados;
		vm.manterVendedor = {};
		vm.manterVendedor.lista = [];
		vm.manterVendedor.filtro = {};
		vm.paginas = pagination;
		vm.page = 1;
		vm.loading = true;
		init();

		function init() {
			validaPermissaoAcesso();
			VendedorService.init().then(function success(response) {
				vm.manterVendedor.filtro = response.data.filtro;
				vm.manterVendedor.lista = response.data.lista;
				vm.loading = false;
			}, function error(response) {
				showMessage('Falha ao carregar os vendedores!');
				vm.loading = false;
			});
		}

		function validaPermissaoAcesso() {
			if (!PermissaoService.containsPermissions(funcoes.meta)) {
				$location.path('/login');
			}
		}
		
		function addItensAlterados(obj) {
			var inserido = false;
			if (!vm.itensAlterados) {
				vm.itensAlterados = [];
			}
			for (var i = 0; i < vm.itensAlterados.length; i++) {
				if (vm.itensAlterados[i].id === obj.id) {
					vm.itensAlterados[i] = obj;
					inserido = true;
					break;
				}
			}
			if (!inserido) {
				vm.itensAlterados.push(obj);
			}
		}
		
		function salvar() {
			vm.loading = true;
			VendedorService.saveList(vm.itensAlterados).then(
					function success(response) {
						vm.loading = false;
						$scope.form.$setPristine();
						$scope.form.$setUntouched();
						showMessage('Alterações salvas com sucesso!');
					}, function error(response) {
						showMessage('Falha ao salvar as alterações!');
						vm.loading = false;
					});
		}

		function reset() {
			vm.page = 1;
			vm.manterVendedor.filtro.paginate.paginaAtual = 0;
			vm.manterVendedor.lista = [];
		}

		function showMessage(_messsage) {
			var snackbarContainer = document.querySelector('#message-screen');
			snackbarContainer.MaterialSnackbar.showSnackbar({
				message : _messsage
			});
		}

		function toPage(page) {
			vm.manterVendedor.filtro.paginate.paginaAtual = page;
			vm.page = vm.manterVendedor.filtro.paginate.paginaAtual + 1;
			vm.listar();
		}

		function selectPage() {
			if (vm.page < 1
					|| vm.page > vm.manterVendedor.filtro.paginate.numTotalPaginas) {
				showMessage('Página inválida!');
				vm.page = 1;
				vm.manterVendedor.filtro.paginate.paginaAtual = 0;
				vm.toPage(vm.manterVendedor.filtro.paginate.paginaAtual);
			} else if (vm.page) {
				vm.toPage(vm.page - 1);
			}
		}

		function quantidadePorPagina() {
			vm.page = 1;
			vm.manterVendedor.filtro.paginate.paginaAtual = 0;
			vm.listar();
		}

		function listar() {
			vm.loading = true;
			VendedorService
					.listar(vm.manterVendedor.filtro)
					.then(
							function success(response) {
								vm.manterVendedor.lista = response.data.lista;
								vm.manterVendedor.filtro.paginate = response.data.filtro.paginate;
								vm.loading = false;
							},
							function error(response) {
								showMessage('Falha ao carregar os vendedores!');
								vm.loading = false;
							});
		}
	}
})();
