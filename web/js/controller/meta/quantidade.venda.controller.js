(function() {
	'use strict';

	QuantidadeVendaController.$inject = ['$location', 'funcoes', 'QuantidadeVendaService', 'PermissaoService', '$scope', 'SessionService', 'TIMEZONE','pagination', 'DownloadService' ];

	angular.module('appBella.sistema.controllers').controller(
			'QuantidadeVendaController', QuantidadeVendaController);

	function QuantidadeVendaController($location, funcoes, QuantidadeVendaService, PermissaoService, $scope, SessionService, TIMEZONE,pagination, DownloadService) {
		var vm = this;
		vm.listar = listar;
		vm.page = 1;
		vm.loading = true;
		vm.toPage = toPage;
		vm.selectPage = selectPage;
		vm.quantidadePorPagina = quantidadePorPagina;
		vm.addItensAlterados = addItensAlterados;
		vm.salvar = salvar;
		vm.manterQuantidadeVenda = {};
		vm.manterQuantidadeVenda.filtro = {};
		vm.itensAlterados = [];
		vm.dataInicialStr = '';
		vm.dataFinalStr = '';
		vm.timezone = TIMEZONE;
		vm.dateFormat = 'DD/MM/YYYY';
		vm.paginas = pagination;
		init();

		function init() {
			validaPermissaoAcesso();
			vm.listar();
		}

		function validaPermissaoAcesso() {
			if (!PermissaoService.containsPermissions(funcoes.meta)) {
				$location.path('/login');
			}
		}

		function showMessage(_messsage) {
			var snackbarContainer = document.querySelector('#message-screen');
			snackbarContainer.MaterialSnackbar.showSnackbar({
				message : _messsage
			});
		}

		function listar() {

			if (vm.dataInicialStr) {
				vm.manterQuantidadeVenda.filtro.dataInicial = moment(
						vm.dataInicialStr, vm.dateFormat).toDate();
			}
			if (vm.dataFinalStr) {
				vm.manterQuantidadeVenda.filtro.dataFinal = moment(vm.dataFinalStr,
						vm.dateFormat).toDate();
			}

			if (vm.manterQuantidadeVenda.filtro.dataInicial > vm.manterQuantidadeVenda.filtro.dataFinal) {
				showMessage('Período de datas inválido!');
				delete vm.dataFinalStr;
				delete vm.manterQuantidadeVenda.dataFinal;

			} else {
				vm.loading = true;
				QuantidadeVendaService
						.init(vm.manterQuantidadeVenda.filtro)
						.then(
								function success(response) {
									vm.manterQuantidadeVenda = response.data;
									if (vm.manterQuantidadeVenda.dataInicialStr) {
										vm.dataInicialStr = vm.manterQuantidadeVenda.dataInicialStr;
									}
									if (vm.manterQuantidadeVenda.dataFinalStr) {
										vm.dataFinalStr = vm.manterQuantidadeVenda.dataFinalStr;
									}
									vm.loading = false;
								},
								function error(response) {
									showMessage('Falha ao carregar os produtos e suas quantidades de venda!');
									vm.loading = false;
								});
			}
		}

		function addItensAlterados(obj) {
			var inserido = false;
			for (var i = 0; i < vm.itensAlterados.length; i++) {
				if (vm.itensAlterados[i].produto.id === obj.produto.id
						&& vm.itensAlterados[i].vendedor.id === obj.vendedor.id) {
					vm.itensAlterados[i] = obj;
					inserido = true;
					break;
				}
			}
			if (!inserido) {
				vm.itensAlterados.push(obj);
			}
		}

		function salvar() {
			vm.loading = true;
			QuantidadeVendaService.saveList(vm.itensAlterados).then(
					function success(response) {
						vm.loading = false;
						$scope.quantidadeVendaForm.$setPristine();
						$scope.quantidadeVendaForm.$setUntouched();
						showMessage('Alterações salvas com sucesso!');
					}, function error(response) {
						showMessage('Falha ao salvar as alterações!');
						vm.loading = false;
					});
		}

		function toPage(page) {
			vm.manterQuantidadeVenda.filtro.produto.paginate.paginaAtual = page;
			vm.page = vm.manterQuantidadeVenda.filtro.produto.paginate.paginaAtual + 1;
			vm.listar();
		}

		function selectPage() {
			if (vm.page < 1
					|| vm.page > vm.manterQuantidadeVenda.filtro.produto.paginate.numTotalPaginas) {
				showMessage('Página inválida!');
				vm.page = 1;
				vm.manterQuantidadeVenda.filtro.produto.paginate.paginaAtual = 0;
				vm.toPage(vm.manterQuantidadeVenda.filtro.produto.paginate.paginaAtual);
			} else if (vm.page) {
				vm.toPage(vm.page - 1);
			}
		}

		function quantidadePorPagina() {
			vm.page = 1;
			vm.manterQuantidadeVenda.filtro.produto.paginate.paginaAtual = 0;
			vm.listar();
		}
	}
})();
