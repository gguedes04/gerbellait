(function() {
	'use strict';

	angular.module('appBella.sistema.controllers').controller('PrecoMedioVendaController', PrecoMedioVendaController);

	PrecoMedioVendaController.$inject = [ '$location', 'funcoes', 'PrecoMedioVendaService', 'PermissaoService', '$scope', 'SessionService', 'TIMEZONE', 'pagination', 'DownloadService' ];

	function PrecoMedioVendaController($location, funcoes, PrecoMedioVendaService, PermissaoService, $scope, SessionService, TIMEZONE, pagination, DownloadService) {
		var vm = this;
		vm.listar = listar;
		vm.page = 1;
		vm.loading = true;
		vm.toPage = toPage;
		vm.selectPage = selectPage;
		vm.quantidadePorPagina = quantidadePorPagina;
		vm.addItensAlterados = addItensAlterados;
		vm.salvar = salvar;
		vm.manterPrecoMedioVenda = {};
		vm.manterPrecoMedioVenda.filtro = {};
		vm.itensAlterados = [];
		vm.dataInicialStr = '';
		vm.dataFinalStr = '';
		vm.timezone = TIMEZONE;
		vm.dateFormat = 'DD/MM/YYYY';
		vm.paginas = pagination;
		init();

		function init() {
			validaPermissaoAcesso();
			vm.listar();
		}

		function validaPermissaoAcesso() {
			if (!PermissaoService.containsPermissions(funcoes.meta)) {
				$location.path('/login');
			}
		}

		function showMessage(_messsage) {
			var snackbarContainer = document.querySelector('#message-screen');
			snackbarContainer.MaterialSnackbar.showSnackbar({
				message : _messsage
			});
		}

		function listar() {

			if (vm.dataInicialStr) {
				vm.manterPrecoMedioVenda.filtro.dataInicial = moment(
						vm.dataInicialStr, vm.dateFormat).toDate();
			}
			if (vm.dataFinalStr) {
				vm.manterPrecoMedioVenda.filtro.dataFinal = moment(vm.dataFinalStr,
						vm.dateFormat).toDate();
			}

			if (vm.manterPrecoMedioVenda.filtro.dataInicial > vm.manterPrecoMedioVenda.filtro.dataFinal) {
				showMessage('Período de datas inválido!');
				delete vm.dataFinalStr;
				delete vm.manterPrecoMedioVenda.dataFinal;

			} else {
				vm.loading = true;
				PrecoMedioVendaService
						.init(vm.manterPrecoMedioVenda.filtro)
						.then(
								function success(response) {
									vm.manterPrecoMedioVenda = response.data;
									vm.manterPrecoMedioVenda.filtro.produto.paginate = response.data.filtro.produto.paginate;
									if (vm.manterPrecoMedioVenda.dataInicialStr) {
										vm.dataInicialStr = vm.manterPrecoMedioVenda.dataInicialStr;
									}
									if (vm.manterPrecoMedioVenda.dataFinalStr) {
										vm.dataFinalStr = vm.manterPrecoMedioVenda.dataFinalStr;
									}
									vm.loading = false;
								},
								function error(response) {
									showMessage('Falha ao carregar os produtos e seus preços médios de venda!');
									vm.loading = false;
								});
			}
		}

		function addItensAlterados(obj) {
			var inserido = false;
			for (var i = 0; i < vm.itensAlterados.length; i++) {
				if (vm.itensAlterados[i].produto.id === obj.produto.id
						&& vm.itensAlterados[i].vendedor.id === obj.vendedor.id) {
					vm.itensAlterados[i] = obj;
					inserido = true;
					break;
				}
			}
			if (!inserido) {
				vm.itensAlterados.push(obj);
			}
		}

		function salvar() {
			vm.loading = true;
			PrecoMedioVendaService.saveList(vm.itensAlterados).then(
					function success(response) {
						vm.loading = false;
						$scope.quantidadeVendaForm.$setPristine();
						$scope.quantidadeVendaForm.$setUntouched();
						showMessage('Alterações salvas com sucesso!');
					}, function error(response) {
						showMessage('Falha ao salvar as alterações!');
						vm.loading = false;
					});
		}

		function toPage(page) {
			vm.manterPrecoMedioVenda.filtro.produto.paginate.paginaAtual = page;
			vm.page = vm.manterPrecoMedioVenda.filtro.produto.paginate.paginaAtual + 1;
			vm.listar();
		}

		function selectPage() {
			if (vm.page < 1
					|| vm.page > vm.manterPrecoMedioVenda.filtro.produto.paginate.numTotalPaginas) {
				showMessage('Página inválida!');
				vm.page = 1;
				vm.manterPrecoMedioVenda.filtro.produto.paginate.paginaAtual = 0;
				vm
						.toPage(vm.manterPrecoMedioVenda.filtro.produto.paginate.paginaAtual);
			} else if (vm.page) {
				vm.toPage(vm.page - 1);
			}
		}

		function quantidadePorPagina() {
			vm.page = 1;
			vm.manterPrecoMedioVenda.filtro.produto.paginate.paginaAtual = 0;
			vm.listar();
		}
	}
})();
