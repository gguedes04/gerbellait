(function() {
	'use strict';

	PeriodoMetaController.$inject = [ 'funcoes', 'PeriodoMetaService',
			'PermissaoService', 'TIMEZONE' ];

	angular.module('appBella.sistema.controllers').controller(
			'PeriodoMetaController', PeriodoMetaController);

	function PeriodoMetaController(funcoes, PeriodoMetaService,
			PermissaoService, TIMEZONE) {
		var vm = this;
		vm.salvar = salvar;
		vm.manterPeriodoMeta = {};
		vm.dataInicialStr = '';
		vm.dataFinalStr = '';
		vm.timezone = TIMEZONE;
		vm.dateFormat = 'DD/MM/YYYY';
		init();

		function init() {
			validaPermissaoAcesso();
			PeriodoMetaService
					.init()
					.then(
							function success(response) {
								vm.manterPeriodoMeta = response.data;
								vm.manterPeriodoMeta.dataInicialMeta = moment(vm.manterPeriodoMeta.dataInicialStr);
								vm.manterPeriodoMeta.dataFinalMeta = moment(vm.manterPeriodoMeta.dataFinalStr);
								vm.dataInicialStr = vm.manterPeriodoMeta.dataInicialStr;
								vm.dataFinalStr = vm.manterPeriodoMeta.dataFinalStr;
							},
							function error(response) {
								showMessage('Falha ao carregar o período da meta!');
							});
		}

		function validaPermissaoAcesso() {
			if (!PermissaoService.containsPermissions(funcoes.meta)) {
				$location.path('/login');
			}
		}

		function showMessage(_messsage) {
			var snackbarContainer = document.querySelector('#message-screen');
			snackbarContainer.MaterialSnackbar.showSnackbar({
				message : _messsage
			});
		}

		function salvar() {

			vm.manterPeriodoMeta.dataInicialMeta = moment(vm.dataInicialStr,
					vm.dateFormat).toDate();
			vm.manterPeriodoMeta.dataFinalMeta = moment(vm.dataFinalStr,
					vm.dateFormat).toDate();

			if (vm.manterPeriodoMeta.dataInicialMeta > vm.manterPeriodoMeta.dataFinalMeta) {
				showMessage('Período de datas inválido!');

			} else {
				delete vm.manterPeriodoMeta.dataFinalStr;
				delete vm.manterPeriodoMeta.dataInicialStr;
				PeriodoMetaService.salvar(vm.manterPeriodoMeta).then(
						function success(response) {
							showMessage('Alterações salvas com sucesso!');
						}, function error(response) {
							showMessage('Falha ao salvar as alterações!');
							vm.loading = false;
						});
			}

		}
	}
})();
