(function() {

	angular.module('app.controllers').controller('MenuController',
			MenuController);

	MenuController.$inject = [ 'SessionService', 'ProjectService' ];

	function MenuController(SessionService, ProjectService) {

		var vm = this;
		vm.project = ProjectService.get();
		var gruposUsuario = SessionService.getUser().grupos;
		vm.hasAccess = hasAccess;

		function hasAccess(gruposAcesso) {
			for (var i = 0; i < gruposAcesso.length; i++) {
				for (var j = 0; j < gruposUsuario.length; j++) {
					if (gruposAcesso[i] === gruposUsuario[j])
						return true;
				}
			}
			return false;
		}
	}
})();
