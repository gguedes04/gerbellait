(function () {
	'use strict';

	angular.module('app.controllers').controller('HeaderController',
		HeaderController);

	HeaderController.$inject = ['$state', 'SessionService'];

	function HeaderController($state, SessionService) {
		var vm = this;

		vm.usuario = SessionService.getUser();
		vm.logout = logout;

		function logout() {
			SessionService.removeToken();
			delete vm.usuario;
			vm.usuario = {};
			$state.go('login');
		}
	}

})();
