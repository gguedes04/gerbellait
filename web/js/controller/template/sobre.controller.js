(function () {
	'use strict';

	angular.module('app.controllers').controller('SobreController', SobreController);

	SobreController.$inject = ['$rootScope'];

	function SobreController($rootScope) {
		$rootScope.currentview.id = 'sobre';
		$rootScope.currentview.title = 'Sobre a Aplicação';
		$rootScope.currentview.icon = 'fa-question';
		$rootScope.currentview.showUserData = true;
		$rootScope.currentview.description = 'Sobre a aplicação';
	}

})();
