(function () {
    'use strict';

    angular.module('app.controllers').component('modalComponent', {
        templateUrl: 'app/view/template/modal.html',
        bindings: {
            resolve: '<',
            close: '&',
            dismiss: '&'
        },
        controller: ModalController
    });

    ModalController.$inject = ['$scope'];

    function ModalController(vm) {
        vm.modal = {};
        vm.modal.title = "Título";
        vm.modal.body = "Corpo";
        vm.modal.ok = "Ok";
        vm.modal.cancel = "Cancel";

        this.$onInit = function () {
            if (this.resolve.modal) {
                vm.modal = this.resolve.modal;
            }
            vm.ok = this.resolve.ok;
            vm.cancel = this.resolve.cancel;

        };

    }

})();
