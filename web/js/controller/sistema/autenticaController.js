(function() {
	'use strict';

	AutenticarController.$inject = [ '$location', 'SessionService',
			'AutorizacaoService', '$timeout', '$window' ];

	angular.module('appBella.sistema.controllers').controller(
			'AutenticarController', AutenticarController);

	function AutenticarController($location, SessionService,
			AutorizacaoService, $timeout, $window) {

		var vm = this;
		vm.autenticarUsuario = autenticarUsuario;
		vm.openDialog = openDialog;
		vm.closeDialog = closeDialog;
		vm.openDialogPrimeiroAcesso = openDialogPrimeiroAcesso;
		vm.closeDialogPrimeiroAcesso = closeDialogPrimeiroAcesso;
		vm.enviarEmail = enviarEmail;
		vm.salvarNovaSenha = salvarNovaSenha;
		vm.usuario = SessionService.getUser();
		vm.usuario = {};
		vm.loading = false;
		vm.confirmasenha = null;
		vm.novasenha = null;

		function toLogin() {
			$timeout(function() {
				$location.path('/login');
			}, 1000);
		}

		function autenticarUsuario() {
			if (vm.usuario) {
				AutorizacaoService
						.autenticar(vm.usuario)
						.then(
								function(response) {
									if (response.data.situacao === 1) {
										if (response.data.icPrimeiroAcesso == true) {
											vm.openDialogPrimeiroAcesso();
										} else 
										{
											delete response.data.senha;
											SessionService.setUser(response.data);
											delete vm.spinner;
											$location.path('/home');
										}
										
									} else {
										var snackbarContainer = document
												.querySelector('#demo-toast-example');
										snackbarContainer.MaterialSnackbar
												.showSnackbar({
													message : 'Usuário sem permissão de acesso!'
												});
										toLogin();
									}
								},
								function error(response) {
									if (response.status === 403) {
										var snackbarContainer = document
												.querySelector('#demo-toast-example');
										snackbarContainer.MaterialSnackbar
												.showSnackbar({
													message : 'Login ou senha inválido!'
												});
										toLogin();
									}
								});
				
				
			}
		}

		function openDialog() {
			document.querySelector('#esqueceuSenhaDialog').showModal();
		}

		function closeDialog() {
			document.querySelector('#esqueceuSenhaDialog').close();
			delete vm.usuario;
		}

		function openDialogPrimeiroAcesso() {
			document.querySelector('#primeiroAcessoDialog').showModal();
		}

		function closeDialogPrimeiroAcesso() {
			document.querySelector('#primeiroAcessoDialog').close();
			delete vm.usuario;
			$location.path('/login');
		}
		
		function enviarEmail() {
			vm.loading = true;
			AutorizacaoService
					.esqueceuEmail(vm.usuario)
					.then(
							function(response) {
								vm.loading = false;
								var snackbarContainer = document
										.querySelector('#demo-toast-example');
								if (response.data === true
										|| response.data === 'true') {
									snackbarContainer.MaterialSnackbar
											.showSnackbar({
												message : 'E-mail enviado com sucesso!'
											});
								} else {
									snackbarContainer.MaterialSnackbar
											.showSnackbar({
												message : 'Login ou e-mail inválido!'
											});
								}
								toLogin();
							},
							function error(response) {
								vm.loading = false;
								var snackbarContainer = document
										.querySelector('#demo-toast-example');
								snackbarContainer.MaterialSnackbar
										.showSnackbar({
											message : 'Falha ao tentar enviar e-mail!'
										});
								toLogin();
							});
		}

		function salvarNovaSenha() {
				AutorizacaoService
					.salvaNovaSenha(vm.usuario)
					.then(
							function(response) {
								vm.loading = false;
								var snackbarContainer = document
										.querySelector('#demo-toast-example');
								if (response.data === true
										|| response.data === 'true') {
									snackbarContainer.MaterialSnackbar
											.showSnackbar({
												message : 'Senha alterada com sucesso!'
											});
								} else {
									snackbarContainer.MaterialSnackbar
											.showSnackbar({
												message : 'Senha não alterada. Verifique com o administrador.'
											});
								}
								toLogin();
							},
							function error(response) {
								vm.loading = false;
								var snackbarContainer = document
										.querySelector('#demo-toast-example');
								snackbarContainer.MaterialSnackbar
										.showSnackbar({
											message : 'Falha ao tentar alterar a senha!'
										});
								toLogin();
							});
		}
		
		$(document).ready(function() {
			$('#header').hide();
			$('#menu').hide();
			$('.mdl-layout__drawer-button').hide();
		});
		
		$window.onload = function() {
			$('#header').hide();
			$('#menu').hide();
			$('.mdl-layout__drawer-button').hide();
		};
		
	}
})();
