(function () {
	'use strict';

	angular.module('app.controllers').controller('PrincipalController',
		PrincipalController);

	PrincipalController.$inject = ['$rootScope'];

	function PrincipalController($rootScope) {
		$rootScope.currentview.id = 'principal';
		$rootScope.currentview.title = 'Principal';
		$rootScope.currentview.icon = 'fa-home';
		$rootScope.currentview.showUserData = true;
		$rootScope.currentview.description = 'Tela Principal';
	}

})();
