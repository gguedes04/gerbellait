(function() {
	'use strict';

	PrecoVendaRotaController.$inject = [ '$location', 'funcoes', 'PrecoVendaRotaService','PermissaoService', '$scope', 'SessionService', 'TIMEZONE', 'pagination', 'DownloadService' ];

	angular.module('appBella.sistema.controllers').controller(
			'PrecoVendaRotaController', PrecoVendaRotaController);

	function PrecoVendaRotaController($location, funcoes, PrecoVendaRotaService,PermissaoService, $scope, SessionService, TIMEZONE, pagination, DownloadService) {
		var vm = this;
		vm.listar = listar;
		vm.page = 1;
		vm.loading = true;
		vm.toPage = toPage;
		vm.selectPage = selectPage;
		vm.quantidadePorPagina = quantidadePorPagina;
		vm.addItensAlterados = addItensAlterados;
		vm.salvar = salvar;
		vm.manterPrecoVendaRota = {};
		vm.manterPrecoVendaRota.filtro = {};
		vm.itensAlterados = [];
		vm.timezone = TIMEZONE;
		vm.dateFormat = 'DD/MM/YYYY';
		vm.paginas = pagination;
		init();

		function init() {
			validaPermissaoAcesso();
			vm.listar();
		}

		function listar() {
			vm.loading = true;
			PrecoVendaRotaService.init(vm.manterPrecoVendaRota.filtro).then(
					function success(response) {
						vm.manterPrecoVendaRota = response.data;
						vm.loading = false;
					}, function error(response) {
						showMessage('Falha ao carregar os produtos e rotas!');
						vm.loading = false;
					});
		}

		function validaPermissaoAcesso() {
			if (!PermissaoService.containsPermissions(funcoes.precoVendaRota)) {
				$location.path('/login');
			}
		}

		function showMessage(_messsage) {
			var snackbarContainer = document.querySelector('#message-screen');
			snackbarContainer.MaterialSnackbar.showSnackbar({
				message : _messsage
			});
		}

		function addItensAlterados(prota) {
			var inserido = false;
			for (var i = 0; i < vm.itensAlterados.length; i++) {
				if (vm.itensAlterados[i].produto.id === prota.produto.id
						&& vm.itensAlterados[i].rota.id === prota.rota.id) {
					vm.itensAlterados[i] = prota;
					inserido = true;
					break;
				}
			}
			if (!inserido) {
				vm.itensAlterados.push(prota);
			}
		}

		function salvar() {
			vm.loading = true;
			PrecoVendaRotaService.saveList(vm.itensAlterados).then(
					function success(response) {
						vm.loading = false;
						$scope.precoVendaRotaForm.$setPristine();
						$scope.precoVendaRotaForm.$setUntouched();
						showMessage('Alterações salvas com sucesso!');
					}, function error(response) {
						showMessage('Falha ao salvar as alterações!');
						vm.loading = false;
					});
		}

		function toPage(page) {
			vm.manterPrecoVendaRota.filtro.paginate.paginaAtual = page;
			vm.page = vm.manterPrecoVendaRota.filtro.paginate.paginaAtual + 1;
			vm.listar();
		}

		function selectPage() {
			if (vm.page < 1
					|| vm.page > vm.manterPrecoVendaRota.filtro.paginate.numTotalPaginas) {
				showMessage('Página inválida!');
				vm.page = 1;
				vm.manterPrecoVendaRota.filtro.paginate.paginaAtual = 0;
				vm.toPage(vm.manterPrecoVendaRota.filtro.paginate.paginaAtual);
			} else if (vm.page) {
				vm.toPage(vm.page - 1);
			}
		}

		function quantidadePorPagina() {
			vm.page = 1;
			vm.manterPrecoVendaRota.filtro.paginate.paginaAtual = 0;
			vm.listar();
		}
	}
})();
