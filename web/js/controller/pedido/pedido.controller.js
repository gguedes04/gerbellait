(function() {
    'use strict';

    PedidoController.$inject = ['$location', 'PedidoService', '$routeParams', '$timeout', 'funcoes', 'PermissaoService', 'SessionService', 'TIMEZONE', 'ProdutoService', 'pagination', '$scope', 'DownloadService'];

    angular.module('appBella.sistema.controllers').controller('PedidoController', PedidoController);

    function PedidoController($location, PedidoService, $routeParams, $timeout, funcoes, PermissaoService, SessionService, TIMEZONE, ProdutoService, pagination, $scope, DownloadService) {

        const vm = this;
        const PENDING = 'pending';
        const EDITING = 'editing';
        const READY = 'ready';
        init();

        async function init() {
            vm.timezone = TIMEZONE;
            vm.paginas = pagination;
            vm.usuario = SessionService.getUser();
            const editEnum = EditEnum();
            vm.editionSequence = {
                [editEnum.CUSTOMER]: editEnum.PAYMENT,
                [editEnum.PAYMENT]: editEnum.PRODUCT,
                [editEnum.PRODUCT]: editEnum.COMMENTS,
                [editEnum.COMMENTS]: editEnum.FINAL_STEP,
                [editEnum.FINAL_STEP]: editEnum.LAST_SELLS,
                [editEnum.LAST_SELLS]: READY
            };
            vm.order = { products: [] };
            vm.readySteps = {};
            vm.quantidadePorPagina = 10;
            vm.lastSellsList = [];
            vm.checkedAllOrdersUltimasVendas = false;

            vm.editar = editar;
            vm.novo = novo;
            vm.novo_init = novo_init;
            vm.salvar = salvar;
            vm.voltar = voltar;
            vm.onChangeCustomer = onChangeCustomer;
            vm.onChangeProduct = onChangeProduct;
            vm.changeEditingStepNext = changeEditingStepNext;
            vm.changeEditingStepPrevious = changeEditingStepPrevious;
            vm.productSearch = productSearch;
            vm.changeQuantidadePorPagina = changeQuantidadePorPagina;
            vm.addProductToList = addProductToList;
            vm.printOrder = printOrder;
            vm.addProductsLastSells = addProductsLastSells;
            vm.removeProductToList = obj => {
                if (vm.order.hasLoadMap) {
                    showMessage('Pedido encontra-se em relatorio de mapa de carga');
                }
                obj.flExcluido = 'EXCLUIDO';
                calcOrderProductsValue();
            };
            vm.setDesconto = () => vm.order.valorDesconto = vm.order.cliente.percDescPadrao;
            vm.justValidRecords = item => item.flExcluido === 'ATIVO';
            vm.getTotalFinalStep = () => vm.order.valorPedido - (vm.order.valorPedido * (vm.order.valorDesconto / 100));

            vm.isEditingCustomer = () => vm.inEdition === editEnum.CUSTOMER;
            vm.isEditingFinalStep = () => vm.inEdition === editEnum.FINAL_STEP;
            vm.isEditingPayment = () => vm.inEdition === editEnum.PAYMENT;
            vm.isEditingProduct = () => vm.inEdition === editEnum.PRODUCT;
            vm.isEditingComments = () => vm.inEdition === editEnum.COMMENTS;
            vm.isEditingLastSells = () => vm.inEdition === editEnum.LAST_SELLS;

            vm.inEdition = editEnum.CUSTOMER;
            vm.step = {
                customer: EDITING,
                finalStep: PENDING,
                comments: PENDING,
                payment: PENDING,
                products: PENDING,
                lastSells: PENDING
            };

            function addProductsLastSells(order, all = false) {
                if (all) {
                    vm.lastSellsList = [];
                    vm.order.ultimasVendas.forEach(obj => {
                        const _order = obj;
                        obj.checked = vm.checkedAllOrdersUltimasVendas;
                        if (vm.checkedAllOrdersUltimasVendas) {
                            vm.lastSellsList.push(_order);
                        }
                    });
                } else {
                    vm.lastSellsList = [];
                    vm.order.ultimasVendas.forEach(obj => obj.checked && vm.lastSellsList.push(obj));
                }
            }

            try {
                const response = await PedidoService.init(vm.usuario.id);
                const { data } = response;
                vm.customerSearch = autocompleteSearcher.bind(data.clientes);
                vm.sellers = data.vendedores;
                vm.paymentsWay = data.formasPagamento;
            } catch (e) {
                showMessage('Falha ao carregar os pedidos!');
            } finally {
                vm.loading = false;
            }

            validaPermissaoAcesso();

            if ($routeParams.id > 0) {
                editar($routeParams.id);
            } else {
                novo();
            }
        }

        async function printOrder(order) {
            vm.loadingExport = true;
            try {
                delete order.cliente.vendedor;
                const { data } = await PedidoService.printOrder(order);

                await DownloadService.download(data, 'relatoriopedidos.xls', 'text/xls');
            } catch (error) {
                if (error.status === 411) {
                    showMessage('Não há dados para exportar!');
                } else {
                    showMessage('Não foi possível exportar os dados!');
                }
            } finally {
                $scope.$applyAsync(() => vm.loadingExport = false);
            }
        }

        function calcOrderProductsValue() {
            vm.order.valorPedido = (vm.order.produtoList || []).reduce((value, item) => {
                if (item.flExcluido === 'EXCLUIDO') {
                    return value;
                }
                if (item.tipoUnidade === 'UN') {
                    const countCalc = item.precoVenda * item.qtdeProduto;
                    value += (countCalc - (countCalc * (item.desconto / 100)));
                } else if (item.tipoUnidade === 'KG') {
                    const pesoCalc = item.precoVenda * item.peso;
                    value += (pesoCalc - (pesoCalc * (item.desconto / 100)));
                }
                return value;
            }, 0);
            return vm.order.valorPedido;
        }

        function addProductToList() {
            if (!vm.order.produtoList) {
                vm.order.produtoList = [];
            }
            const totalAdded = vm.order.produtoList.reduce((value, item) => value += item.id === vm.product.id ? item.qtdeProduto : 0, 0);

            if (vm.product.qtdeProduto > vm.product.qtdeLimitVenda
                || vm.product.qtdeProduto > vm.product.quantidadeEstoqueAtual
                || totalAdded > vm.product.qtdeLimitVenda) {
                showMessage('Quantidade não disponível em estoque!');
                return;
            }
            if (vm.product.qtdeProduto < vm.product.quantidadeMinVenda
                && vm.product.vlUnitario < vm.product.precoVenda) {
                showMessage('Preço ou quantidade abaixo da tabela.');
                return;
            }
            if (vm.product.qtdeProduto >= vm.product.quantidadeMinVenda
                && vm.product.vlUnitario < vm.product.percDesconto) {
                showMessage('Preço ou quantidade abaixo do promocional.');
                return;
            }
            if (vm.order.hasLoadMap) {
                showMessage('Pedido encontra-se em relatorio de mapa de carga');
            }

            vm.order.produtoList.push(angular.copy(vm.product));
            delete vm.product;
            calcOrderProductsValue();
        }

        function changeQuantidadePorPagina() {
            vm.page = 1;
            vm.paginaAtual = 0;
        }

        async function productSearch(text) {
            const response = await ProdutoService.getByDesc(vm.order.cliente.id, text);
            return response.data;
        }

        function changeEditingStepNext(next) {
            if (vm.inEdition !== READY) {
                vm.step[vm.inEdition] = READY;
                vm.inEdition = next || vm.editionSequence[vm.inEdition];
                vm.step[vm.inEdition] = EDITING;
                vm.readySteps[vm.inEdition] = true;
            }
        }

        function changeEditingStepPrevious() {
            if (vm.readySteps[vm.inEdition]) {
                vm.step[vm.inEdition] = READY;
            } else {
                vm.step[vm.inEdition] = EDITING;
            }
            vm.inEdition = Object.keys(vm.editionSequence).find(key => vm.editionSequence[key] === vm.inEdition);
            vm.step[vm.inEdition] = EDITING;
        }

        function onChangeCustomer() {
            if (vm.order.cliente) {
                vm.order.cliente.vendedor = (vm.sellers.find(seller => seller.id === vm.order.cliente.idVendedorRepresentante) || {}).nome || '';
                PedidoService.getUltimasVendas(vm.order.cliente.id).then(response => {
                    vm.order.ultimasVendas = response.data.ultimasVendas;
                });
            } else {
                vm.step.customer = EDITING;
            }
        }

        function onChangeProduct() {
            if (vm.product) {
                let pedido = 0;
                vm.order.ultimasVendas.forEach(venda => {
                    if (venda.idProduto === vm.product.id
                        && venda.idPedido > pedido) {
                        pedido = venda.idPedido;
                        vm.product.vlUnitario = venda.vlUnitario;
                    }
                });
            }
        }


        /**
         * Autocomplete: filtra os valores de acordo com a lista de valores
         */
        function autocompleteSearcher(textSearch) {
            if (textSearch) {
                if (!isNaN(textSearch)) {
                    return this.filter(item => item.id.toString().indexOf(textSearch) > -1);
                } else {
                    return this.filter(item => item.nome.toLowerCase().indexOf(textSearch.toLowerCase()) > -1);
                }
            } else {
                return [];
            }
        }

        function validaPermissaoAcesso() {
            if (!PermissaoService.containsPermissions(funcoes.pedido)) {
                $location.path('/login');
            }
        }

        function showMessage(message) {
            const snackbarContainer = document.querySelector('#message-screen');
            snackbarContainer.MaterialSnackbar.showSnackbar({ message });
        }

        function editar(id) {
            PedidoService.editar(id).then(function success(response) {
                vm.order = response.data.pedido;
            }, function error(response) {
                showMessage('Falha ao carregar o pedido!');
            });
        }

        function novo() {
            PedidoService.novo(vm.usuario.id).then(function success(response) {
                vm.order = response.data.pedido;
            }, function error(response) {
                showMessage('Falha ao carregar o pedido!');
            });
        }

        function novo_init() {
            PedidoService.novo(vm.usuario.id).then(function success(response) {
                vm.order = response.data.pedido;
                init();
            }, function error(response) {
                showMessage('Falha ao carregar o pedido!');
            });
        }

        function salvar() {
            const order = angular.copy(vm.order);
            delete order.cliente.vendedor;
            PedidoService.salvar(order).then(
                function success(response) {
                    vm.order = response.data;
                    showMessage('Pedido salvo com sucesso! Numero ' + vm.order.id);
                    $timeout(function() {
                        $location.path('/pedido/' + vm.order.id);
                    }, 1000);
                }, function error() {
                    showMessage('Falha ao tentar salvar o pedido!');
                });
        }

        function voltar() {
            $location.path('/pedidos');
        }

        function EditEnum() {
            return {
                CUSTOMER: 'customer',
                PAYMENT: 'payment',
                PRODUCT: 'products',
                COMMENTS: 'comments',
                LAST_SELLS: 'lastSells',
                FINAL_STEP: 'finalStep'
            };
        }
    }
})();
