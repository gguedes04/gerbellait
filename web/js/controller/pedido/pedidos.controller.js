(function() {
    'use strict';

    angular.module('appBella.sistema.controllers').controller('PedidosController', PedidosController);

    PedidosController.$inject =
        ['$location', 'PedidoService', 'PermissaoService', 'funcoes', 'pagination', 'SessionService', 'TIMEZONE', '$scope', 'DownloadService'];

    function PedidosController($location, PedidoService, PermissaoService, funcoes, pagination, SessionService, TIMEZONE, $scope, DownloadService) {

        const vm = this;
        initializer();
        init();

        function initializer() {
            vm.manterPedido = {};
            vm.filter = PedidoService.filter;
            vm.records = [];
            vm.sellers = [];
            vm.router = [];
            vm.pedidoSelecionado = {};
            vm.paginas = pagination;
            vm.page = 1;
            vm.produto = {};
            vm.fornecedor = {};
            vm.timezone = TIMEZONE;
            vm.usuario = SessionService.getUser();
            vm.canSearch = false;

            vm.listar = listar;
            vm.seleciona = seleciona;
            vm.novo = novo;
            vm.toEdit = toEdit;
            vm.reset = reset;
            vm.openDialog = openDialog;
            vm.closeDialog = closeDialog;
            vm.excluir = excluir;
            vm.quantidadePorPagina = quantidadePorPagina;
            vm.callServer = callServer;
            vm.printOrder = printOrder;
        }

        async function printOrder(order) {
            vm.loadingExport = true;
            try {
                const { data } = await PedidoService.printOrder(order);

                await DownloadService.download(data, 'relatoriopedidos.xls', 'text/xls');
            } catch (error) {
                if (error.status === 411) {
                    showMessage('Não há dados para exportar!');
                } else {
                    showMessage('Não foi possível exportar os dados!');
                }
            } finally {
                $scope.$applyAsync(() => vm.loadingExport = false);
            }
        }

        function listar() {
            if (vm.filter.vendaInicial && vm.filter.vendaFinal
                && moment(vm.filter.vendaInicial, 'DD/MM/YYYY').isAfter(moment(vm.filter.vendaFinal, 'DD/MM/YYYY'))) {
                showMessage('Período de venda inicial deve ser menor que a final.');
                return;
            } else if (!vm.filter.vendaInicial && vm.filter.vendaFinal) {
                showMessage('Período de venda inicial deve ser informada.');
                return;
            }
            if (vm.filter.entregaInicial && vm.filter.entregaFinal
                && moment(vm.filter.entregaInicial, 'DD/MM/YYYY').isAfter(moment(vm.filter.entregaFinal, 'DD/MM/YYYY'))) {
                showMessage('Período de entrega inicial deve ser menor que a final.');
                return;
            } else if (!vm.filter.entregaInicial && vm.filter.entregaFinal) {
                showMessage('Período de entrega inicial deve ser informada.');
                return;
            }
            // vm.filter.pedido = {idPedidoCliente: vm.filter.pedido}
            vm.canSearch = true;
            vm.tableState.pagination.start = 0;
            PedidoService.alreadyFiltered = true;
            callServer(vm.tableState);
        }

        async function init() {
            vm.loading = true;
            validaPermissaoAcesso();
            try {
                const response = await PedidoService.init(vm.usuario.id);
                const { data } = response;
                vm.sellers = data.vendedores;
                vm.routes = data.rotas;
                // vm.routes = data.rotas.sort((a, b) => a.id.localeCompare(b.id));
                vm.productSearch = autocompleteProductsSearcher.bind(data.produtos);
                vm.customerSearch = autocompleteCustomerSearcher.bind(data.clientes);
                vm.fornecedorSearch = autocompleteSearcher.bind(data.fornecedores);
            } catch (e) {
                showMessage('Falha ao carregar os pedidos!');
            } finally {
                vm.loading = false;
                $scope.$apply();
            }
        }

        async function callServer(tableState) {
            try {
                vm.loading = true;
                vm.filter.usuarioLogado = vm.usuario.id;
                tableState.search.predicateObject = vm.filter;
                tableState.pagination.number = tableState.pagination.number || 10;
                vm.tableState = tableState;
                if (vm.canSearch || PedidoService.alreadyFiltered) {
                    let data;
                    if (!!PedidoService.currentData.pedidos) {
                        data = angular.copy(PedidoService.currentData);
                        PedidoService.currentData = {};
                    } else {
                        data = (await PedidoService.listar(tableState)).data;
                    }
                    $scope.$applyAsync(() => {
                        vm.records = data.pedidos || [];
                        tableState.pagination.numberOfPages = data.tableState.pagination.numberOfPages;
                        vm.loading = false;
                    });
                }
            } catch (e) {
                vm.loading = false;
                showMessage('Falha ao carregar os clientes!');
            }
        }

        function reset() {
            vm.page = 1;
            vm.records = [];
        }

        function showMessage(message) {
            var snackbarContainer = document.querySelector('#message-screen');
            snackbarContainer.MaterialSnackbar.showSnackbar({ message });
        }

        function validaPermissaoAcesso() {
            if (!PermissaoService.containsPermissions(funcoes.pedido)) {
                $location.path('/login');
            }
        }

        function quantidadePorPagina() {
            vm.page = 1;
            vm.listar();
        }

        function novo() {
            $location.path('/pedido/novo');
        }

        function toEdit(order) {
            if (order && order.id) {
                PedidoService.currentData = { pedidos: vm.records, tableState: vm.tableState };
                $location.path('/pedido/' + order.id);
            }
        }

        function seleciona(pedido) {
            vm.pedidoSelecionado = pedido;
            vm.openDialog();
        }

        function openDialog() {
            document.querySelector('#deleteDialog').showModal();
        }

        function closeDialog() {
            document.querySelector('#deleteDialog').close();
        }

        function excluir() {
            if (vm.pedidoSelecionado) {
                PedidoService.excluir(vm.pedidoSelecionado.id).then(
                    function success(response) {
                        closeDialog();
                        showMessage('Pedido excluído com sucesso!');
                        vm.listar();
                        delete vm.pedidoSelecionado;
                    }, function error(response) {
                        closeDialog();
                        showMessage('Falha ao tentar excluir o pedido!');
                        delete vm.pedidoSelecionado;
                    });
            }
        }

        /**
         * Autocomplete: filtra os valores de acordo com a lista de valores
         */
        function autocompleteSearcher(textSearch) {
            if (textSearch) {
                if (!isNaN(textSearch)) {
                    return this.filter(item => item.id.toString().toLowerCase().indexOf(textSearch.toLowerCase()) > -1);
                } else {
                    return this.filter(item => item.nome.toLowerCase().indexOf(textSearch.toLowerCase()) > -1);
                }
            } else {
                return [];
            }
        }

        /**
         * Autocomplete: filtra os valores de acordo com a lista de valores
         */
        function autocompleteCustomerSearcher(textSearch) {
            if (textSearch) {
                if (!isNaN(textSearch)) {
                    return this.filter(item => item.idCodPessoa.toString().toLowerCase().indexOf(textSearch.toLowerCase()) > -1);
                } else {
                    return this.filter(item => item.nome.toLowerCase().indexOf(textSearch.toLowerCase()) > -1);
                }
            } else {
                return [];
            }
        }

        /**
         * Autocomplete: filtra os valores de acordo com a lista de valores
         */
        function autocompleteProductsSearcher(textSearch) {
            if (textSearch) {
                if (!isNaN(textSearch)) {
                    return this.filter(item => item.idCodProduto.toString().toLowerCase().indexOf(textSearch.toLowerCase()) > -1);
                } else {
                    return this.filter(item => item.nome.toLowerCase().indexOf(textSearch.toLowerCase()) > -1);
                }
            } else {
                return [];
            }
        }
    }
})();
