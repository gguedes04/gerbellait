(function() {
	'use strict';

	VendedoresController.$inject = [ '$location', 'VendedorService',
			'DownloadService', 'PermissaoService', 'funcoes', 'pagination' ];

	angular.module('appBella.sistema.controllers').controller(
			'VendedoresController', VendedoresController);

	function VendedoresController($location, VendedorService, DownloadService,
			PermissaoService, funcoes, pagination) {

		var vm = this;
		vm.listar = listar;
		vm.exportar = exportar;
		vm.seleciona = seleciona;
		vm.novo = novo;
		vm.toEdit = toEdit;
		vm.reset = reset;
		vm.toPage = toPage;
		vm.selectPage = selectPage;
		vm.openDialog = openDialog;
		vm.closeDialog = closeDialog;
		vm.excluir = excluir;
		vm.quantidadePorPagina = quantidadePorPagina;
		vm.manterVendedor = {};
		vm.manterVendedor.lista = [];
		vm.manterVendedor.filtro = {};
		vm.vendedorSelecionado = {};
		vm.paginas = pagination;
		vm.page = 1;
		vm.loading = true;
		init();

		function init() {
			validaPermissaoAcesso();
			VendedorService.init().then(function success(response) {
				vm.manterVendedor.filtro = response.data.filtro;
				vm.manterVendedor.tipos = response.data.tipos;
				vm.manterVendedor.rotas = response.data.rotas;
				vm.manterVendedor.lista = response.data.lista;
				vm.loading = false;
			}, function error(response) {
				showMessage('Falha ao carregar os vendedores!');
				vm.loading = false;
			});
		}

		function validaPermissaoAcesso() {
			if (!PermissaoService.containsPermissions(funcoes.vendedor)) {
				$location.path('/login');
			}
		}

		function reset() {
			vm.page = 1;
			vm.manterVendedor.filtro.paginate.paginaAtual = 0;
			vm.manterVendedor.lista = [];
		}

		function showMessage(_messsage) {
			var snackbarContainer = document.querySelector('#message-screen');
			snackbarContainer.MaterialSnackbar.showSnackbar({
				message : _messsage
			});
		}

		function toPage(page) {
			vm.manterVendedor.filtro.paginate.paginaAtual = page;
			vm.page = vm.manterVendedor.filtro.paginate.paginaAtual + 1;
			vm.listar();
		}

		function selectPage() {
			if (vm.page < 1
					|| vm.page > vm.manterVendedor.filtro.paginate.numTotalPaginas) {
				showMessage('Página inválida!');
				vm.page = 1;
				vm.manterVendedor.filtro.paginate.paginaAtual = 0;
				vm.toPage(vm.manterVendedor.filtro.paginate.paginaAtual);
			} else if (vm.page) {
				vm.toPage(vm.page - 1);
			}
		}

		function quantidadePorPagina() {
			vm.page = 1;
			vm.manterVendedor.filtro.paginate.paginaAtual = 0;
			vm.listar();
		}

		function listar() {
			vm.loading = true;
			VendedorService
					.listar(vm.manterVendedor.filtro)
					.then(
							function success(response) {
								vm.manterVendedor.lista = response.data.lista;
								vm.manterVendedor.filtro.paginate = response.data.filtro.paginate;
								vm.loading = false;
							}, function error(response) {
								showMessage('Falha ao carregar os vendedores!');
								vm.loading = false;
							});
		}

		function exportar() {
			vm.loading = true;
			VendedorService.exportar(vm.manterVendedor.filtro).then(
					function success(response) {
						DownloadService.download(response.data, 'vendedores.xls',
								'text/xls');
						vm.loading = false;
					}, function error(response) {
						showMessage('Falha ao exportar os vendedores!');
						vm.loading = false;
					});
		}

		function novo() {
			$location.path('/vendedor/novo');
		}

		function toEdit(vendedor) {
			if (vendedor && vendedor.id) {
				$location.path('/vendedor/' + vendedor.id);
			}
		}

		function seleciona(vendedor) {
			vm.vendedorSelecionado = vendedor;
			vm.openDialog();
		}

		function openDialog() {
			document.querySelector('#deleteDialog').showModal();
		}

		function closeDialog() {
			document.querySelector('#deleteDialog').close();
		}

		function excluir() {
			if (vm.vendedorSelecionado) {
				VendedorService.excluir(vm.vendedorSelecionado).then(
						function success(response) {
							closeDialog();
							showMessage('Vendedor excluído com sucesso!');
							vm.listar();
							delete vm.vendedorSelecionado;
						}, function error(response) {
							closeDialog();
							showMessage('Falha ao tentar excluir o vendedor!');
							delete vm.vendedorSelecionado;
						});
			}
		}
	}
})();
