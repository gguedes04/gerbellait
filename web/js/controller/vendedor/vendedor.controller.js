(function() {
	VendedorController.$inject = [ '$location', 'VendedorService',
			'$routeParams', '$timeout', 'funcoes', 'PermissaoService',
			'$filter' ];

	angular.module('appBella.sistema.controllers').controller(
			'VendedorController', VendedorController);

	function VendedorController($location, VendedorService, $routeParams,
			$timeout, funcoes, PermissaoService, $filter) {

		var vm = this;
		vm.salvar = salvar;
		vm.voltar = voltar;
		vm.mask = mask;
		vm.manterVendedor = {};
		vm.situacoes = [];
		vm.grupos = [];
		vm.unidades = [];
		vm.tiposVenda = [];
		vm.ufs = [];
		vm.rotas = [];
		vm.comissaoSelecionada = {};
		vm.rotaSelecionada = {};
		vm.salvaComissao = salvaComissao;
		vm.excluirComissao = excluirComissao;
		vm.salvaRota = salvaRota;
		vm.excluirRota = excluirRota;
		vm.comissoesRemovidas = [];
		vm.rotasRemovidas = [];

		init();

		function init() {
			validaPermissaoAcesso();
			if ($routeParams.id > 0) {
				editar($routeParams.id);
			} else {
				novo();
			}
		}

		function mask() {
		}

		function getIndexById(id) {
			var retorno = null;
			if (vm.manterVendedor.comissoes
					&& vm.manterVendedor.comissoes.length > 0) {
				for (var i = 0; i < vm.manterVendedor.comissoes.length; i++) {
					if (vm.manterVendedor.comissoes[i].id === id) {
						retorno = i;
						break;
					}
				}
			}
			return retorno;
		}

		function salvaComissao() {
			if (!vm.manterVendedor.comissoes) {
				vm.manterVendedor.comissoes = [];
			}
			if (vm.comissaoSelecionada.id) {
				vm.comissaoSelecionada.acao = 'alterar';
				var index = getIndexById(vm.comissaoSelecionada.id);
				vm.manterVendedor.comissoes[index] = vm.comissaoSelecionada;
			} else {
				vm.comissaoSelecionada.acao = 'novo';
				vm.manterVendedor.comissoes.push(angular
						.copy(vm.comissaoSelecionada));
			}
			vm.comissaoSelecionada = {};
		}

		function excluirComissao(index) {
			if (vm.manterVendedor.comissoes[index].id) {
				var comissaoRemover = angular
						.copy(vm.manterVendedor.comissoes[index]);
				comissaoRemover.acao = 'excluir';
				vm.comissoesRemovidas.push(comissaoRemover);
			}
			vm.manterVendedor.comissoes.splice(index, 1);
			showMessage('Comissão removida da lista');
		}

		function permiteAdicionarRota() {
			var retorno = true;
			for (var i = 0; i < vm.manterVendedor.rotas.length; i++) {
				if (vm.rotaSelecionada.id === vm.manterVendedor.rotas[i].rota.id) {
					retorno = false;
					break;
				}
			}
			return retorno;
		}

		function salvaRota() {
			if (!vm.manterVendedor.rotas) {
				vm.manterVendedor.rotas = [];
			}
			if (permiteAdicionarRota()) {
				var pRotaSelecionada = {};
				pRotaSelecionada.vendedor = {};
				pRotaSelecionada.vendedor.id = vm.manterVendedor.id;
				pRotaSelecionada.rota = angular.copy(vm.rotaSelecionada);
				pRotaSelecionada.acao = 'novo';
				vm.manterVendedor.rotas.push(angular.copy(pRotaSelecionada));
			} else {
				showMessage('Rota já adicionada');
			}
			vm.rotaSelecionada = {};
		}

		function excluirRota(index) {
			if (vm.manterVendedor.rotas[index].id) {
				var rotaRemover = angular.copy(vm.manterVendedor.rotas[index]);
				rotaRemover.acao = 'excluir';
				vm.rotasRemovidas.push(rotaRemover);
			}
			vm.manterVendedor.rotas.splice(index, 1);
			showMessage('Rota removida da lista');
		}

		function validaPermissaoAcesso() {
			if (!PermissaoService.containsPermissions(funcoes.vendedor)) {
				$location.path('/login');
			}
		}

		function showMessage(_messsage) {
			var snackbarContainer = document.querySelector('#message-screen');
			snackbarContainer.MaterialSnackbar.showSnackbar({
				message : _messsage
			});
		}

		function editar(id) {
			VendedorService.editar(id).then(function success(response) {
				vm.manterVendedor = response.data.vendedor;
				vm.situacoes = response.data.situacoes;
				vm.grupos = response.data.grupos;
				vm.tiposVenda = response.data.tipos;
				vm.rotas = response.data.rotas;
			}, function error(response) {
				showMessage('Falha ao carregar o vendedor!');
			});
		}

		function novo() {
			VendedorService.novo($routeParams.id).then(
					function success(response) {
						vm.manterVendedor = response.data.vendedor;
						vm.situacoes = response.data.situacoes;
						vm.grupos = response.data.grupos;
						vm.tiposVenda = response.data.tipos;
						vm.rotas = response.data.rotas;
					}, function error(response) {
						showMessage('Falha ao carregar o vendedor!');
					});
		}

		function salvar() {
			if (vm.manterVendedor.comissoes && vm.manterVendedor.rotas
					&& vm.manterVendedor.comissoes.length > 0
					&& vm.manterVendedor.rotas.length > 0) {

				// add comissões com id para remover da base
				var comissoesFull = vm.manterVendedor.comissoes
						.concat(vm.comissoesRemovidas);
				vm.manterVendedor.comissoes = comissoesFull;

				// add rotas com id para remover da base
				var rotasFull = vm.manterVendedor.rotas
						.concat(vm.rotasRemovidas);
				vm.manterVendedor.rotas = rotasFull;

				VendedorService.salvar(vm.manterVendedor).then(
						function success(response) {
							showMessage('Vendedor salvo com sucesso!');
							$timeout(function() {
								vm.voltar();
							}, 1000);
						}, function error(response) {
							showMessage('Falha ao tentar salvar o vendedor!');
						});
			} else {
				if (!(vm.manterVendedor.comissoes && vm.manterVendedor.comissoes.length > 0)) {
					showMessage('Ao menos um comissão deve ser incluída.');
				}
				if (!(vm.manterVendedor.rotas && vm.manterVendedor.rotas.length > 0)) {
					showMessage('Ao menos uma rota deve ser incluída.');
				}
			}
		}

		function voltar() {
			$location.path('/vendedores');
		}
	}
})();
