(function() {
	'use strict';

	UsuarioController.$inject = [ '$location', 'UsuarioService',
			'$routeParams', '$timeout', 'funcoes', 'PermissaoService' ];

	angular.module('appBella.sistema.controllers').controller(
			'UsuarioController', UsuarioController);

	function UsuarioController($location, UsuarioService, $routeParams,
			$timeout, funcoes, PermissaoService) {
		var vm = this;
		vm.salvar = salvar;
		vm.voltar = voltar;
		vm.manterUsuario = {};
		vm.confirmasenha = null;
		vm.situacoes = [];
		vm.perfis = [];
		vm.situacoes = [];
		vm.vendedores = [];

		init();

		function init() {
			validaPermissaoAcesso();
			if ($routeParams.id > 0) {
				editar($routeParams.id);
			} else {
				novo();
			}
		}

		function validaPermissaoAcesso() {
			if (!PermissaoService.containsPermissions(funcoes.manterUsuario)) {
				$location.path('/login');
			}
		}

		function editar(id) {
			UsuarioService.editar(id).then(
					function success(response) {
						vm.manterUsuario = response.data.usuario;
						delete vm.manterUsuario.senha;
						vm.situacoes = response.data.situacoes;
						vm.perfis = response.data.perfis;
						vm.vendedores = response.data.vendedores;
					},
					function error(response) {
						var snackbarContainer = document
								.querySelector('#message-screen');
						snackbarContainer.MaterialSnackbar.showSnackbar({
							message : 'Falha ao carregar o usuário!'
						});
					});
		}

		function novo() {
			UsuarioService.novo($routeParams.id).then(
					function success(response) {
						vm.manterUsuario = response.data.usuario;
						delete vm.manterUsuario.senha;
						vm.situacoes = response.data.situacoes;
						vm.perfis = response.data.perfis;
						vm.vendedores = response.data.vendedores;
					},
					function error(response) {
						var snackbarContainer = document
								.querySelector('#message-screen');
						snackbarContainer.MaterialSnackbar.showSnackbar({
							message : 'Falha ao carregar o usuário!'
						});
					});
		}

		function salvar() {
			UsuarioService.salvar(vm.manterUsuario).then(
					function success(response) {
						var snackbarContainer = document
								.querySelector('#message-screen');
						snackbarContainer.MaterialSnackbar.showSnackbar({
							message : 'Usuário salvo com sucesso!'
						});
						$timeout(function() {
							vm.voltar();
						}, 1000);
					},
					function error(response) {
						var snackbarContainer = document
								.querySelector('#message-screen');
						snackbarContainer.MaterialSnackbar.showSnackbar({
							message : 'Falha ao tentar salvar o usuário!'
						});
					});
		}

		function voltar() {
			$location.path('/usuarios');
		}
	}
})();
