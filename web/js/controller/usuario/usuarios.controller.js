(function() {
	'use strict';

	UsuariosController.$inject = [ '$location', 'UsuarioService', 'funcoes', 'PermissaoService' ];

	angular.module('appBella.sistema.controllers').controller(
			'UsuariosController', UsuariosController);

	function UsuariosController($location, UsuarioService, funcoes, PermissaoService) {

		var vm = this;
		vm.listar = listar;
		vm.toEdit = toEdit;
		vm.seleciona = seleciona;
		vm.excluir = excluir;
		vm.novo = novo;
		vm.openDialog = openDialog;
		vm.closeDialog = closeDialog;
		vm.manterUsuario = {};
		vm.usuarioSelecionado = {};
		vm.perfis = [];
		init();

		function init() {
			validaPermissaoAcesso();
			vm.manterUsuario.lista = [];
			vm.manterUsuario.filtro = {};
			UsuarioService.init(vm.manterUsuario.filtro).then(
					function success(response) {
						vm.manterUsuario.lista = response.data.lista;
						vm.perfis = response.data.perfis;
					},
					function error(response) {
						var snackbarContainer = document
								.querySelector('#message-screen');
						snackbarContainer.MaterialSnackbar.showSnackbar({
							message : 'Falha ao carregar os usuários!'
						});
					});
		}
		
		function validaPermissaoAcesso() {
			if (!PermissaoService.containsPermissions(funcoes.manterUsuario)) {
				$location.path('/login');
			}
		}


		function listar() {
			UsuarioService.listar(vm.manterUsuario.filtro).then(
					function success(response) {
						vm.manterUsuario.lista = response.data;
					},
					function error(response) {
						var snackbarContainer = document
								.querySelector('#message-screen');
						snackbarContainer.MaterialSnackbar.showSnackbar({
							message : 'Falha ao carregar os usuários!'
						});
					});
		}

		function novo() {
			$location.path('/usuario/novo');
		}

		function toEdit(usuario) {
			if (usuario && usuario.id) {
				$location.path('/usuario/' + usuario.id);
			}
		}

		function seleciona(usuario) {
			vm.usuarioSelecionado = usuario;
			vm.openDialog();
		}

		function openDialog() {
			document.querySelector('#deleteDialog').showModal();
		}

		function closeDialog() {
			document.querySelector('#deleteDialog').close();
		}

		function excluir() {
			if (vm.usuarioSelecionado && vm.usuarioSelecionado.id) {
				UsuarioService.excluir(vm.usuarioSelecionado).then(
						function success(response) {
							vm.listar();
							var snackbarContainer = document
									.querySelector('#message-screen');
							snackbarContainer.MaterialSnackbar.showSnackbar({
								message : 'Usuário excluído com sucesso!'
							});
						},
						function error(response) {
							var snackbarContainer = document
									.querySelector('#message-screen');
							snackbarContainer.MaterialSnackbar.showSnackbar({
								message : 'Falha ao tentar remover o usuário!'
							});
						});
			}
			vm.closeDialog();
			delete vm.usuarioSelecionado;
		}
	}
})();
