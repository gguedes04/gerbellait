(function() {
    'use strict';

    angular.module('appBella.sistema.controllers').controller('MapasCargaController', MapasCargaController);

    MapasCargaController.$inject =
        ['$location', 'MapCargaService', 'PermissaoService', 'funcoes', 'pagination', 'SessionService', 'TIMEZONE', '$scope'];

    function MapasCargaController($location, MapCargaService, PermissaoService, funcoes, pagination, SessionService, TIMEZONE, $scope) {
        const vm = this;
        init();

        function init() {
            vm.timezone = TIMEZONE;
            vm.loading = false;
            vm.filter = {};
            vm.routes = [];
            vm.selectedRoutes = [];
            vm.loadMapFullList = [];

            vm.addRoute = addRouteToList;
            vm.removeRoute = removeRouteFromList;
            vm.removeLoadMap = removeLoadMapById;
            vm.editLoadMap = editLoadMapById;
            vm.callServer = callServer;
            vm.search = () => {
                vm.tableState.pagination.start = 0;
                searchByFilter();
            };

            vm.hasRouteAdded = opt => vm.selectedRoutes.some(route => route.id === opt.id);
            _getData();

        }

        function callServer(tableState) {
            vm.tableState = tableState;
            vm.loadingOrders = true;

            const pagination = tableState.pagination;

            const start = pagination.start || 0;
            const number = pagination.number || 10;
            const result = getPage(vm.loadMapFullList, start, number);

            vm.loadMapList = result.data;
            tableState.pagination.numberOfPages = result.numberOfPages;
            vm.loadingOrders = false;
            $scope.$applyAsync();
        }

        function getPage(list, start, number) {
            const result = list.slice(start, start + number);

            return {
                data: result,
                numberOfPages: Math.ceil(list.length / number)
            };
        }

        function removeRouteFromList(route) {
            const index = vm.selectedRoutes.findIndex(_route => _route.id === route.id);
            if (index > -1) {
                vm.selectedRoutes.splice(index, 1);
            }
        }

        function addRouteToList() {
            if (vm.route) {
                vm.selectedRoutes.push(vm.route);
                delete vm.route;
            }
        }

        async function _getData() {
            try {
                vm.loading = true;
                const { data } = await MapCargaService.init();
                vm.autocompleteSearcher = data.reports && autocompleteSearcher.bind(data.reports);
                vm.routes = data.routes || [];
            } catch (e) {
                showMessage('Falha ao buscar os dados.');
            } finally {
                vm.loading = false;
            }
        }

        function showMessage(message) {
            const snackbarContainer = document.querySelector('#message-screen');
            snackbarContainer.MaterialSnackbar.showSnackbar({ message });
        }

        function autocompleteSearcher(textSearch) {
            if (textSearch) {
                if (!isNaN(textSearch)) {
                    return this.filter(item => item.id.toString().indexOf(textSearch) > -1);
                }
            } else {
                return [];
            }
        }

        async function searchByFilter() {
            if (vm.filter.dataInicial && vm.filter.dataFinal
                && moment(vm.filter.dataInicial, 'DD/MM/YYYY').isAfter(moment(vm.filter.dataFinal, 'DD/MM/YYYY'))) {
                showMessage('Data inicial deve ser menor que a final.');
                return;
            }
            vm.filter.selectedRoutes = vm.selectedRoutes.map(route => route.id);
            vm.loadingOrders = true;
            try {
                const { data } = await MapCargaService.getLoadMapByFilter(vm.filter);
                vm.loadMapFullList = data;
                $scope.$applyAsync();
                callServer(vm.tableState);
            } catch (e) {
                showMessage('Falha ao buscar os dados.');
            } finally {
                vm.loadingOrders = false;
            }
        }

        async function removeLoadMapById(loadMapId) {
            await MapCargaService.deleteLoadMap(loadMapId);
            await searchByFilter();
        }

        function editLoadMapById(loadMapId) {
            $location.path('/mapadecarga').search({ 'id': loadMapId });
        }
    }
})();
