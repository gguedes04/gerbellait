(function() {
    'use strict';

    mapaCargaController.$inject = ['$location', 'MapCargaService', 'pagination', 'TIMEZONE', '$scope'];

    angular.module('appBella.sistema.controllers').controller('MapaCargaController', mapaCargaController);

    function mapaCargaController($location, MapCargaService, pagination, TIMEZONE, $scope) {
        const vm = this;
        init();

        function init() {
            vm.timezone = TIMEZONE;
            vm.loading = false;
            vm.loadingOrders = false;
            vm.filter = {};
            vm.sellers = [];
            vm.routes = [];
            vm.linkedOrdersFullList = [];
            vm.unlinkedOrdersFullList = [];
            vm.selectedRoutes = [];
            vm.selectedSellers = [];
            vm.selectedUnlinkedOrders = [];
            vm.linkedOrders = [];
            vm.unlinkedOrders = [];
            vm.selectedOrders = [];
            vm.paginas = pagination;
            vm.page = 1;
            vm.total = 0;
            vm.loadMapId = $location.search().id;

            vm.addSeller = addSellerToList;
            vm.addRoute = addRouteToList;
            vm.removeRoute = removeRouteFromList;
            vm.removeSeller = removeSellerFromList;
            vm.search = search;
            vm.callServerOrders = callServerOrders;
            vm.callServerUnlinkedOrders = callServerUnlinkedOrders;
            vm.addOrderToList = addOrderToList;
            vm.voltar = voltar;
            vm.removeFromList = removeFromLinkedList;

            vm.filterByOrders = item => !vm.linkedOrdersFullList.some(order => order.pedido === item.pedido);

            vm.hasSellerAdded = opt => vm.selectedSellers.some(seller => seller.id === opt.id);
            vm.hasRouteAdded = opt => vm.selectedRoutes.some(seller => seller.id === opt.id);
            vm.save = async () => {
                if (vm.selectedOrders.length) {
                    await MapCargaService.save(vm.selectedOrders);
                    showMessage('Registros salvos com sucesso!');
                    search();
                }
            };

            _getData();
        }

        function voltar() {
            $location.path('/mapasdecarga');
        }


        function callServerOrders(tableState) {
            vm.tableStateOrders = tableState;
            vm.loadingOrders = true;

            const pagination = tableState.pagination;

            const start = pagination.start || 0;
            const number = pagination.number || 10;
            const result = getPage(vm.linkedOrdersFullList, start, number);

            vm.linkedOrders = result.data;
            tableState.pagination.numberOfPages = result.numberOfPages;
            vm.loadingOrders = false;
            $scope.$applyAsync();
        }

        function callServerUnlinkedOrders(tableState) {
            vm.tableStateUnlinkedOrders = tableState;
            vm.loadingOrders = true;

            const pagination = tableState.pagination;

            const start = pagination.start || 0;
            const number = pagination.number || 10;
            const result = getPage(vm.unlinkedOrdersFullList.filter(vm.filterByOrders), start, number);

            vm.unlinkedOrders = result.data;
            tableState.pagination.numberOfPages = result.numberOfPages;
            vm.loadingOrders = false;
            $scope.$applyAsync();
        }

        function getPage(list, start, number) {
            const result = list.slice(start, start + number);

            return {
                data: result,
                numberOfPages: Math.ceil(list.length / number)
            };
        }

        function addOrderToList(order, all = false) {
            if (all) {
                if (vm.checkedAllOrders) {
                    vm.unlinkedOrdersFullList.forEach(obj => {
                        const _order = obj;
                        obj.checked = true;
                        vm.selectedOrders.push(_order);
                    });
                }
            } else {
                order.checked = true;
                vm.selectedOrders.push(order);
            }
            addToLinkedList();
        }

        function addToLinkedList() {
            vm.tableStateUnlinkedOrders.pagination.start = 0;
            vm.selectedOrders.forEach(_order => {
                const contains = vm.linkedOrdersFullList.some(__order => __order.pedido === _order.pedido);
                if (!contains) {
                    vm.linkedOrdersFullList.push(_order);
                }
            });
            callServerUnlinkedOrders(vm.tableStateUnlinkedOrders);
            callServerOrders(vm.tableStateOrders);
        }

        function removeFromLinkedList(order) {
            vm.tableStateOrders.pagination.start = 0;
            vm.checkedAllOrders = false;
            order.checked = false;
            const index = vm.linkedOrders.findIndex(_order => _order.pedido === order.pedido);
            if (index > -1) {
                vm.linkedOrders.splice(index, 1);
            }
            const indexFullList = vm.linkedOrdersFullList.findIndex(_order => _order.pedido === order.pedido);
            if (indexFullList > -1) {
                vm.linkedOrdersFullList.splice(indexFullList, 1);
            }
            callServerOrders(vm.tableStateOrders);
            callServerUnlinkedOrders(vm.tableStateUnlinkedOrders);
        }


        async function search() {
            if (vm.filter.dataInicial && vm.filter.dataFinal
                && moment(vm.filter.dataInicial, 'DD/MM/YYYY').isAfter(moment(vm.filter.dataFinal, 'DD/MM/YYYY'))) {
                showMessage('Data inicial deve ser menor que a final.');
                return;
            }
            vm.filter.selectedSellers = vm.selectedSellers.map(seller => seller.id);
            vm.filter.selectedRoutes = vm.selectedRoutes.map(seller => seller.id);
            vm.filter.loadMapNumber = { id: vm.loadMapId };
            vm.loadingOrders = true;
            const { data } = await MapCargaService.getAllOrders(vm.filter);
            vm.loadingOrders = false;
            vm.linkedOrdersFullList = data.linkedOrders;
            vm.unlinkedOrdersFullList = data.unlinkedOrders;
            callServerOrders(vm.tableStateOrders);
            callServerUnlinkedOrders(vm.tableStateUnlinkedOrders);
        }

        async function _getData() {
            try {
                vm.loading = true;
                const { data } = await MapCargaService.init();
                vm.sellers = data.sellers;
                vm.routes = data.routes;
                vm.autocompleteSearcher = autocompleteSearcher.bind(data.reports);
            } catch (e) {
                showMessage('Falha ao carregar!');
            } finally {
                vm.loading = false;
            }
        }

        function showMessage(message) {
            const snackbarContainer = document.querySelector('#message-screen');
            snackbarContainer.MaterialSnackbar.showSnackbar({ message });
        }

        function addSellerToList() {
            if (vm.seller) {
                vm.selectedSellers.push(vm.seller);
                delete vm.seller;
            }
        }

        function addRouteToList() {
            if (vm.route) {
                vm.selectedRoutes.push(vm.route);
                delete vm.route;
            }
        }

        function removeSellerFromList(seller) {
            const index = vm.selectedSellers.findIndex(_seller => _seller.id === seller.id);
            if (index > -1) {
                vm.selectedSellers.splice(index, 1);
            }
        }

        function removeRouteFromList(route) {
            const index = vm.selectedRoutes.findIndex(_route => _route.id === route.id);
            if (index > -1) {
                vm.selectedRoutes.splice(index, 1);
            }
        }

        function autocompleteSearcher(textSearch) {
            if (textSearch) {
                if (!isNaN(textSearch)) {
                    return this.filter(item => item.id.toString().indexOf(textSearch) > -1);
                }
            } else {
                return [];
            }
        }
    }
})();
