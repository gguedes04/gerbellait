(function() {

    angular.module('appBella.sistema.config').config(Config);

    Config.$inject = ['$httpProvider', '$routeProvider', '$compileProvider'];

    function Config($httpProvider, $routeProvider, $compileProvider) {

        MaterialSnackbar.prototype.showSnackbar = function(data) {
            if (data === undefined) {
                throw new Error(
                    'Please provide a data object with at least a message to display.');
            }
            if (data['message'] === undefined) {
                throw new Error('Please provide a message to be displayed.');
            }
            if (data['actionHandler'] && !data['actionText']) {
                throw new Error('Please provide action text with the handler.');
            }
            if (this.active) {
                this.queuedNotifications_.push(data);
            } else {
                this.active = true;
                this.message_ = data['message'];
                if (data['timeout']) {
                    this.timeout_ = data['timeout'];
                } else {
                    this.timeout_ = 4500;
                }
                if (data['actionHandler']) {
                    this.actionHandler_ = data['actionHandler'];
                }
                if (data['actionText']) {
                    this.actionText_ = data['actionText'];
                }
                this.displaySnackbar_();
            }
        };

        $compileProvider.debugInfoEnabled(false);

        $httpProvider.interceptors
            .push([
                '$q',
                '$location',
                'SessionService',
                function($q, $location, SessionService) {
                    return {
                        request: function(config) {
                            config.headers = config.headers || {};
                            var usuario = SessionService.getUser();
                            if (usuario) {
                                config.headers.Authorization = usuario.token;
                            }
                            return config;
                        },
                        response: function(response) {

                            String.prototype.insertAt = function(index,
                                                                 string) {
                                return this.substr(0, index) + string
                                    + this.substr(index);
                            };

                            $('.mascaraHoraMinuto option')
                                .each(
                                    function() {

                                        if ($(this).text().length === 1) {
                                            $(this)
                                                .text(
                                                    '0'
                                                    + $(
                                                    this)
                                                        .text());
                                        }

                                        if ($(this).attr(
                                            'label')
                                            && $(this)
                                                .attr(
                                                    'label').length === 1) {
                                            $(this)
                                                .attr(
                                                    'label',
                                                    $(
                                                        this)
                                                        .attr(
                                                            'label')
                                                        .insertAt(
                                                            0,
                                                            '0'));
                                        }
                                    });

                            return response;
                        },
                        responseError: function(rejection) {
                            if (rejection.status === 401) {
                                SessionService.removeUser();
                                $location.path('/');
                            }
                            return $q.reject(rejection);
                        }
                    };
                }]);

        $routeProvider.when('/', {
            templateUrl: 'view/template/login.html',
            controller: 'AutenticarController',
            controllerAs: 'aut'
        }).when('/home', {
            templateUrl: 'view/home.html',
            controller: 'HomeController',
            controllerAs: 'hom',
            access: {
                requiresLogin: true,
                permissionType: 'home'
            }
        }).when('/usuarios', {
            templateUrl: 'view/usuario/usuarios.html',
            controller: 'UsuariosController',
            controllerAs: 'vm'
        }).when('/usuario/:id', {
            templateUrl: 'view/usuario/usuario.html',
            controller: 'UsuarioController',
            controllerAs: 'vm'
        }).when('/clientes', {
            templateUrl: 'view/cliente/clientes.html',
            controller: 'ClientesController',
            controllerAs: 'vm'
        }).when('/cliente/:id', {
            templateUrl: 'view/cliente/cliente.html',
            controller: 'ClienteController',
            controllerAs: 'vm'
        }).when('/produtos', {
            templateUrl: 'view/produto/produtos.html',
            controller: 'ProdutosController',
            controllerAs: 'vm'
        }).when('/produto/:id', {
            templateUrl: 'view/produto/produto.html',
            controller: 'ProdutoController',
            controllerAs: 'vm'
        }).when('/vendedores', {
            templateUrl: 'view/vendedor/vendedores.html',
            controller: 'VendedoresController',
            controllerAs: 'vm'
        }).when('/vendedor/:id', {
            templateUrl: 'view/vendedor/vendedor.html',
            controller: 'VendedorController',
            controllerAs: 'vm'
        }).when('/precovendarota', {
            templateUrl: 'view/precovenda/preco-venda-rota.html',
            controller: 'PrecoVendaRotaController',
            controllerAs: 'vm'
        }).when('/metas', {
            templateUrl: 'view/meta/periodo-meta.html',
            controller: 'PeriodoMetaController',
            controllerAs: 'vm'
        }).when('/limitesvenda', {
            templateUrl: 'view/vendas/limite-vendas.html',
            controller: 'LimiteVendasController',
            controllerAs: 'vm'
        }).when('/recadovendedor', {
            templateUrl: 'view/recadovendedor/recados-vendedores.html',
            controller: 'RecadosVendedoresController',
            controllerAs: 'vm'
        }).when('/recadovendedor/:id', {
            templateUrl: 'view/recadovendedor/recado-vendedor.html',
            controller: 'RecadoVendedorController',
            controllerAs: 'vm'
        }).when('/quantidadevenda', {
            templateUrl: 'view/meta/quantidade-venda.html',
            controller: 'QuantidadeVendaController',
            controllerAs: 'vm'
        }).when('/precomediovenda', {
            templateUrl: 'view/meta/preco-medio-venda.html',
            controller: 'PrecoMedioVendaController',
            controllerAs: 'vm'
        }).when('/descontoquantidademinima', {
            templateUrl: 'view/meta/perc-desconto-qtde-minima.html',
            controller: 'DescontoQuantidadeMinimaController',
            controllerAs: 'vm'
        }).when('/vendageralvendedor', {
            templateUrl: 'view/meta/venda-geral-vendedor.html',
            controller: 'VendaGeralVendedorController',
            controllerAs: 'vm'
        }).when('/ajusteestoque', {
            templateUrl: 'view/ajusteestoque/lista-ajuste-estoque.html',
            controller: 'ListaAjusteEstoqueController',
            controllerAs: 'vm'
        }).when('/ajusteestoque/:id', {
            templateUrl: 'view/ajusteestoque/manter-ajuste-estoque.html',
            controller: 'ManterAjusteEstoqueController',
            controllerAs: 'vm'
        }).when('/transportador', {
            templateUrl: 'view/transportador/transportadores.html',
            controller: 'TransportadoresController',
            controllerAs: 'vm'
        }).when('/transportador/:id', {
            templateUrl: 'view/transportador/transportador.html',
            controller: 'TransportadorController',
            controllerAs: 'vm'
        }).when('/pedidos', {
            templateUrl: 'view/pedido/pedidos.html',
            controller: 'PedidosController',
            controllerAs: 'vm'
        }).when('/pedido/:id', {
            templateUrl: 'view/pedido/pedido.html',
            controller: 'PedidoController',
            controllerAs: 'vm'
        }).when('/mapadecargascobranca', {
            templateUrl: 'view/relatorios/mapa-de-carga-cobranca.html',
            controller: 'MapCargaCobranca',
            controllerAs: 'vm'
        }).when('/relatorioestoqueproduto', {
            templateUrl: 'view/relatorios/relatorio-estoque-produto.html',
            controller: 'RelatorioEstoqueProdutoController',
            controllerAs: 'vm'
        }).when('/mapasdecarga', {
            templateUrl: 'view/mapas-carga/mapas-carga.view.html',
            controller: 'MapasCargaController',
            controllerAs: 'vm'
        }).when('/mapadecarga', {
            templateUrl: 'view/mapas-carga/mapa-carga.view.html',
            controller: 'MapaCargaController',
            controllerAs: 'vm'
        }).otherwise({
            redirectTo: '/'
        });
    }
})();
