(function() {

	angular.module('app.services').service('ProjectService', ProjectService);

	function ProjectService() {

		this.project = {};

		return {
			set : set,
			get : get
		}

		function set(obj) {
			this.project = obj;
		}

		function get() {
			return this.project;
		}

	}

})();
