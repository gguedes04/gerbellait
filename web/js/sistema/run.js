(function() {
	'use strict';

	function Run($rootScope, AutorizacaoService) {
		$rootScope.$on('$viewContentLoaded', function() {
			componentHandler.upgradeAllRegistered();
		});

		$rootScope.$on("$routeChangeStart", function(event, next) {
			AutorizacaoService.autorizarUsuario(next.access);
		});
	}

	Run.$inject = [ '$rootScope', 'AutorizacaoService' ];

	angular.module('appBella.sistema.run').run(Run);

})();
