angular.module('appBella').directive('pageSelect', function() {
    return {
        restrict: 'E',
        template: '<input type="text" class="mdl-textfield__input width-paginate" ng-model="inputPage" ng-change="selectPage(inputPage)">',
        link: function(scope) {
            scope.$watch('currentPage', function(c) {
                scope.inputPage = c;
            });
        }
    };
});
