package br.gerbellait.arquitetura.resources;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Validator;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import br.gerbellait.arquitetura.util.Utils;

public abstract class AbstractResource {

	@Inject
	private Validator validator;

	@Context
	private HttpServletRequest httpRequest;

	public Validator getValidator() {
		return validator;
	}

	public boolean isJSON() {
		List<String> accepts = new ArrayList<>();
		if (!Utils.isNullOrEmpty(httpRequest.getHeader("accept"))) {
			accepts.addAll(Arrays.asList(httpRequest.getHeader("accept").split(",")));
		}
		return accepts.contains(MediaType.APPLICATION_JSON);
	}

}