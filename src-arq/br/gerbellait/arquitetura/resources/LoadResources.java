package br.gerbellait.arquitetura.resources;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.inject.Inject;
import javax.servlet.ServletContext;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.DailyRollingFileAppender;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.PropertyConfigurator;

import br.gerbellait.arquitetura.ApplicationContext;
import br.gerbellait.arquitetura.util.DefaultConstants;
import br.gerbellait.arquitetura.util.Utils;

public class LoadResources {

	@Inject
	private ApplicationContext appContext;

	private Properties getPropertyFile(String fileName) {
		Properties props = new Properties();
		try {
			props.load(Thread.currentThread().getContextClassLoader()
					.getResourceAsStream(fileName));
		} catch (IOException e) {
			throw new ExceptionInInitializerError(
					"Impossível inicializar a aplicação! " + e.getMessage());
		}
		return props;
	}

	public void processEvent(ServletContext context) {
		logProperties(context);

		appProperties(context);

		sqlProperties();
	}

	private void logProperties(ServletContext context) {

		// Valida o arquivo de properties para configuração do LOG4J
		String logParam = DefaultConstants.LOG4J_BUNDLE;
		if (!Utils.isNullOrEmpty(logParam)) {
			URL url = Thread.currentThread().getContextClassLoader()
					.getResource(logParam);
			if (url != null) {
				// Precisa definir o caminho do arquivo de log
				appContext.setLog4j(this.getPropertyFile(logParam));
				// Inicializa o LOG4J definindo o arquivo de properties do mesmo
				PropertyConfigurator.configure(url);
				String filename = System.getProperty("jboss.server.log.dir")
						+ System.getProperty("file.separator")
						+ context.getInitParameter(DefaultConstants.SYSTEM)
						+ ".log";
				PatternLayout pattern = new PatternLayout(
						"%-2d{dd/MM/yyyy HH:mm:ss} %5p %m%n");
				try {
					DailyRollingFileAppender fileAppender = new DailyRollingFileAppender(
							pattern, filename, "'.'dd-MM-yyyy");
					fileAppender.setName("FILE");
					BasicConfigurator.configure(fileAppender);
				} catch (IOException e) {
					throw new ExceptionInInitializerError(
							"Impossível definir o diretório de log da aplicação.");
				}
			} else {
				throw new ExceptionInInitializerError(
						"Impossível inicializar a aplicação! Arquivo "
								+ logParam + " não na pasta de configurações.");
			}
		}

	}

	private void sqlProperties() {
		// Valida o arquivo de properties para externalização de SQL's
		String sqlParams[] = appContext.getAppProperty("sql.properties").split(
				",");

		for (String sqlParam : sqlParams) {
			if (!Utils.isNullOrEmpty(sqlParam)) {
				sqlParam = sqlParam.trim();
				URL url = Thread.currentThread().getContextClassLoader()
						.getResource(sqlParam);
				if (url != null) {
					appContext.setSql(this.getPropertyFile(sqlParam));
				} else {
					throw new ExceptionInInitializerError(
							"Impossível inicializar a aplicação! Arquivo "
									+ sqlParam
									+ " não na pasta de configurações.");
				}
			}
		}
	}

	private void appProperties(ServletContext context) {
		// Valida o arquivo de properties da aplicação
		String appParam = DefaultConstants.APPLICATION_BUNDLE;
		if (!Utils.isNullOrEmpty(appParam)) {
			URL url = Thread.currentThread().getContextClassLoader()
					.getResource(appParam);
			if (url != null) {
				// Carrega o arquivo de propriedades
				appContext.setApplication(this.getPropertyFile(appParam));
				// Adiciona o sistema como uma propriedade da aplicação
				appContext.getApplication().put(DefaultConstants.SYSTEM,
						context.getInitParameter(DefaultConstants.SYSTEM));
				// Recupera os recursos externos da aplicação
				String resources = context
						.getInitParameter(DefaultConstants.APP_RESOURCES);
				if (!Utils.isNullOrEmpty(resources)) {
					List<String> errorMessages = new ArrayList<>();
					List<String> listResources = Arrays.asList(resources
							.split(","));
					for (String resource : listResources) {
						// Retira espaços do nome do recurso
						resource = resource.trim();
						if (!Utils.isNullOrEmpty(resource)) {
							// Recupea o valor do recurso e verifica se o mesmo
							// existe
							String propValue = System.getProperty(resource);
							if (!Utils.isNullOrEmpty(propValue)) {
								appContext.getApplication().put(resource,
										propValue);
							} else {
								errorMessages.add("Propriedade " + resource
										+ " não possui valor definido.");
							}
						}
					}
					// Valida as propriedades do JBoss
					if (!errorMessages.isEmpty()) {
						StringBuilder message = new StringBuilder();
						for (String error : errorMessages) {
							message.append(error).append("\n");
						}
						throw new ExceptionInInitializerError(
								"Impossível inicializar a aplicação! \n"
										+ message.toString());
					}
				}
				ApplicationContext.PROPS = appContext.getApplication();
			} else {
				throw new ExceptionInInitializerError(
						"Impossível inicializar a aplicação! Arquivo "
								+ appParam + " não na pasta de configurações.");
			}
		}
	}

}
