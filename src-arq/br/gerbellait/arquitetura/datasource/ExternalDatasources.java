package br.gerbellait.arquitetura.datasource;

import java.sql.Connection;

import javax.inject.Inject;
import javax.sql.DataSource;

import br.gerbellait.arquitetura.interceptors.annotations.SiicoDataSource;

public final class ExternalDatasources implements Integration {

	public static final int SIICO = 1;

	@Inject
	@SiicoDataSource
	private DataSource siico;

	@Override
	public Connection getConnection(int datasource) throws Exception {

		/*
		 * Para cada banco de dados existente criar um atributo estático e
		 * adicionar uma condição na instrução abaixo, com exceção do DEFAULT
		 * que possui o valor ZERO.
		 */
		switch (datasource) {
		case SIICO:
			return this.siico.getConnection();
		default:
			throw new Exception("ERROR: Invalid datasource constant value " + datasource);
		}

	}

}