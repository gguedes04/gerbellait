package br.gerbellait.arquitetura.datasource;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public interface Transaction {

	public Connection getConnection();

	public void setConnection(Connection connection);

	public void setConnection(Connection connection, boolean external);

	public PreparedStatementWrapper prepareStatement(StringBuilder sql) throws SQLException;

	public PreparedStatementWrapper prepareInsertStatement(StringBuilder sql) throws SQLException;

	public Statement statement(StringBuilder sql) throws SQLException;

	public void beginTransaction() throws SQLException;

	public boolean isClosed() throws SQLException;

	public boolean isExternalClosed() throws SQLException;

	public void closeExternal() throws SQLException;

	public void close() throws SQLException;

	public void commit() throws SQLException;

	public void rollback() throws SQLException;

	public boolean isExternal();

}