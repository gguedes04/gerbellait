package br.gerbellait.arquitetura.datasource;

import java.sql.Connection;

public interface Integration {

	/**
	 * Recupera uma conexão para o datasource informado.
	 * @param datasource | Informe o valor que representa o datasource desejado.
	 * @return Conexão com o banco de dados
	 * @throws Exception
	 */
	public Connection getConnection(int datasource) throws Exception;
	
}