package br.gerbellait.arquitetura.entities;

import java.io.Serializable;

public class AbstractEntity implements Cloneable, Serializable {

    private static final long serialVersionUID = 1L;
    
    private Long id;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        return id != null ? id.toString() : null;
    }

    @Override
    public int hashCode() {
        return id.intValue();
    }

    @Override
    public boolean equals(Object id) {

        if (this.id == null)
            return false;

        if (!(id instanceof Long))
            return false;

        return this.id.equals((Long) id);
    }

    @Override
    public AbstractEntity clone() {
        AbstractEntity abstractEntity = null;
        try {
            abstractEntity = (AbstractEntity) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return abstractEntity;
    }

}
