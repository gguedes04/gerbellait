package br.gerbellait.arquitetura.service;

import java.lang.reflect.Field;
import java.util.Collection;

import javax.inject.Inject;

import br.gerbellait.arquitetura.ApplicationContext;
import br.gerbellait.arquitetura.entities.AbstractEntity;
import br.gerbellait.arquitetura.exceptions.ServiceLayerException;

public abstract class AbstractService<Entity extends AbstractEntity> {

	@Inject
	private ApplicationContext context;

	protected String getProperty(String key) {
		return this.context.getAppProperty(key);
	}

	protected String getMessage(String key) {
		return this.context.getMessage(key);
	}

	protected String getMessage(String key, Object... parameters) {
		return this.context.getMessage(key, parameters);
	}

	protected String getLabel(String key) {
		return this.context.getLabel(key);
	}

	protected ApplicationContext getContext() {
		return this.context;
	}

	protected abstract void validate(Entity t) throws ServiceLayerException;

	/**
	 * use este metodo caso exista um metodo get(Entity t) no seu Service que faça
	 * um load do teu objeto. Isto porque o seu metodo get sera responsavel por
	 * carregar os filhos do tipo List no teu objeto principal
	 * 
	 */
	@SuppressWarnings("unchecked")
	public <DAO> Entity salvarDefaultGet(Entity t, DAO dao) throws ServiceLayerException {

		validate(t);

		try {
			if (t.getId() == null) {
				return (Entity) dao.getClass().getDeclaredMethod("insert", t.getClass().getSuperclass()).invoke(dao, t);
			} else {
				Entity clone = (Entity) t.clone();
				clone = (Entity) this.getClass().getDeclaredMethod("get", clone.getClass()).invoke(this, clone);

				t = (Entity) dao.getClass().getDeclaredMethod("update", t.getClass().getSuperclass()).invoke(dao, t);

				Object[] entities = diff(clone, t);
				dao.getClass().getDeclaredMethod("diff", t.getClass().getSuperclass(), t.getClass().getSuperclass())
						.invoke(dao, entities[0], entities[1]);
				return t;

			}

		} catch (Exception e) {
			throw new ServiceLayerException(e.getMessage());
		}

	}

	/**
	 * use este metodo quando for salvar um objeto principal.
	 * 
	 */
	@SuppressWarnings("unchecked")
	public <DAO> Entity salvar(Entity t, DAO dao) throws ServiceLayerException {
		validate(t);
		try {
			if (t.getId() == null) {
				t = (Entity) dao.getClass().getDeclaredMethod("insert", t.getClass().getSuperclass()).invoke(dao, t);
			} else {
				t = (Entity) dao.getClass().getDeclaredMethod("update", t.getClass().getSuperclass()).invoke(dao, t);
			}
			return t;
		} catch (Exception e) {
			throw new ServiceLayerException(e.getMessage());
		}
	}

	/**
	 * Use este metodo quando tiver que fazer auditoria para filhos do tipo List de
	 * um objeto principal.
	 * 
	 */
	@SuppressWarnings("unchecked")
	public <DAO> Entity salvarAlterar(Entity tAntes, Entity tDepois, DAO dao) throws ServiceLayerException {
		try {
			tDepois = (Entity) dao.getClass().getDeclaredMethod("update", tDepois.getClass().getSuperclass())
					.invoke(dao, tDepois);
			Object[] entities = diff(tAntes, tDepois);
			dao.getClass()
					.getDeclaredMethod("diff", tAntes.getClass().getSuperclass(), tAntes.getClass().getSuperclass())
					.invoke(dao, entities[0], entities[1]);
			return tDepois;

		} catch (Exception e) {
			throw new ServiceLayerException(e.getMessage());
		}
	}

	/**
	 * O mesmo que o salvar, porem este sem validar o metodo validate();
	 * 
	 */
	@SuppressWarnings("unchecked")
	public <DAO> Entity salvarNoValidate(Entity t, DAO dao) throws ServiceLayerException {

		try {
			if (t.getId() == null) {
				return (Entity) dao.getClass().getDeclaredMethod("insert", t.getClass().getSuperclass()).invoke(dao, t);
			} else {
				return (Entity) dao.getClass().getDeclaredMethod("update", t.getClass().getSuperclass()).invoke(dao, t);

			}

		} catch (Exception e) {
			throw new ServiceLayerException(e.getMessage());
		}

	}

	/**
	 * Caso nao tenha que se preocupar com filhos do tipo List para este objeto, use
	 * este metodo passando por parametro apenas o id do objeto.
	 * 
	 */
	@SuppressWarnings("unchecked")
	public <DAO> void delete(Entity t, DAO dao) throws Exception {
		t = (Entity) dao.getClass().getDeclaredMethod("getById", t.getClass().getSuperclass()).invoke(dao, t);
		dao.getClass().getDeclaredMethod("delete", t.getClass().getSuperclass()).invoke(dao, t);
	}

	/**
	 * use este metodo caso tenha desenvolvido um metodo get(Entity t) em seu
	 * service que faça o loading dos objetos filhos do tipo List.
	 */
	@SuppressWarnings("unchecked")
	public <DAO> void deleteDefaultGet(Entity t, DAO dao) throws ServiceLayerException {

		try {
			t = (Entity) this.getClass().getDeclaredMethod("get", t.getClass()).invoke(this, t);
			dao.getClass().getDeclaredMethod("delete", t.getClass().getSuperclass()).invoke(dao, t);
		} catch (Exception e) {
			throw new ServiceLayerException(e.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	private Object[] diff(Entity antes, Entity depois) throws Exception {

		Class<Entity> tAntesClass = (Class<Entity>) antes.getClass();
		Class<Entity> tDepoisClass = (Class<Entity>) depois.getClass();

		Entity tDiffAntes = tAntesClass.newInstance();
		Entity tDiffDepois = tDepoisClass.newInstance();
		Class<Entity> tDiffClass = (Class<Entity>) tDiffAntes.getClass();

		for (Field fieldAntes : tAntesClass.getDeclaredFields()) {
			for (Field fieldDepois : tDepoisClass.getDeclaredFields()) {
				try {
					fieldAntes.setAccessible(true);
					fieldDepois.setAccessible(true);

					if (!fieldAntes.getName().equals(fieldDepois.getName()))
						continue;

					Object valueAntes = fieldAntes.get(antes);
					Object valueDepois = fieldDepois.get(depois);

					if (valueAntes instanceof Collection<?> || valueDepois instanceof Collection<?>) {

						Collection<? extends AbstractEntity> listAntes = (Collection<? extends AbstractEntity>) valueAntes;
						Collection<? extends AbstractEntity> listDepois = (Collection<? extends AbstractEntity>) valueDepois;

						if (listAntes == null && listDepois == null || listAntes != null && listAntes.isEmpty()
								&& listDepois != null && listDepois.isEmpty())
							break;

					} else if (valueAntes instanceof AbstractEntity || valueDepois instanceof AbstractEntity) {
						AbstractEntity entityAntes = (AbstractEntity) valueAntes;
						AbstractEntity entityDepois = (AbstractEntity) valueDepois;

						if (entityAntes == null && entityDepois == null)
							break;

						if (entityAntes != null && entityDepois != null) {
							if ((entityAntes.getId() != null && entityDepois.getId() != null)) {
								if (entityAntes.getId() == null && entityDepois.getId() == null
										|| entityAntes.getId().equals(entityDepois.getId()))
									break;
							}

						}

					} else if ((valueAntes != null && valueDepois != null && valueAntes.equals(valueDepois))
							|| (valueAntes == null && valueDepois == null)) {
						break;
					}

					Field fieldDiffAntes = tDiffClass.getDeclaredField(fieldAntes.getName());
					Field fieldDiffDepois = tDiffClass.getDeclaredField(fieldDepois.getName());

					fieldDiffAntes.setAccessible(true);
					fieldDiffDepois.setAccessible(true);

					if (valueAntes != null && valueDepois == null && valueAntes instanceof AbstractEntity)
						valueDepois = valueAntes.getClass().newInstance();
					else if (valueDepois != null && valueAntes == null && valueDepois instanceof AbstractEntity)
						valueAntes = valueDepois.getClass().newInstance();

					fieldDiffAntes.set(tDiffAntes, valueAntes);
					fieldDiffDepois.set(tDiffDepois, valueDepois);

					break;

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}

		return new Object[] { tDiffAntes, tDiffDepois };

	}
}