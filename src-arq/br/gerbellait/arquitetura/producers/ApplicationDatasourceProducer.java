package br.gerbellait.arquitetura.producers;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.h2.jdbcx.JdbcConnectionPool;

import br.gerbellait.arquitetura.ApplicationContext;
import br.gerbellait.arquitetura.interceptors.annotations.SiicoDataSource;
import br.gerbellait.arquitetura.util.DefaultConstants;

public class ApplicationDatasourceProducer {

	@Inject
	private ApplicationContext context;

	@Produces
	@RequestScoped
	@Default
	public DataSource getAppDataSource() throws NamingException {
		String test = context.getAppProperty(DefaultConstants.TEST_MODE);
		if (Boolean.parseBoolean(test)) {
			return JdbcConnectionPool.create(
					"jdbc:h2:mem:test;DB_CLOSE_DELAY=-1", "user", "password");
		} else {
			String jndi = context.getAppProperty(DefaultConstants.SYSTEM)
					.toLowerCase()
					+ "."
					+ DefaultConstants.APPLICATION_JNDI_NAME;
			return (DataSource) new InitialContext().lookup(context
					.getAppProperty(jndi));
		}
	}

	@Produces
	@RequestScoped
	@SiicoDataSource
	public DataSource getSiicoDataSource() throws NamingException {
		InitialContext ic = new InitialContext();
		String jndi = context.getAppProperty(DefaultConstants.SYSTEM)
				.toLowerCase() + "." + DefaultConstants.APPLICATION_JNDI_SIICO;
		return (DataSource) ic.lookup(context.getAppProperty(jndi));
	}

}