package br.gerbellait.arquitetura;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.inject.Singleton;

import br.gerbellait.arquitetura.util.DefaultConstants;

@Singleton
public class ApplicationContext implements Serializable {
	
    private static final long serialVersionUID = 1L;
    
    public static Properties PROPS;
    
	private ResourceBundle messages = ResourceBundle.getBundle(DefaultConstants.MESSAGE_BUNDLE);

	// Arquivos de property inicializados na classe SystemInitConfiguration
	private Properties application;
	private List<Properties> sql = new ArrayList<Properties>();
	private Properties log4j;
	
	/**
	 * Recupera o valor para a chave informada do arquivo de configurações da aplicação
	 * @param Chave para recuperar o valor
	 * @return Valor da chave informada
	 */
	public String getAppProperty(String key) {
		return (String) this.application.get(key);
	}

	/**
	 * Recupera o valor para a chave informada do arquivo de SQL externalizado
	 * @param Chave para recuperar o valor
	 * @return Valor da chave informada
	 */
	public String getSqlProperty(String key) {
	    
	    for (Properties prop : this.sql) {
	        if (prop.containsKey(key))
	            return (String) prop.get(key);
	    }
	    return null;
	}

	/**
	 * Recupera o valor para a chave informada do arquivo de configurações do LOG da aplicação (Log4J)
	 * @param Chave para recuperar o valor
	 * @return Valor da chave informada
	 */
	public String getLog4jProperty(String key) {
		return (String) this.log4j.get(key);
	}
	
	/**
	 * Recupera o valor para a chave informada do arquivo de mensagens da aplicação
	 * @param Chave para recuperar o valor
	 * @return Valor da chave informada
	 */
	public String getMessage(String key) {
	    try {
	    	return this.messages.getString(key);
	    	//return new String(message.getBytes("ISO-8859-1"), "UTF-8");
	    } catch (Exception e) {
	    	return null;
		}
	}
	
	
	/**
	 * Recupera o valor para a chave informada do arquivo de mensagens da aplicação
	 * @param Chave para recuperar o valor
	 * @param Lista de parâmetros para substituição na mensagens
	 * @return Valor da chave informada
	 */
	public String getMessage(String key, Object... parameters) {
		String message = this.getMessage(key);
		MessageFormat formatter = new MessageFormat(message);
		return formatter.format(parameters);
	}	
	
	/**
	 * Recupera o valor para a chave informada do arquivo de labels da aplicação
	 * @param Chave para recuperar o valor
	 * @param Lista de parâmetros para substituição na mensagens
	 * @return Valor da chave informada
	 */
	public String getLabel(String key, Object... parameters) {
		String message = this.getLabel(key);
		MessageFormat formatter = new MessageFormat(message);
		return formatter.format(parameters);
	}

	public Properties getApplication() {
		return application;
	}

	public void setApplication(Properties application) {
		this.application = application;
	}

	public void setSql(Properties sql) {
		this.sql.add(sql);
	}

	public Properties getLog4j() {
		return log4j;
	}

	public void setLog4j(Properties log4j) {
		this.log4j = log4j;
	}

}