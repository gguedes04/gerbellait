package br.gerbellait.arquitetura.util;

import br.gerbellait.sistema.entity.Usuario;
import br.gerbellait.sistema.util.Constants;

public class PerfilUsuario {

	private PerfilUsuario() {
		throw new IllegalStateException("PerfilUsuario class");
	}

	public static boolean isAdministrador(Usuario usuario) {
		return usuario.getPerfil() != null && usuario.getPerfil().getId() != null
				? usuario.getPerfil().getId() == Constants.ID_PERFIL_ADMINISTRADOR
				: false;
	}

	public static boolean isCadastro(Usuario usuario) {
		return usuario.getPerfil() != null && usuario.getPerfil().getId() != null
				? usuario.getPerfil().getId() == Constants.ID_PERFIL_CADASTRO
				: false;
	}

	public static boolean isVendedor(Usuario usuario) {
		return usuario.getPerfil() != null && usuario.getPerfil().getId() != null
				? usuario.getPerfil().getId() == Constants.ID_PERFIL_VENDEDOR
				: false;
	}
}