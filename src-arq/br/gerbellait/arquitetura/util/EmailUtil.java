package br.gerbellait.arquitetura.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.inject.Singleton;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

import br.gerbellait.arquitetura.ApplicationContext;
import br.gerbellait.arquitetura.exceptions.ServiceLayerException;

@Singleton
public class EmailUtil {

	private static final Logger LOGGER = Logger.getLogger(EmailUtil.class);

	public EmailUtil() {
		// empty
	}

	public void sendMail(ApplicationContext context, String to, String subject, StringBuilder message)
			throws ServiceLayerException {

		final String jndi = context.getAppProperty(DefaultConstants.SYSTEM).toLowerCase() + "."
				+ DefaultConstants.APPLICATION_JNDI_MAIL;

		final String from = context.getAppProperty(DefaultConstants.SYSTEM).toLowerCase() + ".mail.from";

		try {
			List<InternetAddress> adress = new ArrayList<>();
			if (!Utils.isNullOrEmpty(to) && isEmail(to)) {
				adress.add(new InternetAddress(to.trim()));
			}

			Session mailSession = (Session) new InitialContext().lookup(context.getAppProperty(jndi));
			MimeMessage m = new MimeMessage(mailSession);
			m.setFrom(new InternetAddress(context.getAppProperty(from).trim()));
			m.setSentDate(new java.util.Date());
			m.setSubject(subject);
			m.setContent(message.toString(), "text/html; charset=utf-8");
			m.setRecipients(Message.RecipientType.TO, adress.toArray(new InternetAddress[0]));

			Transport.send(m);
		} catch (MessagingException | NamingException e) {
			LOGGER.error(e);
			throw new ServiceLayerException(e);
		}
	}

	public void sendMail(ApplicationContext context, String[] to, String subject, StringBuilder message)
			throws ServiceLayerException {

		final String jndi = context.getAppProperty(DefaultConstants.SYSTEM).toLowerCase() + "."
				+ DefaultConstants.APPLICATION_JNDI_MAIL;

		final String from = context.getAppProperty(DefaultConstants.SYSTEM).toLowerCase() + ".mail.from";

		try {
			List<InternetAddress> adress = new ArrayList<>();
			for (String s : to) {
				if (!Utils.isNullOrEmpty(s) && isEmail(s)) {
					adress.add(new InternetAddress(s.trim()));
				}
			}

			Session mailSession = (Session) new InitialContext().lookup(context.getAppProperty(jndi));
			MimeMessage m = new MimeMessage(mailSession);
			m.setFrom(new InternetAddress(context.getAppProperty(from).trim()));
			m.setSentDate(new java.util.Date());
			m.setSubject(subject);
			m.setContent(message.toString(), "text/html; charset=utf-8");
			m.setRecipients(Message.RecipientType.TO, adress.toArray(new InternetAddress[0]));

			Transport.send(m);
		} catch (MessagingException | NamingException e) {
			LOGGER.error(e);
			throw new ServiceLayerException(e);
		}
	}

	public boolean sendMail(ApplicationContext context, String subject, StringBuilder content, String to)
			throws ServiceLayerException {

		if (!Utils.isNullOrEmpty(to) && isEmail(to)) {

			try {

				final String username = context.getAppProperty(DefaultConstants.MAIL_USERNAME).toLowerCase();
				final String password = context.getAppProperty(DefaultConstants.MAIL_PASSWORD).toLowerCase();
				final String from = context.getAppProperty(DefaultConstants.MAIL_FROM).toLowerCase();
				final String host = context.getAppProperty(DefaultConstants.MAIL_HOST).toLowerCase();

				Properties props = new Properties();
				props.put("mail.smtp.auth", "true");
				props.put("mail.smtp.starttls.enable", "true");
				props.put("mail.smtp.host", host);
				props.put("mail.smtp.ssl.trust", host);
				props.put("mail.smtp.port", context.getAppProperty(DefaultConstants.MAIL_SMTP_PORT));

				Session session = Session.getInstance(props, new javax.mail.Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				});

				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(from));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
				message.setSubject(subject);
				message.setContent(content.toString(), "text/html; charset=utf-8");
				Transport.send(message);

				return true;

			} catch (MessagingException e) {
				LOGGER.error(e);
				throw new ServiceLayerException(e);
			}
		} else {
			LOGGER.error("E-mail " + to + " inválido!");
		}
		return false;
	}

	private boolean isEmail(String s) {
		return s.matches("^.+@.+$");
	}
}
