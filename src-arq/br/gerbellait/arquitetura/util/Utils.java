package br.gerbellait.arquitetura.util;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

import br.gerbellait.arquitetura.entities.AbstractEntity;
import br.gerbellait.sistema.dto.SituacaoDto;

public class Utils {

	private static Logger logger;

	private Utils() {
		throw new IllegalStateException("Utils class");
	}

	/**
	 * Retira acentuação de qualquer string
	 * 
	 * @param Valor
	 *            com acentuação
	 * @return Valor sem acentuação
	 */
	public static String normalize(String str) {
		String result = "";
		if (str != null && !str.isEmpty()) {
			result = Normalizer.normalize(str, Normalizer.Form.NFD);
			result = result.replaceAll("[^\\p{ASCII}]", "");
		}
		return result;
	}

	/**
	 * Verifica se a string informada é nula ou vazia.z
	 * 
	 * @param String
	 * @return true | false
	 */
	public static boolean isNullOrEmpty(String str) {
		return !(str instanceof String) || str.trim().isEmpty();
	}

	public static boolean isNullOrEmptyOrUndefined(String str) {
		return !(str instanceof String) || str.trim().isEmpty() || str.trim().equals("undefined");
	}

	/**
	 * Verifica se o id do objeto é diferente de zero.
	 * 
	 * @param e
	 * @return
	 */
	public static boolean isNullOrZero(AbstractEntity e) {
		return e == null || e.getId() == null || e.getId().equals(0L);
	}

	/**
	 * Verifica se a lista informada é nula ou vazia.
	 * 
	 * @param List
	 *            <?>
	 * @return true | false
	 */
	public static boolean isNullOrEmpty(List<?> list) {
		return !(list instanceof List) || list.isEmpty();
	}

	/**
	 * Recover service implementation from service interface and JNDI name
	 * 
	 * @param name
	 * @param type
	 * @return
	 */
	public static <T> T getServiceInstance(String name, Class<T> type) {
		try {
			InitialContext initialContext = new InitialContext();
			return type.cast(initialContext.lookup(name));
		} catch (NamingException e) {
			logger = Logger.getLogger(type);
			logger.warn(e);
		}
		return null;
	}

	/**
	 * Recover service implementation from service interface
	 * 
	 * @param Interface
	 *            class
	 * @return Class implementation
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getServiceInstance(final Class<T> type) {
		BeanManager beanManager = null;
		try {
			InitialContext initialContext = new InitialContext();
			beanManager = (BeanManager) initialContext.lookup("java:comp/BeanManager");
			Bean<T> bean = (Bean<T>) beanManager.getBeans(type).iterator().next();
			CreationalContext<T> ctx = beanManager.createCreationalContext(bean);
			return (T) beanManager.getReference(bean, type, ctx);
		} catch (Exception e) {
			// Se não encontrar não levanta exceção, apenas loga como warning
			logger = Logger.getLogger(type);
			logger.warn("Não foi encontrada implementação para a interface de autorização " + type.getName());
		}
		return null;
	}

	/**
	 * Transforma a primeira letra de um texto em minuscula
	 * 
	 * @param Texto
	 *            a transformar
	 * @return Texto transformado
	 */
	public static String lowerCaseFirstLetter(String texto) {
		return texto.substring(0, 1).toLowerCase() + texto.substring(1);
	}

	/**
	 * Adiciona máscara ao valor informado
	 * 
	 * @param Formato
	 *            de máscara
	 * @param Valor
	 * @param Caracter
	 *            representado pela máscara
	 * @return Valor formatado
	 */
	public static String format(String format, String value, char c) {
		if (format == null || format.trim().equals(""))
			return null;
		int index = 0;
		StringBuilder s = new StringBuilder();
		char charsFormat[] = format.toCharArray();
		char charsValue[] = value.toCharArray();
		char arr$[] = charsFormat;
		int len$ = arr$.length;
		for (int i$ = 0; i$ < len$; i$++) {
			char element = arr$[i$];
			if (element != c)
				s.append(element);
			else
				s.append(charsValue[index++]);
		}

		return s.toString();
	}

	/**
	 * Formata o valor informado de acordo com a máscara
	 * 
	 * @param Formato
	 *            definido com o uso de #
	 * @param Valor
	 *            a formatar
	 * @return Valor formatado
	 */
	public static String format(String format, String value) {
		return format(format, value, '#');
	}

	/**
	 * Remove a formatação do campo
	 * 
	 * @param Format
	 *            definido com o uso de #
	 * @param Valor
	 *            formatado
	 * @return Valor sem formatação
	 */
	public static String unformat(final String format, final String value) {
		return Utils.unformat(format, value, '#');
	}

	/**
	 * Remove a formatação do campo
	 * 
	 * @param Format
	 *            definido com o uso de #
	 * @param Valor
	 *            formatado
	 * @param Caracter
	 *            representado pela máscara
	 * @return Valor sem formatação
	 */
	public static String unformat(final String format, final String value, final char c) {

		if (format == null || format.trim().equals(""))
			return null;

		if (value == null || value.trim().equals(""))
			return null;

		StringBuilder s = new StringBuilder();

		char[] charsFormat = format.toCharArray();
		char[] charsValue = value.toCharArray();

		for (int i = 0; i < charsFormat.length; i++) {
			if (charsFormat[i] == c) {
				s.append(charsValue[i]);
			}
		}

		return s.toString();
	}

	/**
	 * Remove formato de máscara de campos CPF, CNPJ, Telefone, CEP Remove qualquer
	 * máscara que contenha: . / -
	 * 
	 * @param Valor
	 *            a ser removida máscara
	 * @return Valor sem máscara
	 */
	public static String unformat(final String value) {
		return value.replace(".", "").replace("/", "").replace("-", "");
	}

	/**
	 * Recupera o primeiro dia da data informada ou data atual
	 * 
	 * @param Data
	 *            (se nulo, define a data corrente)
	 * @return Data com primeiro dia definido
	 */
	public static Date getFirstDayOfDate(Date date) {
		Calendar calendar = Calendar.getInstance(new Locale("pt", "BR"));
		if (date != null) {
			calendar.setTime(date);
		}
		calendar.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().getActualMinimum(Calendar.DAY_OF_MONTH));
		return calendar.getTime();
	}

	/**
	 * Recupera o último dia da data informada ou data atual
	 * 
	 * @param Data
	 *            (se nulo, define a data corrente)
	 * @return Data com último dia definido
	 */
	public static Date getLastDayOfDate(Date date) {
		Calendar calendar = Calendar.getInstance(new Locale("pt", "BR"));
		if (date != null) {
			calendar.setTime(date);
		}
		calendar.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH));
		return calendar.getTime();
	}

	/**
	 * convert \\mypc\\sgare\text.txt to file://mypc//sgare/text.txt
	 * 
	 * @param string
	 * @return
	 */
	public static String unc(String link) {
		if (!link.startsWith("\\"))
			return link;

		return "file://" + link.substring(2).replace('\\', '/');
	}

	public static List<SituacaoDto> getSituacoes() {
		List<SituacaoDto> retorno = new ArrayList<>();
		retorno.add(new SituacaoDto(1, "Ativo"));
		retorno.add(new SituacaoDto(0, "Desativado"));
		return retorno;
	}

	public static String onlyCharacters(String value) {
		if (Utils.isNullOrEmpty(value)) {
			return null;
		}
		return value.replaceAll("\\d", "");
	}

	public static String onlyNumbers(String value) {
		if (Utils.isNullOrEmpty(value)) {
			return null;
		}
		return value.replaceAll("[^\\d]", "");
	}
}