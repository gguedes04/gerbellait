package br.gerbellait.arquitetura.util;

public class StringUtils {
    
    public static boolean isNullOrEmpty(String value) {
        return value == null || "".equals(value);
    }
}
