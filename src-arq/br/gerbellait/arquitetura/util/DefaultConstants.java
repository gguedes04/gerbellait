package br.gerbellait.arquitetura.util;

public abstract class DefaultConstants {

	public static final String SYSTEM = "sistema";
	public static final String APP_RESOURCES = "resources";
	public static final String APPLICATION_BUNDLE = "conf/app.properties";
	public static final String LOG4J_BUNDLE = "conf/log4j.properties";

	public static final String MESSAGE_BUNDLE = "conf.messages";
	public static final String TEST_MODE = "test.mode";

	public static final String APPLICATION_JNDI_NAME = "jndi";
	public static final String APPLICATION_JNDI_SIICO = "siico.jndi";
	public static final String APPLICATION_JNDI_MAIL = "mail.jndi";

	public static final String MAIL_HOST = "mail.host";
	public static final String MAIL_FROM = "mail.from";
	public static final String MAIL_SMTP_PORT = "mail.smtp.port";
	public static final String MAIL_USERNAME = "mail.username";
	public static final String MAIL_PASSWORD = "mail.password";

}