package br.gerbellait.arquitetura.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

import com.yahoo.platform.yui.compressor.YUICompressor;

public class BuildAutomation {

	private enum Extensions {
		JS, CSS
	}

	/**
	 * Nome dos arquivos unificados (tanto CSS quanto JS)
	 */
	private static final String MERGED_FILENAME = "gerbellait";

	/**
	 * Recupera a lista de arquivos para minificar
	 * 
	 * @param Diretório
	 *            raiz para pesquisa
	 * @param Extensão
	 *            para identificação dos arquivos
	 * @return Lista de arquivos
	 * @throws IOException
	 */
	private static List<Path> getFiles(final String path, final String extension)
			throws IOException {
		final List<Path> files = new ArrayList<Path>();
		Files.walkFileTree(Paths.get(path), new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult visitFile(Path file,
					BasicFileAttributes attrs) throws IOException {
				if (file.getFileName().toString().endsWith(extension)) {
					files.add(file);
				}
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult preVisitDirectory(Path dir,
					BasicFileAttributes attrs) throws IOException {
				if ("lib".equals(dir.getFileName().toString())
						|| "font".equals(dir.getFileName().toString())) {
					return FileVisitResult.SKIP_SUBTREE;
				}
				return FileVisitResult.CONTINUE;
			}
		});
		return files;
	}

	public static void main(String[] args) throws IOException {

		// Valida o arqumento esperado pelo programa
		if (args == null || args.length < 1) {
			throw new IllegalArgumentException(
					"Basedir do script ANT não informado.");
		}

		// Monta o caminho para recuperar arquivos para serem minificados
		String webProjectPath = args[0] + File.separator + "web";

		// Minificar javascript
		minifier(webProjectPath, Extensions.JS);

		// Minificar css
		//minifier(webProjectPath, Extensions.CSS);

	}

	private static void minifier(String path, Extensions extension) {
		String ext = extension.toString().toLowerCase();

		// Valida a existencia dos arquivos
		File merged = new File(path + File.separator + ext + File.separator
				+ "lib" + File.separator + MERGED_FILENAME + "." + ext);
		if (merged.exists()) {
			merged.delete();
		}
		File minified = new File(merged.getParent() + File.separator
				+ MERGED_FILENAME + ".min." + ext);
		if (minified.exists()) {
			minified.delete();
		}

		// Elenca os arquivos que serão minificados e descarta alguns
		String jsPath = path + File.separator + ext;
		List<Path> moduleFiles = new ArrayList<Path>();
		List<Path> sistemaFiles = new ArrayList<Path>();
		List<Path> normalFiles = new ArrayList<Path>();
		try {
			for (Path file : getFiles(jsPath, "." + ext)) {
				// Ignora o arquivo que esta sendo criado
				if (file.toFile().getName().equals(merged.getName())
						|| file.toFile().getName().equals("app.js")
						|| file.toFile().getName().equals("bellaModule.js")) {
					continue;
				}
				System.out.println(file.toFile().getName());
				// Se o arquivo for um módulo Angular
				if (file.toFile().getName().toLowerCase().contains("module")) {
					moduleFiles.add(file);
				} else if (file.toFile().getPath().toLowerCase()
						.contains("js/sistema")
						|| file.toFile().getPath().toLowerCase()
								.contains("js/utils")) {
					sistemaFiles.add(file);
				} else {
					normalFiles.add(file);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Realizar o processo de merge dos arquivos com a extensão informada
		try (FileWriter fstream = new FileWriter(merged, true);
				BufferedWriter out = new BufferedWriter(fstream)) {
			// Minifica os módulos
			for (Path file : moduleFiles) {
				try (FileInputStream fis = new FileInputStream(file.toFile());
						BufferedReader in = new BufferedReader(
								new InputStreamReader(fis))) {
					String aLine = null;
					while ((aLine = in.readLine()) != null) {
						out.write(aLine);
						out.newLine();
					}
				}
				out.flush();
			}
			// Minifica os arquivos de sistema
			for (Path file : sistemaFiles) {
				try (FileInputStream fis = new FileInputStream(file.toFile());
						BufferedReader in = new BufferedReader(
								new InputStreamReader(fis))) {
					String aLine = null;
					while ((aLine = in.readLine()) != null) {
						out.write(aLine);
						out.newLine();
					}
				}
				out.flush();
			}
			// Minifica os demais arquivos
			for (Path file : normalFiles) {
				try (FileInputStream fis = new FileInputStream(file.toFile());
						BufferedReader in = new BufferedReader(
								new InputStreamReader(fis))) {
					String aLine = null;
					while ((aLine = in.readLine()) != null) {
						out.write(aLine);
						out.newLine();
					}
				}
				out.flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Minifica o arquivo
		YUICompressor.main(new String[] { merged.getAbsolutePath(), "-o",
				minified.getAbsolutePath() });

		// Excluir arquivo unificado
		merged.delete();

	}

}