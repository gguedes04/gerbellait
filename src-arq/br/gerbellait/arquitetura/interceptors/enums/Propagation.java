package br.gerbellait.arquitetura.interceptors.enums;

public enum Propagation {

	REQUIRES_NEW, NESTED;
	
}