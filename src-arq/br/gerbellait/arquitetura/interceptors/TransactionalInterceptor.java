package br.gerbellait.arquitetura.interceptors;

import java.io.Serializable;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import br.gerbellait.arquitetura.datasource.Integration;
import br.gerbellait.arquitetura.datasource.Transaction;
import br.gerbellait.arquitetura.interceptors.annotations.Transactional;
import br.gerbellait.arquitetura.interceptors.enums.Propagation;

@Interceptor
@Transactional
public class TransactionalInterceptor implements Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger log = Logger
			.getLogger(TransactionalInterceptor.class);

	@Inject
	private Transaction transaction;

	@Inject
	private DataSource local;

	@Inject
	private Integration external;

	@AroundInvoke
	public Object manageTransaction(InvocationContext ctx) throws Exception {

		// Busca no método (quando a anotação esta no método)
		Transactional trx = ctx.getMethod().getAnnotation(Transactional.class);
		if (trx == null) {
			throw new Exception(
					"Transactional annotation can not be used in a class context. Just use it in a method context.");
		}

		// Mantem a mesma transação
		if (!trx.propagation().equals(Propagation.NESTED)) {
			if (trx.transactionManager() != 0) {

				// Fecha a conexão quando na mesma requisição iniciar uma
				// conexão externa
				if (!this.transaction.isExternalClosed()) {
					log.debug("Closing connection "
							+ this.transaction.getConnection());
					this.transaction.closeExternal();
				}
				this.transaction.setConnection(
						this.external.getConnection(trx.transactionManager()),
						true);
				log.debug("Get external connection "
						+ this.transaction.getConnection());

			} else {
				if (this.transaction.isClosed()) {
					this.transaction.setConnection(this.local.getConnection());
					log.debug("Get local connection "
							+ this.transaction.getConnection());
				}
			}
		}

		Object result = null;
		try {

			if (!trx.propagation().equals(Propagation.NESTED)
					&& !trx.readOnly()) {
				this.transaction.beginTransaction();
				log.debug("Started a new transaction and closing the connection "
						+ this.transaction.getConnection());
			}

			// Executa o método interceptado
			result = ctx.proceed();

			if (!trx.propagation().equals(Propagation.NESTED)
					&& !trx.readOnly()) {
				this.transaction.commit();
				log.debug("Commiting the transaction and closing the connection "
						+ this.transaction.getConnection());
				// Se for uma transação externa
			} else if (trx.transactionManager() != 0) {
				log.debug("Closing external connection "
						+ this.transaction.getConnection());
				this.transaction.closeExternal();
			}

		} catch (Exception e) {
			if (!trx.propagation().equals(Propagation.NESTED)
					&& !trx.readOnly()) {
				this.transaction.rollback();
				log.debug("Rollbacking and Closing the transaction on connection "
						+ this.transaction.getConnection());
			}
			throw e;
		}

		return result;

	}

}