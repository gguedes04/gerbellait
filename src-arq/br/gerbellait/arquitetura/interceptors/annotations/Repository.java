package br.gerbellait.arquitetura.interceptors.annotations;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.interceptor.InterceptorBinding;

@InterceptorBinding
@Inherited
@Documented
@Target(TYPE)
@Retention(RUNTIME)
/**
 * Used just in a method context
 * @author Daniel Jeremias
 * @version 1.0.0
 */
public @interface Repository {
}