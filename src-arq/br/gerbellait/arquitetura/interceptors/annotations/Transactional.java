package br.gerbellait.arquitetura.interceptors.annotations;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.enterprise.util.Nonbinding;
import javax.interceptor.InterceptorBinding;

import br.gerbellait.arquitetura.interceptors.enums.Propagation;

@InterceptorBinding
@Inherited
@Documented
@Target({ TYPE, METHOD })
@Retention(RUNTIME)
/**
 * Used just in a method context
 * @author Daniel Jeremias
 * @version 1.0.0
 */
public @interface Transactional {
	
	/**
	 * Define se a transação será gerenciada pela aplicação ou terá
	 * comportamente de apenas leitura, sem commit e rollback.
	 * @return true ser a transação for somente leitura e false se for
	 * gerenciada pela aplicação.
	 */
	@Nonbinding boolean readOnly() default true;
	
	/**
	 * Define o nome do banco de dados para estabelecer conexão e abrir
	 * transação.
	 * @return nome do banco de dados.
	 */
	@Nonbinding int transactionManager() default 0;
	
	/**
	 * Define o modo de propagação da transação, utilizado para aninhar
	 * transações e definir quando uma nova transação deve ser aberta. 
	 * @return nova transação ou transação aninhada.
	 */
	@Nonbinding Propagation propagation() default Propagation.REQUIRES_NEW;
	
}