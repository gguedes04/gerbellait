package br.gerbellait.arquitetura.interceptors;

import java.io.Serializable;
import java.sql.SQLException;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import br.gerbellait.arquitetura.exceptions.DataAccessFailureException;
import br.gerbellait.arquitetura.exceptions.DataIntegrityViolationException;
import br.gerbellait.arquitetura.interceptors.annotations.Repository;

@Interceptor
@Repository
public class RepositoryInterceptor implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private static Logger log = Logger.getLogger(RepositoryInterceptor.class);
	
	/**
	 * http://www.postgresql.org/docs/8.1/static/errcodes-appendix.html
	 */
	private final String DUP_VAL_ON_INDEX = "23505";
	
	@AroundInvoke
	public Object handleDaoException(InvocationContext ctx) throws Exception {
		Object result = null;
		try {
			// Executa o método interceptado
			result = ctx.proceed();
		}catch(Exception e){			
			this.handleException(e);
		}
		return result;
	}

	private void handleException(Exception e) throws Exception{
		if(e.getCause() instanceof SQLException){
			SQLException sqlex = (SQLException) e.getCause();
			//String excptionClass = sqlex.getSQLState().substring(0, 2);
			if(DUP_VAL_ON_INDEX.equals(sqlex.getSQLState())){
				throw new DataIntegrityViolationException(sqlex);
			}
			log.error(sqlex);
			throw new DataAccessFailureException(sqlex);
		}else{
			log.error(e);
			throw e;
		}
	}	
	
}