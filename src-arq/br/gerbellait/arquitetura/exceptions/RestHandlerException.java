package br.gerbellait.arquitetura.exceptions;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class RestHandlerException implements
		ExceptionMapper<RestLayerException> {

	@Override
	public Response toResponse(RestLayerException exception) {
		return Response.status(Status.BAD_REQUEST)
				.entity(getMessage(exception.getMessage()))
				.type(MediaType.APPLICATION_JSON).build();
	}

	private Object getMessage(String message) {
		return new ErrorMessage(message);
	}
}

class ErrorMessage {
	private String message;

	public ErrorMessage(String message) {
		this.message = message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
