package br.gerbellait.arquitetura.exceptions;

public class DataAccessFailureException extends DaoLayerException {

	private static final long serialVersionUID = 1L;

	public DataAccessFailureException() {
		super();
	}

	public DataAccessFailureException(String message, Throwable cause) {
		super(message, cause);
	}

	public DataAccessFailureException(String message) {
		super(message);
	}

	public DataAccessFailureException(Throwable cause) {
		super(cause);
	}

}