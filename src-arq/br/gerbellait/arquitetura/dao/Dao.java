package br.gerbellait.arquitetura.dao;

import java.io.Serializable;
import java.util.Collection;

import br.gerbellait.arquitetura.entities.AbstractEntity;

public interface Dao<Entity extends AbstractEntity> extends Serializable {

	Collection<Entity> getAll(Class<Entity> clazz);

	Entity getById(Entity entity);

	Entity update(Entity entity);

	Entity insert(Entity entity);

	void delete(Entity entity);

	void diff(Entity entityAntes, Entity entityDepois);
}
