package br.gerbellait.arquitetura.dao;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.apache.log4j.Logger;

import br.gerbellait.arquitetura.ApplicationContext;
import br.gerbellait.arquitetura.datasource.PreparedStatementWrapper;
import br.gerbellait.arquitetura.datasource.Transaction;
import br.gerbellait.arquitetura.entities.AbstractEntity;
import br.gerbellait.arquitetura.exceptions.DaoLayerException;

public abstract class AbstractDao<Entity extends AbstractEntity> {
	private static final Logger LOGGER = Logger.getLogger(AbstractDao.class);
	protected static Map<String, String> banco = new HashMap<String, String>();

	@Inject
	private Transaction transaction;

	@Inject
	private ApplicationContext context;

	protected String getAppProperty(String key) {
		return this.context.getAppProperty(key);
	}

	protected String getProperty(String key) {
		return this.context.getSqlProperty(key);
	}

	protected Transaction getTransaction() {
		return transaction;
	}

	private String getEsquema() {
		return "`" + getProperty("esquema").trim() + "`";
	}

	private String getNomeTabela(String nomeTabela) {
		return "`" + nomeTabela.trim() + "`";
	}

	private StringBuilder getSqlInsert(Entity t, String nomeTabela)
			throws IllegalArgumentException, IllegalAccessException {

		String campos = "";
		String values = "";

		String esquema = getEsquema();
		String tableName = this.getNomeTabela(banco.get(nomeTabela));
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO");
		sql.append(" ").append(esquema).append(".").append(tableName);

		for (Field field : t.getClass().getDeclaredFields()) {

			field.setAccessible(true);

			String nomeCampo = nomeTabela.concat(".").concat(field.getName());

			if (nomeCampo.endsWith("serialVersionUID") || (!field.getType().equals(Date.class) && field.get(t) == null)
					|| banco.get(nomeCampo) == null)
				continue;

			if ("".equals(campos)) {
				campos = campos.concat(banco.get(nomeCampo));
				values = values.concat("?");
			} else {
				campos = campos.concat("," + banco.get(nomeCampo));
				values = values.concat(",?");
			}
		}

		sql.append("(");
		sql.append(campos);
		sql.append(")");
		sql.append(" VALUES ");
		sql.append("(");
		sql.append(values);
		sql.append(")");

		return sql;
	}

	public Entity insert(Entity t) {

		PreparedStatementWrapper pstmt = null;
		ResultSet genKey = null;
		String nomeTabela = t.getClass().getSimpleName().toLowerCase();

		try {
			pstmt = getTransaction().prepareInsertStatement(getSqlInsert(t, nomeTabela));

			int count = 1;

			for (Field field : t.getClass().getDeclaredFields())
				if (setPreparedStatement(field, t, nomeTabela, count, pstmt))
					++count;

			LOGGER.debug(pstmt);
			pstmt.executeUpdate();
			genKey = pstmt.getGeneratedKeys();

			if (genKey.next()) {
				Field idField = t.getClass().getSuperclass().getDeclaredField("id");
				idField.setAccessible(true);
				idField.set(t, genKey.getLong(1));
			}

		} catch (Exception e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (genKey != null) {
					genKey.close();
				}
			} catch (SQLException e) {
				LOGGER.error(e);
			}
		}
		return t;
	}

	private boolean setPreparedStatement(Field field, Entity t, String nomeTabela, int count,
			PreparedStatementWrapper pstmt) throws Exception {
		field.setAccessible(true);

		String nomeCampo = nomeTabela.concat(".").concat(field.getName());

		if (field.getName().endsWith("serialVersionUID")
				|| (!field.getType().equals(Date.class) && field.get(t) == null) || banco.get(nomeCampo) == null)
			return false;

		final Class<?> tipoDoCampo = field.getType();

		if (tipoDoCampo.equals(String.class)) {
			pstmt.setString(count, ((String) field.get(t)).trim());
		} else if (tipoDoCampo.equals(BigDecimal.class)) {
			pstmt.setBigDecimal(count, (BigDecimal) field.get(t));
		} else if (tipoDoCampo.equals(Double.class)) {
			pstmt.setDouble(count, (Double) field.get(t));
		} else if (tipoDoCampo.equals(Boolean.class)) {
			pstmt.setBoolean(count, (Boolean) field.get(t));
		} else if (tipoDoCampo.equals(Long.class)) {
			pstmt.setLong(count, (Long) field.get(t));
		} else if (tipoDoCampo.equals(Integer.class)) {
			pstmt.setInt(count, (Integer) field.get(t));
		} else if (tipoDoCampo.equals(byte[].class)) {
			pstmt.setBytes(count, (byte[]) field.get(t));
		} else if (tipoDoCampo.equals(Date.class)) {
			if (field.get(t) == null)
				pstmt.setTimestamp(count, null);
			else
				pstmt.setTimestamp(count, new java.sql.Timestamp(((Date) field.get(t)).getTime()));
		} else if (tipoDoCampo.getSuperclass() != null && tipoDoCampo.getSuperclass().equals(AbstractEntity.class)) {
			AbstractEntity entity = (AbstractEntity) field.get(t);
			if (entity != null) {
				Field idEntity = entity.getClass().getSuperclass().getDeclaredField("id");
				idEntity.setAccessible(true);

				if (idEntity.get(entity) == null)
					pstmt.setNull(count, Types.NULL);
				else
					pstmt.setLong(count, ((Long) idEntity.get(entity)).longValue());

			} else {
				pstmt.setObject(count, null);
			}
		} else if (tipoDoCampo.getSuperclass() != null && tipoDoCampo.getSuperclass().equals(Enum.class)) {
			pstmt.setString(count, ((Enum<?>) field.get(t)).name());
		}

		return true;
	}

	private StringBuilder getSqlUpdate(Entity t, String nomeTabela)
			throws IllegalArgumentException, IllegalAccessException {
		String campos = "";

		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE");
		sql.append(" ").append(getEsquema()).append(".").append(this.getNomeTabela(banco.get(nomeTabela)));
		sql.append(" ").append("SET").append(" ");

		for (Field field : t.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			String nomeCampo = nomeTabela.concat(".").concat(field.getName());

			if (nomeCampo.endsWith("serialVersionUID") || (!field.getType().equals(Date.class) && field.get(t) == null)
					|| banco.get(nomeCampo) == null) {
				continue;
			}

			if (!"".equals(campos)) {
				campos = campos.concat(",");
			}
			campos = campos.concat("`" + banco.get(nomeCampo).concat("` = ? "));
		}

		sql.append(campos);
		sql.append(" WHERE `" + banco.get(nomeTabela.concat(".").concat("id")) + "` = ?");

		return sql;
	}

	public Entity update(Entity t) {

		PreparedStatementWrapper pstmt = null;
		String nomeTabela = t.getClass().getSimpleName().toLowerCase();

		try {

			pstmt = getTransaction().prepareInsertStatement(getSqlUpdate(t, nomeTabela));

			int count = 1;

			for (Field field : t.getClass().getDeclaredFields())
				if (setPreparedStatement(field, t, nomeTabela, count, pstmt))
					++count;

			Field id = t.getClass().getSuperclass().getDeclaredField("id");
			id.setAccessible(true);
			pstmt.setLong(count, ((Long) id.get(t)).longValue());
			LOGGER.debug(pstmt);
			pstmt.executeUpdate();

		} catch (Exception e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (SQLException e) {
				LOGGER.error(e);
			}
		}
		return t;
	}

	public void delete(Entity t) {

		PreparedStatementWrapper pstmt = null;

		try {
			String nomeTabela = t.getClass().getSimpleName().toLowerCase();
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM");
			sql.append(" ").append(getEsquema()).append(".").append(this.getNomeTabela(banco.get(nomeTabela)));
			sql.append(" ").append("WHERE");
			sql.append(" ").append(banco.get(nomeTabela.concat(".").concat("id"))).append(" = ?");
			pstmt = getTransaction().prepareInsertStatement(sql);
			Field id = t.getClass().getSuperclass().getDeclaredField("id");
			id.setAccessible(true);
			pstmt.setLong(1, ((Long) id.get(t)).longValue());
			LOGGER.debug(pstmt);
			pstmt.executeUpdate();
		} catch (Exception e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (SQLException e) {
				LOGGER.error(e);
			}
		}

	}

	@SuppressWarnings("unchecked")
	public Entity getById(Entity t1) {

		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		Entity t = null;
		try {
			t = (Entity) t1.getClass().newInstance();
			String nomeTabela = t.getClass().getSimpleName().toLowerCase();

			StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM");
			sb.append(" ").append(getEsquema().trim()).append(".");
			sb.append(this.getNomeTabela(banco.get(nomeTabela)));
			sb.append(" ").append("WHERE");
			sb.append(" ").append(banco.get(nomeTabela.concat(".").concat("id"))).append(" = ?");

			pstmt = getTransaction().prepareStatement(sb);
			Field id = t1.getClass().getSuperclass().getDeclaredField("id");
			id.setAccessible(true);
			pstmt.setLong(1, (Long) id.get(t1));
			LOGGER.debug(pstmt);
			rs = pstmt.executeQuery();
			t.setId(t1.getId());
			if (rs.next()) {
				populateObject(t, nomeTabela, rs);
			} else
				return null;
		} catch (Exception e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				LOGGER.error(e);
			}
		}

		return t;
	}

	/**
	 * Nome da Class ponto class.
	 * 
	 * @param classT
	 * @param orderByFieldName
	 * @return
	 */
	public Collection<Entity> getAll(Class<Entity> classT) {

		PreparedStatementWrapper pstmt = null;
		ResultSet rs = null;
		Collection<Entity> ts = new ArrayList<>();

		try {

			String nomeTabela = classT.getSimpleName().toLowerCase();
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM");
			sql.append(" ").append(getEsquema()).append(".").append(this.getNomeTabela(banco.get(nomeTabela)));
			sql.append(" ").append("ORDER BY");
			sql.append(" ").append(banco.get(nomeTabela.concat(".").concat("id")));

			pstmt = getTransaction().prepareStatement(sql);
			LOGGER.debug(pstmt);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				Entity t = classT.newInstance();
				populateObject(t, nomeTabela, rs);
				Field idField = classT.getSuperclass().getDeclaredField("id");
				idField.setAccessible(true);
				idField.set(t, rs.getLong(banco.get(nomeTabela.concat(".").concat("id"))));
				ts.add(t);
			}

		} catch (Exception e) {
			throw new DaoLayerException(e);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				LOGGER.error(e);
			}
		}
		return ts;
	}

	private void populateObject(Entity t, String nomeTabela, ResultSet rs) throws Exception {
		for (Field field : t.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			String nomeCampo = nomeTabela.concat(".").concat(field.getName());
			setObject(field, t, nomeCampo, rs);
		}
	}

	private void setObject(Field field, Entity t, String nomeCampo, ResultSet rs) throws Exception {

		if (nomeCampo.endsWith("serialVersionUID") || banco.get(nomeCampo) == null) {
			return;
		}

		Class<?> tipoDoCampo = field.getType();

		if (tipoDoCampo.equals(String.class)) {
			field.set(t, rs.getString(banco.get(nomeCampo)));
		} else if (tipoDoCampo.equals(BigDecimal.class)) {
			field.set(t, rs.getBigDecimal(banco.get(nomeCampo)));
		} else if (tipoDoCampo.equals(Double.class)) {
			field.set(t, rs.getDouble(banco.get(nomeCampo)));
		} else if (tipoDoCampo.equals(Boolean.class)) {
			field.set(t, rs.getBoolean(banco.get(nomeCampo)));
		} else if (tipoDoCampo.equals(Long.class)) {
			field.set(t, rs.getLong(banco.get(nomeCampo)));
		} else if (tipoDoCampo.equals(Integer.class)) {
			field.set(t, rs.getInt(banco.get(nomeCampo)));
		} else if (tipoDoCampo.equals(byte[].class)) {
			field.set(t, rs.getBytes(banco.get(nomeCampo)));
		} else if (tipoDoCampo.equals(Date.class)) {
			field.set(t, rs.getTimestamp(banco.get(nomeCampo)));
		} else if (tipoDoCampo.getSuperclass() != null && tipoDoCampo.getSuperclass().equals(AbstractEntity.class)) {
			Long idEntity = rs.getLong(banco.get(nomeCampo));
			if (idEntity.longValue() == 0)
				return;
			field.set(t, tipoDoCampo.newInstance());
			AbstractEntity entity = (AbstractEntity) field.get(t);
			entity.setId(idEntity);

		} else if (tipoDoCampo.getSuperclass() != null && tipoDoCampo.getSuperclass().equals(Enum.class)) {
			String value = rs.getString(banco.get(nomeCampo));
			Enum<?>[] enumerations = (Enum[]) tipoDoCampo.getEnumConstants();
			for (Enum<?> enumeration : enumerations)
				if (enumeration.name().equals(value)) {
					field.set(t, enumeration);
					break;
				}
		}
	}

	public void diff(Entity tAntes, Entity tDepois) {
		// not used
	}

}