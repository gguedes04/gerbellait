package br.gerbellait.arquitetura.rest;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.gerbellait.arquitetura.exceptions.RestLayerException;
import br.gerbellait.arquitetura.exceptions.ServiceLayerException;
import br.gerbellait.arquitetura.rest.vo.ObjectRest;

public abstract class AbstractRest {

	abstract protected Object getCurrentUser(HttpServletRequest request);

	@GET
	@Path("{action}/{json}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("action") String action, @PathParam("json") String json,
			@Context HttpServletRequest request) throws RestLayerException {
		boolean e401 = false;
		try {
			Object userToken = getCurrentUser(request);
			if (userToken == null) {
				e401 = true;
				throw new ServiceLayerException();
			}
			ObjectRest objectRest = new ObjectRest();
			objectRest.setAction(action);
			objectRest.setJson(json);
			return Response.ok(invokeAction(objectRest)).build();
		} catch (Exception e) {
			if (e401) {
				return Response.status(Status.UNAUTHORIZED).build();
			}

			if (e.getCause() instanceof ServiceLayerException) {
				throw new RestLayerException(e.getCause().getMessage());
			}
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response save(ObjectRest objectRest, @Context HttpServletRequest request) throws RestLayerException {
		boolean e401 = false;
		try {
			Object userToken = getCurrentUser(request);
			if (userToken == null) {
				e401 = true;
				throw new ServiceLayerException();
			}
			return Response.ok(objectRest).build();
		} catch (Exception e) {
			if (e401) {
				return Response.status(Status.UNAUTHORIZED).build();
			}
			if (e.getCause() instanceof ServiceLayerException) {
				throw new RestLayerException(e.getCause().getMessage());
			}
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(ObjectRest objectRest, @Context HttpServletRequest request) throws RestLayerException {
		boolean e401 = false;
		try {
			Object userToken = getCurrentUser(request);
			if (userToken == null) {
				e401 = true;
				throw new ServiceLayerException();
			}
			return Response.ok(objectRest).build();
		} catch (Exception e) {
			if (e401) {
				return Response.status(Status.UNAUTHORIZED).build();
			}
			if (e.getCause() instanceof ServiceLayerException) {
				throw new RestLayerException(e.getCause().getMessage());

			}
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@DELETE
	@Path("{action}/{json}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("action") String action, @PathParam("json") String json,
			@Context HttpServletRequest request) throws RestLayerException {
		boolean e401 = false;
		try {
			Object userToken = getCurrentUser(request);
			if (userToken == null) {
				e401 = true;
				throw new ServiceLayerException();
			}
			ObjectRest objectRest = new ObjectRest();
			objectRest.setAction(action);
			objectRest.setJson(json);
			return Response.ok(objectRest).build();
		} catch (Exception e) {
			if (e401) {
				return Response.status(Status.UNAUTHORIZED).build();
			}
			if (e.getCause() instanceof ServiceLayerException) {
				throw new RestLayerException(e.getCause().getMessage());
			}
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	private Object invokeAction(ObjectRest objectRest) throws NoSuchFieldException, SecurityException,
			IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {

		String[] str = objectRest.getAction().split("\\.");
		String method = str[1];
		String referenceEjb = str[0];

		Object retorno = null;
		Field field = getClass().getDeclaredField(referenceEjb);
		field.setAccessible(true);
		Object service = field.get(this);

		if (!"{}".equals(objectRest.getJson())) {
			try {
				retorno = service.getClass().getDeclaredMethod(method, String.class).invoke(service,
						objectRest.getJson());
			} catch (NoSuchMethodException e) {
				retorno = service.getClass().getDeclaredMethod(method, String.class).invoke(service,
						objectRest.getJson());
			}

		} else {
			try {
				retorno = service.getClass().getDeclaredMethod(method).invoke(service);
			} catch (NoSuchMethodException e) {
				retorno = service.getClass().getDeclaredMethod(method).invoke(service);
			}
		}
		return retorno;
	}
}