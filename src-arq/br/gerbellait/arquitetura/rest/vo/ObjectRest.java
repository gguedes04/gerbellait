package br.gerbellait.arquitetura.rest.vo;

public class ObjectRest {

	private String action;
	private String json;

	public void setAction(String action) {
		this.action = action;
	}

	public String getAction() {
		return action;
	}

	public void setJson(String json) {
		this.json = json;
	}

	public String getJson() {
		return json;
	}
}
