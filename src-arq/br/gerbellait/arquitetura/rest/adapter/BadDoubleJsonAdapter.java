package br.gerbellait.arquitetura.rest.adapter;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

public class BadDoubleJsonAdapter implements JsonDeserializer<Double> {

	@Override
	public Double deserialize(JsonElement element, Type type,
			JsonDeserializationContext context) throws JsonParseException {

		try {
			return element.getAsDouble();
		} catch (NumberFormatException e) {
			String el = element.getAsString();
			el = el.replaceAll("\\.", "").replaceFirst(",", ".");
			if ("".equals(el))
				return null;
			return Double.valueOf(el);
		}
	}

}
